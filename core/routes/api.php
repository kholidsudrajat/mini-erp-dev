<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/test-print', function() {
	try {
		$connector = new \Mike42\Escpos\PrintConnectors\NetworkPrintConnector("10.0.0.111", 9100);
		$printer = new \Mike42\Escpos\Printer($connector);
		$printer->text("Hello World!\n");
		$printer->cut();

		/* Close printer */
		$printer->close();
	} catch (Exception $e) {
		echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
	}
});

Route::group(['prefix' => 'v1', 'middleware' => ['requiredParameterJson']], function () {
	Route::group(['prefix' => 'auth'], function () {
		Route::post('/login', 'Api\AuthController@login');
		
		Route::group(['middleware' => ['jwt.auth']], function() {
			Route::post('/logout', 'Api\AuthController@logout');
		});
	});
	
	Route::group(['middleware' => ['jwt.auth']], function() {
        Route::get('dashboard', 'Api\\DashboardController@index');
		
		// warehouse
		Route::group(['prefix' => 'warehouse'], function () {
			Route::get('index', 'Api\\WareHouseController@index');
			Route::get('exportpdf', 'Api\\WareHouseController@exportpdf');
			Route::get('show/{id}', 'Api\\WareHouseController@show');
			Route::post('store', 'Api\\WareHouseController@store');
			Route::patch('update/{id}', 'Api\\WareHouseController@update');
			Route::delete('delete/{id}', 'Api\\WareHouseController@destroy');
		});
		
		// shop
		Route::group(['prefix' => 'shop'], function () {
			Route::get('pluck', 'Api\\ShopController@pluck');
			Route::get('index', 'Api\\ShopController@index');
			Route::get('exportpdf', 'Api\\ShopController@exportpdf');
			Route::get('show/{id}', 'Api\\ShopController@show');
			Route::post('store', 'Api\\ShopController@store');
			Route::patch('update/{id}', 'Api\\ShopController@update');
			Route::delete('delete/{id}', 'Api\\ShopController@destroy');
		});
		
		// payment method
		Route::group(['prefix' => 'payment-method'], function () {
			Route::get('index', 'Api\\PaymentMethodController@index');
			Route::get('exportpdf', 'Api\\PaymentMethodController@exportpdf');
			Route::get('show/{id}', 'Api\\PaymentMethodController@show');
			Route::post('store', 'Api\\PaymentMethodController@store');
			Route::patch('update/{id}', 'Api\\PaymentMethodController@update');
			Route::delete('delete/{id}', 'Api\\PaymentMethodController@destroy');
		});
		
		// Shipping
		Route::group(['prefix' => 'shipping'], function () {
			Route::get('index', 'Api\\ShippingController@index');
			Route::get('exportpdf', 'Api\\ShippingController@exportpdf');
			Route::get('show/{id}', 'Api\\ShippingController@show');
			Route::post('store', 'Api\\ShippingController@store');
			Route::patch('update/{id}', 'Api\\ShippingController@update');
			Route::delete('delete/{id}', 'Api\\ShippingController@destroy');
			Route::get('pluck', 'Api\\ShippingController@pluck');
		});
		
		// product models
		Route::group(['prefix' => 'model'], function () {
			Route::get('index', 'Api\\ModelController@index');
			Route::get('exportpdf', 'Api\\ModelController@exportpdf');
			Route::get('show/{id}', 'Api\\ModelController@show');
			Route::delete('delete/{id}', 'Api\\ModelController@destroy');
			Route::post('getProduk', 'Api\\ModelController@getProduk');
			Route::get('pluck', 'Api\\ModelController@pluck');
		});
		
		// Stock
		Route::group(['prefix' => 'stock'], function () {
			Route::get('list', 'Api\\StockController@lists');
			Route::get('index', 'Api\\StockController@index');
			Route::get('exportpdf', 'Api\\StockController@exportpdf');
		});
		
		// Stock Adjustment
		Route::group(['prefix' => 'stock-adjustment'], function () {
			Route::get('getstock', 'Api\\StockAdjustmentController@getstock');
			Route::get('index', 'Api\\StockAdjustmentController@index');
			Route::get('exportpdf', 'Api\\StockAdjustmentController@exportpdf');
			Route::post('store', 'Api\\StockAdjustmentController@store');
			Route::get('show/{id}', 'Api\\StockAdjustmentController@show');
			Route::patch('update/{id}', 'Api\\StockAdjustmentController@update');
		});
		
		// Customer
		Route::group(['prefix' => 'customer'], function () {
			Route::get('index', 'Api\\CustomerController@index');
			Route::get('exportpdf', 'Api\\CustomerController@exportpdf');
			Route::get('show/{id}', 'Api\\CustomerController@show');
			Route::post('store', 'Api\\CustomerController@store');
			Route::post('address/add', 'Api\\CustomerController@tambah');
			Route::post('address/delete', 'Api\\CustomerController@hapus');
			Route::patch('update/{id}', 'Api\\CustomerController@update');
			Route::delete('delete/{id}', 'Api\\CustomerController@destroy');
			Route::get('pluck', 'Api\\CustomerController@pluck');
		});
		
		// Notification
		Route::group(['prefix' => 'notifications'], function () {
			Route::get('index', 'Api\\NotificationController@index');
			Route::get('show/{id}', 'Api\\NotificationController@show');
		});
		
		// Scheduler
		Route::group(['prefix' => 'scheduler'], function () {
			Route::get('index', 'Api\\SchedulerController@index');
			Route::post('store', 'Api\\SchedulerController@store');
			Route::delete('delete/{id}', 'Api\\SchedulerController@destroy');
		});
		
		// Petty Cash
		Route::group(['prefix' => 'petty-cash'], function () {
			Route::get('index', 'Api\\PettyCashController@index');
			Route::get('exportpdf', 'Api\\PettyCashController@exportpdf');
			Route::get('show/{id}', 'Api\\PettyCashController@show');
			Route::delete('delete/{id}', 'Api\\PettyCashController@destroy');
		});
		
		// User
		Route::group(['prefix' => 'user'], function () {
			Route::get('index', 'Api\\UserController@index');
			Route::get('exportpdf', 'Api\\UserController@exportpdf');
			Route::get('show/{id}', 'Api\\UserController@show');
			Route::post('store', 'Api\\UserController@store');
			Route::patch('update/{id}', 'Api\\UserController@update');
			Route::delete('delete/{id}', 'Api\\UserController@destroy');
		});
		
		// Role User
		Route::group(['prefix' => 'role-user'], function () {
			Route::get('pluck', 'Api\\RoleUserController@pluck');
			Route::get('index', 'Api\\RoleUserController@index');
			Route::get('show/{id}', 'Api\\RoleUserController@show');
			Route::post('store', 'Api\\RoleUserController@store');
			Route::patch('update/{id}', 'Api\\RoleUserController@update');
			Route::delete('delete/{id}', 'Api\\RoleUserController@destroy');
		});
		
		// Reports
		Route::group(['prefix' => 'report'], function () {
			
			// sales customer
			Route::group(['prefix' => 'sales-customer'], function () {
				Route::get('index', 'Api\\ReportCustomerController@index');
				Route::get('exportpdf', 'Api\\ReportCustomerController@exportpdf');
				Route::get('index-data', 'Api\\ReportCustomerController@indexData');
				Route::get('show/{id}', 'Api\\ReportCustomerController@show');
				Route::get('show/{id}/filter', 'Api\\ReportCustomerController@filter');
				Route::get('show/cashier/{id}/{cashierId}', 'Api\\ReportCustomerController@cashierDetail');
				Route::post('paydebt', 'Api\\ReportCustomerController@paydebt');
			});
			
			// sales product
			Route::group(['prefix' => 'sales-product'], function () {
				Route::get('index', 'Api\\ReportProductController@index');
				Route::get('exportpdf', 'Api\\ReportProductController@exportpdf');
				Route::get('index-data', 'Api\\ReportProductController@indexData');
				Route::get('show/{id}', 'Api\\ReportProductController@show');
				Route::get('show/{id}/filter', 'Api\\ReportProductController@filter');
				Route::get('show/cashier/{id}/{idm}', 'Api\\ReportProductController@cashierDetail');
			});
			
			// sales invoice
			Route::group(['prefix' => 'sales-invoice'], function () {
				Route::get('index', 'Api\\ReportInvoiceController@index');
				Route::get('exportpdf', 'Api\\ReportInvoiceController@exportpdf');
				Route::get('index-data', 'Api\\ReportInvoiceController@indexData');
				Route::get('show/{id}', 'Api\\ReportInvoiceController@show');
				Route::get('show/{id}/filter', 'Api\\ReportInvoiceController@filter');
			});
			
			// Stock Movement
			Route::group(['prefix' => 'stock-movement'], function () {
				Route::get('index', 'Api\\ReportStockMovementController@index');
				Route::get('exportpdf', 'Api\\ReportStockMovementController@exportpdf');
				Route::get('index-data', 'Api\\ReportStockMovementController@indexData');
				Route::get('show/{id}', 'Api\\ReportStockMovementController@show');
				Route::get('show/{id}/filter', 'Api\\ReportStockMovementController@filter');
			});
			
			// Stock
			Route::group(['prefix' => 'stock'], function () {
				Route::get('index', 'Api\\ReportStockController@index');
				Route::get('exportpdf', 'Api\\ReportStockController@exportpdf');
				Route::get('index-data', 'Api\\ReportStockController@indexData');
				Route::get('show/{id}', 'Api\\ReportStockController@show');
				Route::get('show/{id}/filter', 'Api\\ReportStockController@filter');
			});
			
			// Profit Loss
			Route::group(['prefix' => 'profit-loss'], function () {
				Route::get('index', 'Api\\ReportProfitLossController@index');
				Route::get('filter', 'Api\\ReportProfitLossController@filter');
			});
			
			// Order Supplier
			Route::group(['prefix' => 'order-supplier'], function () {
				Route::get('index', 'Api\\ReportOrderSupplierController@index');
				Route::get('exportpdf', 'Api\\ReportOrderSupplierController@exportpdf');
				Route::get('show/{id}', 'Api\\ReportOrderSupplierController@show');
				Route::get('show/cashier/{id}/{idm}', 'Api\\ReportOrderSupplierController@shows');
			});
			
			// Order PO
			Route::group(['prefix' => 'order-po'], function () {
				Route::get('index', 'Api\\ReportOrderPoController@index');
				Route::get('exportpdf', 'Api\\ReportOrderPoController@exportpdf');
				Route::get('show/{id}', 'Api\\ReportOrderPoController@show');
			});
			
			// Order Payment
			Route::group(['prefix' => 'order-payment'], function () {
				Route::get('index', 'Api\\ReportOrderPaymentController@index');
				Route::get('exportpdf', 'Api\\ReportOrderPaymentController@exportpdf');
				Route::get('show/{id}', 'Api\\ReportOrderPaymentController@show');
			});
			
			// Order Delivery
			Route::group(['prefix' => 'order-delivery'], function () {
				Route::get('index', 'Api\\ReportOrderDeliveryController@index');
				Route::get('exportpdf', 'Api\\ReportOrderDeliveryController@exportpdf');
				Route::get('show/{id}', 'Api\\ReportOrderDeliveryController@show');
			});
			
			// Order Retur
			Route::group(['prefix' => 'order-retur'], function () {
				Route::get('index', 'Api\\ReportOrderReturController@index');
				Route::get('exportpdf', 'Api\\ReportOrderReturController@exportpdf');
				Route::get('show/{id}', 'Api\\ReportOrderReturController@show');
			});
			
			// retur product
			Route::group(['prefix' => 'retur-product'], function () {
				Route::get('index', 'Api\\ReturProductController@index');
				Route::get('exportpdf', 'Api\\ReturProductController@exportpdf');
				Route::get('show/{id}', 'Api\\ReturProductController@show');
				Route::get('show/{id}/filter', 'Api\\ReturProductController@filter');
				Route::get('show/detail/{id}/{noret}', 'Api\\ReturProductController@shows');
			});
			
			// retur No Retur
			Route::group(['prefix' => 'retur-noret'], function () {
				Route::get('index', 'Api\\ReturNoretController@index');
				Route::get('exportpdf', 'Api\\ReturNoretController@exportpdf');
				Route::get('show/{id}', 'Api\\ReturNoretController@show');
			});
			
			// retur Customer
			Route::group(['prefix' => 'retur-customer'], function () {
				Route::get('index', 'Api\\ReturCustomerController@index');
				Route::get('exportpdf', 'Api\\ReturCustomerController@exportpdf');
				Route::get('show/{id}', 'Api\\ReturCustomerController@show');
				Route::get('show/{id}/filter', 'Api\\ReturCustomerController@filter');
				Route::get('show/detail/{id}/{noret}', 'Api\\ReturCustomerController@shows');
			});
		});
		
		// retur invoice
		Route::group(['prefix' => 'retur-invoice'], function () {
			Route::get('index', 'Api\\ReturInvoiceController@index');
			Route::get('show/{id}', 'Api\\ReturInvoiceController@show');
			Route::post('save', 'Api\\ReturGeneralController@save');
		});
		
		// retur invoice
		Route::group(['prefix' => 'retur-general'], function () {
			Route::post('save', 'Api\\ReturGeneralController@save');
		});
		
		// supplier order
		Route::group(['prefix' => 'supplier-order'], function () {
			Route::get('index', 'Api\\SupplierOrderController@index');
			Route::get('exportpdf', 'Api\\SupplierOrderController@exportpdf');
			Route::get('show/{id}', 'Api\\SupplierOrderController@show');
			Route::post('store', 'Api\\SupplierOrderController@store');
			Route::patch('update/{id}', 'Api\\SupplierOrderController@update');
			Route::delete('delete/{id}', 'Api\\SupplierOrderController@destroy');
			Route::get('pluck', 'Api\\SupplierOrderController@pluck');
		});
		
		
		// order
		Route::group(['prefix' => 'order'], function () {
			Route::get('index', 'Api\\OrderController@index');
			Route::get('exportpdf', 'Api\\OrderController@exportpdf');
			Route::get('show/{id}', 'Api\\OrderController@show');
		});
		
		
		// cashier
		Route::group(['prefix' => 'cashier'], function () {
			Route::get('index', 'Api\\CashierController@index');
			Route::get('addtocart', 'Api\\CashierController@addqty');
			Route::get('getnotes/{id}', 'Api\\CashierController@getnotes');
			Route::get('removecart/{id}', 'Api\\CashierController@hapusqty');
			Route::get('shop/{id}/choose', 'Api\\CashierController@shop');
            Route::get('shipping/{noref}', 'Api\\CashierController@shipping');
            Route::get('print/{noref}', 'Api\\CashierController@prints');
            Route::get('prints', 'Api\\CashierController@prints_');
		});
		// cashier
		Route::group(['prefix' => 'cashier-2'], function () {
			Route::get('index', 'Api\\CashierController@index2');
			Route::get('addtocart', 'Api\\CashierController@addqty2');
			Route::get('getnotes/{id}', 'Api\\CashierController@getnotes');
			Route::get('removecart/{id}', 'Api\\CashierController@hapusqty');
			Route::get('shop/{id}/choose', 'Api\\CashierController@shop');
            Route::get('shipping/{noref}', 'Api\\CashierController@shipping');
            Route::get('print/{noref}', 'Api\\CashierController@prints');
            Route::get('prints', 'Api\\CashierController@prints_');
		});
        
		// transfer stok
		Route::group(['prefix' => 'transfer-stock'], function () {
			Route::get('index', 'Api\\TransferStockController@index');
			Route::get('transfer/{id}', 'Api\\TransferStockController@transfer');
			Route::get('multiple', 'Api\\TransferStockController@multiple');
		});
        
		// list penjualan
		Route::group(['prefix' => 'list_penjualan'], function () {
			Route::get('index', 'Api\\PenjualanController@index');
			Route::get('show/{id}', 'Api\\PenjualanController@show');
		});
		
	});
});

Route::group(['prefix' => 'v1', 'middleware' => ['jwt.auth']], function () {
	// only for file upload
	
	// product models
	Route::group(['prefix' => 'model'], function () {
		Route::post('store', 'Api\\ModelController@store');
		Route::post('update/{id}', 'Api\\ModelController@update');
	});
	
	Route::group(['prefix' => 'petty-cash'], function () {
		Route::post('store', 'Api\\PettyCashController@store');
	});
	
	// Input Biaya
	Route::get('/inputcost/exportpdf', 'Admin\\InputCostController@exportpdf');
	Route::get('/input-cost/data', ['as' => 'input-cost.data', 'uses' => 'Admin\\InputCostController@anyData']);
	Route::resource('/input-cost', 'Admin\\InputCostController', [
		'names' => [
			'index' => 'input-cost',
			'create' => 'input-cost.new',
			'edit' => 'input-cost.edit',
			'show' => 'input-cost.show'
		]
	]);
	Route::post('/inputcost/add','Admin\\InputCostController@add');
	Route::post('/inputcost/update','Admin\\InputCostController@updates');
	Route::post('/inputcost/delete','Admin\\InputCostController@destroys');
	Route::post('/inputcost/getbydate','Admin\\InputCostController@getbydate');

    Route::post('/shop/changedefault', 'Api\\ShopController@changedefault');
	// form pembelian
	Route::group(['prefix' => 'pembelian'], function () {
		Route::get('form', 'Api\\PembelianController@form');
		Route::get('getmodel', 'Api\\PembelianController@getmodel');
		Route::post('submit', 'Api\\PembelianController@submit');
		
		// Pembayaran
		Route::group(['prefix' => 'bayar'], function () {
			Route::get('index', 'Api\\OrderPaymentController@index');
			Route::get('exportpdf', 'Api\\OrderPaymentController@exportpdf');
			Route::get('show/{id}', 'Api\\OrderPaymentController@show');
			Route::get('{id}/add', 'Api\\OrderPaymentController@add');
			Route::post('{id}/submit', 'Api\\OrderPaymentController@submit');
		});
		
		// Pengiriman
		Route::group(['prefix' => 'pengiriman'], function () {
			Route::get('index', 'Api\\OrderDeliveryController@index');
			Route::get('exportpdf', 'Api\\OrderDeliveryController@exportpdf');
			Route::get('indexdata', 'Api\\OrderDeliveryController@indexdata');
			Route::get('show/{id}', 'Api\\OrderDeliveryController@show');
			Route::get('{id}/add', 'Api\\OrderDeliveryController@add');
			Route::get('detail/{id}/{oid}', 'Api\\OrderDeliveryController@detail');
			Route::post('{id}/submit', 'Api\\OrderDeliveryController@submit');
	        Route::post('{id}/takeall', 'Api\\OrderDeliveryController@takeall');
		});
		
	});
		
    // order
    Route::group(['prefix' => 'order'], function () {
    	Route::post('update/{id}', 'Api\\OrderController@update');
        Route::post('delete/{id}', 'Api\\OrderController@destroy');

        Route::post('adddetail/{id}', 'Api\\OrderController@adddetail');
        Route::post('updatedetail/{id}', 'Api\\OrderController@updatedetail');
        Route::post('deletedetail/{id}', 'Api\\OrderController@deletedetail');
    });
    // cashier
    Route::group(['prefix' => 'cashier'], function () {
        Route::post('ajaxmodel', 'Api\\CashierController@ajaxmodel');
        Route::post('bayar', 'Api\\CashierController@bayar');
        Route::post('changeaddress', 'Api\\CashierController@changeaddress');
        Route::post('saveshipping', 'Api\\CashierController@saveshipping');
    });
    // cashier
    Route::group(['prefix' => 'cashier-2'], function () {
        Route::post('ajaxmodel', 'Api\\CashierController@ajaxmodels');
        Route::post('bayar', 'Api\\CashierController@bayars');
        Route::post('changeaddress', 'Api\\CashierController@changeaddress2');
        Route::post('saveshipping', 'Api\\CashierController@saveshipping2');
    });
    
    // transfer
    Route::group(['prefix' => 'transfer-stock'], function () {
        Route::post('prosestransfer', 'Api\\TransferStockController@prosestransfer');
        Route::post('multiple/prosestransfer', 'Api\\TransferStockController@transfermultiple');
    });
    
    // penjualan
    Route::group(['prefix' => 'list_penjualan'], function () {
        Route::post('updatenoresi', 'Api\\PenjualanController@updatenoresi');
        Route::post('paydebt', 'Api\\PenjualanController@paydebt');
    });
    
    // penjualan
    Route::group(['prefix' => 'stock'], function () {
        Route::post('store', 'Api\\StockController@store');
    });

    Route::post('profile/update/{id}', 'Api\\UserController@update');
			
});