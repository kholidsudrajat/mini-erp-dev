<?php

Breadcrumbs::register('dashboard', function($breadcrumbs)
{
    $breadcrumbs->push('Dashboard', route('admin.home'));
});

Breadcrumbs::register('notice-daily', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Daily Notice', route('notice-daily'));
});

Breadcrumbs::register('notice-monthly', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Monthly Notice', route('notice-monthly'));
});

Breadcrumbs::register('user', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('User', route('user'));
});
Breadcrumbs::register('user.new', function($breadcrumbs)
{
    $breadcrumbs->parent('user');
    $breadcrumbs->push('New', route('user.new'));
});
Breadcrumbs::register('user.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('user');
    $breadcrumbs->push('Edit', route('user.edit', $data->id));
});

Breadcrumbs::register('report-stock-adjustment', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Laporan Penyesuaian Stok', route('report-stock-adjustment'));
});

Breadcrumbs::register('stock_movement', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Laporan Perpindahan Stok', route('stock_movement'));
});

Breadcrumbs::register('stock_movement.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('stock_movement');
    $breadcrumbs->push('Detail', route('stock_movement', $data->id));
});

Breadcrumbs::register('stock-bahan', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Stok Bahan', route('stock-bahan'));
});

Breadcrumbs::register('stock-bahan.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('stock-bahan');
    $breadcrumbs->push('Detail', route('stocks', $data->id));
});

Breadcrumbs::register('stocks', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Laporan Stok', route('stocks'));
});

Breadcrumbs::register('stocks.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('stocks');
    $breadcrumbs->push('Detail', route('stocks', $data->id));
});

Breadcrumbs::register('order-payment', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Laporan Pembelian per Pembayaran', route('order-payment'));
});

Breadcrumbs::register('order-payment.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('order-payment');
    $breadcrumbs->push('Detail Pembayaran', route('order-payment', $data->id));
});

Breadcrumbs::register('order-retur', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Laporan Retur Pembelian', route('order-retur'));
});

Breadcrumbs::register('order-retur.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('order-retur');
    $breadcrumbs->push('Detail Retur Pembelian', route('order-retur', $data->id));
});

Breadcrumbs::register('order-delivery', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Laporan Pembelian per Pengiriman', route('order-delivery'));
});

Breadcrumbs::register('order-delivery.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('order-delivery');
    $breadcrumbs->push('Detail Pengiriman', route('order-delivery', $data->id));
});

Breadcrumbs::register('order-po', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Laporan Pembelian per PO', route('order-po'));
});

Breadcrumbs::register('order-po.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('order-po');
    $breadcrumbs->push('Detail PO', route('order-po', $data->id));
});

Breadcrumbs::register('order-suppliers', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Laporan Pembelian per Supplier', route('order-suppliers'));
});

Breadcrumbs::register('order-suppliers.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('order-suppliers');
    $breadcrumbs->push('Detail Pembelian Supplier', route('order-suppliers', $data->id));
});

Breadcrumbs::register('order-suppliers.shows', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('order-suppliers');
    $breadcrumbs->push('Detail PO', route('order-suppliers', $data->id));
});

Breadcrumbs::register('sales_customer', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Laporan Penjualan Pelanggan', route('sales_customer'));
});

Breadcrumbs::register('sales_customer.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('sales_customer');
    $breadcrumbs->push('Detail Penjualan Pelanggan', route('sales_customer', $data->id));
});

Breadcrumbs::register('sales_customer.shows', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('sales_customer');
    $breadcrumbs->push('Detail Penjualan Pelanggan', route('sales_customer', $data->id));
});

Breadcrumbs::register('sales_product', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Laporan Penjualan Produk', route('sales_product'));
});

Breadcrumbs::register('sales_product.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('sales_product');
    $breadcrumbs->push('Detail Penjualan Produk', route('sales_product', $data->id));
});

Breadcrumbs::register('sales_product.shows', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('sales_product');
    $breadcrumbs->push('Detail Penjualan Produk', route('sales_product', $data->id));
});

Breadcrumbs::register('sales_invoice', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Laporan Penjualan Invoice', route('sales_invoice'));
});

Breadcrumbs::register('sales_invoice.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('sales_invoice');
    $breadcrumbs->push('Detail Penjualan Invoice', route('sales_invoice', $data->id));
});

Breadcrumbs::register('profit-loss', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Laporan Rugi Laba', route('profit-loss'));
});

Breadcrumbs::register('profit-loss.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('profit-loss');
    $breadcrumbs->push('Detail Rugi Laba', route('profit-loss', $data->id));
});

##scheduler
Breadcrumbs::register('scheduler', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Notifikasi Penagihan', route('scheduler'));
});
##end scheduler

##role-menu
Breadcrumbs::register('role-menu', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Role Menu', route('role-menu'));
});
##end role-menu

##shipping
Breadcrumbs::register('shipping', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Pengiriman', route('shipping'));
});
##end shipping

##role-user
Breadcrumbs::register('role-user', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Role User', route('role-user'));
});
##end role-menu

Breadcrumbs::register('cashier', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Penjualan', route('cashier'));
});

Breadcrumbs::register('model-detail', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Produk Detail', route('model-detail', $data->id));
});

Breadcrumbs::register('cashier-2', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Penjualan Nota', route('cashier-2'));
});

Breadcrumbs::register('order_form', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Form Pembelian', route('order_form'));
});

Breadcrumbs::register('delivery', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Pengiriman', route('delivery'));
});
Breadcrumbs::register('delivery.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('delivery');
    $breadcrumbs->push('Detail', route('delivery.show', $data->id));
});

Breadcrumbs::register('order', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('List Pembelian', route('order'));
});
Breadcrumbs::register('order.new', function($breadcrumbs)
{
    $breadcrumbs->parent('order');
    $breadcrumbs->push('New', route('order.new'));
});
Breadcrumbs::register('order.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('order');
    $breadcrumbs->push('Edit', route('order.edit', $data->id));
});
Breadcrumbs::register('order.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('order');
    $breadcrumbs->push('Detail', route('order.show', $data->id));
});

Breadcrumbs::register('bayar-po', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('List Pembelian', route('bayar-po'));
});
Breadcrumbs::register('bayar-po.new', function($breadcrumbs)
{
    $breadcrumbs->parent('bayar-po');
    $breadcrumbs->push('New', route('bayar-po.new'));
});
Breadcrumbs::register('bayar-po.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('bayar-po');
    $breadcrumbs->push('Edit', route('bayar-po.edit', $data->id));
});
Breadcrumbs::register('bayar-po.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('bayar-po');
    $breadcrumbs->push('Detail', route('bayar-po.show', $data->id));
});

Breadcrumbs::register('supplier_order', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Supplier Order', route('supplier_order'));
});
Breadcrumbs::register('supplier_order.new', function($breadcrumbs)
{
    $breadcrumbs->parent('supplier_order');
    $breadcrumbs->push('New', route('supplier_order.new'));
});
Breadcrumbs::register('supplier_order.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('supplier_order');
    $breadcrumbs->push('Edit', route('supplier_order.edit', $data->id));
});
Breadcrumbs::register('supplier_order.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('supplier_order');
    $breadcrumbs->push('Detail', route('supplier_order.show', $data->id));
});

Breadcrumbs::register('supplier', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Supplier', route('supplier'));
});
Breadcrumbs::register('supplier.new', function($breadcrumbs)
{
    $breadcrumbs->parent('supplier');
    $breadcrumbs->push('New', route('supplier.new'));
});
Breadcrumbs::register('supplier.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('supplier');
    $breadcrumbs->push('Edit', route('supplier.edit', $data->id));
});
Breadcrumbs::register('supplier.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('supplier');
    $breadcrumbs->push('Detail', route('supplier.show', $data->id));
});

Breadcrumbs::register('customer', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Customer', route('customer'));
});
Breadcrumbs::register('customer.new', function($breadcrumbs)
{
    $breadcrumbs->parent('customer');
    $breadcrumbs->push('New', route('customer.new'));
});
Breadcrumbs::register('customer.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('customer');
    $breadcrumbs->push('Edit', route('customer.edit', $data->id));
});
Breadcrumbs::register('customer.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('customer');
    $breadcrumbs->push('Detail', route('customer.show', $data->id));
});

Breadcrumbs::register('stock', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Stock', route('stock'));
});
Breadcrumbs::register('stock.new', function($breadcrumbs)
{
    $breadcrumbs->parent('stock');
    $breadcrumbs->push('New', route('stock.new'));
});
Breadcrumbs::register('stock.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('stock');
    $breadcrumbs->push('Edit', route('stock.edit', $data->id));
});
Breadcrumbs::register('stock.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('stock');
    $breadcrumbs->push('Detail', route('stock.show', $data->id));
});

Breadcrumbs::register('shop', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Toko', route('shop'));
});
Breadcrumbs::register('shop.new', function($breadcrumbs)
{
    $breadcrumbs->parent('shop');
    $breadcrumbs->push('New', route('shop.new'));
});
Breadcrumbs::register('shop.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('shop');
    $breadcrumbs->push('Edit', route('shop.edit', $data->id));
});
Breadcrumbs::register('shop.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('shop');
    $breadcrumbs->push('Detail', route('shop.show', $data->id));
});

Breadcrumbs::register('warehouse', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Gudang', route('warehouse'));
});
Breadcrumbs::register('warehouse.new', function($breadcrumbs)
{
    $breadcrumbs->parent('warehouse');
    $breadcrumbs->push('New', route('warehouse.new'));
});
Breadcrumbs::register('warehouse.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('warehouse');
    $breadcrumbs->push('Edit', route('warehouse.edit', $data->id));
});
Breadcrumbs::register('warehouse.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('warehouse');
    $breadcrumbs->push('Detail', route('warehouse.show', $data->id));
});

Breadcrumbs::register('potong-bahan', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Potong Bahan', route('potong-bahan'));
});

Breadcrumbs::register('potong-bahan.potong', function($breadcrumbs)
{
    $breadcrumbs->parent('potong-bahan');
    $breadcrumbs->push('Potong', route('potong-bahan.potong'));
});

Breadcrumbs::register('potong-bahan.potongmodel', function($breadcrumbs)
{
    $breadcrumbs->parent('potong-bahan');
    $breadcrumbs->push('Potong', route('potong-bahan.potongmodel'));
});

Breadcrumbs::register('masuk_bahan', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Masuk Bahan', route('masuk_bahan'));
});

Breadcrumbs::register('notification', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Notification', route('notification'));
});
Breadcrumbs::register('notification.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('notification');
    $breadcrumbs->push('Detail', route('notification.show', $data->id));
});

Breadcrumbs::register('kirim-stock', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Stock', route('kirim-stock'));
});
Breadcrumbs::register('kirim-stock.transfer', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('kirim-stock');
    $breadcrumbs->push('Kirim', route('kirim-stock.transfer', $data));
});
Breadcrumbs::register('kirim-stock.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('kirim-stock');
    $breadcrumbs->push('Edit', route('kirim-stock.edit', $data->id));
});
Breadcrumbs::register('kirim-stock.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('kirim-stock');
    $breadcrumbs->push('Detail', route('kirim-stock.show', $data->id));
});

Breadcrumbs::register('warna', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Warna', route('warna'));
});
Breadcrumbs::register('warna.new', function($breadcrumbs)
{
    $breadcrumbs->parent('warna');
    $breadcrumbs->push('New', route('warna.new'));
});
Breadcrumbs::register('warna.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('warna');
    $breadcrumbs->push('Edit', route('warna.edit', $data->id));
});
Breadcrumbs::register('warna.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('warna');
    $breadcrumbs->push('Detail', route('warna.show', $data->id));
});

Breadcrumbs::register('cost', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Biaya', route('cost'));
});
Breadcrumbs::register('cost.new', function($breadcrumbs)
{
    $breadcrumbs->parent('cost');
    $breadcrumbs->push('New', route('cost.new'));
});
Breadcrumbs::register('cost.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('cost');
    $breadcrumbs->push('Edit', route('cost.edit', $data->id));
});
Breadcrumbs::register('cost.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('cost');
    $breadcrumbs->push('Detail', route('cost.show', $data->id));
});

Breadcrumbs::register('model', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Produk', route('model'));
});
Breadcrumbs::register('model.new', function($breadcrumbs)
{
    $breadcrumbs->parent('model');
    $breadcrumbs->push('New', route('model.new'));
});
Breadcrumbs::register('model.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('model');
    $breadcrumbs->push('Edit', route('model.edit', $data->id));
});

Breadcrumbs::register('expense_konveksi', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Pengeluaran Konveksi', route('expense_konveksi'));
});
Breadcrumbs::register('expense_konveksi.new', function($breadcrumbs)
{
    $breadcrumbs->parent('expense_konveksi');
    $breadcrumbs->push('New', route('expense_konveksi.new'));
});
Breadcrumbs::register('expense_konveksi.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('expense_konveksi');
    $breadcrumbs->push('Edit', route('expense_konveksi.edit', $data->id));
});

Breadcrumbs::register('expense_toko', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Pengeluaran Toko', route('expense_toko'));
});
Breadcrumbs::register('expense_toko.new', function($breadcrumbs)
{
    $breadcrumbs->parent('expense_toko');
    $breadcrumbs->push('New', route('expense_toko.new'));
});
Breadcrumbs::register('expense_toko.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('expense_toko');
    $breadcrumbs->push('Edit', route('expense_toko.edit', $data->id));
});

Breadcrumbs::register('payment_method', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Metode Pembayaran', route('payment_method'));
});
Breadcrumbs::register('payment_method.new', function($breadcrumbs)
{
    $breadcrumbs->parent('payment_method');
    $breadcrumbs->push('New', route('payment_method.new'));
});
Breadcrumbs::register('payment_method.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('payment_method');
    $breadcrumbs->push('Edit', route('payment_method.edit', $data->id));
});

Breadcrumbs::register('operasional', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Biaya Operasional', route('operasional'));
});
Breadcrumbs::register('operasional.new', function($breadcrumbs)
{
    $breadcrumbs->parent('operasional');
    $breadcrumbs->push('New', route('operasional.new'));
});
Breadcrumbs::register('operasional.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('operasional');
    $breadcrumbs->push('Edit', route('operasional.edit', $data->id));
});

Breadcrumbs::register('input-cost', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Input Biaya', route('input-cost'));
});
Breadcrumbs::register('input-cost.new', function($breadcrumbs)
{
    $breadcrumbs->parent('input-cost');
    $breadcrumbs->push('New', route('input-cost.new'));
});
Breadcrumbs::register('input-cost.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('input-cost');
    $breadcrumbs->push('Edit', route('input-cost.edit', $data->id));
});

Breadcrumbs::register('salesman', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Salesman', route('salesman'));
});
Breadcrumbs::register('salesman.new', function($breadcrumbs)
{
    $breadcrumbs->parent('salesman');
    $breadcrumbs->push('New', route('salesman.new'));
});
Breadcrumbs::register('salesman.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('salesman');
    $breadcrumbs->push('Edit', route('salesman.edit', $data->id));
});

Breadcrumbs::register('list_penjualan', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Daftar Penjualan', route('list_penjualan'));
});
Breadcrumbs::register('list_penjualan.new', function($breadcrumbs)
{
    $breadcrumbs->parent('list_penjualan');
    $breadcrumbs->push('New', route('list_penjualan.new'));
});
Breadcrumbs::register('list_penjualan.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('list_penjualan');
    $breadcrumbs->push('Edit', route('list_penjualan.edit', $data->id));
});
Breadcrumbs::register('list_penjualan.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('list_penjualan');
    $breadcrumbs->push('Detail', route('list_penjualan.show', $data->id));
});

Breadcrumbs::register('petty-cash', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Uang Kas', route('petty-cash'));
});
Breadcrumbs::register('petty-cash.new', function($breadcrumbs)
{
    $breadcrumbs->parent('petty-cash');
    $breadcrumbs->push('New', route('petty-cash.new'));
});
Breadcrumbs::register('petty-cash.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('petty-cash');
    $breadcrumbs->push('Edit', route('petty-cash.edit', $data->id));
});
Breadcrumbs::register('petty-cash.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('petty-cash');
    $breadcrumbs->push('Detail', route('petty-cash.show', $data->date));
});

Breadcrumbs::register('stock-adjustment', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Penyesuaian Stok', route('stock-adjustment'));
});
Breadcrumbs::register('stock-adjustment.new', function($breadcrumbs)
{
    $breadcrumbs->parent('stock-adjustment');
    $breadcrumbs->push('New', route('stock-adjustment.new'));
});
Breadcrumbs::register('stock-adjustment.edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('stock-adjustment');
    $breadcrumbs->push('Edit', route('stock-adjustment.edit', $data->id));
});
Breadcrumbs::register('stock-adjustment.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('stock-adjustment');
    $breadcrumbs->push('Detail', route('stock-adjustment.show', $data->date));
});

Breadcrumbs::register('retur-general', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Retur tanpa Faktur', route('retur-general'));
});

Breadcrumbs::register('retur-invoice', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Retur dengan Faktur', route('retur-invoice'));
});

Breadcrumbs::register('retur-customer', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Laporan Retur By Customer', route('retur-customer'));
});

Breadcrumbs::register('retur-customer.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('retur-customer');
    $breadcrumbs->push('Detail Laporan Retur', route('retur-customer', $data->id));
});

Breadcrumbs::register('retur-customer.shows', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('retur-customer');
    $breadcrumbs->push('Detail Retur', route('retur-customer', $data->id));
});

Breadcrumbs::register('retur-product', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Laporan Retur By Produk', route('retur-product'));
});

Breadcrumbs::register('retur-product.show', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('retur-product');
    $breadcrumbs->push('Detail Laporan Retur', route('retur-product', $data->id));
});

Breadcrumbs::register('retur-product.shows', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('retur-product');
    $breadcrumbs->push('Detail Retur', route('retur-product', $data->id));
});

Breadcrumbs::register('retur-noret', function($breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Laporan Retur By Nomor Retur', route('retur-noret'));
});

Breadcrumbs::register('retur-noret.shows', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('retur-noret');
    $breadcrumbs->push('Detail Laporan Retur', route('retur-noret', $data->id));
});
