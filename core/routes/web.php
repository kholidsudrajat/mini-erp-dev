<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/admin/dashboard', 'AdminController@index')->name('admin.home');
Route::get('/admin', function () {
    return redirect('/admin/dashboard');
});

Route::get('/profil/{id}/edit', 'AdminController@edit')->name('profil.edit');

Route::post('test', 'Admin\\CashierController@test');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
	Route::get('/analytic', 'AdminController@analytic');
	Route::get('/reminder', 'AdminController@reminder');
	Route::get('/dashboard', 'AdminController@index')->name('admin.home');
    Route::get('/changemonthsales/{year}', 'AdminController@changemonthsales');

    Route::post('/model/tambah', 'Admin\\PembelianController@tambah_produk');

    Route::get('/notification/data', ['as' => 'notification.data', 'uses' => 'Admin\\NotificationController@anyData']);
    Route::get('/notification/{id}', ['uses' => 'Admin\\NotificationController@show']);
    Route::resource('/notification', 'Admin\\NotificationController@index', [
        'names' => [
            'index' => 'notification',
            'show' => 'notification.show'
        ]
    ]);
    
	Route::get('/user/data', ['as' => 'user.data', 'uses' => 'Admin\\UserController@anyData']);
	Route::resource('/user', 'Admin\\UserController', [
		'names' => [
			'index' => 'user',
			'create' => 'user.new',
			'edit' => 'user.edit',
			'show' => 'user.show'
		]
	]);
    
	Route::get('/customer/data', ['as' => 'customer.data', 'uses' => 'Admin\\CustomerController@anyData']);
	Route::resource('/customer', 'Admin\\CustomerController', [
		'names' => [
			'index' => 'customer',
			'create' => 'customer.new',
			'edit' => 'customer.edit',
			'show' => 'customer.show'
		]
	]);
    
	Route::get('/supplier/data', ['as' => 'supplier.data', 'uses' => 'Admin\\SupplierController@anyData']);
    Route::post('/supplier/tambah', 'Admin\\PembelianController@tambah');
	Route::resource('/supplier', 'Admin\\SupplierController', [
		'names' => [
			'index' => 'supplier',
			'create' => 'supplier.new',
			'edit' => 'supplier.edit',
			'show' => 'supplier.show'
		]
	]);
    
    Route::get('addbahansupplier/{id}', ['uses' => 'Admin\\SupplierController@addbahan']);
    Route::get('deletebahansupplier/{id}', ['uses' => 'Admin\\SupplierController@deletebahan']);
    Route::post('savebahansupplier', ['uses' => 'Admin\\SupplierController@savebahan']);
    
    Route::get('addmodelsupplier/{id}', ['uses' => 'Admin\\SupplierController@addmodel']);
    Route::get('deletemodelsupplier/{id}', ['uses' => 'Admin\\SupplierController@deletemodel']);
    Route::post('savemodelsupplier', ['uses' => 'Admin\\SupplierController@savemodel']);
    
    
	Route::get('/pembelian/supplier/data', ['as' => 'supplier_order.data', 'uses' => 'Admin\\SupplierOrderController@anyData']);
	Route::resource('/pembelian/supplier', 'Admin\\SupplierOrderController', [
		'names' => [
			'index' => 'supplier_order',
			'create' => 'supplier_order.new',
			'edit' => 'supplier_order.edit',
			'show' => 'supplier_order.show'
		]
	]);
    
	Route::get('/order/data', ['as' => 'order.data', 'uses' => 'Admin\\OrderController@anyData']);
	Route::resource('/order', 'Admin\\OrderController', [
		'names' => [
			'index' => 'order',
			'create' => 'order.new',
			'edit' => 'order.edit',
			'show' => 'order.show'
		]
	]);
	Route::post('/order/update/{id}', ['uses' => 'Admin\\OrderController@update']);
	Route::post('/order/adddetail/{id}', ['uses' => 'Admin\\OrderController@adddetail']);
	Route::post('/order/updatedetail/{id}', ['uses' => 'Admin\\OrderController@updatedetail']);
	Route::post('/order/deletedetail/{id}', ['uses' => 'Admin\\OrderController@deletedetail']);
    
	Route::get('pembelian/delivery/data', ['as' => 'delivery.data', 'uses' => 'Admin\\OrderDeliveryController@anyData']);
	Route::resource('pembelian/delivery', 'Admin\\OrderDeliveryController', [
		'names' => [
			'index' => 'delivery',
			'create' => 'delivery.new',
			'edit' => 'delivery.edit',
			'show' => 'delivery.show'
		]
	]);
	Route::get('pembelian/delivery/{id}/add', ['uses' => 'Admin\\OrderDeliveryController@add']);
	Route::post('pembelian/delivery/{id}/takeall', ['uses' => 'Admin\\OrderDeliveryController@takeall']);
	Route::get('pembelian/delivery/{id}/{oid}', ['uses' => 'Admin\\OrderDeliveryController@detail']);
	Route::post('pembelian/delivery/{id}/submit', ['uses' => 'Admin\\OrderDeliveryController@submit']);
    
	Route::get('pembelian/bayar/data', ['as' => 'bayar-po.data', 'uses' => 'Admin\\OrderPaymentController@anyData']);
	Route::resource('pembelian/bayar', 'Admin\\OrderPaymentController', [
		'names' => [
			'index' => 'bayar-po',
			'create' => 'bayar-po.new',
			'edit' => 'bayar-po.edit',
			'show' => 'bayar-po.show'
		]
	]);
	Route::get('pembelian/bayar/{id}/add', ['uses' => 'Admin\\OrderPaymentController@add']);
	Route::post('pembelian/bayar/{id}/submit', ['uses' => 'Admin\\OrderPaymentController@submit']);
    
    Route::get('pembelian/form/getmodel', 'Admin\\PembelianController@getmodel');
    Route::get('pembelian/form', ['as' => 'order_form', 'uses' => 'Admin\\PembelianController@form']);
    Route::post('pembelian/form/submit', ['uses' => 'Admin\\PembelianController@submit']);
    Route::get('potong-bahan', ['as' => 'potong-bahan', 'uses' => 'Admin\\PotongBahanController@index']);
    Route::get('potong-bahan/potong', ['as' => 'potong-bahan.potong', 'uses' => 'Admin\\PotongBahanController@potong']);
    Route::get('potong-bahan/potongmodel', ['as' => 'potong-bahan.potongmodel', 'uses' => 'Admin\\PotongBahanController@potongmodel']);
    Route::get('potong-bahan/getmodel', ['uses' => 'Admin\\PotongBahanController@getmodel']);
    Route::post('potong-bahan/cekstock', ['uses' => 'Admin\\PotongBahanController@cekstock']);
    Route::post('potong-bahan/submit', ['uses' => 'Admin\\PotongBahanController@submit']);
    Route::post('potong-bahan/submitmodel', ['uses' => 'Admin\\PotongBahanController@submitmodel']);
    Route::post('potong-bahan/remove', ['uses' => 'Admin\\PotongBahanController@remove']);
    Route::post('potong-bahan/delete', ['uses' => 'Admin\\PotongBahanController@delete']);
    Route::post('potong-bahan/submitall', ['uses' => 'Admin\\PotongBahanController@submitall']);
    Route::get('masuk_bahan', ['as' => 'masuk_bahan', 'uses' => 'Admin\\BahanController@index']);
    Route::get('pilihbahan/{sup}', ['uses' => 'Admin\\BahanController@pilihbahan']);
    Route::get('cleartemp', ['uses' => 'Admin\\BahanController@cleartemp']);
    Route::get('detailkg/{id}', ['uses' => 'Admin\\BahanController@detailkg']);
    Route::post('submitbahan', ['uses' => 'Admin\\BahanController@submitbahan']);
    Route::post('createtempbahan', ['uses' => 'Admin\\BahanController@createtemp']);
    Route::post('createtempdetail', ['uses' => 'Admin\\BahanController@createtempdetail']);
    
	Route::get('/stock-bahan/{id}/{ids}', ['uses' => 'Admin\\StockBahanController@shows']);
	Route::get('/stock-bahan/data', ['as' => 'stock-bahan.data', 'uses' => 'Admin\\StockBahanController@anyData']);
	Route::resource('/stock-bahan', 'Admin\\StockBahanController', [
		'names' => [
			'index' => 'stock-bahan',
			'create' => 'stock-bahan.new',
			'edit' => 'stock-bahan.edit',
			'show' => 'stock-bahan.show'
		]
	]);
    
    Route::get('/stock/getstock', 'Admin\\StockController@getstock');
	Route::get('/stock/data', ['as' => 'stock.data', 'uses' => 'Admin\\StockController@anyData']);
	Route::resource('/stock', 'Admin\\StockController', [
		'names' => [
			'index' => 'stock',
			'create' => 'stock.new',
			'edit' => 'stock.edit',
			'show' => 'stock.show'
		]
	]);
    
	Route::get('/shop/data', ['as' => 'shop.data', 'uses' => 'Admin\\ShopController@anyData']);
	Route::resource('/shop', 'Admin\\ShopController', [
		'names' => [
			'index' => 'shop',
			'create' => 'shop.new',
			'edit' => 'shop.edit',
			'show' => 'shop.show'
		]
	]);
    
    Route::post('/shop/changedefault', 'Admin\\ShopController@changedefault');
    
	Route::get('/warehouse/data', ['as' => 'warehouse.data', 'uses' => 'Admin\\WarehouseController@anyData']);
	Route::resource('/warehouse', 'Admin\\WarehouseController', [
		'names' => [
			'index' => 'warehouse',
			'create' => 'warehouse.new',
			'edit' => 'warehouse.edit',
			'show' => 'warehouse.show'
		]
	]);
    
    Route::get('/kirim-stock/multiple', ['uses' => 'Admin\\TransferStockController@multiple']);
    Route::post('/kirim-stock/multiple/transfer', ['uses' => 'Admin\\TransferStockController@transfermultiple']);
	Route::get('/kirim-stock/data', ['as' => 'kirim-stock.data', 'uses' => 'Admin\\TransferStockController@anyData']);
	Route::resource('/kirim-stock', 'Admin\\TransferStockController', [
		'names' => [
			'index' => 'kirim-stock',
			'edit' => 'kirim-stock.edit',
			'show' => 'kirim-stock.show'
		]
	]);
    
    Route::get('/kirim-stock/{id}/transfer', ['as' => 'kirim-stock.transfer', 'uses' => 'Admin\\TransferStockController@transfer']);
    Route::get('/prosestransferstock', ['uses' => 'Admin\\TransferStockController@prosestransfer']);
    
	Route::get('/warna/data', ['as' => 'warna.data', 'uses' => 'Admin\\WarnaController@anyData']);
	Route::resource('/warna', 'Admin\\WarnaController', [
		'names' => [
			'index' => 'warna',
			'create' => 'warna.new',
			'edit' => 'warna.edit',
			'show' => 'warna.show'
		]
	]);
    
	Route::get('/cost/data', ['as' => 'cost.data', 'uses' => 'Admin\\CostController@anyData']);
	Route::resource('/cost', 'Admin\\CostController', [
		'names' => [
			'index' => 'cost',
			'create' => 'cost.new',
			'edit' => 'cost.edit',
			'show' => 'cost.show'
		]
	]);
    
	Route::get('/model/data', ['as' => 'model.data', 'uses' => 'Admin\\ModelsController@anyData']);
    Route::resource('/model', 'Admin\\ModelsController', [
		'names' => [
			'index' => 'model',
			'create' => 'model.new',
			'edit' => 'model.edit',
			'show' => 'model.show'
		]
	]);
    Route::get('/model/{id}/changeimage', 'Admin\\ModelsController@changeimage');
    Route::post('/model/filter', 'Admin\\ModelsController@filter');
    Route::post('/model/{id}/update', 'Admin\\ModelsController@update');
    
	Route::get('/expense_konveksi/data', ['as' => 'expense_konveksi.data', 'uses' => 'Admin\\ExpenseKonveksiController@anyData']);
	Route::resource('/expense_konveksi', 'Admin\\ExpenseKonveksiController', [
		'names' => [
			'index' => 'expense_konveksi',
			'create' => 'expense_konveksi.new',
			'edit' => 'expense_konveksi.edit',
			'show' => 'expense_konveksi.show'
		]
	]);
    
	Route::get('/expense_toko/data', ['as' => 'expense_toko.data', 'uses' => 'Admin\\ExpenseTokoController@anyData']);
	Route::resource('/expense_toko', 'Admin\\ExpenseTokoController', [
		'names' => [
			'index' => 'expense_toko',
			'create' => 'expense_toko.new',
			'edit' => 'expense_toko.edit',
			'show' => 'expense_toko.show'
		]
	]);
    
	Route::get('/payment_method/data', ['as' => 'payment_method.data', 'uses' => 'Admin\\PaymentMethodController@anyData']);
	Route::resource('/payment_method', 'Admin\\PaymentMethodController', [
		'names' => [
			'index' => 'payment_method',
			'create' => 'payment_method.new',
			'edit' => 'payment_method.edit',
			'show' => 'payment_method.show'
		]
	]);
    
	Route::get('/operasional/data', ['as' => 'operasional.data', 'uses' => 'Admin\\OperationalController@anyData']);
	Route::resource('/operasional', 'Admin\\OperationalController', [
		'names' => [
			'index' => 'operasional',
			'create' => 'operasional.new',
			'edit' => 'operasional.edit',
			'show' => 'operasional.show'
		]
	]);
    
	Route::get('/input-cost/data', ['as' => 'input-cost.data', 'uses' => 'Admin\\InputCostController@anyData']);
	Route::resource('/input-cost', 'Admin\\InputCostController', [
		'names' => [
			'index' => 'input-cost',
			'create' => 'input-cost.new',
			'edit' => 'input-cost.edit',
			'show' => 'input-cost.show'
		]
	]);
    Route::post('/inputcost/add','Admin\\InputCostController@add');
    Route::post('/inputcost/update','Admin\\InputCostController@updates');
    Route::post('/inputcost/delete','Admin\\InputCostController@destroys');
    Route::post('/inputcost/getbydate','Admin\\InputCostController@getbydate');
	
	Route::get('/salesman/data', ['as' => 'salesman.data', 'uses' => 'Admin\\SalesmanController@anyData']);
	Route::resource('/salesman', 'Admin\\SalesmanController', [
		'names' => [
			'index' => 'salesman',
			'create' => 'salesman.new',
			'edit' => 'salesman.edit',
			'show' => 'salesman.show'
		]
	]);
    
    Route::get('/cashier/', ['as' => 'cashier', 'uses' => 'Admin\\CashierController@index']);
    Route::get('/cashier/getnotes/{id}', ['uses' => 'Admin\\CashierController@getnotes']);
    Route::get('/cashier/shop/{id}/choose', ['uses' => 'Admin\\CashierController@shop']);
    Route::get('/cashier/tab/choose', ['uses' => 'Admin\\CashierController@tabs']);
    Route::get('/cashier/details/{tab}', ['uses' => 'Admin\\CashierController@details']);
    Route::get('/cashier/shipping/{noref}', ['uses' => 'Admin\\CashierController@shipping']);
    Route::post('/cashier/changeaddress', ['uses' => 'Admin\\CashierController@changeaddress']);
    Route::post('/cashier/saveshipping', ['uses' => 'Admin\\CashierController@saveshipping']);
    Route::get('/cashier/print/{noref}', ['uses' => 'Admin\\CashierController@prints']);
    Route::get('/cashier/prints', ['uses' => 'Admin\\CashierController@prints_']);
    Route::post('/cashier/bayar', ['uses' => 'Admin\\CashierController@bayar']);
    Route::get('/cashier/addqty', ['uses' => 'Admin\\CashierController@addqty']);
    Route::get('/cashier/adddisc', ['uses' => 'Admin\\CashierController@adddisc']);
    Route::get('/cashier/hapusqty/{id}', ['uses' => 'Admin\\CashierController@hapusqty']);
    Route::post('/cashier/ajax_model', ['uses' => 'Admin\\CashierController@ajaxmodel']);
    Route::get('/model_detail/{id}', ['as' => 'model-detail', 'uses' => 'Admin\\CashierController@modelDetail']);
    
    Route::get('/cashier-2/', ['as' => 'cashier-2', 'uses' => 'Admin\\CashierController@index2']);
    Route::post('/cashier-2/bayar/', ['uses' => 'Admin\\CashierController@bayars']);
    Route::get('/cashier-2/addqty', ['uses' => 'Admin\\CashierController@addqty2']);
    Route::get('/cashier-2/hapusqty/{id}', ['uses' => 'Admin\\CashierController@hapusqty2']);
    Route::post('/cashier-2/ajax_model', ['uses' => 'Admin\\CashierController@ajaxmodels']);
    Route::get('/cashier-2/adddisc', ['uses' => 'Admin\\CashierController@adddiscs']);
    Route::get('/cashier-2/shipping/{noref}', ['uses' => 'Admin\\CashierController@shipping2']);
    Route::post('/cashier-2/changeaddress', ['uses' => 'Admin\\CashierController@changeaddress2']);
    Route::post('/cashier-2/saveshipping', ['uses' => 'Admin\\CashierController@saveshipping2']);
    Route::get('/cashier-2/print/{noref}', ['uses' => 'Admin\\CashierController@prints2']);
    Route::get('/cashier-2/prints', ['uses' => 'Admin\\CashierController@prints_2']);
    
	Route::get('/list_penjualan/data', ['as' => 'list_penjualan.data', 'uses' => 'Admin\\PenjualanController@anyData']);
	Route::resource('/list_penjualan', 'Admin\\PenjualanController', [
		'names' => [
			'index' => 'list_penjualan',
			'create' => 'list_penjualan.new',
			'edit' => 'list_penjualan.edit',
			'show' => 'list_penjualan.show'
		]
	]);
    Route::post('/list_penjualan/updatenoresi', 'Admin\\PenjualanController@updatenoresi');
    Route::post('/list_penjualan/paydebt', 'Admin\\PenjualanController@paydebt');
    
    Route::get('/report/order-retur/data', ['as' => 'order-retur.data', 'uses' => 'Admin\\ReportOrderReturController@anyData']);
    Route::resource('/report/order-retur', 'Admin\\ReportOrderReturController', [
		'names' => [
			'index' => 'order-retur',
			'show' => 'order-retur.show'
		]
	]);
    
    Route::get('/report/order-delivery/data', ['as' => 'order-delivery.data', 'uses' => 'Admin\\ReportOrderDeliveryController@anyData']);
    Route::resource('/report/order-delivery', 'Admin\\ReportOrderDeliveryController', [
		'names' => [
			'index' => 'order-delivery',
			'show' => 'order-delivery.show'
		]
	]);
    
    Route::get('/report/order-payment/data', ['as' => 'order-payment.data', 'uses' => 'Admin\\ReportOrderPaymentController@anyData']);
    Route::resource('/report/order-payment', 'Admin\\ReportOrderPaymentController', [
		'names' => [
			'index' => 'order-payment',
			'show' => 'order-payment.show'
		]
	]);
    
    Route::get('/report/order-po/data', ['as' => 'order-po.data', 'uses' => 'Admin\\ReportOrderPoController@anyData']);
    Route::resource('/report/order-po', 'Admin\\ReportOrderPoController', [
		'names' => [
			'index' => 'order-po',
			'show' => 'order-po.show'
		]
	]);
    
    Route::get('/report/order-suppliers/data', ['as' => 'order-suppliers.data', 'uses' => 'Admin\\ReportOrderController@anyData']);
    Route::resource('/report/order-suppliers', 'Admin\\ReportOrderController', [
		'names' => [
			'index' => 'order-suppliers',
			'show' => 'order-suppliers.show'
		]
	]);
    
    Route::get('/report/order-suppliers/{id}/{idm}', ['as' => 'order-suppliers.shows', 'uses' => 'Admin\\ReportOrderController@shows']);
    
    Route::get('/report/sales_customer/data', ['as' => 'sales_customer.data', 'uses' => 'Admin\\ReportCustomerController@anyData']);
    Route::resource('/report/sales_customer', 'Admin\\ReportCustomerController', [
		'names' => [
			'index' => 'sales_customer',
			'show' => 'sales_customer.show'
		]
	]);
    
    Route::get('/report/sales_customer/{id}/{idm}/show', ['as' => 'sales_customer.shows', 'uses' => 'Admin\\ReportCustomerController@shows']);
    
    Route::post('/report/sales_customer/{id}/filter', ['uses' => 'Admin\\ReportCustomerController@filter']);
    
    Route::get('/report/sales_product/data', ['as' => 'sales_product.data', 'uses' => 'Admin\\ReportProductController@anyData']);
    Route::resource('/report/sales_product', 'Admin\\ReportProductController', [
		'names' => [
			'index' => 'sales_product',
			'show' => 'sales_product.show'
		]
	]);
    
    Route::get('/report/sales_product/{id}/{idm}/show', ['as' => 'sales_product.shows', 'uses' => 'Admin\\ReportProductController@shows']);
    
    Route::post('/report/sales_product/{id}/filter', ['uses' => 'Admin\\ReportProductController@filter']);
    
    Route::get('/report/sales_invoice/data', ['as' => 'sales_invoice.data', 'uses' => 'Admin\\ReportInvoiceController@anyData']);
    Route::resource('/report/sales_invoice', 'Admin\\ReportInvoiceController', [
		'names' => [
			'index' => 'sales_invoice',
			'show' => 'sales_invoice.show'
		]
	]);
    
    Route::post('/report/sales_invoice/{id}/filter', ['uses' => 'Admin\\ReportInvoiceController@filter']);
    
    Route::get('/report/stock-adjustment/data', ['as' => 'report-stock-adjustment.data', 'uses' => 'Admin\\ReportStockAdjustController@anyData']);
    Route::resource('/report/stock-adjustment', 'Admin\\ReportStockAdjustController', [
		'names' => [
			'index' => 'report-stock-adjustment',
			'show' => 'report-stock-adjustment.show'
		]
	]);
    
    Route::get('/report/stock_movement/data', ['as' => 'stock_movement.data', 'uses' => 'Admin\\ReportStockController@anyData']);
    Route::resource('/report/stock_movement', 'Admin\\ReportStockController', [
		'names' => [
			'index' => 'stock_movement',
			'show' => 'stock_movement.show'
		]
	]);
    
    Route::post('/report/stock_movement/{id}/filter', ['uses' => 'Admin\\ReportStockController@filter']);
    
    Route::get('/report/stock/data', ['as' => 'stocks.data', 'uses' => 'Admin\\ReportStocksController@anyData']);
    Route::resource('/report/stock', 'Admin\\ReportStocksController', [
		'names' => [
			'index' => 'stocks',
			'show' => 'stocks.show'
		]
	]);
    
    Route::post('/report/stock/{id}/filter', ['uses' => 'Admin\\ReportStocksController@filter']);
    
    Route::get('/report/profit-loss/', ['as' => 'profit-loss', 'uses' => 'Admin\\ReportProfitLossController@index']);
    Route::post('/report/profit-loss/filter', 'Admin\\ReportProfitLossController@filter');

    ##scheduler
    Route::get('/scheduler/data', ['as' => 'scheduler.data', 'uses' => 'Admin\\SchedulerController@anyData']);
    Route::post('/scheduler/tambah', 'Admin\\SchedulerController@tambah');
    Route::get('/scheduler/delete/{id}', 'Admin\\SchedulerController@delete');
    Route::resource('/scheduler', 'Admin\\SchedulerController@index', [
        'names' => [
            'index' => 'scheduler',
            'show' => 'scheduler.show'
        ]
    ]);

    ##menu (role)
    Route::get('/role-menu/data', ['as' => 'role-menu.data', 'uses' => 'Admin\\RoleMenuController@anyData']);
    Route::post('/role-menu/tambah', 'Admin\\RoleMenuController@tambah');
    Route::get('/role-menu/delete/{id}', 'Admin\\RoleMenuController@delete');
    Route::resource('/role-menu', 'Admin\\RoleMenuController@index', [
        'names' => [
            'index' => 'role-menu',
            'show' => 'role-menu.show'
        ]
    ]);

    ##user (role)
    Route::get('/role-user/data', ['as' => 'role-user.data', 'uses' => 'Admin\\RoleUserController@anyData']);
    Route::post('/role-user/tambah', 'Admin\\RoleUserController@tambah');
    Route::get('/role-user/delete/{id}', 'Admin\\RoleUserController@delete');
    Route::get('/role-user/select/{id}', 'Admin\\RoleUserController@select');
    Route::post('/insert/role-menu', 'Admin\\RoleUserController@update');
    Route::resource('/role-user', 'Admin\\RoleUserController@index', [
        'names' => [
            'index' => 'role-user',
            'show' => 'role-user.show'
        ]
    ]);

    ##shipping (ekspedisi)
    Route::get('/shipping/data', ['as' => 'shipping.data', 'uses' => 'Admin\\ShippingController@anyData']);
    Route::post('/shipping/tambah', 'Admin\\ShippingController@tambah');
    Route::get('/shipping/delete/{id}', 'Admin\\ShippingController@delete');
    Route::resource('/shipping', 'Admin\\ShippingController@index', [
        'names' => [
            'index' => 'shipping',
            'show' => 'shipping.show'
        ]
    ]);
    
	Route::get('/petty-cash/data', ['as' => 'petty-cash.data', 'uses' => 'Admin\\PettyCashController@anyData']);
	Route::resource('/petty-cash', 'Admin\\PettyCashController', [
		'names' => [
			'index' => 'petty-cash',
			'create' => 'petty-cash.new',
			'edit' => 'petty-cash.edit',
			'show' => 'petty-cash.show'
		]
	]);
    
    Route::get('/stock-adjustment/getstock', 'Admin\\StockAdjustmentController@getstock');
	Route::get('/stock-adjustment/data', ['as' => 'stock-adjustment.data', 'uses' => 'Admin\\StockAdjustmentController@anyData']);
	Route::resource('/stock-adjustment', 'Admin\\StockAdjustmentController', [
		'names' => [
			'index' => 'stock-adjustment',
			'create' => 'stock-adjustment.new',
			'edit' => 'stock-adjustment.edit',
			'show' => 'stock-adjustment.show'
		]
	]);
    
	Route::get('/retur-general/data', ['as' => 'retur-general.data', 'uses' => 'Admin\\ReturGeneralController@anyData']);
	Route::resource('/retur-general', 'Admin\\ReturGeneralController', [
		'names' => [
			'index' => 'retur-general'
		]
	]);
    Route::post('/retur-general/save', 'Admin\\ReturGeneralController@save');
    
	Route::get('/retur-invoice/data', ['as' => 'retur-invoice.data', 'uses' => 'Admin\\ReturInvoiceController@anyData']);
	Route::resource('/retur-invoice', 'Admin\\ReturInvoiceController', [
		'names' => [
			'index' => 'retur-invoice'
		]
	]);
    Route::get('/retur-invoice/{noref}', 'Admin\\ReturInvoiceController@detail');
    
    Route::get('/report/retur-product/data', ['as' => 'retur-product.data', 'uses' => 'Admin\\ReturProductController@anyData']);
    Route::resource('/report/retur-product', 'Admin\\ReturProductController', [
		'names' => [
			'index' => 'retur-product',
			'show' => 'retur-product.show'
		]
	]);
    Route::post('/report/retur-product/{id}/filter', ['uses' => 'Admin\\ReturProductController@filter']);
    Route::get('/report/retur-product/{id}/{noret}', ['uses' => 'Admin\\ReturProductController@shows']);
    
    Route::get('/report/retur-noret/data', ['as' => 'retur-noret.data', 'uses' => 'Admin\\ReturNoretController@anyData']);
    Route::resource('/report/retur-noret', 'Admin\\ReturNoretController', [
		'names' => [
			'index' => 'retur-noret',
			'show' => 'retur-noret.show'
		]
	]);
    Route::get('/report/retur-noret/{id}', ['uses' => 'Admin\\ReturNoretController@shows']);
    
    Route::get('/report/retur-customer/data', ['as' => 'retur-customer.data', 'uses' => 'Admin\\ReturCustomerController@anyData']);
    Route::resource('/report/retur-customer', 'Admin\\ReturCustomerController', [
		'names' => [
			'index' => 'retur-customer',
			'show' => 'retur-customer.show'
		]
	]);
    Route::post('/report/retur-customer/{id}/filter', ['uses' => 'Admin\\ReturCustomerController@filter']);
    Route::get('/report/retur-customer/{id}/{noret}', ['uses' => 'Admin\\ReturCustomerController@shows']);
    
    Route::post('/changeshop', 'Admin\\ReturGeneralController@shop');
    Route::post('/getProduk', 'Admin\\ReturGeneralController@getProduk');

    ##multiple address
    Route::post('/address/tambah', 'Admin\\CustomerController@tambah');
    Route::get('/address/hapus/{id}', 'Admin\\CustomerController@hapus');


    Route::get('/sent/sms/{id}', 'Admin\\CustomerController@sms');

    Route::get('/sent/whatsapp', 'AdminController@whatsapp');

    Route::get('/notice-daily', ['as' => 'notice-daily', 'uses' => 'Admin\\NoticeController@daily']);
    Route::get('/notice-monthly', ['as' => 'notice-monthly', 'uses' => 'Admin\\NoticeController@monthly']);
    Route::get('/config', 'Admin\\NoticeController@config');
    Route::post('/configscheduler', 'Admin\\SchedulerController@config');
});
