<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/structure.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/fonts.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/font-awesome.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/owl.carousel.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/owl.theme.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/icomoon.css') }}">
	<title>Gym Website Template</title>
</head>
<style type="text/css">
	.backgroundimage{
		background:url('assets/images/image2.jpg');
		background-position: center;
		background-size: cover;
		height: 100vh;
	}
</style>
<body>
	<nav class="navbar navbar-default backgroundtransparent positionabsolute top0 width100 zindex20">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        			<span class="sr-only">Toggle navigation</span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
      			</button>
				<a class="navbar-brand colororange fontavenirheavy textuppercase" href="#home">POWER</a>
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="textuppercase fontavenirheavy"><a href="#home">Home</a></li>
					<li class="textuppercase fontavenirheavy"><a href="#about">About</a></li>
					<li class="textuppercase fontavenirheavy"><a href="#classes">Classes</a></li>
					<li class="textuppercase fontavenirheavy"><a href="#trainers">Trainers</a></li>
					<li class="textuppercase fontavenirheavy"><a href="#contactus">Join</a></li>
				</ul>
			</div>
		</div>
	</nav>
    
    @yield('content')
    
	<script type="text/javascript" src="{{ url('assets/js/jquery-1.9.1.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/js/bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/js/owl.carousel.js') }}"></script>
	<script type="text/javascript">
		$(function() {
			$('a[href*="#"]:not([href="#"])').click(function() {
				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
					var target = $(this.hash);
					target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
					if (target.length) {
						$('html, body').animate({
				  			scrollTop: target.offset().top
						}, 1000);
						return false;
					}
				}
			});
		});
		$(document).ready(function() {
 
			$("#owl-example").owlCarousel();

		});
		$(document).ready(function() {
			$("#owl-demo").owlCarousel({

			navigation : true,
			slideSpeed : 300,
			paginationSpeed : 400,
			singleItem : true

			});
		});
	</script>
</body>
</html>