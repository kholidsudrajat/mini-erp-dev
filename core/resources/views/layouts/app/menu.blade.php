<li class="m-t-30 {{ (Request::is('admin/dashboard*')) ? 'active' : ''}}">
    <a href="{{ url('admin') }}">Dashboard</a>
    <span class="{{ (Request::is('admin/dashboard*')) ? 'bg-success' : '' }} icon-thumbnail"><i class="fa fa-home"></i></span>
</li>

@php
$user = \App\User::whereId(Auth::user()->id)->first();
$menuId = explode(',', $user->roleUser->role_menu);
$roleMenus = \App\RoleMenu::select(['role_menu.*'])->join('role_menu as parent', 'role_menu.parent_menu', '=', 'parent.id')->whereIn('role_menu.parent_menu', $menuId)->groupBy('role_menu.parent_menu')->orderBy('parent.order', 'asc')->get();

$parentMenus = [];
foreach ($roleMenus as $menu)
{
    $menus = \App\RoleMenu::whereIn("id", $menuId)->whereParentMenu($menu->parent_menu)->orderBy('order', 'asc')->get();
    $request = '';
    foreach ($menus as $child) {
        if (Request::is('admin/'.$child->url_name .'*')) {
            $request = 'open active';
        }
        if(Request::is('admin/report/sales_customer') || Request::is('admin/report/sales_product') || Request::is('admin/report/sales_invoice')){
            if($child->url_name == 'report/sales_customer' || $child->url_name == 'report/sales_product' || $child->url_name == 'report/sales_invoice'){
                $request = 'open active';
            }
        } elseif(Request::is('admin/report/retur-product') || Request::is('admin/report/retur-noret') || Request::is('admin/report/retur-customer')){
            if($child->url_name == 'report/retur-product' || $child->url_name == 'report/retur-noret' || $child->url_name == 'report/retur-customer'){
                $request = 'open active';
            }
        }
    }

    $parent = count($menu) > 0 ? $menu->parentMenu->menu : null;
    echo "<li class='{$request}'><a href='javascript:;'><span class='title'>" . $parent . "</span>
    <span class='arrow {$request}'></span>
    </a>
    <span class='{$request} icon-thumbnail'>
        <i class='fa fa-database'></i>
    </span>";

    echo "<ul class='sub-menu'>";
    foreach ($menus as $menu) {
        $request = (Request::is('admin/'.$menu->url_name .'*')) ? 'active' : '';
        if($menu->url_name == 'report/sales_customer' || $menu->url_name == 'report/sales_product' || $menu->url_name == 'report/sales_invoice'){
            $request = (Request::is('admin/report/sales*')) ? 'active' : '';
        }
        $url = url('admin/' . $menu->url_name);
        $icon = explode(' ', $menu->menu);
        $first = strtoupper(substr(reset($icon), 0,1));
        $last = strtoupper(substr(preg_replace("/[^A-Z]+/", "", end($icon)), 0,1));
        if(count($icon)>1)
            $icon = $first . $last;
        else
            $icon = $first;
        if($menu->id != 12 && $menu->id != 9 && $menu->id != 10){
        echo "<li class='{$request}'>
        <a href='{$url}'>".$menu->menu . "</a>
        <span class='{$request} icon-thumbnail'>
            {$icon}
        </span>
        </li>";
        }
    }
    echo "</ul></li>";
}

@endphp