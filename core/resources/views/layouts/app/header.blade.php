<link href="{{ url('plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('plugins/bootstrapv3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{ url('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{ url('plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{ url('plugins/nvd3/nv.d3.min.css') }}" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{ url('plugins/mapplic/css/mapplic.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('plugins/rickshaw/rickshaw.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('plugins/dragula/dragula.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{ url('plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{ url('plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ url('plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ url('plugins/jquery-datatable/extensions/Buttons/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet"/>
<link href="{{ url('plugins/jquery-datatable/extensions/Buttons/css/buttons.bootstrap.css') }}" type="text/css" rel="stylesheet"/>
<link href="https://cdn.datatables.net/rowreorder/1.2.3/css/rowReorder.dataTables.min.css" media="screen" type="text/css" rel="stylesheet"/>
<link href="https://cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" media="screen" type="text/css" rel="stylesheet"/>
<link href="{{ url('plugins/jquery-metrojs/MetroJs.css') }}" rel="stylesheet" type="text/css" media="screen"/>
<link href="{{ url('css/pages-iconss.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('css/pages.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/jcrop/css/jquery.jcrop.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/css/style.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('plugins/jquery-datatable/extensions/FixedHeader/css/dataTables.fixedHeader.min.css') }}" type="text/css" rel="stylesheet"/>
<link class="main-stylesheet" href="{{ url('css/aldesign.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">

/* Apply these styles only when #preview-pane has
   been placed within the Jcrop widget */
.jcrop-holder #preview-pane {
  display: block;
  position: absolute;
  z-index: 2000;
  top: 10px;
  right: -280px;
  padding: 6px;
  border: 1px rgba(0,0,0,.4) solid;
  background-color: white;

  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;

  -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
}

/* The Javascript code will set the aspect ratio of the crop
   area based on the size of the thumbnail preview,
   specified here */
#preview-pane .preview-container {
  width: 250px;
  height: 170px;
  overflow: hidden;
}

</style>
<script src="{{asset('plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!--[if lte IE 9]>
    <link href="{{ url('plugins/codrops-dialogFx/dialog.ie.css') }}" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->

<script>
    window.onload = function()
    {
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    };

    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>
