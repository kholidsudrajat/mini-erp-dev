@extends('layouts.app.frame')
@section('title', 'Create New Pengeluaran Toko')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('expense_toko.new'))
@section('button', '<a href="'.url('/admin/expense_toko').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/expense_toko', 'id' => 'formValidate', 'files' => true, 'novalidate' => '-']) !!}

		@include ('admin.expense_toko.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');
});
</script>
@endpush
