@extends('layouts.app.frame')
@section('title', 'Edit Pengeluaran Toko')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('expense_toko.edit', $expense_toko))
@section('button', '<a href="'.url('/admin/expense_toko').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')


    {!! Form::model($expense_toko, [
            'method' => 'PATCH',
            'url' => ['/admin/expense_toko', $expense_toko->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}

        @include ('admin.expense_toko.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');

});
</script>
@endpush