@extends('layouts.app.frame')
@section('title', 'Petty Cash Per Tanggal ' . $pettycash->date)
@section('description', 'Details')
@section('breadcrumbs', Breadcrumbs::render('petty-cash.show', $pettycash))
@section('button', '<a href="'.url('/admin/petty-cash').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">summary</h3>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-condensed">
                <tr>
                    <th>Tanggal</th>
                    <td>{{ $pettycash->date }}</td>
                </tr>
                <tr>
                    <th>Pemasukkan</th>
                    <td>{{ number_format($pettycash->cost_in) }}</td>
                </tr>
                <tr>
                    <th>Pengeluaran</th>
                    <td>{{ number_format($pettycash->cost_out) }}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Detail</h3>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <th>Tanggal</th>
                    <th>Tipe</th>
                    <th>Biaya</th>
                    <th>Keterangan</th>
                    <th>File</th>
                </thead>
                <tbody>
                    @foreach($petty as $pet)
                    <tr>
                        <td>{{ $pet->date }}</td>
                        <td>
                            @if($pet->type == "in")
                            <span class="btn btn-xs btn-success">Pemasukkan</span>
                            @else
                            <span class="btn btn-xs btn-danger">Pengeluaran</span>
                            @endif
                        </td>
                        <td>{{ number_format($pet->cost) }}</td>
                        <td>{{ $pet->description }}</td>
                        <td>
                            @if($pet->file != "")
                            <a id="{{$pet->file}}" class="fancybox" rel="group" href="{{url('').'/files/petty-cash/'.$pet->file}}">
                                <img src="{{url('').'/files/petty-cash/'.$pet->file}}" style="max-width:200px;" alt="" />
                            </a>
                            @else
                            -
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@push('script')
<script type="text/javascript">
    $(document).ready(function() {
		$(".fancybox").each(function(){
            $(this).fancybox({
                'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                'speedIn': 600,
                'speedOut': 200,
            });
        });
	});
</script>
@endpush