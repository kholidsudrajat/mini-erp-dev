@extends('layouts.app.frame')
@section('title', 'Edit Keterangan')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('stock-adjustment.edit', $stockadjustment))
@section('button', '<a href="'.url('/admin/stock-adjustment').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')


    {!! Form::model($stockadjustment, [
            'method' => 'PATCH',
            'url' => ['/admin/stock-adjustment', $stockadjustment->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}

        @include ('admin.stock-adjustment.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$('#datepicker').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
    $(this).datepicker('hide');
});
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');

});
</script>
@endpush