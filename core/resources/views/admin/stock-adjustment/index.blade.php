@extends('layouts.app.frame')
@section('title', 'Penyesuaian Stok')
@section('description', 'Penyesuaian Stok')
@section('breadcrumbs', Breadcrumbs::render('stock-adjustment'))
@section('button', '<div class="clearfix"></div>')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form id="formValidate" method="post" action="{{ url('') }}/admin/stock-adjustment" enctype="multipart/form-data">
                <input hidden name="_token" value="{{csrf_token()}}" />
                <div class="col-md-4" style="padding:0px;">
                    <div class="form-group form-group-default required">
                        <label>Tanggal</label>
                        <input class="form-control" type="text" id="datepicker" name="date" placeholder="Pilih Tanggal" required />
                    </div>
                </div>
                <div class="col-md-4" style="padding:0px;">
                    <div class="form-group form-group-default form-group-default-select2 required">
                        <label>Toko/Gudang</label>
                        <select class="full-width" data-init-plugin="select2" name="shop_id" id="shops">
                            <option value="-">Pilih Toko</option>
                            @foreach($shop as $sh)
                            <option value="{{ $sh->id }}">{{ $sh->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4" style="padding:0px;">
                    <div class="form-group form-group-default form-group-default-select2 required">
                        <label>Produk</label>
                        <select class="full-width" data-init-plugin="select2" name="model_id" id="models">
                            <option value="-">Pilih Produk</option>
                            @foreach($model as $mo)
                            <option value="{{ $mo->id }}">{{ $mo->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-2" style="padding:0px;">
                    <div class="form-group form-group-default">
                        <label>QTY TOTAL</label>
                        <input style="color:black; font-weight:600; opacity:0.7;" type="number" class="form-control" readonly id="qty_total" />
                    </div>
                </div>
                <div class="col-md-2" style="padding:0px;">
                    <div class="form-group form-group-default">
                        <label id="lbl_qty">QTY</label>
                        <input style="color:black; font-weight:600; opacity:0.7;" type="number" class="form-control" readonly id="qtys" name="qtys" />
                    </div>
                </div>
                <div class="col-md-2" style="padding:0px;">
                    <div class="form-group form-group-default required">
                        <label>Penyesuaian</label>
                        <input type="number" class="form-control" name="qty" min="1" value="1" required />
                    </div>
                </div>
                <div class="col-md-6" style="padding:0px;">
                    <div class="form-group form-group-default">
                        <label>Keterangan</label>
                        <input type="text" class="form-control" name="note" placeholder="Keterangan" />
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
    <input type="hidden" id="drs" name="drange"/>
    <input type="hidden" id="did" name="did"/>
    <div class="form-group-attached">
        <div class="row clearfix">
            <div class="col-sm-6 col-xs-12">
                <div class="form-group form-group-default">
                    <label>Pencarian</label>
                    <form id="formsearch">
                        <input type="text" id="search-table" class="form-control" name="firstName" placeholder="Masukkan Keyword">
                    </form>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group form-group-default">
                    <label>Tanggal Mulai</label>
                    <input type="text" id="datepicker-start" class="form-control" name="firstName" placeholder="pilih tanggal">
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group form-group-default">
                    <label>Tanggal Berakhir</label>
                    <input type="text" id="datepicker-end" class="form-control" name="firstName" placeholder="pilih tanggal">
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <table class="table table-hover" id="stock-adjustment-table">
        <thead>
            <tr>
                <th> Tanggal </th><th> Toko/Gudang </th><th> Produk </th><th> QTY </th><th> Catatan </th><th width="20%"> Tindakan </th>
            </tr>
        </thead>
    </table>

@endsection


@push("script")
<script>
    var oTable;
    oTable = $('#stock-adjustment-table').DataTable({
        processing: true,
        serverSide: true,
        dom: 'lBfrtip',
        order:  [[ 0, "desc" ]],
        buttons: [
            {
                extend: 'print',
                autoPrint: true,
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'padding', '2px' )
                        .prepend(
                            '<font style="font-size:14px;margin-top: 5px;margin-bottom:20px;"> Laporan Uang Kas </font><br/><br/><font style="font-size:8px;margin-top:15px;">{{date('Y-m-d h:i:s')}}</font></h5><br/><br/>'
                        );


                    $(win.document.body).find( 'div' )
                        .css( {'padding': '2px', 'text-align': 'center', 'margin-top': '-50px'} )
                        .prepend(
                            ''
                        );

                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( { 'font-size': '9px', 'padding': '2px' } );


                },
                title: '',
                orientation: 'landscape',
                exportOptions: {columns: ':visible'} ,
                text: '<i class="fa fa-print" data-toggle="tooltip" title="" data-original-title="Print"></i>'
            },
            {extend: 'colvis', text: '<i class="fa fa-eye" data-toggle="tooltip" title="" data-original-title="Column visible"></i>'},
            {extend: 'csv', text: '<span>CSV</span>'}
        ],
        sDom: "<'table-responsive fixed't><'row'<p i>> B",
        sPaginationType: "bootstrap",
        destroy: true,
        responsive: true,
        scrollCollapse: true,
        oLanguage: {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Menampilkan <b>_START_ to _END_</b> dari _TOTAL_ entri"
        },
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        ajax: {
        url: 
            '{!! route('stock-adjustment.data') !!}'
        ,
            data: function (d) {
                d.range = $('input[name=drange]').val();
            }
        },
        columns: [
            { data: "date", name: "date" },
            { data: "shop_name", name: "shops.name" },
            { data: "model_name", name: "models.name" },
            { data: "qty", name: "qty" },
            { data: "note", name: "note" },
            { data: "action", name: "action", orderable: false , searchable: false , printable : false},
        ],
    }).on( 'processing.dt', function ( e, settings, processing ) {if(processing){Pace.start();} else {Pace.stop();}});

    $("#stock-adjustment-table_wrapper > .dt-buttons").appendTo("div.export-options-container");


    $('#datepicker-start').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
        $(this).datepicker('hide');
        if($('#datepicker-end').val() != ""){
            $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
            oTable.draw();
        }else{
            $('#datepicker-end').focus();
        }

    });
    $('#datepicker-end').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
        $(this).datepicker('hide');
        if($('#datepicker-start').val() != ""){
            $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
            oTable.draw();
        }else{
            $('#datepicker-start').focus();
        }

    });
    $('#datepicker').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });

    $('#formsearch').submit(function () {
        oTable.search( $('#search-table').val() ).draw();
        return false;
    } );

    oTable.page.len(25).draw();



    function deleteData(id) {
        $('#modalDelete').modal('show');
        $('#did').val(id);
    }

    function hapus(){
        $('#modalDelete').modal('hide');
        var id = $('#did').val();
        $.ajax({
            url: '{{url("admin/stock-adjustment")}}' + "/" + id + '?' + $.param({"_token" : '{{ csrf_token() }}' }),
            type: 'DELETE',
            complete: function(data) {
                oTable.draw();
            }
        });
    }
    
    $('#models').add('#shops').on('change', function(){
        var model = $('#models :selected').val();
        var shop = $('#shops :selected').val();
        
        if(model != '-' && shop != '-'){
            $.ajax({
                type    : 'GET',
                url     : '{{ url("admin/stock-adjustment") }}/getstock',
                data    : {
                    model   : model,
                    shop    : shop
                },
                success : function(res){
                    $('#qty_total').val('0');
                    $('#qtys').val('0');
                    Pace.restart();
                    var allstock = res.allstock;
                    var stocks = res.stocks;
                    var model = res.model;
                    var shop = res.shop;
                    var qtys = parseFloat(allstock['qty']);
                    
                    if(stocks != null)
                        var qty = parseFloat(stocks['qty']);
                    else
                        var qty = 0;
                    
                    if(allstock['qty'] != null)
                        var qtys = parseFloat(allstock['qty']);
                    else
                        var qtys = 0;
                    
                    $('#qty_total').val(qtys);
                    $('#lbl_qty').html('QTY DI '+shop.name);
                    $('#qtys').val(qty);
                }
            })
        }
    })
    
</script>

@endpush
