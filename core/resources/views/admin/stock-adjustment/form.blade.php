<div aria-required="true" class="form-group required form-group-default {{ $errors->has('date') ? 'has-error' : ''}}">
    {!! Form::label('date', 'Tanggal') !!}
    {!! Form::text('date', null, ['class' => 'form-control', 'required' => '', 'id' => 'datepicker']) !!}
</div>
{!! $errors->first('date', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group required form-group-default {{ $errors->has('note') ? 'has-error' : ''}}">
    {!! Form::label('note', 'Keterangan') !!}
    {!! Form::textarea('note', null, ['class' => 'form-control', 'required' => '']) !!}
</div>
{!! $errors->first('note', '<label class="error">:message</label>') !!}

{{--<div class="pull-left">--}}
    {{--<div class="checkbox check-success">--}}
        {{--<input id="checkbox-agree" type="checkbox" required> <label for="checkbox-agree">Saya sudah mengecek data sebelum menyimpan</label>--}}
    {{--</div>--}}
{{--</div>--}}
<br/>

{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Save', ['type' => 'submit', 'class' => 'btn btn-success']) !!}
<button class="btn btn-default" type="reset"><i class="pg-close"></i> Clear</button>