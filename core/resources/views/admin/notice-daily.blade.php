@extends('layouts.app.frame')
@section('title', 'Notice')
@section('breadcrumbs', Breadcrumbs::render('notice-daily'))
@section('button', '<div class="clearfix"></div>')
@section('content')
<ul class="nav nav-tabs nav-justified">
  <li class="{{ $current == 1 ? 'active' : '' }}"><a onclick="totab(1)" data-toggle="tab" href="#tab_1">Omzet</a></li>
  <li class="{{ $current == 2 ? 'active' : '' }}"><a onclick="totab(2)" data-toggle="tab" href="#tab_2">Best Seller</a></li>
  <li class="{{ $current == 3 ? 'active' : '' }}"i><a onclick="totab(3)" data-toggle="tab" href="#tab_3">Informasi Stok</a></li>
</ul>
<div class="tab-content">
    <div id="tab_1" class="tab-pane fade {{ $current == 1 ? 'in active' : '' }}">
        <div class="panel panel-default" >
            <div class="panel-body">
                <div class="form-group-attached" style="text-align: center">
                    <div style="border: 1px solid rgba(0,0,0,.07);" class="form-group form-group-default col-md-12">
                        <label>Omzet</label>
                        <b>{{ str_replace(',', '.', number_format($omzet)) }}</b>
                    </div>
                    <div style="border: 1px solid rgba(0,0,0,.07);" class="form-group form-group-default col-md-6">
                        <label>Cash</label>
                        <b>{{ str_replace(',', '.', number_format($cash)) }}</b>
                    </div>
                    <div style="border: 1px solid rgba(0,0,0,.07);" class="form-group form-group-default col-md-6">
                        <label>Piutang</label>
                        <b>{{ str_replace(',', '.', number_format($debt * -1)) }}</b>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="tab_2" class="tab-pane fade {{ $current == 2 ? 'in active' : '' }}">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Product's Best Seller</h3>
            </div>
            <div class="panel-body">
                <div class="col-md-3 pull-right">
                    <div class="form-group">
                        <label>Jumlah Produk yang ditampilkan : </label>
                        <input id="prod" value="{{ $prod }}" type="number" class="form-control" />
                    </div>
                    <a onclick="config('product_best_seller')" class="btn btn-primary"><i class="fa fa-check"></i></a>
                </div>
                <div class="clearfix"></div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <th width="5%">#</th>
                            <th>Produk</th>
                            <th>Terjual</th>
                            <th>Harga</th>
                            <th>Total</th>
                        </thead>
                        <tbody id="tbs">
                            @php $i = 1; @endphp
                            @foreach($product as $prod)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $prod->names }}</td>
                                <td>{{ $prod->qty }}</td>
                                <td>{{ str_replace(',', '.', number_format($prod->harga / $prod->qty)) }}</td>
                                <td>{{ str_replace(',', '.', number_format($prod->harga)) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="tab_3" class="tab-pane fade {{ $current == 3 ? 'in active' : '' }}">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Low Stock</h3>
            </div>
            <div class="panel-body">
                <div class="col-md-3 pull-right">
                    <div class="form-group">
                        <label>Tampilkan stok yang kurang dari : </label>
                        <input id="min" value="{{ $min }}" type="number" class="form-control" />
                    </div>
                    <a onclick="config('low_stock')" class="btn btn-primary"><i class="fa fa-check"></i></a>
                </div>
                <div class="clearfix"></div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <th width="5%">#</th>
                            <th>Produk</th>
                            <th>Toko / Gudang</th>
                            <th>Stok</th>
                        </thead>
                        <tbody id="tbs">
                            @php $o = 1; @endphp
                            @foreach($low as $lo)
                            <tr>
                                <td>{{ $o++ }}</td>
                                <td>{{ $lo->names }}</td>
                                <td>{{ $lo->shop_name }}</td>
                                <td>{{ $lo->qty }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">High Stock</h3>
            </div>
            <div class="panel-body">
                <div class="col-md-3 pull-right">
                    <div class="form-group">
                        <label>Tampilkan stok yang lebih dari : </label>
                        <input id="max" value="{{ $max }}" type="number" class="form-control" />
                    </div>
                    <a onclick="config('high_stock')" class="btn btn-primary"><i class="fa fa-check"></i></a>
                </div>
                <div class="clearfix"></div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <th width="5%">#</th>
                            <th>Produk</th>
                            <th>Toko / Gudang</th>
                            <th>Stok</th>
                        </thead>
                        <tbody id="tbs">
                            @php $u = 1; @endphp
                            @foreach($high as $hig)
                            <tr>
                                <td>{{ $u++ }}</td>
                                <td>{{ $hig->names }}</td>
                                <td>{{ $hig->shop_name }}</td>
                                <td>{{ $hig->qty }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('p').remove(); 
        console.log('{{ $current }}');
    });
    
    function totab(id){
        $.ajax({
            url     : '{{ url("") }}/admin/notice-daily',
            type    : 'GET',
            data    : {
                current     : id
            }
        })
    }
    
    function config(type){
        if(type == 'low_stock')
            var value = $('#min').val();
        else if(type == 'high_stock')
            var value = $('#max').val();
        else if(type == 'product_best_seller')
            var value = $('#prod').val();
        
        $.ajax({
            url     : '{{ url("") }}/admin/config',
            type    : 'GET',
            data    : {
                type    : type,
                value   : value
            },
            complete    : function(){
                location.reload();
            }
        })
    }
</script>
@endpush