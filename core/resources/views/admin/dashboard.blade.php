@extends('layouts.app.frame')
@section('title', 'Dashboard')
@section('content')
<div class="container-fluid container-fixed-lg">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-md-6">
                <label>Top 10 Pelanggan</label>
                <div id="customer_charts" style="width: 100%; height: 400px; float:left;"></div>
            </div>
            <div class="col-md-6">
                <label>Top 10 Produk</label>
                <div id="product_charts" style="width: 100%; height: 400px; float:left;"></div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <label>Bagan Penjualan</label>
                <div class="clearfix"></div>
                <div class="form-group col-sm-3">
                    <br/>
                    {!! Form::select('months', $years, null, ['class' => 'form-control', 'data-init-plugin' => 'select2', 'id' => 'month_sales']) !!}
                </div>
                <div id="sales_charts" style="width: 100%; height: 400px; float:left;"></div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script type="text/javascript">
//////////////////////////
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(customer);

function customer() {

var data = google.visualization.arrayToDataTable([
['Customer', 'Sales'],
    @foreach($customer as $cus)
    ['{{ $cus->nama }}', {{ $cus->sales }}],
    @endforeach
]);

var chart = new google.visualization.PieChart(document.getElementById('customer_charts'));

chart.draw(data);
}
    
//////////////////////////
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(product);

function product() {

var data = google.visualization.arrayToDataTable([
['Product', 'Total'],
    @foreach($product as $pro)
    ['{{ $pro->names }}', {{ $pro->qty }}],
    @endforeach
]);

var chart = new google.visualization.PieChart(document.getElementById('product_charts'));

chart.draw(data);
}
    
//////////////////////////
var bulan ={
    1  : "Januari",
    2  : "Februari",
    3  : "Maret",
    4  : "April",
    5  : "Mei",
    6  : "Juni",
    7  : "Juli",
    8  : "Agustus",
    9  : "September",
    10 : "Oktober",
    11 : "November",
    12 : "Desember",
}
google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
    
var data = google.visualization.arrayToDataTable([
    ["Bulan", "Total"],
    @for($i=1; $i<=12; $i++)
        [bulan[{{$i}}], {{ $sales[$i]->totals > 0 ? $sales[$i]->totals : 0 }}],
    @endfor
]);
   
var option = {
    legend: {position: 'none'},
}
    
var chart = new google.visualization.ColumnChart(document.getElementById("sales_charts"));
chart.draw(data, option);
}
    
////////////////////
$('#month_sales').on('change', function(){
    var mo = $('#month_sales :selected').val();
    
    $.ajax({
        url : '{{ url("") }}/admin/changemonthsales/'+mo,
        type : 'GET',
        success : function(data){
            var datas = google.visualization.arrayToDataTable([
                ["Bulan", "Total"],
                ["Januari", parseInt(data[1].totals)],
                ["Februari", parseInt(data[2].totals)],
                ["Maret", parseInt(data[3].totals)],
                ["April", parseInt(data[4].totals)],
                ["Mei", parseInt(data[5].totals)],
                ["Juni", parseInt(data[6].totals)],
                ["Juli", parseInt(data[7].totals)],
                ["Agustus", parseInt(data[8].totals)],
                ["September", parseInt(data[9].totals)],
                ["Oktober", parseInt(data[10].totals)],
                ["November", parseInt(data[11].totals)],
                ["Desember", parseInt(data[12].totals)],
            ]);
            
            var option = {
                legend: {position: 'none'},
            }

            var chart = new google.visualization.ColumnChart(document.getElementById("sales_charts"));
            chart.draw(datas, option);
        }
    });
});
</script>
@endpush