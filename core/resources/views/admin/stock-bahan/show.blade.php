@extends('layouts.app.frame')
@section('title', 'Detail Stok Bahan')
@section('description', 'Detail Stok Bahan')
@section('breadcrumbs', Breadcrumbs::render('stock-bahan.show', $stock))
@section('button', '<a href="'.url('/admin/stock-bahan').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Detail Stok Bahan</h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
				<table class="table table-condensed">
					<tbody>
						<tr>
							<th>Supplier</th>
							<td>{{ $stock->supplier_name }}</td>
						</tr>
						<tr>
							<th>Bahan</th>
							<td>{{ $stock->bahan_name }}</td>
						</tr>
						<tr>
							<th>Motif</th>
							<td>{{ $stock->motif_name }}</td>
						</tr>
						<tr>
							<th>Roll</th>
							<td>{{ $stock->roll }}</td>
						</tr>
						<tr>
							<th>QTY Kg</th>
							<td>{{ $stock->qty }}</td>
						</tr>
						<tr>
							<th>Total</th>
							<td>{{ str_replace(',', '.', number_format($stock->total)) }}</td>
						</tr>
					</tbody>
				</table>
			</div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">List Stok</h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
				<table class="table table-bordered">
                    <thead>
                        <th width="5%">#</th>
                        <th>Warna</th>
                        <th>Roll</th>
                        <th>Kg</th>
                        <th>Total</th>
                        <th>Detail</th>
                    </thead>
                    <tbody>
                    @php $i = 1; @endphp
                        @foreach($detail as $dtl)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $dtl->name }}</td>
                            <td>{{ $dtl->roll }}</td>
                            <td>{{ $dtl->kg }}</td>
                            <td>{{ str_replace(',', '.', number_format($dtl->total)) }}</td>
                            <td><a href="{{ url('') }}/admin/stock-bahan/{{ $stock->id.'/'.$dtl->warna_id }}" class="btn btn-primary">Detail</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('script')
<script type="text/javascript">
</script>
@endpush