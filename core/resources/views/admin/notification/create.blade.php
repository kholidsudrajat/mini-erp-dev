@extends('layouts.app.frame')
@section('title', 'Create New Warehouse')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('warehouse.new'))
@section('button', '<a href="'.url('/admin/warehouse').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/warehouse', 'id' => 'formValidate', 'files' => true]) !!}

		@include ('admin.warehouse.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();

});
</script>
@endpush
