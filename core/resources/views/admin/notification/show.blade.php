@extends('layouts.app.frame')
@section('title', 'Notification #' . $notification->id)
@section('description', 'Notification Details')
@section('breadcrumbs', Breadcrumbs::render('notification.show', $notification))
@section('button', '<a href="'.url('/admin/notification').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
	<div class="panel panel-default">
		<div class="panel-heading">
            <h3 class="panel-title">{{ $notification->title }}</h3>
        </div>
		<div class="panel-body">
            @foreach($stock as $st)
                <hr/> Stok Barang : {{$st->model_name}} di {{$st->shop_name}} tersisa {{$st->qty}}.
            @endforeach
		</div>
	</div>
@endsection
@push('script')
<script type="text/javascript">
</script>
@endpush