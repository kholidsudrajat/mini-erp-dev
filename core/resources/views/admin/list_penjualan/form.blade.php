<div aria-required="true" class="form-group required form-group-default form-group-default-select2 {{ $errors->has('model_id') ? 'has-error' : ''}}">
    {!! Form::label('model_id', 'Model') !!}
    {!! Form::select('model_id', $model, null, ['class' => 'full-width', 'required' => '', 'data-init-plugin' => 'select2']) !!}
</div>
{!! $errors->first('model_id', '<label class="error">:message</label>') !!}

@if(isset($shop))
<div aria-required="true" class="form-group required form-group-default form-group-default-select2 {{ $errors->has('shop_id') ? 'has-error' : ''}}">
    {!! Form::label('shop_id', 'Kirim ke Toko / Gudang') !!}
    {!! Form::select('shop_id', $shop, null, ['class' => 'full-width', 'data-init-plugin' => 'select2', 'required']) !!}
</div>
{!! $errors->first('shop_id', '<label class="error">:message</label>') !!}
@endif

<div aria-required="true" class="form-group required form-group-default {{ $errors->has('qty') ? 'has-error' : ''}}">
    {!! Form::label('qty', 'QTY') !!}
    {!! Form::number('qty', null, ['class' => 'form-control', 'required']) !!}
</div>
{!! $errors->first('qty', '<label class="error">:message</label>') !!}

{{--<div class="pull-left">--}}
    {{--<div class="checkbox check-success">--}}
        {{--<input id="checkbox-agree" type="checkbox" required> <label for="checkbox-agree">Saya sudah mengecek data sebelum menyimpan</label>--}}
    {{--</div>--}}
{{--</div>--}}
<br/>

{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Save', ['type' => 'submit', 'class' => 'btn btn-success']) !!}
<button class="btn btn-default" type="reset"><i class="pg-close"></i> Clear</button>
