@extends('layouts.app.frame')
@section('title', 'Create New Shop')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('shop.new'))
@section('button', '<a href="'.url('/admin/shop').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/shop', 'id' => 'formValidate', 'files' => true]) !!}

		@include ('admin.shop.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();

});
</script>
@endpush
