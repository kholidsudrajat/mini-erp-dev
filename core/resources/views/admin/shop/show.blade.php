@extends('layouts.app.frame')
@section('title', 'Supplier #' . $supplier->id)
@section('description', 'Supplier Details')
@section('breadcrumbs', Breadcrumbs::render('supplier.show', $supplier))
@section('button', '<a href="'.url('/root/supplier').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
	<div class="panel panel-default">
		<div class="panel-body">
			<a href="{{ url('root/supplier/' . $supplier->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Supplier"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
			{!! Form::open([
				'method'=>'DELETE',
				'url' => ['root/supplier', $supplier->id],
				'style' => 'display:inline'
			]) !!}
				{!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
						'type' => 'submit',
						'class' => 'btn btn-danger btn-xs',
						'title' => 'Delete Supplier',
						'onclick'=>'return confirm("Confirm delete?")'
				))!!}
			{!! Form::close() !!}
			<br/>
			<br/>

			<div class="table-responsive">
				<table class="table table-condensed">
					<tbody>
						<tr>
							<th>ID</th>
							<td>{{ $supplier->id }}</td>
						</tr>
                        <tr>
                            <th>Nama</th>
                            <th>{{ $supplier->nama }}</th>
                        </tr>
                        <tr>
                            <th>No Telp</th>
                            <th>{{ $supplier->no_telp }}</th>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <th>{{ $supplier->email }}</th>
                        </tr>
                        <tr>
                            <th>PIC</th>
                            <th>{{ $supplier->pic }}</th>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <th>{{ $supplier->alamat }}</th>
                        </tr>
                        <tr>
                            <th>Keterangan</th>
                            <th>{{ $supplier->keterangan }}</th>
                        </tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
    
    <div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">List Bahan</h3>
		</div>
        <div class="panel-body table-responsive">
            <table class="table table-condensed">
                <thead>
                    <th width="10%">#</th>
                    <th>Nama</th>
                </thead>
                <tbody>
                    @php $i = 1; @endphp
                    @foreach($bahan as $bhn)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $bhn['name'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
    <div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">List Model</h3>
		</div>
        <div class="panel-body table-responsive">
            <table class="table table-condensed">
                <thead>
                    <th width="10%">#</th>
                    <th>Nama</th>
                </thead>
                <tbody>
                    @php $i = 1; @endphp
                    @foreach($model as $mdl)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $mdl['name'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('script')
<script type="text/javascript">
    function deleteData(idnya){
        var conf = confirm("Delete Data?");
        if(conf){
            $.ajax({
                type: "GET",
                url: "{{url('').'/admin/deletedata/'}}"+idnya+"/delete",
                success: function(){
                    location.reload();  
                }
            });
        }
    }
</script>
@endpush