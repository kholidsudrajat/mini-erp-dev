@extends('layouts.app.frame')
@section('title', 'Model Detail')
@section('description', '')
@section('breadcrumbs', Breadcrumbs::render('model-detail',$model))
@section('button', '')

@section('content')
    <div class="form-group col-md-12">
        <div class="row">
            <div class="form-group-default col-md-4">
                <img src="{{ url('files/models').'/'.$model['image'] }}" style="width:100%; margin-bottom:10px;" />
            </div>
            <div class="form-group-default col-md-8">
                <label>Nama Barang</label>
                {{ $model['name'] }}
                <label>Nama Model</label>
                {{ $model['model_name'] }}
                <label>Harga</label>
                IDR {{ number_format($model['price']) }}.00
                
                @foreach($shop as $shops)
                    @if($qty[$shops['id']] > 0)
                        <label>Stok di {{ $shops['name'] }}</label>
                        {{ $qty[$shops['id']] }}
                    @endif
                @endforeach
                
                <div class="clearfix"></div>
                <br/>
                <form action="{{ url('admin/cashier/addqty') }}" method="get" id="myForm">
                    <input name="qty" type="number" value="1" />
                    <input hidden name="model_id" value="{{ $model['id'] }}" />
                    <input hidden name="_token" value="{{ csrf_token() }}" />
                    <button onclick="closeME();" class="btn btn-sm btn-primary">Tambah ke keranjang</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('p').remove();
    });
    
    function closeME() {
        event.preventDefault();
        parent.$.fancybox.close();
        $('#myForm').submit();
    }
</script>
@endpush