@extends('layouts.app.frame')
@section('title', 'Pilih Metode Pengiriman')
@section('description', '')
@section('breadcrumbs', Breadcrumbs::render('cashier'))
@section('button', '<div class="clearfix"></div>')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group col-md-12">
            <div class="radio radio-success">
                <input type="radio" checked value="pending" name="method" id="pending">
                <label for="pending">Pending</label>
                <input type="radio" value="pickup" name="method" id="pickup">
                <label for="pickup">Pick-Up</label>
                <input type="radio" value="kirim" name="method" id="ship">
                <label for="ship">Kirim</label>
            </div>
        </div>
        <br/>
        <div id="shipm" style="display:none">
            <hr/>
            <div class="form-group col-md-6">
                <label>Pilih Alamat Pengiriman</label>
                <select class="full-width" data-init-plugin="select2" id="address">
                    @foreach($address as $add)
                        <option value="{{ $add->id }}" {{ $addr->id == $add->id ? 'selected' : '' }}>{{ $add->alamat }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-6">
                <label>Pilih Ekspedisi</label>
                <select class="full-width" data-init-plugin="select2" id="shipping">
                    @foreach($shipping as $shipp)
                        <option value="{{ $shipp->id }}" {{ $ship->id == $shipp->id ? 'selected' : '' }}>{{ $shipp->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <a class="btn btn-primary pull-right" onclick="return next()" href="javascript:;">Lanjut</a>
</div>
@endsection
@push('script')
<script type="text/javascript">
    $(document).ready(function(){
        $(".container-fluid.relative").remove();
        $('p').remove();
    });
    
    $('#address').on('change', function(){
        var address = $('#address :selected').val();
        var data = {
            _token : '{{csrf_token()}}',
            address : address
        }
        
        $.ajax({
            type : 'POST',
            url : '{{ url("") }}/admin/cashier/changeaddress',
            data : data,
            success : function(res){
                Pace.restart();
                $('#shipping').select2('val',res);
            }
        })
    });
    
    $('input[name=method]').on('change', function(){
        Pace.restart();
        if($('#ship').is(':checked'))
            $('#shipm').show();
        else
            $('#shipm').hide();
    });
    
    function next(){
        var noref = '{{ $noref }}';
        var status = 'pending';
        var address_id = '';
        var shipping_id = '';
        
        if($('#ship').is(':checked')){
            status = 'dikirim';
            address_id = $('#address :selected').val();
            shipping_id = $('#shipping :selected').val();
        }else if($('#pickup').is(':checked'))
            status = 'pickup';
        
        var datanya = {
            _token : '{{ csrf_token() }}',
            no_ref : noref,
            status : status,
            address_id : address_id,
            shipping_id : shipping_id
        };
        
        $.ajax({
            type : 'POST',
            url : '{{ url("") }}/admin/cashier/saveshipping',
            data : datanya,
            success : function(){
                window.location.href = '{{ url("") }}/admin/cashier/print/'+noref;
            }
        });
    }
</script>
@endpush