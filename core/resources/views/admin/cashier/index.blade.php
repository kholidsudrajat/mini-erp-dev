@extends('layouts.app.frame')
@section('title', 'Penjualan')
@section('description', '')
@section('breadcrumbs', Breadcrumbs::render('cashier'))
@section('button', '')
@section('content')

<div class="se-pre-con"></div>
<ul class="nav nav-tabs nav-justified">
  <li class="{{ $current == 1 ? 'active' : '' }}"><a onclick="return changetab(1)" data-toggle="tab" href="#tab_1">Tab 1</a></li>
  <li class="{{ $current == 2 ? 'active' : '' }}"><a onclick="return changetab(2)" data-toggle="tab" href="#tab_2">Tab 2</a></li>
  <li class="{{ $current == 3 ? 'active' : '' }}"><a onclick="return changetab(3)" data-toggle="tab" href="#tab_3">Tab 3</a></li>
</ul>
<div class="tab-content row">
    @for($u = 1; $u <= 3; $u++)
    <div id="tab_{{ $u }}" class="tab-pane fade {{ $u == $current ? 'in active' : '' }}">
        <div class="form-group col-md-12">
            <div class="row clearfix">
                <div class="col-md-4">
                    <div class="form-group form-group-default">
                        <label>Input Nomor Nota</label>
                        <input type="text" class="form-control" value="0{{ $u.date('YmdHis') }}" id="no_ref_{{ $u }}" placeholder="Masukkan Nomor Nota" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-group-default">
                        <label>Pilih Toko</label> 
                        {!! Form::select('shops_id', $shop, $idshop[$u], ['class' => 'full-width', 'data-init-plugin' => 'select2', 'id' => 'shops'.$u, 'onchange' => 'return changeshop('.$u.')']) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group form-group-default">
                        <label>Cari Berdasarkan Nama Barang / Nama Model</label>
                        <form id="formsearch">
                            <input type="text" id="search-box{{ $u }}" class="form-control" name="keyword" placeholder="pencarian">
                        </form>
                    </div>
                </div>
            </div>
            <div class="row" id="list_models{{$u}}">
                @foreach($model[$u] as $models)
                <div class="form-group col-lg-2 col-md-3 col-sm-4 col-xs-4" id="mods{{ $u }}_{{ $models['id'] }}">
                    <div class="form-group-default">
                        <div class="displaytable">
                            <div class="displaytablecell">
                                <label class="title-prod">{{ $models['name'] }}</label>
                            </div>
                        </div>
                        <a class="col-md-12 gmb" style="cursor:pointer;" id="popup" href="{{ url('').'/admin/model_detail/'.$models['id'] }}">
                            <div class="frame-image">
                                <img src="{{ url('files/models').'/'.$models['image'] }}" />
                                <!--img src="{{ url('files/models').'/'.$models['image'] }}" style="max-height:200px; margin-bottom:10px;" /-->
                                <div class="text-overlay">
                                    <span><i class="font-10">Rp {{ str_replace(',','.',number_format($models['price'])) }}</i></span>
                                    <span class="font-10">
                                        ({{ $models['qty'] }} / {{ $allqty[$models['id']] }} pcs)
                                    </span>
                                </div>
                            </div>
                        </a>
                        <span class="font-10">{{ $models['model_name'] }}</span>
                        <br/>
                        <div class="clearfix" style="padding-bottom: 3px;"></div>
                        <input name="qty" type="number" value="1" style="width:70%" id="qty{{$models['id'].'_'.$u}}" />
                        <input style="display:none;" id="stk{{$models['id']}}" value="{{ $models['qty'] }}" />
                        <input style="display:none;" id="stocks{{$models['id']}}" value="{{ $models['qty'] }}" />
                        <a onclick="addtocart({{$u.','.$models['id'].','.$models['price']}})" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
            
            <!--div class="row" id="list_models{{$u}}">
                @foreach($model[$u] as $models)
                <div class="form-group col-md-4">
                    <div class="form-group-default">
                        <a class="col-md-12 gmb" style="cursor:pointer;" id="popup" href="{{ url('').'/admin/model_detail/'.$models['id'] }}">
                            <img src="{{ url('files/models').'/'.$models['image'] }}" style="max-height:200px; margin-bottom:10px;" />
                        </a>
                        <label>{{ $models['name'] }}</label>
                        <span>{{ $models['model_name'] }}</span>
                        <br/>
                        <i>IDR {{ str_replace(',','.',number_format($models['price'])) }}</i>
                        <br/>
                        <div class="pull-right">
                            ({{ $models['qty'] }} Pcs)
                        </div>
                        <div class="clearfix"></div>
                        <br/>
                        <div>
                            <input name="qty" type="number" value="1" style="width:70%" id="qty{{$models['id'].'_'.$u}}" />
                            <input style="display:none;" id="stk{{$models['id']}}" value="{{ $models['qty'] }}" />
                            <input style="display:none;" id="stocks{{$models['id']}}" value="{{ $models['qty'] }}" />
                            <a onclick="addtocart({{$u.','.$models['id'].','.$models['price']}})" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div-->
            
            <div class="row" id="list_model{{$u}}"></div>
            <div class="col-md-12" style="text-align:center">
                <ul class="pagination" id="paginations{{$u}}">
                    @for($a=1; $a<=$pagin[$u]; $a++) 
                        <li><a href="javascript:;" onclick="return toPage({{$u.','.$a}})">{{ $a }}</a></li>
                    @endfor
                </ul>
                <input hidden id="pages{{$u}}" value="1" />
            </div>
        </div>
        <!--div class="col-md-4 hidden-sm hidden-xs">
            <form id="formbayar">

                <div class="row clearfix">
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group form-group-default form-group-default-select2 required">
                            <label>Masukkan Nama Pelanggan</label> {!! Form::select('customer_id', $cus, 0, ['id' => 'cus'.$u, 'class' => 'full-width', 'data-init-plugin' => 'select2', 'required' => '']) !!}
                        </div>
                    </div>
                </div>
                <b style="cursor:pointer" data-toggle="collapse" data-target="#formcus{{ $u }}" class="btn btn-success">Pelanggan Baru</b>
                <div class="row clearfix collapse" id="formcus{{ $u }}">
                    <br>
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group form-group-default">
                            <label>Nama Pelanggan</label>
                            <input type="text" id="cus_name{{ $u }}" class="form-control" />
                        </div>
                        <div class="form-group form-group-default">
                            <label>Nomor Handphone</label>
                            <input type="number" id="cus_numb{{ $u }}" class="form-control" />
                        </div>
                        <div class="form-group form-group-default">
                            <label>Alamat</label>
                            <input type="text" id="cus_addr{{ $u }}" class="form-control" />
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-success" onclick="return addCus({{ $u }})"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="panel panel-default" id="ncu{{ $u }}" style="display:none;">
                    <div class="panel-body">
                        <b class="panel-title">Catatan : </b>
                        <div id="note_cus{{ $u }}"></div>
                    </div>
                </div>
                <div class="row clearfix" {{ Auth::user()->role != 3 ? 'style="display:none"' : '' }}>
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group form-group-default form-group-default-select2">
                            <label>Pilih Sales</label>
                            @if(Auth::user()->role == 3)
                                {!! Form::select('salesman_id', $sales, Auth::user()->id, ['id' => 'sales'.$u, 'class' => 'full-width', 'data-init-plugin' => 'select2']) !!}
                            @else
                                {!! Form::select('salesman_id', $sales, null, ['id' => 'sales'.$u, 'class' => 'full-width', 'data-init-plugin' => 'select2']) !!}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="cart">
                        <div class="col-md-12"></div>
                        @php $s = array(); @endphp 
                        @if($cart[$u] != "") 
                        @php $i = 0; $s = array(); @endphp 
                        @foreach($cart[$u] as $in) 
                        @php $s[$i] = $in->subtotal; @endphp
                        <div class="col-md-12">
                            <a onclick="return hapus({{$u.','.$i.','.$in->subtotal.','.$in->model_id}})" href="javascript:;"><i class="fa fa-times"></i></a>
                            <label>{{  $name[$in->model_id] }}</label>
                            <span>({{  $mname[$in->model_id] }})</span>
                            <br/>
                            <div class="pull-right">
                                <b>{{ $in->qty }} x IDR {{ str_replace(',','.',number_format($price[$in->model_id])) }}</b>
                            </div>
                        </div>
                        @php $i++ @endphp 
                        @endforeach
                        @endif
                        <br/>
                        <hr> 
                        <div class="col-md-12"></div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <b>Subtotal</b>
                    <div class="pull-right">
                        @if($subtotal > 0)
                        <b>IDR {{ str_replace(',','.',number_format($subtotal)) }}</b> 
                        @else
                        <b>IDR {{ str_replace(',','.',number_format(array_sum($s))) }}</b> 
                        @endif
                    </div>
                    <br/><br/>
                    <b>Total</b>
                    <div class="pull-right">
                        @if($total > 0)
                        <b>IDR {{ str_replace(',','.',number_format($total)) }}</b>
                        @else
                        <b>IDR {{ str_replace(',','.',number_format(array_sum($s))) }}</b> 
                        @endif
                    </div>
                </div>
                @if($total > 0 || array_sum($s) > 0)
                <div class="col-sm-12 ">
                    <hr/>
                    <a onclick="return bayar({{$u}})" class="btn btn-primary pull-right">Bayar</a>
                </div>
                @endif
            </form>
        </div-->
        
        <div class="col-sm-4">
            <div class="box">
                <div class="option"><div class="auto-midle"><span>Menu</span></div></div>
                <form id="formbayar">

                    <div class="row clearfix">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group form-group-default form-group-default-select2 required">
                                <label>Masukkan Nama Pelanggan</label> {!! Form::select('customer_id', $cus, 0, ['id' => 'cus'.$u, 'class' => 'full-width', 'data-init-plugin' => 'select2', 'required' => '']) !!}
                            </div>
                        </div>
                    </div>
                    <di class="col-md-12">
                        <b style="cursor:pointer;" data-toggle="collapse" data-target="#formcust{{ $u }}" class="btn btn-success">Pelanggan Baru</b>
                    </di>
                    <div class="row clearfix collapse" id="formcust{{ $u }}" style="padding: 10px 12px 0 12px;">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group form-group-default">
                                <label>Nama Pelanggan</label>
                                <input type="text" id="cus_names{{ $u }}" class="form-control" />
                            </div>
                            <div class="form-group form-group-default">
                                <label>Nomor Handphone</label>
                                <input type="number" id="cus_numbs{{ $u }}" class="form-control" />
                            </div>
                            <div class="form-group form-group-default">
                                <label>Alamat</label>
                                <input type="text" id="cus_addrs{{ $u }}" class="form-control" />
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-success" onclick="return addCust({{ $u }})"><i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="panel panel-default" id="ncu{{ $u }}" style="display:none;">
                        <div class="panel-body">
                            <b class="panel-title">Catatan : </b>
                            <div id="note_cus{{ $u }}"></div>
                        </div>
                    </div>
                    <div class="row clearfix" {{ Auth::user()->role != 3 ? 'style="display:none"' : '' }}>
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Pilih Sales</label>
                                @if(Auth::user()->role == 3)
                                    {!! Form::select('salesman_id', $sales, Auth::user()->id, ['id' => 'sales'.$u, 'class' => 'full-width', 'data-init-plugin' => 'select2']) !!}
                                @else
                                    {!! Form::select('salesman_id', $sales, null, ['id' => 'sales'.$u, 'class' => 'full-width', 'data-init-plugin' => 'select2']) !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="cart">
                            <div class="col-md-12"></div>
                            @php $s = array(); @endphp 
                            @if($cart[$u] != "") 
                            @php $i = 0; $s = array(); @endphp 
                            @foreach($cart[$u] as $in) 
                            @php $s[$i] = $in->subtotal; @endphp
                            <div class="col-md-12">
                                <a onclick="return hapus({{$u.','.$i.','.$in->subtotal.','.$in->model_id}})" href="javascript:;"><i class="fa fa-times"></i></a>
                                <label>{{  $name[$in->model_id] }}</label>
                                <span>({{  $mname[$in->model_id] }})</span>
                                <br/>
                                <div class="pull-right">
                                    <b>{{ $in->qty }} x Rp {{ str_replace(',','.',number_format($price[$in->model_id])) }}</b>
                                </div>
                            </div>
                            @php $i++ @endphp 
                            @endforeach
                            @endif
                            <br/>
                            <hr> 
                            <div class="col-md-12"></div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <b>Subtotal</b>
                        <div class="pull-right">
                            @if($subtotal > 0)
                            <b>Rp {{ str_replace(',','.',number_format($subtotal)) }}</b> 
                            @else
                            <b>Rp {{ str_replace(',','.',number_format(array_sum($s))) }}</b> 
                            @endif
                        </div>
                        <br/><br/>
                        <b>Total</b>
                        <div class="pull-right">
                            @if($total > 0)
                            <b>Rp {{ str_replace(',','.',number_format($total)) }}</b>
                            @else
                            <b>Rp {{ str_replace(',','.',number_format(array_sum($s))) }}</b> 
                            @endif
                        </div>
                    </div>
                    @if($total > 0 || array_sum($s) > 0)
                    <div class="col-sm-12 ">
                        <hr/>
                        <a onclick="return bayar({{$u}})" class="btn btn-primary pull-right">Bayar</a>
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
    @endfor
</div>
@endsection 
@push('script')
<script type="text/javascript">
    $(document).ready(function() {
        localStorage.setItem('sales', '0');
        localStorage.setItem('period', '0');
        
        $('p').remove();
        @if($pot == "1")
        $('#disco1').hide();
        $('#discount_nom').hide();
        $('#disc2').show();
        $('#disco2').show();
        @elseif($pot == "2")
        $('#discount_per').hide();
        $('#disco2').hide();
        $('#disc2').show();
        $('#disco1').show();
        @else
        $('#disc').hide();
        $('#disc2').hide();
        @endif
        
        $('#search-box{{ $current }}').val('{{ $key }}');
        
        @for($u = 1; $u <= 3; $u++)
            localStorage.setItem('cus{{$u}}',0);
            localStorage.setItem('sales{{$u}}',0);
            localStorage.setItem('noref{{$u}}',0);
            @foreach($model[$u] as $models)
                $('#stocks{{$models["id"]}}').val({{ $models['qty'] }})
            @endforeach
        @endfor
    });
    $(".option").on("click", function () {
        $(".box").toggleClass("open");
    });

    $(document).ready(function() {
        @for($u = 1; $u <= 3; $u++)
            var cus = localStorage.getItem('cus_{{$u}}');
            if(cus != null)
                $('#cus{{$u}}').select2().select2('val', cus);
        
            var sales = localStorage.getItem('sales_{{$u}}');
            if(sales != null)
                $('#sales{{$u}}').select2().select2('val', sales);
        
            localStorage.setItem('shop_id{{$u}}', $('#shops{{$u}}').val());
            $("#search-box{{$u}}").keyup(function() {
                if ($("#search-box{{$u}}").val().length > 2) {
                    $.ajax({
                        type: "POST",
                        url: "{{url('/')}}/admin/cashier/ajax_model",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "keyword": $(this).val(),
                            "tab": {{$u}}
                        },
                        success: function(data) {
                            $("#list_models{{$u}}").hide();
                            $("#list_model{{$u}}").show();
                            $("#list_model{{$u}}").html("");
                            $("#list_model{{$u}}").html(data);
                            $('#paginations{{$u}}').hide();
                        }
                    });
                } else {
                    $("#list_model{{$u}}").hide();
                    $("#list_models{{$u}}").show();
                    $('#paginations{{$u}}').show();
                }
            });
        @endfor
    });

    $('#popup').fancybox({
        type: 'iframe',
        afterLoad : function(){
            $('.fancybox-iframe').contents().find(".container-fluid.relative").remove();
        },
        afterClose: function() {
            location.reload(true);
        }
    });

    function changetab(id){
        $.ajax({
            url: '{{url("/")}}/admin/cashier/tab/choose?tab='+id,
            type: 'GET',
            success: function() {
            }
        })
    }
    
    function changeshop(id) {
        var ids = $('#shops'+id+' :selected').val();
        localStorage.setItem('shop_id'+id,ids);
        $.ajax({
            url: '{{url("/")}}/admin/cashier/shop/' + ids + '/choose?tab='+id,
            type: 'GET',
            success: function() {
                window.location.href = window.location.href;
            }
        })
    };
    
    function toPage(id, pages) {
        $.ajax({
            type: "POST",
            url: "{{url('/')}}/admin/cashier/ajax_model",
            data: {
                "_token": "{{ csrf_token() }}",
                "page": pages,
                "keyword": $("#search-box").val(),
                "tab":id
            },
            success: function(data) {
                $("#list_models"+id).hide();
                $("#list_model"+id).show();
                $("#list_model"+id).html("");
                $("#list_model"+id).html(data);
            }
        });
    }

    function hapus(tab, id, subtotal, mod) {
        $.ajax({
            url: '{{url("/")}}/admin/cashier/hapusqty/' + id + '?subtotal=' + subtotal + '&tab=' + tab + '&mod=' + mod,
            type: 'GET',
            success: function() {
                window.location.href = window.location.href;
            }
        })
    }

    function addtocart(tab, mod, price) {
        var keyword = $("#search-box").val();
        var qty = parseInt($('#qty'+mod+'_'+tab).val());
        var subtotal = qty * price;
        var stocks = parseInt($('#stocks' + mod).val());
        var datanya = {
            model_id : mod,
            qty : qty,
            subtotal : subtotal,
            tab : tab,
            stocks : stocks
        };
        
        if (qty > stocks || qty == 0) {
            alert('Stok Kurang!');
        } else {
            localStorage.setItem('stocks_' + mod, stocks - qty);
            $.ajax({
                url: '{{url("/")}}/admin/cashier/addqty',
                type: 'GET',
                data: datanya,
                success: function(data) {
                    if(typeof(data) === "object")
                        alert('Stok Kurang!');
                    else
                        window.location.href = window.location.href;
                }
            });
        }
    }

    function bayar(tab) {
        var cus = $('#cus'+tab+' :selected').val();
        var sales = $('#sales'+tab+' :selected').val();
        var noref = $('#no_ref_'+tab+'').val();
        
        localStorage.setItem('cus'+tab,cus);
        localStorage.setItem('sales'+tab,sales);
        localStorage.setItem('noref'+tab,noref);
        
        var ur = '{{ url("") }}/admin/cashier/details/'+tab+'?cus='+cus+'&sales='+sales;
        if(noref != "")
            ur += '&noref='+noref;
        
        window.location.href = ur;
    };

    function addCus(tab) {
        var datas = {
            _token: '{{ csrf_token() }}',
            nama: $('#cus_name'+tab).val(),
            no_telp: $('#cus_numb'+tab).val(),
            alamat: $('#cus_addr'+tab).val(),
        };

        $.ajax({
            type: 'POST',
            url: '{{url("/")}}/admin/customer',
            data: datas,
            complete: function() {
                location.reload();
            }
        });

    };

    function addCust(tab) {
        var datas = {
            _token: '{{ csrf_token() }}',
            nama: $('#cus_names'+tab).val(),
            no_telp: $('#cus_numbs'+tab).val(),
            alamat: $('#cus_addrs'+tab).val(),
        };

        $.ajax({
            type: 'POST',
            url: '{{url("/")}}/admin/customer',
            data: datas,
            complete: function() {
                location.reload();
            }
        });

    };
    
    @for($u=1; $u<=3; $u++)
    
    $('#cus{{$u}}').on('change', function(){
        var cus = $('#cus{{$u}} :selected').val();
        
        $.ajax({
            type : 'GET',
            url : '{{ url("") }}/admin/cashier/getnotes/'+cus,
            success : function(data){
                localStorage.setItem('cus_{{ $u }}', cus);
                $('#ncu{{ $u }}').show();
                $('#note_cus{{ $u }}').html(data);
            }
        })
    });   
    
    $('#sales{{$u}}').on('change', function(){
        var sales = $('#sales{{$u}} :selected').val();
        localStorage.setItem('sales_{{ $u }}', sales);
    });   
        
    @endfor
</script>
@endpush
