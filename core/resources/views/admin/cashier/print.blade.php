<html>
    <head>
        <style>
            table {
                border-collapse: collapse;
                width: 100%;
            }

            table, td, th {
                border: 1px solid black;
                padding: 10px;
                text-align: left;
            }

            table.noborder, table.noborder td, table.noborder th{
                border: 0px;
                width: 100%;
            }
            
            .pull-right{
                width: 50%;
                float: right;
            }
        </style>
    </head>
    <body>
        <h3>Transaksi #{{ $cashier->no_ref }} <a style="float:right;">{{ $reprint }}</a></h3>
        <table>
            <tbody>
                <tr>
                    <th>Tanggal</th>
                    <td>{{ $cashier->created_at }}</td>
                </tr>
                <tr>
                    <th>Di Toko</th>
                    <td>{{ $cashier->shop_name }}</td>
                </tr> 
                <tr>
                    <th>Pelanggan</th>
                    <td>{{ $cashier->customer_name != "" ? $cashier->customer_name : 'Guest' }}</td>
                </tr>
                <tr>
                    <th>Subtotal</th>
                    <td>{{ str_replace(',', '.', number_format($cashier->subtotal)) }}</td>
                </tr>
                <tr>
                    <th>Potongan</th>
                    <td>{{ str_replace(',', '.', number_format($cashier->discount)) }}</td>
                </tr>
                <tr>
                    <th>Total</th>
                    <td>{{ str_replace(',', '.', number_format($cashier->total)) }}</td>
                </tr>
            </tbody>
        </table>
        <br/>
        <table>
            <thead>
                <tr>
                    <th width="15px">#</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Harga Satuan</th>
                    <th>QTY</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @php $i=1; @endphp
                @foreach($detail as $dtl)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $dtl->model_name }}</td>
                        <td>{{ $dtl->name }}</td>
                        <td>{{ str_replace(',', '.', number_format($dtl->price)) }}</td>
                        <td>{{ $dtl->qty }}</td>
                        <td>{{ str_replace(',', '.', number_format($dtl->price * $dtl->qty)) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <hr/>
        <div style="width:25%; float:left;">
            <br/>
            <div style="border:1px solid black; padding:5px;">
                <b>Catatan : </b>
                <p>{{ $cashier->note }}</p>
            </div>
            <div class="clearfix"></div>
            @if($cashier->signature != "")
            <br/>
            <br/>
            <label>Tanda Tangan</label>
            <br/>
            <img src="{{ url('').'/'.$cashier->signature }}" style="width:100%" />
            @endif
        </div>
        <div class="pull-right">
            <h4>Pembayaran</h4>
            <hr/>
            <table class="noborder">
                <tr>
                    <th>Subtotal</th>
                    <td style="text-align: right">{{ str_replace(',', '.', number_format($cashier->subtotal)) }}</td>
                </tr>
                <tr>
                    <th>Potongan</th>
                    <td style="text-align: right">{{ (str_replace(',', '.', number_format($cashier->discount)) > 0) ? number_format($cashier->discount) : '-' }}</td>
                </tr>
                <tr>
                    <th>Total</th>
                    <td style="text-align: right">{{ str_replace(',', '.', number_format($cashier->total)) }}</td>
                </tr>
                @php $pay = 0; @endphp
                @foreach($payment as $payments)
                @php $pay += $payments->pay @endphp
                <tr>
                    <th>Bayar dengan {{ $payments->name }}</th>
                    <td style="text-align: right"><b>{{ str_replace(',', '.', number_format($payments->pay)) }}</b></td>
                </tr>
                @endforeach
                <tr>
                    <th>Hutang</th>
                    <td style="text-align: right"><b>{{ (($debt['debt'] * -1) > 0) ? str_replace(',', '.', number_format(($debt['debt'] * -1))) : '-' }}</b></td>
                </tr>
                @if(isset($debt['debt']))
                @php $date = date_create($debt['due_date']); $date = date_format($date, 'd/m/Y'); @endphp
                <tr>
                    <th>Jatuh Tempo</th>
                    <td style="text-align: right"><b>{{ $date }}</b></td>
                </tr>
                @endif
            </table>
        </div>
    </body>
</html>