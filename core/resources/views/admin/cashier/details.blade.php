@extends('layouts.app.frame')
@section('title', 'Cashier')
@section('description', '')
@section('breadcrumbs', Breadcrumbs::render('cashier'))
@section('button', '')

@section('content')
<div class="col-sm-12 col-xs-12">
    <div class="form-group form-group-default form-group-default-select2 required">
        <label>Masukkan Nama Pelanggan</label> 
        {!! Form::select('customer_id', $cus, $cu, ['id' => 'cus', 'class' => 'full-width', 'data-init-plugin' => 'select2', 'required' => '']) !!}
    </div>
</div>
<div class="col-md-12">
    <div class="panel panel-default" {{ strlen($notes) == 0 ? 'style=display:none;' : "" }}>
        <div class="panel-body">
            <b class="panel-title">Catatan : </b>
            <div id="note_cus">{{ $notes }}</div>
        </div>
    </div>
	<form id="formbayar">
        <div class="col-md-12">
            <br/>
            <div id="cart">
                @php $s = array(); @endphp
                @if($cart != "")
				@php $i = 0; @endphp
				@php $s = array(); @endphp
				@foreach($cart as $in)
				@php $s[$i] = $in->subtotal; @endphp
				<div class="col-md-12">
					<b>{{  $name[$in->model_id] }}</b>
					<span>({{  $mname[$in->model_id] }})</span>
					<br/>
					<div class="pull-right">
						<b>{{ $in->qty }} x IDR {{ str_replace(',', '.', number_format($price[$in->model_id])) }}</b>        
					</div>
				</div>
				@php $i++ @endphp
				@endforeach
				<br/>
				<hr>
				<div class="form-group col-md-12">
					<label>Potongan</label>
                    <br/>
					<select class="full-width" data-init-plugin="select2" id="pot">
						<option value="0">Pilih Potongan</option>
						<option value="1">Persen (%)</option>
						<option value="3">Per Qty (Nominal)</option>
						<option value="4">All Qty (Nominal)</option>
					</select>
				</div>
				<div class="form-group" id="disc">
					<div class="col-md-10">
						<div class="input-group transparent" id="disco1">
							<span class="input-group-addon">
								IDR
							</span>
							<input type="text" value="0" id="discount_qtyall" class="form-control" placeholder="Masukan Nominal Potongan" />
						</div>
						<div class="input-group transparent" id="disco2">
							<input type="text" value="0" id="discount_per" class="form-control" placeholder="Masukan Prosentase Potongan" />
							<span class="input-group-addon">
								%
							</span>
						</div>
                        <div class="form-group col-md-6" style="padding-left:0px; padding-right:5px;" id="disco3">
                            <select id="products" class="form-control" data-init-plugin="select2">
				                @foreach($cart as $in)
                                    <option value="{{ $in->model_id }}">{{ $name[$in->model_id] }} ({{  $mname[$in->model_id] }})</option>
                                @endforeach
                            </select>
                        </div>
						<div class="input-group transparent col-md-6" style="padding-left:5px; padding-right:0px;" id="disco4">
							<span class="input-group-addon">
								IDR
							</span>
							<input type="text" value="0" id="discount_qty" class="form-control" placeholder="Masukan Nominal Potongan" />
						</div>
					</div>
                    <div class="form-group col-md-2">
                        <a onclick="return adddiscount()" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></a>
                    </div>
				</div>
                @endif
                <div class="col-md-12">
                    <table width="100%" id="tabs">
                    </table>
                </div>
            </div>
            <textarea style="display:none" id="jsondisc"></textarea>
        </div>
        <br/>
        <hr/>
        <div class="col-sm-12">
            <b>Subtotal</b>
            <div class="pull-right" style="margin-bottom:10px;">
                <b>IDR {{ str_replace(',', '.', number_format(array_sum($s))) }}</b>
            </div>
            <br/>
            <div>
				<div class="col-md-4" style="padding-left:0px; margin-bottom:10px;">
                    <b>Pembulatan</b>
                    <div class="clearfix"></div>
				    <input type="text" value="0" id="discount_nom" class="form-control" placeholder="Masukan Nominal Potongan" />
				</div>
                <div class="col-md-2">
                    <br/>
                    <a onclick="return addnominal()" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></a>
                </div>
                <div class="pull-right" style="margin-bottom:10px;" id="pots">
                    <b id="rows2"></b>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="disc2">
				<b>Potongan</b>
				<div class="pull-right" style="margin-bottom:10px;">
					<b id="discount"></b>        
				</div>
				<br/>
            </div>
        </div>
        <div class="col-sm-12">
            <br/>
            <div class="form-group">
                <label>Metode Pembayaran</label>
                <br/>
                <select class="full-width" data-init-plugin="select2" id="method">
                    @php $i = 1; @endphp
                    @foreach($method as $mtd => $mt)
                    <option {{ $i == 1 ? 'selected' : '' }} value="{{ $mtd }}">{{$mt}}</option>
                    @php $i++; @endphp
                    @endforeach
                </select>
            </div>
            @php $i = 1; @endphp
            @foreach($method as $mtd => $mt)
            <div id="pay{{$mtd}}" style="display:{{ $i == 1 ? 'block' : 'none' }};">
                <div class="col-md-10" style="padding:0px;">
                    <div class="input-group transparent">
                        <span class="input-group-addon">
                            IDR
                        </span>
                        <input type="text" value="0" id="pays{{$mtd}}" class="form-control" />
                    </div>
                </div>
                <div class="form-group col-md-2">
                    <a onclick="return addpayment()" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            @php $i++; @endphp
            @endforeach
            <textarea style="display: none;" id="method_pembayaran" name="method_pembayaran"></textarea>
            <div class="col-md-12">
                <table width="100%" id="tabss">
                </table>
            </div>
            <div class="clearfix"></div>
            <hr/>
            <b>Total</b>
            <div class="pull-right" style="margin-bottom:10px;">
                <b id="totals">IDR {{ str_replace(',', '.', number_format(array_sum($s))) }}</b>    
            </div>
            <div class="clearfix"></div>
            <div id="hutang" style="display:none;">
                <b>Hutang untuk {{ $cus[$cu] }}</b>
                <div class="pull-right">
                    <b id="hutangs"></b>    
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-12" id="periods" style="display:none; margin-top:10px;">
            <hr/>
            <div class="form-group col-sm-12">
                <label>Periode Hutang (Hari)</label>
            </div>
            <div class="form-group col-sm-3">
                <b id="sisa" style="color:red"></b>
            </div>
            <div class="form-group col-sm-6">
                <label id="sisa"></label>
                <input id="period" type="number" class="form-control pull-right" value="7" />
            </div>
            <div class="col-sm-3">
                <a class="btn btn-success" onclick="return addperiod()">
                    <i class="fa fa-check"></i>
                </a>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-12">
            <hr/>
            <div class="form-group">
                <label>Catatan : </label>
                <textarea class="form-control" id="notes" placeholder="Catatan"></textarea>
            </div>
        </div>

        <div class="col-sm-12">
            <div id="signature"></div>
            <div>
                <a id="clear" class="btn btn-warning pull-left">Clear</a>
            </div>
        </div>

        <div class="col-sm-12">
            <hr/>
            <a onclick="return bayar()" class="btn btn-primary pull-right" id="getimage">Bayar</a>
        </div>
	</form>
</div>
@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function(){
        localStorage.setItem('tdiscount',0);
        $(".container-fluid.relative").remove();
        if(localStorage.getItem('period') > 0){
            $('#periods').show();
            $('#period').val(localStorage.getItem('period'));
        }
        
        $('#disc').hide();
        $('#disc2').hide();
        $('#disco1').hide();
        $('#disco2').hide();
        $('#disco3').hide();
        $('#disco4').hide();
        
        $('p').remove();
        @foreach($model as $models)
            if(localStorage.getItem('stocks_{{$models["id"]}}') < {{$models['qty']}} && localStorage.getItem('stocks_{{$models["id"]}}') > 0)
                $('#stocks{{$models["id"]}}').val(localStorage.getItem('stocks_{{$models["id"]}}'));
            else
                $('#stocks{{$models["id"]}}').val({{$models['qty']}})
        @endforeach
    });
    
    $('#cus').on('change', function(){
        var cus = $('#cus :selected').val();
        var newUrl = location.href.replace("cus={{$cu}}", "cus="+cus);
        
        location.href = newUrl;
    })
    
    $('#pot').on('change', function(){
        var pot = $('#pot :selected').val();
        $('#discount_nom').val('0');
        $('#discount_per').val('0');
        $('#discount_qty').val('0');
        
        if(pot == 0){
            $('#disc').hide();
            $('#disc2').hide();
            
            var subtotal = {{ array_sum($s) }};
            var discount = 0;
            var total = subtotal - discount;

            var datanya = {
                'pot'       :   pot,
                'subtotal'  :   subtotal,
                'discount'  :   discount,
                'total'     :   total
            };

            $.ajax({
                url: '{{url("/")}}/admin/cashier/adddisc',
                type: 'GET',
                data: datanya,
                success: function(){
                    window.location.href = window.location.href;
                }
            });
            
        }else if(pot == 1){
            $('#disc').show();
            $('#disco2').show();
            $('#disco1').hide();
            $('#disco3').hide();
            $('#disco4').hide();
        }else if(pot == 4){
            $('#disc').show();
            $('#disco1').show();
            $('#disco2').hide();
            $('#disco3').hide();
            $('#disco4').hide();
        }else if(pot == 3){
            $('#disc').show();
            $('#disco3').show();
            $('#disco4').show();
            $('#disco1').hide();
            $('#disco2').hide();
        }
    });
    
    function hapus(pot, str){
        var v_old = $('#jsondisc').val();
        var v_new = v_old.replace(str,'');
        var disc = str.split(':');
        disc = parseInt(disc[2]);
        
        if(pot.substr(0,1) == 1)
            disc = disc / 100 * {{ array_sum($s) }};
    
        var tdiscount = localStorage.getItem('tdiscount');
        tdiscount -= disc;
        localStorage.setItem('tdiscount',tdiscount);
    
        var total = {{ array_sum($s) }} - tdiscount;
    
        $('#jsondisc').val(v_new);
        $('#rows_'+pot).remove();
        
        if(tdiscount == 0){
            $('#disc2').hide();
            $('#discount').hide();
        }else
            $('#discount').html('IDR '+parseInt(tdiscount).toLocaleString().replace(/,/g, '.'));
    
        $('#totals').html('IDR '+parseInt(total).toLocaleString().replace(/,/g, '.'));
    
        var subtotal = {{ array_sum($s) }};
        var disc = localStorage.getItem('tdiscount');
        var total = subtotal - disc;
        var pays = 0;
        @foreach($method as $mtd => $mt)
            pays += parseInt($('#pays{{$mtd}}').val().replace(/,/g, ''));
        @endforeach

        if(total > pays){
            var hutang = total - pays;
            hutang = hutang.toLocaleString(2).replace(/,/g, '.');
            $('#hutang').show();
            $('#hutangs').html('IDR '+hutang);
        }
    }
    
    function adddiscount(){
        var tdiscount = localStorage.getItem('tdiscount');
        if(tdiscount == '')
            tdiscount = 0;
        var subtotal = {{ array_sum($s) }};
        var discount = 0;
        var disc = 0;
    
        var pot = $('#pot :selected').val();
        var product = 0;
        var prod = '';
        if(pot == 1){
            disc = parseInt($('#discount_per').val().replace(/,/g, ''));
        }else if(pot == 2){
            disc = parseInt($('#discount_nom').val().replace(/,/g, ''));
        }else if(pot == 3){
            disc = parseInt($('#discount_qty').val().replace(/,/g, ''));
            product = $('#products :selected').val();
            prod = '('+$('#products :selected').text()+')';
        }
    
        if(pot != 4){
            if($('#rows_'+pot+product).length == 0 && disc > 0){
                $('#disc2').show();
                $('#discount').show();
                
                if(pot == 1){
                    discount = (disc / 100) * subtotal;
                    var disc2 = disc+' %';
                    var potn = 'Persen';
                }else if(pot == 2){
                    discount = disc;
                    var disc2 = disc.toLocaleString().replace(/,/g, '.');
                    var potn = 'Nominal';
                }else if(pot == 3){
                    @foreach($cart as $in)
                        if(product == {{$in->model_id}})
                            disc = disc * {{$in->qty}};
                    @endforeach
                    discount = disc;
                    var disc2 = disc.toLocaleString().replace(/,/g, '.');
                    var potn = 'Nominal Per Qty';
                }

                $('#jsondisc').val($('#jsondisc').val()+'{"'+pot+'" : {"'+product+'" : '+disc+'}},');

                var tabs = '<tr id="rows_'+pot+product+'"><th width="50%">Potongan ('+potn+')</th><th width="5%"> : </th><td>'+disc2+' '+prod+'</td><td width="5%"><a onclick="return hapus(&quot;'+pot+product+'&quot;,	&#39;{&quot;'+pot+'&quot; : {&quot;'+product+'&quot; : '+disc+'}},&#39;)" href="javascript:;"><i class="fa fa-times"></i></a></td></tr>';

                tdiscount = parseInt(tdiscount) + discount;
                var total = subtotal - tdiscount;

                localStorage.setItem('tdiscount',tdiscount);

                $('#tabs').append(tabs);
                $('#discount').html('IDR '+parseInt(tdiscount).toLocaleString().replace(/,/g, '.'));
                $('#totals').html('IDR '+parseInt(total).toLocaleString().replace(/,/g, '.'));
            }
        }else{
            disc = parseInt($('#discount_qtyall').val().replace(/,/g, ''));
            @foreach($cart as $in)
            if($('#rows_3{{ $in->model_id }}').length == 0 && disc > 0){
                $('#disc2').show();
                $('#discount').show();
                
                discount = disc * {{ $in->qty }} ;
                var disc2 = discount.toLocaleString().replace(/,/g, '.');
                var potn = 'Nominal';
                var prod = '({{ $name[$in->model_id] }} ({{ $mname[$in->model_id] }}))'
                $('#jsondisc').val($('#jsondisc').val()+'{"'+pot+'" : {"{{ $in->model_id }}" : '+discount+'}},');

                var tabs = '<tr id="rows_3{{ $in->model_id }}"><th width="50%">Potongan ('+potn+')</th><th width="5%"> : </th><td>'+disc2+' '+prod+'</td><td width="5%"><a onclick="return hapus(&quot;3{{ $in->model_id }}&quot;,	&#39;{&quot;3&quot; : {&quot;{{ $in->model_id }}&quot; : '+discount+'}},&#39;)" href="javascript:;"><i class="fa fa-times"></i></a></td></tr>';

                tdiscount = parseInt(tdiscount) + discount;
                var total = subtotal - tdiscount;

                localStorage.setItem('tdiscount',tdiscount);

                $('#tabs').append(tabs);
                $('#discount').html('IDR '+parseInt(tdiscount).toLocaleString().replace(/,/g, '.'));
                $('#totals').html('IDR '+parseInt(total).toLocaleString().replace(/,/g, '.'));
            }
            @endforeach
        }
    
        var subtotal = {{ array_sum($s) }};
        var disc = localStorage.getItem('tdiscount');
        var total = subtotal - disc;
        var pays = 0;
        @foreach($method as $mtd => $mt)
            pays += parseInt($('#pays{{$mtd}}').val().replace(/,/g, ''));
        @endforeach

        if(total > pays){
            var hutang = total - pays;
            hutang = hutang.toLocaleString(2).replace(/,/g, '.');
            $('#hutang').show();
            $('#hutangs').html('IDR '+hutang);
        }
    }
    
    function addnominal(){
        var tdiscount = localStorage.getItem('tdiscount');
        if(tdiscount == '')
            tdiscount = 0;
        var subtotal = {{ array_sum($s) }};
        var discount = 0;
        var disc = 0;
    
        var pot = $('#pot :selected').val();
        var product = 0;
        var prod = '';
    
        disc = parseInt($('#discount_nom').val().replace(/,/g, ''));
        discount = disc;
        var disc2 = disc.toLocaleString().replace(/,/g, '.');
        var potn = 'Nominal';
    
        if($('#rows2').html().length == 0 && disc > 0){
            $('#disc2').show();
            $('#discount').show();
    
            $('#jsondisc').val($('#jsondisc').val()+'{"2" : {"'+product+'" : '+disc+'}},');

            tdiscount = parseInt(tdiscount) + discount;
            var total = subtotal - tdiscount;

            localStorage.setItem('tdiscount',tdiscount);

            $('#discount').html('IDR '+parseInt(tdiscount).toLocaleString().replace(/,/g, '.'));
            $('#rows2').html('IDR '+parseInt(discount).toLocaleString().replace(/,/g, '.'));
            $('#totals').html('IDR '+parseInt(total).toLocaleString().replace(/,/g, '.'));
        }else if($('#rows2').html().length != 0 && disc > 0){
            var olddisc = $('#rows2').html().replace('IDR','');
            olddisc = parseFloat(olddisc.replace(/\./g,''));
            
            $('#disc2').show();
            $('#discount').show();
    
            $('#jsondisc').val($('#jsondisc').val().replace('{"2" : {"'+product+'" : '+olddisc+'}},', '{"2" : {"'+product+'" : '+disc+'}},'));

            tdiscount = parseInt(tdiscount) + discount - olddisc;
            var total = subtotal - tdiscount;

            localStorage.setItem('tdiscount',tdiscount);

            $('#discount').html('IDR '+parseInt(tdiscount).toLocaleString().replace(/,/g, '.'));
            $('#rows2').html('IDR '+parseInt(discount).toLocaleString().replace(/,/g, '.'));
            $('#totals').html('IDR '+parseInt(total).toLocaleString().replace(/,/g, '.'));
        }
    
        var subtotal = {{ array_sum($s) }};
        var disc = localStorage.getItem('tdiscount');
        var total = subtotal - disc;
        var pays = 0;
        @foreach($method as $mtd => $mt)
            pays += parseInt($('#pays{{$mtd}}').val().replace(/,/g, ''));
        @endforeach

        if(total > pays){
            var hutang = total - pays;
            hutang = hutang.toLocaleString(2).replace(/,/g, '.');
            $('#hutang').show();
            $('#hutangs').html('IDR '+hutang);
        }
    }
    
    $('#method').on('change', function(){
        var pay = $('#method :selected').val();
        
        @foreach($method as $mtd => $mt)
            $('#pay{{$mtd}}').hide();
            $('#pays{{$mtd}}').hide();
        @endforeach
        
        $('#pay'+pay).show();
        $('#pays'+pay).show();
    });
    
    function addpayment(){
        var pay = $('#method :selected').val();
        var tpay = $('#method :selected').text();
        var payment = parseInt($('#pays'+pay).val().replace(/,/g, '')).toLocaleString().replace(/,/g, '.');

        var method = $('#method_pembayaran').val();
        if($('#row_'+pay).length == 0){
            var tabs = '<tr id="row_'+pay+'"><th width="50%">Bayar dengan '+tpay+'</th><th width="5%"> : </th><td>'+payment+'</td><td width="5%"><a onclick="return hapuspay('+pay+')" href="javascript:;"><i class="fa fa-times"></i></a></td></tr>';

            method += pay+',' ;

            $('#tabss').append(tabs);
            $('#method_pembayaran').val(method);
            $('#periods').hide();
            var subtotal = {{ array_sum($s) }};
            var disc = localStorage.getItem('tdiscount');
            var total = subtotal - disc;
            var pays = 0;
            @foreach($method as $mtd => $mt)
                pays += parseInt($('#pays{{$mtd}}').val().replace(/,/g, ''));
            @endforeach
            
            if(total > pays){
                var hutang = total - pays;
                hutang = hutang.toLocaleString(2).replace(/,/g, '.');
                $('#hutang').show();
                $('#hutangs').html('IDR '+hutang);
            }
        }
    }
     
    function hapuspay(pay){
        @foreach($method as $mtd => $mt)
            $('#pay{{$mtd}}').hide();
            $('#pays{{$mtd}}').hide();
        @endforeach


        $('#pay'+pay).show();
        $('#pays'+pay).show();
        $('#pays'+pay).val(0);
        $('#pays'+pay).focus();
        $('#row_'+pay).remove();

        var a = $('#method_pembayaran').val().replace(pay+',', '');
        $('#method_pembayaran').val(a);

        var subtotal = {{ array_sum($s) }};
        var disc = localStorage.getItem('tdiscount');
        var total = subtotal - disc;
        var pays = 0;
        @foreach($method as $mtd => $mt)
            pays += parseInt($('#pays{{$mtd}}').val().replace(/,/g, ''));
        @endforeach

        if(total > pays){
            var hutang = total - pays;
            hutang = hutang.toLocaleString(2).replace(/,/g, '.');
            $('#hutang').show();
            $('#hutangs').html('IDR '+hutang);
        }
    }
    
    function addperiod(){
        Pace.restart();
        localStorage.setItem('period',$('#period').val());
    }
    
    
    function GetURLParameter(elem) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == elem)
            {
                return decodeURIComponent(sParameterName[1]);
            }
        }
    }
        
    function bayar()
    {
        var subtotal = '{{ array_sum($s) }}';
        var discount = 0;
        var payment = '';
        var pay = 0;
    
        @foreach($method as $mtd => $mt)
            payment += ',{"{{$mtd}}":'+$('#pays{{$mtd}}').val().replace(/,/g, '')+"}";
            pay += parseInt($('#pays{{$mtd}}').val().replace(/,/g, ''));
        @endforeach
        var disc = localStorage.getItem('tdiscount');
        var total = subtotal - disc;
        var debt  = total - pay;
    
        if(debt > 0 && localStorage.getItem('period') == 0){
            $('#periods').show();
            $('#period').focus();
            $('#sisa').html('Sisa hutang : '+debt.toLocaleString(2));
            
            if(localStorage.getItem('period') > 0)
                return true;
            else
                return false;
        }
    
        var jdisc = $('#jsondisc').val();
        var method_pembayaran = $('#method_pembayaran').val();
        var notes = $('#notes').val();
        var tanda_tangan = $('#signature').jSignature("getData", "image");
        tanda_tangan = tanda_tangan[1];
        var ids = localStorage.getItem('shop_id{{$tab}}');
        var cus = GetURLParameter('cus');
        var sales = GetURLParameter('sales');
        var noref = GetURLParameter('noref');
        if(noref == "")
            noref = 0;
        var datas = {
            'customer_id'       :   cus,
            'salesman_id'       :   sales,
            'discount'          :   disc,
            'jdisc'             :   jdisc,
            'subtotal'          :   subtotal,
            'total'             :   total,
            'shop_id'           :   ids,
            'payment'           :   payment,
            'noref'             :   noref,
            'notes'             :   notes,
            'method_pembayaran' :   method_pembayaran,
            'tanda_tangan'      :   tanda_tangan,
            'due_date'          :   localStorage.getItem('period'),
            '_token'            :   '{{ csrf_token() }}',
            'tab'               :   '{{ $tab }}'
        };
    
        if(total != 0){
            $.ajax({
                url: '{{ url('/admin/cashier/bayar') }}',
                type: 'POST',
                data: datas,
                success: function(res){
                    window.location.href = '{{url("/admin/cashier/shipping/")}}/'+res;
                }
            });
        }else{
            alert('Keranjang Belanja Masih Kosong!');
        }
    };

    $('#discount_nom').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    
    $('#discount_qty').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    
    $('#discount_qtyall').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    
    @foreach($method as $mtd => $mt)
    $('#pays{{ $mtd }}').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    @endforeach
</script>

<script>
    $(document).ready(function() {
        $("#signature").jSignature();

        $( "#clear" ).click(function() {
            $("#signature").jSignature("reset");
        });
    })
</script>
@endpush
