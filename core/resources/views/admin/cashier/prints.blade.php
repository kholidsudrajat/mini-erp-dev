@extends('layouts.app.frame')
@section('title', 'Transaksi #'.$cashier->no_ref)
@section('description', '')
@section('breadcrumbs', Breadcrumbs::render('cashier'))
@section('button', '')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-condensed">
                <tbody>
                    <tr>
                        <th>Tanggal</th>
                        <td>{{ $cashier->created_at }}</td>
                    </tr>
                    <tr>
                        <th>Di Toko</th>
                        <td>{{ $cashier->shop_name }}</td>
                    </tr>
                    <tr>
                        <th>Pelanggan</th>
                        <td>{{ $cashier->customer_name != "" ? $cashier->customer_name : 'Guest' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-hover">
            <thead>
                <th width="5%">#</th>
                <th>Kode Barang</th>
                <th>Nama Barang</th>
                <th>Harga Satuan</th>
                <th>QTY</th>
                <th>Total</th>
            </thead>
            <tbody>
                @php $i=1; @endphp @foreach($detail as $dtl)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $dtl->model_name }}</td>
                    <td>{{ $dtl->name }}</td>
                    <td>{{ str_replace(',', '.', number_format($dtl->price)) }}</td>
                    <td>{{ $dtl->qty }}</td>
                    <td>{{ str_replace(',', '.', number_format($dtl->price * $dtl->qty)) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <br/>
        <div class="form-group form-group-default row">
            <label>Catatan : </label>
            <p>{{ $cashier->note }}</p>
        </div>
        <div class="clearfix"></div>
        <img src="{{ url('').'/'.$cashier->signature }}" style="width:100%" />
    </div>
    <div class="col-md-6 pull-right">
        <h4>Pembayaran</h4>
        <table class="table table-condensed">
            <tr>
                <th>Subtotal</th>
                <td style="text-align: right">{{ str_replace(',', '.', number_format($cashier->subtotal)) }}</td>
            </tr>
            <tr>
                <th>Potongan</th>
                <td style="text-align: right">{{ (str_replace(',', '.', number_format($cashier->discount)) > 0) ? str_replace(',', '.', number_format($cashier->discount)) : '-' }}</td>
            </tr>
            <tr>
                <th>Total</th>
                <td style="text-align: right">{{ str_replace(',', '.', number_format($cashier->total)) }}</td>
            </tr>
            @php $pay = 0; @endphp 
            @foreach($payment as $payments) 
            @php $pay += $payments->pay @endphp
            <tr>
                <th>Bayar dengan {{ $payments->name }}</th>
                <td style="text-align: right"><b>{{ str_replace(',', '.', number_format($payments->pay)) }}</b></td>
            </tr>
            @endforeach 
            <tr>
                <th>Hutang</th>
                <td style="text-align: right"><b>{{ (($debt['debt'] * -1) > 0) ? str_replace(',', '.', number_format(($debt['debt'] * -1))) : '-' }}</b></td>
            </tr>
            @if(isset($debt['debt']))
            @php $date = date_create($debt['due_date']); $date = date_format($date, 'd/m/Y'); @endphp
            <tr>
                <th>Jatuh Tempo</th>
                <td style="text-align: right"><b>{{ $date }}</b></td>
            </tr>
            @endif
        </table>
    </div>
</div>
<br/>
<div class="col-md-6">
    <div class="checkbox check-success">
        <input id="checkbox-agree" type="checkbox"> <label for="checkbox-agree" style="font-size:20px;">Re-Print</label>
    </div>
</div>
<div class="col-md-6">
    <a class="btn btn-primary pull-right" onclick="return print('{{$cashier->no_ref}}')" href="javascript:;">Cetak</a>
</div>
@endsection
@push('script')
<script type="text/javascript">
    $(document).ready(function(){
        $(".container-fluid.relative").remove();
    });
    
    function print(norefs){
        if($('#checkbox-agree').is(':checked'))
            var reprint = 1;
        else
            var reprint = 0;
        
        window.location.href = '{{url("")}}/admin/cashier/prints?noref='+norefs+'&reprint='+reprint;
    }
    
</script>
@endpush