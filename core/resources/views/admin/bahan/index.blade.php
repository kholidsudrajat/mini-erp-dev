@extends('layouts.app.frame')
@section('title', 'Masuk Bahan')
@section('description', 'Masuk Bahan')
@section('breadcrumbs', Breadcrumbs::render('masuk_bahan'))
@section('button', '<div class="clearfix"></div>')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Header</h3>
    </div>
    @if($cek == 0)
    <div class="panel-body">
        <div class="col-md-6">
            <div class="form-group col-md-12">
                <label>Tanggal</label>
                {!! Form::text('tanggal', date('m/d/Y'), ['id' => 'tanggal', 'class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-12">
                <label>Supplier</label>
                {!! Form::select('supplier', $sup, null, ['data-init-plugin' => 'select2', 'class' => 'form-control', 'id' => 'suppliers']) !!}
                {!! Form::hidden('suppliers', 0, ['id' => 'supp']) !!}
            </div>
            <div class="form-group col-md-12">
                <label>Harga per Kg</label>
                {!! Form::text('harga', 0, ['class' => 'form-control', 'id' => 'harga']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group col-md-12">
                <label>Bahan</label>
                {!! Form::select('bahan', array('-'), null, ['data-init-plugin' => 'select2', 'class' => 'form-control', 'id' => 'bahans']) !!}
            </div>
            <div class="form-group col-md-12">
                <label>Motif</label>
                {!! Form::select('motif', array('-'), null, ['data-init-plugin' => 'select2', 'class' => 'form-control', 'id' => 'motifs']) !!}
            </div>
        </div>
        <div class="clearfix"></div>
        <hr/>
        <div class="pull-right">
            <a class="btn btn-primary" id="create">Create</a>
        </div>
    </div>
    @else
    <div class="panel-body">
        <div class="col-md-6">
            <div class="form-group col-md-12">
                <label>Tanggal</label>
                {!! Form::text('tanggal', $temp['tanggal'], ['id' => 'tanggal', 'class' => 'form-control', 'disabled']) !!}
            </div>
            <div class="form-group col-md-12">
                <label>Supplier</label>
                {!! Form::select('supplier', $sup, $temp['supplier_id'], ['data-init-plugin' => 'select2', 'class' => 'form-control', 'id' => 'suppliers', 'disabled']) !!}
                {!! Form::hidden('suppliers', $temp['supplier_id'], ['id' => 'supp']) !!}
            </div>
            <div class="form-group col-md-12">
                <label>Harga per Kg</label>
                {!! Form::text('harga', $temp['harga'], ['class' => 'form-control', 'id' => 'harga', 'disabled']) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group col-md-12">
                <label>Bahan</label>
                {!! Form::select('bahan', $bahan, $temp['bahan_id'], ['data-init-plugin' => 'select2', 'class' => 'form-control', 'id' => 'bahans', 'disabled']) !!}
            </div>
            <div class="form-group col-md-12">
                <label>Motif</label>
                {!! Form::select('motif', $motif, $temp['motif_id'], ['data-init-plugin' => 'select2', 'class' => 'form-control', 'id' => 'motifs', 'disabled']) !!}
            </div>
        </div>
    </div>
    @endif
</div>
@if($cek > 0)
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Detail</h3>
    </div>
    <div class="panel-body">
        <div class="form-group col-md-4">
            <label>Warna</label>
            {!! Form::select('warna', $warna, null, ['data-init-plugin' => 'select2', 'class' => 'form-control', 'id' => 'warna']) !!}
        </div>
        <div class="form-group col-md-4">
            <label>Roll</label>
            {!! Form::number('roll', 0, ['class' => 'form-control', 'id' => 'roll']) !!}
        </div>
        <div class="form-group col-md-4">
            <label>Kg</label>
            {!! Form::text('kg', null, ['placeholder' => 'Kg', 'class' => 'form-control', 'id' => 'kg']) !!}
            <label class="error">*Pisah dengan koma. contoh: 21,22,23 (jika roll = 3)</label>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" id="addbahan">Tambah Bahan</a>
        </div>
        <div class="clearfix"></div>
        <hr/>
        <table class="table table-bordered">
            <thead>
                <th width="5%">No</th>
                <th>Warna</th>
                <th>Roll</th>
                <th>Kg</th>
                <th>Harga per Kg</th>
                <th>Total</th>
                <th>Action</th>
            </thead>
            <tbody>
                @php 
                $i = 1; 
                $total = 0;
                @endphp
                @foreach($detail as $dtl)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $dtl->name }}</td>
                    <td>{{ $dtl->roll }}</td>
                    <td>{{ $dtl->kg }}</td>
                    <td>{{ str_replace(',', '.', number_format($temp['harga'])) }}</td>
                    <td>{{ str_replace(',', '.', number_format($dtl->kg * $temp['harga'])) }}</td>
                    <td><a onclick="detailkg({{ $dtl['temp_id'] }})" class="btn btn-success">Detail Kg</a></td>
                </tr>
                @php $total += $dtl->kg * $temp['harga']; @endphp
                @endforeach
            </tbody>
        </table>
        <div class="pull-right">
            <h4>Total : {{ str_replace(',', '.', number_format($total)) }}</h4>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<a class="btn btn-success" id="submit">Submit</a>
<a class="btn btn-danger" id="clear">Clear</a>
@endif
<div class="modal fade stick-up" id="modalKg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Detail Kg</h5>
            </div>
            <div class="modal-body text-center">
                <table class="table table-bordered">
                    <thead>
                        <th width="15%" style="text-align:center; padding-left:8px !important;">No</th>
                        <th style="text-align:center;">Kg</th>
                    </thead>
                    <tbody id="detailkg">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection


@push("script")
<script type="text/javascript">
    $('#tanggal').datepicker();
    
    $('#suppliers').on('change', function(){
        var supp = $('#suppliers :selected').val();
        
        if(supp > 0){
            $('#supp').val(supp);
            $.ajax({
                type    : 'GET',
                url     : '{{ url("") }}/admin/pilihbahan/'+supp,
                success : function(res){
                    $.each(res, function(index, value){
                        if(index == 'bahan'){
                            $('#bahans').html('').select2({data: [{id: '', text: ''}]});
                            var data = [];
                            $.each(value, function(val){
                                data.push({id : value[val]['id'], text : value[val]['name']});
                            })
                            $('#bahans').html('').select2({
                                data: data
                            });
                        }else if(index == 'motif'){
                            $('#motifs').html('').select2({data: [{id: '', text: ''}]});
                            var data = [];
                            $.each(value, function(val){
                                data.push({id : value[val]['id'], text : value[val]['name']});
                            })
                            $('#motifs').html('').select2({
                                data: data
                            });
                        }
                    })
                }
            })
        }
    });
    
    $('#harga').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    
    $('#create').on('click', function(){
        var tanggal = $('#tanggal').val();
        var harga = parseInt($('#harga').val().replace(/,/g, ''));
        var supp = $('#supp').val();
        var bahan = $('#bahans :selected').val();
        var motif = $('#motifs :selected').val();
        
        if(supp > 0 && bahan != "" && motif != ""){
            var data = {
                _token      : '{{ csrf_token() }}',
                tanggal     : tanggal,
                harga       : harga,
                supplier_id : supp,
                bahan_id    : bahan,
                motif_id    : motif,
            };
            
            $.ajax({
                type    : 'POST',
                url     : '{{ url("") }}/admin/createtempbahan',
                data    : data,
                success : function(){
                    location.reload();
                }
            });
        }
    });
    
    $('#addbahan').on('click', function(){
        var warna = $('#warna :selected').val();
        var roll = parseInt($('#roll').val());
        var kg = $('#kg').val().split(",");
        
        if(warna != "" && roll > 0 && kg.length == roll){
            $.ajax({
                url     : '{{ url("") }}/admin/createtempdetail',
                type    : 'POST',
                data    : {
                    _token  : '{{ csrf_token() }}',
                    kg      : kg,
                    warna_id: warna,
                    roll    : roll
                },
                success : function(){
                    location.reload();
                }
            })
        }
    });
    
    $('#clear').on('click', function(){
        $.ajax({
            url     : '{{ url("") }}/admin/cleartemp',
            type    : 'GET',
            success : function(){
                location.reload();
            }
        });
    });
    
    $('#submit').on('click', function(){
        $.ajax({
            url     : '{{ url("") }}/admin/submitbahan',
            type    : 'POST',
            data    : {
                _token  : '{{ csrf_token() }}'
            },
            success : function(){
                location.reload();
            }
        });
    });
    
    function detailkg(id){
        $.ajax({
            url     : '{{ url("") }}/admin/detailkg/'+id,
            type    : 'GET',
            success : function(res){
                var tab = '';
                var i = 1;
                $.each(res, function(index, value){
                    tab += '<tr><td>'+i+'</td><td>'+value['kg']+'</td></tr>';
                    i++;
                });
                
                $('#detailkg').html(tab);
                $('#modalKg').modal('show');
            }
        })
    }
</script>
@endpush
