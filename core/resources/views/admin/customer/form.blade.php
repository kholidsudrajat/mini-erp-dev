<div aria-required="true" class="form-group required form-group-default {{ $errors->has('nama') ? 'has-error' : ''}}">
    {!! Form::label('nama', 'Nama') !!}
    {!! Form::text('nama', null, ['class' => 'form-control', 'required' => '']) !!}
</div>
{!! $errors->first('nama', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group required form-group-default {{ $errors->has('no_telp') ? 'has-error' : ''}}">
    {!! Form::label('no_telp', 'No Telp') !!}
    {!! Form::text('no_telp', null, ['class' => 'form-control', 'required' => '']) !!}
</div>
{!! $errors->first('no_telp', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group form-group-default {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('email', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group form-group-default {{ $errors->has('note') ? 'has-error' : ''}}">
    {!! Form::label('note', 'Catatan') !!}
    {!! Form::text('note', null, ['class' => 'form-control']) !!}
</div>
{!! $errors->first('note', '<label class="error">:message</label>') !!}

@if(isset($customer))
<div aria-required="true" class="form-group form-group-default {{ $errors->has('limit_debt') ? 'has-error' : ''}}">
    {!! Form::label('limit_debt', 'Limit Piutang') !!}
    {!! Form::text('limit_debt', null, ['class' => 'form-control', 'id' => 'limit_piutang']) !!}
</div>
{!! $errors->first('limit_debt', '<label class="error">:message</label>') !!}
@else
<div class="col-md-6" style="padding:0px">
    <div aria-required="true" class="form-group required form-group-default {{ $errors->has('alamat') ? 'has-error' : ''}}">
        {!! Form::label('alamat', 'Alamat') !!}
        {!! Form::text('alamat', null, ['class' => 'form-control', 'required' => '']) !!}
    </div>
{!! $errors->first('alamat', '<label class="error">:message</label>') !!}
</div>
<div aria-required="true" class="form-group col-md-6 required form-group-default form-group-default-select2 {{ $errors->has('shipping') ? 'has-error' : ''}}">
    {!! Form::label('shipping', 'Shipping') !!}
    {!! Form::select('shipping', $shipping, null, ['id' => 'shipping', 'class' => 'full-width', 'data-init-plugin' => 'select2', 'required' => '']) !!}
</div>
{!! $errors->first('shipping', '<label class="error">:message</label>') !!}
@endif

<br/>
@if(isset($customer))
<div class="panel panel-default">
    <div class="panel-body">
        <form method="post">
            <div aria-required="true" class="col-md-6 form-group required form-group-default {{ $errors->has('alamats') ? 'has-error' : ''}}">
                {!! Form::label('alamats', 'Alamat') !!}
                {!! Form::text('alamats', null, ['class' => 'form-control', 'required' => '', 'id' => 'alamat', 'placeholder' => 'Masukkan Alamat']) !!}
            </div>
            {!! $errors->first('alamats', '<label class="error">:message</label>') !!}
            <input type="hidden" name="customer_id" id="customer_id" value="{{ $customer->id }}"/>
            <div class="col-sm-3 col-xs-6">
                <div aria-required="true" class="form-group required form-group-default form-group-default-select2 {{ $errors->has('shipping') ? 'has-error' : ''}}">
                    {!! Form::label('shipping', 'Shipping') !!}
                    {!! Form::select('shipping', $shipping, null, ['id' => 'shipping', 'class' => 'full-width', 'data-init-plugin' => 'select2', 'required' => '']) !!}
                </div>
                {!! $errors->first('shipping', '<label class="error">:message</label>') !!}
            </div>
            <div class="col-span1" style="padding-top:9px;">
                <a class="btn btn-primary" id="tambah" onclick="tambah();">Tambah</a>
            </div>
        </form>
        <br/>
        @if(isset($address))
            @if(count($address) != 0)
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th> Alamat </th> <th> Shipping </th><th> Action </th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($address as $item)
                        <tr>
                            <td>{{ $item->alamat }}</td>
                            <td>{{ $item->name }}</td>
                            <td><a onclick="return hapus({{ $item->id }})" class="bb btn btn-danger"><i class="fa fa-times"></i></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        @endif
    </div>
</div>
@endif
<br>
{{--<div class="pull-left">--}}
    {{--<div class="checkbox check-success">--}}
        {{--<input id="checkbox-agree" type="checkbox" required> <label for="checkbox-agree">Saya sudah mengecek data sebelum menyimpan</label>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<br/>--}}

{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Save', ['type' => 'submit', 'class' => 'btn btn-success']) !!}
<button class="btn btn-default" type="reset"><i class="pg-close"></i> Clear</button>

@push('script')
    <script>
        function hapus(id){
            var conf = confirm('Yakin hapus?');
            if(conf) {
                $.ajax({
                    url: '{{url("admin/address/hapus")}}' + "/" + id,
                    type: 'GET',
                    complete: function (data) {
                        console.log(data);
                        window.location.reload();
                    }
                });
            }
        }

        $('#limit_piutang').inputmask("numeric", {
            radixPoint: ".",
            groupSeparator: ",",
            digits: 2,
            autoGroup: true,
            rightAlign: false,
            oncleared: function () { self.Value(''); }
        });
    </script>
@endpush
