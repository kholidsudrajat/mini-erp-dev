@extends('layouts.app.frame')
@section('title', 'Create New Customer')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('customer.new'))
@section('button', '<a href="'.url('/admin/customer').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/customer', 'id' => 'formValidate', 'files' => true, 'novalidate' => '-']) !!}

		@include ('admin.customer.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');
});
</script>
@endpush
