@extends('layouts.app.frame')
@section('title', 'Edit Customer')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('customer.edit', $customer))
@section('button', '<a href="'.url('/admin/customer').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    {!! Form::model($customer, [
            'method' => 'PATCH',
            'url' => ['/admin/customer', $customer->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}

        @include ('admin.customer.form')

	{!! Form::close() !!}

@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();

});
    
tinymce.init({
      selector: "textarea",
      theme: "modern",
      paste_data_images: true,
      height : 250,
      plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
      ],
      toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
      image_advtab: true
    });

    $('#alamat').keypress(function(e){
        if(e.which == 13){
            $('#tambah').click();
            return false;
        }
    });

    function tambah()
    {
        if($('#alamat').val() == ''){
            alert('Alamat tidak boleh kosong');
            return false;
        }else {
            $.ajax({
                url: '{{url('/')}}/admin/address/tambah',
                type: 'POST',
                data: {
                    customer_id: $('#customer_id').val(),
                    alamat: $('#alamat').val(),
                    shipping : $('#shipping').val(),
                    _token: '{{ csrf_token() }}'
                },
                success: function (data) {
                    console.log(data);
                    window.location.reload();
                }
            });
        }
    }
    
</script>
@endpush
