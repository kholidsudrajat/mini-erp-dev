@extends('layouts.app.frame')
@section('title', 'Customer #' . $customer->id)
@section('description', 'Customer Details')
@section('breadcrumbs', Breadcrumbs::render('customer.show', $customer))
@section('button', '<a href="'.url('/root/customer').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
	<div class="panel panel-default">
		<div class="panel-body">
			<a href="{{ url('root/customer/' . $customer->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Customer"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
			{!! Form::open([
				'method'=>'DELETE',
				'url' => ['root/customer', $customer->id],
				'style' => 'display:inline'
			]) !!}
				{!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
						'type' => 'submit',
						'class' => 'btn btn-danger btn-xs',
						'title' => 'Delete Customer',
						'onclick'=>'return confirm("Confirm delete?")'
				))!!}
			{!! Form::close() !!}
			<br/>
			<br/>

			<div class="table-responsive">
				<table class="table table-condensed">
					<tbody>
						<tr>
							<th>ID</th>
							<th>{{ $customer->id }}</th>
						</tr>
                        <tr>
                            <th>Nama</th>
                            <th>{{ $customer->nama }}</th>
                        </tr>
                        <tr>
                            <th>No Telp</th>
                            <th>{{ $customer->no_telp }}</th>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <th>{{ $customer->alamat }}</th>
                        </tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Deposit</h3>
        </div>
        <div class="panel-body">
            <table class="table tabl-hover">
                <thead>
                    <th>#</th>
                    <th>Tanggal</th>
                    <th>Deposit</th>
                    <th>Keterangan</th>
                </thead>
                <tbody>
                    @php $i=1; @endphp
                    @foreach($deposit as $dep)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ substr($dep->created_at,0,10) }}</td>
                        <td>{{ $dep->deposit }}</td>
                        <td>{{ $dep->note }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('script')
<script type="text/javascript">
    function deleteData(idnya){
        var conf = confirm("Delete Data?");
        if(conf){
            $.ajax({
                type: "GET",
                url: "{{url('').'/admin/deletedata/'}}"+idnya+"/delete",
                success: function(){
                    location.reload();  
                }
            });
        }
    }
</script>
@endpush