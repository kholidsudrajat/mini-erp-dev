@extends('layouts.app.frame')
@section('title', 'Edit Metode Pembayaran')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('payment_method.edit', $payment_method))
@section('button', '<a href="'.url('/admin/payment_method').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')


    {!! Form::model($payment_method, [
            'method' => 'PATCH',
            'url' => ['/admin/payment_method', $payment_method->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}

        @include ('admin.payment_method.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');

});
</script>
@endpush