@extends('layouts.app.frame')
@section('title', 'Create New Metode Pembayaran')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('payment_method.new'))
@section('button', '<a href="'.url('/admin/payment_method').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/payment_method', 'id' => 'formValidate', 'files' => true, 'novalidate' => '-']) !!}

		@include ('admin.payment_method.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');
});
</script>
@endpush
