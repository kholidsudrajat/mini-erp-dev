@extends('layouts.app.frame')
@section('title', 'Create New Salesman')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('salesman.new'))
@section('button', '<a href="'.url('/admin/salesman').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/salesman', 'id' => 'formValidate', 'files' => true, 'novalidate' => '-']) !!}

		@include ('admin.salesman.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');
});
</script>
@endpush
