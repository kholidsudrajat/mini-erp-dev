@extends('layouts.app.frame')
@section('title', 'Form Pembelian')
@section('description', 'Form Pembelian')
@section('breadcrumbs', Breadcrumbs::render('order_form'))
@section('button', '<div class="clearfix"></div>')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Form Pembelian</h3>
    </div>
    <div class="panel-body">
        <hr/>
        <div class="form-group col-md-6">
            <label>Nomor PO</label>
            <input type="text" class="form-control" name="po_number" value="PO-{{ date('YmdHis') }}" id="po_number" placeholder="Nomor PO" />
        </div>
        <div class="form-group col-md-3">
            <label>Supplier</label>
            {!! Form::select('supplier_id', $supplier, null, ['class' => 'form-control', 'data-init-plugin' => 'select2', 'id' => 'supplier_id']) !!}
        </div>
        <div class="form-group col-md-3">
            <b style="cursor:pointer; margin-top: 25px;" data-toggle="collapse" data-target="#formsupplier" class="btn btn-success">Tambah Supplier Baru</b>
            <br>
        </div>
        <div class="row clearfix collapse" id="formsupplier">
            <br>
            <br>
            <div class="col-sm-12 col-xs-12">
                <div class="form-group form-group-default">
                    <label>Nama Supplier</label>
                    <input type="text" id="name_supplier" class="form-control" />
                </div>
                <div class="form-group form-group-default">
                    <label>No Telp Supplier</label>
                    <input type="text" id="notelp_supplier" class="form-control" />
                </div>
                <div class="form-group form-group-default">
                    <label>Email Supplier</label>
                    <input type="text" id="email_supplier" class="form-control" />
                </div>
                <div class="form-group form-group-default">
                    <label>PIC Supplier</label>
                    <input type="text" id="pic_supplier" class="form-control" />
                </div>
                <div class="form-group form-group-default">
                    <label>Alamat Supplier</label>
                    <input type="text" id="alamat_supplier" class="form-control" />
                </div>
                <div class="pull-right">
                    <a class="btn btn-success" onclick="return addSupplier()"><i class="fa fa-plus"></i></a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <hr/>
        <div class="table-responsive">
            <h5>List Produk</h5>
            <table class="table table-bordered">
                <thead>
                    <th width="30%">Produk</th>
                    <th width="10%">Qty</th>
                    <th width="20%">Harga</th>
                    <th width="15%">Jumlah</th>
                    <th width="25%">Tindakan</th>
                </thead>
                <tbody id="tab">
                    <tr>
                        <td>
                            {!! Form::select('model_id', $model, null, ['class' => 'form-control', 'data-init-plugin' => 'select2', 'id' => 'model_id']) !!}
                        </td>
                        <td>
                            {!! Form::number('qty', 1, ['class' => 'form-control', 'id' => 'qty', 'min' => '1']) !!}
                        </td>
                        <td>
                            {!! Form::text('price', 0, ['class' => 'form-control', 'id' => 'price']) !!}
                        </td>
                        <td>
                            <b id="total">0</b>
                        </td>
                        <td>
                            <b style="cursor:pointer;" data-toggle="collapse" data-target="#formProduk" class="btn btn-success">Produk Baru</b>
                            <a class="btn btn-primary" id="add"><i class="fa fa-plus"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <textarea style="display:none;" id="json"></textarea>
        </div>
        <div class="row clearfix collapse" id="formProduk">
            <div class="col-sm-12 col-xs-12">
                <div class="form-group form-group-default">
                    <label>Nama Produk</label>
                    <input type="text" id="name_produk" class="form-control" />
                </div>
                <div class="form-group form-group-default">
                    <label>Kode Produk</label>
                    <input type="text" id="kode_produk" class="form-control" />
                </div>
                <div class="form-group form-group-default">
                    <label>Kategori Produk</label>
                    <input type="text" id="kategori_produk" class="form-control" />
                </div>
                <div class="form-group form-group-default">
                    <label>Gambar</label>
                    {!! Form::file('images_produk', ['class' => 'form-control', 'required' => '', 'id' => 'images_produk']) !!}
                </div>
                <div class="form-group form-group-default">
                    <label>Keterangan Produk</label>
                    <input type="text" id="info_produk" class="form-control" />
                </div>
                <div class="panel-heading" style="padding-left:4px;">
                    <div class="panel-title">
                        Ongkos
                    </div>
                </div>
                <hr>
                @foreach($cost as $costs)
                    <div style="display:none;" aria-required="true" class="col-md-6 form-group form-group-default {{ $errors->has($costs['id']) ? 'has-error' : ''}}">
                        @php $mc = 0; @endphp
                        @if(isset($modelcost))
                            @foreach($modelcost as $mcs)
                                @if($mcs['cost_id'] == $costs['id'])
                                    @php $mc = $mcs['costs']; @endphp
                                @endif
                            @endforeach
                        @endif
                        {!! Form::label($costs['id'], $costs['name']) !!}
                        {!! Form::text($costs['id'], $mc, ['id' => 'cost'.$costs['id'], 'class' => 'form-control']) !!}
                    </div>
                    {!! $errors->first($costs['id'], '<label class="error">:message</label>') !!}
                @endforeach
                <div aria-required="true" class="col-md-6 form-group required form-group-default {{ $errors->has('total_cost') ? 'has-error' : ''}}">
                    {!! Form::label('total_cost', 'Modal Dasar') !!}
                    {!! Form::text('total_cost', null, ['id' => 'total_cost', 'class' => 'form-control']) !!}
                </div>
                <div aria-required="true" class="col-md-6 form-group required form-group-default {{ $errors->has('price') ? 'has-error' : ''}}">
                    {!! Form::label('price', 'Harga') !!}
                    {!! Form::text('price', null, ['id' => 'price_produk', 'class' => 'form-control', 'required' => '']) !!}
                </div>
                <div aria-required="true" class="col-md-6 form-group form-group-default {{ $errors->has('diff') ? 'has-error' : ''}}">
                    {!! Form::label('diff', 'Selisih', ['id' => 'diff-style']) !!}
                    {!! Form::text('diff', null, ['id' => 'diff_produk', 'readonly' => '', 'class' => 'form-control']) !!}
                </div>
            </div>
            <br>
            <div class="pull-left" style="margin-left: 1.5%;">
                <a class="btn btn-success" onclick="return addProduk()"><i class="fa fa-plus"></i></a>
                <br>
            </div>
        </div>
        <div class="pull-right">
            <h5>Total : <b id="totals">0</b></h5>
        </div>
        <div class="clearfix"></div>
        <h5>Shipping</h5>
        <hr/>
        <div class="form-group col-md-12">
            <div class="radio radio-success">
                <input type="radio" checked value="pending" name="method" id="pending">
                <label for="pending">Tunda</label>
                <input type="radio" value="pickup" name="method" id="pickup">
                <label for="pickup">Pick-Up</label>
                <input type="radio" value="kirim" name="method" id="ship">
                <label for="ship">Kirim</label>
            </div>
        </div>
        <br/>
        <div id="shipm" style="display:none">
            <div class="form-group col-md-6">
                <b style="cursor:pointer" data-toggle="collapse" data-target="#formekspedisi" class="btn btn-success">Ekspedisi Baru</b>
                <div class="row clearfix collapse" id="formekspedisi">
                    <br>
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group form-group-default">
                            <label>Nama Ekspedisi</label>
                            <input type="text" id="name_shipping" class="form-control" />
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-success" onclick="return addShipping()"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <label>Pilih Ekspedisi</label>
                <select class="full-width" data-init-plugin="select2" id="shipping">
                    @foreach($shipping as $shipp)
                        <option value="{{ $shipp->id }}">{{ $shipp->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="clearfix"></div>
        <br/>
        <h5>Payment</h5>
        <hr/>
        <div class="form-group col-md-12">
            <div class="radio radio-success">
                @php $i = 1; @endphp
                @foreach($payment as $pay)
                <input type="radio" {{ $i == 1 ? 'checked' : '' }} value="{{ $pay->id }}" name="payment" id="{{ str_slug($pay->name) }}" />
                <label for="{{ str_slug($pay->name) }}">{{ $pay->name }}</label>
                @php $i++; @endphp
                @endforeach
            </div>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" onclick="submit()">Submit</a>
        </div>
    </div>
</div>
@endsection

@push("script")
<script type="text/javascript">
    $('input[name=method]').on('change', function(){
        Pace.restart();
        if($('#ship').is(':checked'))
            $('#shipm').show();
        else
            $('#shipm').hide();
    });
    
    $('#price').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    
    $('#model_id').on('change', function(){        
        var model = $('#model_id :selected').val();
        
        $.ajax({
            type    : 'GET',
            url     : '{{ url("admin/pembelian/form/getmodel") }}',
            data    : {
                model   : model
            },
            success : function(res){
                $('#price').val(res);
                $('#qty').val(1);
                $('#total').html(parseFloat(res).toLocaleString(2).replace(/,/g, '.'));
            }
        })
    });
    
    $('#qty').add('#price').on('change', function(){
        var qty = parseInt($('#qty').val());
        var price = parseInt($('#price').val().replace(/,/g, ''));
        
        if(qty < 0)
            qty = 0;
        if(price < 0)
            price = 0;
        
        var total = qty * price;
        
        $('#qty').val(qty);
        $('#price').val(price);
        $('#total').html(total.toLocaleString(2).replace(/,/g, '.'));
    });
    
    $('#qty').add('#price').keypress(function(e){
        if(e.which == 13){
            $('#add').click();
            return false;
        }
    });
    
    $('#add').on('click', function(){
        var produk = $('#model_id :selected').text();
        var prod = $('#model_id :selected').val();
        var qty = $('#qty').val();
        var price = parseInt($('#price').val().replace(/,/g, ''));
        var total = parseInt(qty) * price;
        var totals = parseInt($('#totals').html().replace(/\./g,''));
        totals = totals + total;
        var tab = '';
        
        if($('#row_'+prod).length == 0 && prod != '-' && qty > 0 && price > 0){
            var json = '{&quot;model_id&quot; : '+prod+',&quot;qty&quot; : '+qty+',&quot;price&quot; : '+price+',&quot;total&quot; : '+total+'},';
            var jsons = '{"model_id" : '+prod+',"qty" : '+qty+',"price" : '+price+',"total" : '+total+'},';

            tab += '<tr id="row_'+prod+'"><td>'+produk+'</td><td>'+qty+'</td><td>'+price.toLocaleString(2).replace(/,/g, '.')+'</td>';
            tab += '<td id="total_'+prod+'">'+total.toLocaleString(2).replace(/,/g,'.')+'</td><td><a class="btn btn-danger" onclick="remove('+prod+',&#39;'+json+'&#39;)"><i class="fa fa-times"></i></a></td></tr>';
            
            $('#json').val($('#json').val() + jsons);
            $('#tab').append(tab);
            $('#totals').html(totals.toLocaleString(2).replace(/,/g, '.'));
            
            $('#model_id').select2().select2('val','0');
            $('#qty').val(0);
            $('#price').val(0);
            $('#total').html(0);
        }
    });

    function addShipping(){
        if($('#name_shipping').val() == ''){
            alert('Ekspedisi tidak boleh kosong');
            return false;
        } else {
            $.ajax({
                url: '{{url('/')}}/admin/shipping/tambah',
                type: 'POST',
                data: {
                    name: $('#name_shipping').val(),
                    _token: '{{ csrf_token() }}',
                    cache: "false"
                },
                success: function (data) {
                    console.log(data);
                    window.location.reload();
                }
            });
        }
    }

    function addSupplier(){
        if($('#name_supplier').val() == ''){
            alert('Nama Supplier tidak boleh kosong');
            return false;
        }else if($('#notelp_supplier').val() == ''){
            alert('No Telp Supplier tidak boleh kosong');
            return false;
        }else if($('#email_supplier').val() == ''){
            alert('Email Supplier tidak boleh kosong');
            return false;
        } else if($('#pic_supplier').val() == ''){
            alert('PIC Supplier tidak boleh kosong');
            return false;
        } else if($('#alamat_supplier').val() == ''){
            alert('Alamat Supplier tidak boleh kosong');
            return false;
        }else {
            $.ajax({
                url: '{{url('/')}}/admin/supplier/tambah',
                type: 'POST',
                data: {
                    nama: $('#name_supplier').val(),
                    no_telp: $('#notelp_supplier').val(),
                    email: $('#email_supplier').val(),
                    pic: $('#pic_supplier').val(),
                    alamat: $('#alamat_supplier').val(),
                    _token: '{{ csrf_token() }}',
                    cache: "false"
                },
                success: function (data) {
                    console.log(data);
                    window.location.reload();
                }
            });
        }
    }

    function addProduk(){
        if($('#name_produk').val() == ''){
            alert('Nama Produk tidak boleh kosong');
            return false;
        }else if($('#kode_produk').val() == ''){
            alert('Kode Produk tidak boleh kosong');
            return false;
        }else if($('#kategori_produk').val() == ''){
            alert('Kategori Produk tidak boleh kosong');
            return false;
        }else if($('#images_produk')[0].files[0] == ''){
            alert('Images Produk tidak boleh kosong');
            return false;
        }else if($('#total_cost').val() == ''){
            alert('Modal Dasar Produk tidak boleh kosong');
            return false;
        }else if($('#price_produk').val() == ''){
            alert('Harga Produk tidak boleh kosong');
            return false;
        }else {
            var formData = new FormData();
            formData.append('name', $('#name_produk').val());
            formData.append('model_name', $('#kode_produk').val());
            formData.append('category', $('#kategori_produk').val());
            formData.append('images', $('input[type=file]')[0].files[0]);
            formData.append('info', $('#info_produk').val());
            formData.append('total_cost', $('#total_cost').val());
            formData.append('price', $('#price_produk').val());
            formData.append('diff', $('#diff_produk').val());
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('cache', 'false');
            $.ajax({
                url: '{{url('/')}}/admin/model/tambah',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                mimeType: "multipart/form-data",
                success: function (data) {
                    console.log(data);
                    window.location.reload();
                }
            });
        }
    }
    
    function remove(id, str){
        var olds = $('#json').val();
        var news = olds.replace(str, '');
        var old_total = parseInt($('#totals').html().replace(/\./g,''));
        var new_total = old_total - parseInt($('#total_'+id).html().replace(/\./g,''));
        new_total = new_total.toLocaleString(2).replace(/,/g, '.');
        
        $('#row_'+id).remove();
        $('#json').val(news);
        $('#totals').html(new_total);
    }
    
    function submit(){
        var nopo = $('#po_number').val();
        var supp = $('#supplier_id :selected').val();
        var json = $('#json').val();
        var total= parseInt($('#totals').html().replace(/\./g,''));
        var shid = $('input[name=method]').filter(":checked").val();
        var ship = $('#shipping :selected').val();
        if(shid != 'kirim')
            ship = 0;
        var paym = $('input[name=payment]').filter(":checked").val();
        
        if(nopo != "" && json != ""){
			console.log({
                    _token          : '{{ csrf_token() }}',
                    po_number       : nopo,
                    supplier_id     : supp,
                    shipping_id     : ship,
                    shipping_type   : shid,
                    payment_type    : paym,
                    total           : total,
                    json            : json
                });
            $.ajax({
                type    : 'POST',
                url     : '{{ url("") }}/admin/pembelian/form/submit',
                data    : {
                    _token          : '{{ csrf_token() }}',
                    po_number       : nopo,
                    supplier_id     : supp,
                    shipping_id     : ship,
                    shipping_type   : shid,
                    payment_type    : paym,
                    total           : total,
                    json            : json
                },
                success : function(res){
                    if(res == 0){
                        alert('Nomor PO sudah terdaftar!');
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                        $('#po_number').focus();
                    }else
                        location.reload();
                }
            })
        }else if(nopo == ""){
            alert('Nomor PO tidak boleh kosong!');
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $('#po_number').focus();
        }else{
            alert('Belum pilih produk!');
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }
    }

    $('#price_produk').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    $('#total_cost').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    $('#diff_produk').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });

    $('#price_produk').on('keyup', function(){
        var total_cost = $('#total_cost').val().replace(/,/g, '');
        var price = parseInt($('#price_produk').val().replace(/,/g, ''));
        var diff = price - total_cost;

        if(diff > 0){
            $('#diff-style').html('profit');
            $('#diff-style').css('color', 'green');
        }else{
            $('#diff-style').html('loss');
            $('#diff-style').css('color', 'red');
        }

        $('#diff_produk').val(diff);
    });

    var diff =  $('#diff_produk').val();
    $(document).ready(function(){
        if(diff >= 0){
            $('#diff-style').html('profit');
            $('#diff-style').css('color', 'green');
        }else{
            $('#diff-style').html('loss');
            $('#diff-style').css('color', 'red');
        }
    });
</script>
@endpush
