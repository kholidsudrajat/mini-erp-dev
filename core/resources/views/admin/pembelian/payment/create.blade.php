@extends('layouts.app.frame')
@section('title', 'Tambah Pembayaran')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('bayar-po'))
@section('button', '<a onclick="backs()" class="btn btn-info btn-xs no-border">Back</a>')

@section('content')
    {!! Form::open(['url' => '/admin/pembelian/bayar', 'id' => 'formValidate', 'files' => true]) !!}

		@include ('admin.pembelian.payment.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
    $(document).ready(function() {
        $('#formValidate').validate();
    });
    
    function backs(){
        var back = window.location.href.replace('/add','');
        window.location.href = back;
    }
    
    $('#total').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    
    $('#submit').on('click', function(){
        var total = parseInt($('#total').val().replace(/,/g,''));
        var inv_number = $('#inv').val();
        
        if(total <= {{$totalnya}} && inv_number != ""){
            $.ajax({
                type    : 'POST',
                url     : '{{ url("") }}/admin/pembelian/bayar/{{ $id }}/submit',
                data    : {
                    _token      : '{{ csrf_token() }}',
                    order_id    : '{{ $id }}',
                    inv_number  : inv_number,
                    total       : total
                },
                success : function(){
                    window.location.href = '{{ url("") }}/admin/pembelian/bayar/{{ $id }}';
                }
            });
        }else{
            $('#inv').focus();
        }
    });
</script>
@endpush
