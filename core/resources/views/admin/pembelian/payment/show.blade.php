@extends('layouts.app.frame')
@section('title', 'Pembayaran')
@section('description', '<div class="clearfix"></div>')
@section('breadcrumbs', Breadcrumbs::render('order.show', $order))
@section('button', '<a href="'.url('/admin/pembelian/bayar').'" class="btn btn-info btn-xs no-border">Back</a>')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Bayar</h3>
    </div>
    <div class="panel-body">
        <hr/>
        <div class="form-group col-md-6">
            <label>Nomor PO</label><br/>
            <b>{{ $order->po_number }}</b>
        </div>
        <div class="form-group col-md-6">
            <label>Supplier</label><br/>
            <b>{{ $supplier[$order->supplier_id] }}</b>
        </div>
        <div class="clearfix"></div>
        <hr/>
        <div class="table-responsive">
            <h5>List Produk</h5>
            <table class="table table-bordered">
                <thead>
                    <th width="30%">Produk</th>
                    <th width="10%">Qty</th>
                    <th width="25%">Price</th>
                    <th width="25%">Total</th>
                </thead>
                <tbody id="tab">
                    @foreach($detail as $dtl)
                    <tr id="row_{{ $dtl->model_id }}">
                        <td>{{ $dtl->name.' - ('.$dtl->model_name.')' }}</td>
                        <td>{{ $dtl->qty }}</td>
                        <td>{{ str_replace(',','.',number_format($dtl->price)) }}</td>
                        <td id="total_{{ $dtl->model_id }}">{{ str_replace(',','.',number_format($dtl->total)) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <textarea style="display:none;" id="json">
            @foreach($detail as $dtl)
            {"model_id" : {{ $dtl->model_id }}, "qty" : {{ $dtl->qty }}, "price" : {{ $dtl->price }}, "total" : {{ $dtl->total }}},
            @endforeach
            </textarea>
        </div>
        <div class="pull-right">
        </div>
        <div class="clearfix"></div>
        <h5>Shipping</h5>
        <hr/>
        <div class="form-group col-md-12">
            <div class="radio radio-success">
                <input disabled type="radio" {{ $order->shipping_type == 'pending' ? 'checked' : '' }} value="pending" name="method" id="pending">
                <label for="pending">Pending</label>
                <input disabled type="radio" {{ $order->shipping_type == 'pickup' ? 'checked' : '' }} value="pickup" name="method" id="pickup">
                <label for="pickup">Pick-Up</label>
                <input disabled type="radio" {{ $order->shipping_type == 'kirim' ? 'checked' : '' }} value="kirim" name="method" id="ship">
                <label for="ship">Kirim</label>
            </div>
        </div>
        <div class="clearfix"></div>
        <br/>
        <h5>Payment</h5>
        <hr/>
        <div class="form-group col-md-12">
            <div class="radio radio-success">
                @php $i = 1; @endphp
                @foreach($payment as $pay)
                <input disabled type="radio" {{ $order->payment_type == $pay->id ? 'checked' : '' }} value="{{ $pay->id }}" name="payment" id="{{ str_slug($pay->name) }}" />
                <label for="{{ str_slug($pay->name) }}">{{ $pay->name }}</label>
                @php $i++; @endphp
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">History Pembayaran</h3>
        <div class="clearfix"><hr/></div>
        @if($totalnya != 0)
        <a class="btn btn-primary" href="{{ url('') }}/admin/pembelian/bayar/{{ $order->id }}/add">Tambah Pembayaran</a>
        @endif
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <th width="20%">Tanggal</th>
                    <th>Nomor Invoice</th>
                    <th>Total</th>
                </thead>
                <tbody>
                @php $total = 0; @endphp
                @foreach($payments as $pay)
                    <tr>
                        <td>{{ $pay->created_at }}</td>
                        <td>{{ $pay->inv_number }}</td>
                        <td>{{ str_replace(',','.',number_format($pay->total)) }}</td>
                    </tr>
                @php $total += $pay->total; @endphp
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="pull-right col-md-4">
            <table width="100%">
                <tr>
                    <td width="50%"><h5>Total Pembelian</h5></td>
                    <td width="5%"> : </td>
                    <td width="45%" style="text-align:right;"><b id="totals">{{ str_replace(',','.',number_format($order->total)) }}</b></td>
                </tr>
                <tr>
                    <td width="50%"><h5>Total Bayar</h5></td>
                    <td width="5%"> : </td>
                    <td width="45%" style="text-align:right;"><b>{{ str_replace(',','.',number_format($total)) }}</b></td>
                </tr>
                @if($totalnya != 0)
                <tr>
                    <td width="50%"><h5>Sisa</h5></td>
                    <td width="5%"> : </td>
                    <td width="45%" style="text-align:right;"><b>{{ str_replace(',','.',number_format($totalnya)) }}</b></td>
                </tr>
                @else
                <tr>
                    <td width="50%"><h5>Status</h5></td>
                    <td width="5%"> : </td>
                    <td width="45%" style="text-align:right;"><b>Lunas</b></td>
                </tr>
                @endif
            </table>
        </div>
    </div>
</div>
@endsection

@push("script")
<script type="text/javascript">
    $(document).ready(function(){
        if($('#ship').is(':checked'))
            $('#shipm').show();
        else
            $('#shipm').hide(); 
    });
    
    $('#price').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
</script>
@endpush
