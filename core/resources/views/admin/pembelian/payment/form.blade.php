<div aria-required="true" class="form-group required form-group-default {{ $errors->has('inv_number') ? 'has-error' : ''}}">
    {!! Form::label('inv_number', 'Invoice Number') !!}
    {!! Form::text('inv_number', 'INV-'.date('YmdHis'), ['class' => 'form-control', 'required', 'placeholder' => 'Masukkan Invoice Number', 'id' => 'inv']) !!}
</div>
{!! $errors->first('inv_number', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group required form-group-default {{ $errors->has('total') ? 'has-error' : ''}}">
    {!! Form::label('total', 'Total') !!}
    {!! Form::text('total', $totalnya, ['class' => 'form-control', 'required', 'id' => 'total']) !!}
</div>
{!! $errors->first('total', '<label class="error">:message</label>') !!}

<a class="btn btn-success" id="submit">Save</a>