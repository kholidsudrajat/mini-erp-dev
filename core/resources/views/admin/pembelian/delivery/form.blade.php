<div aria-required="true" class="form-group required form-group-default {{ $errors->has('total') ? 'has-error' : ''}}">
    {!! Form::label('total', 'Total') !!}
    {!! Form::text('total', $totalnya, ['class' => 'form-control', 'required', 'id' => 'total']) !!}
</div>
{!! $errors->first('total', '<label class="error">:message</label>') !!}

<a class="btn btn-success" id="submit">Save</a>