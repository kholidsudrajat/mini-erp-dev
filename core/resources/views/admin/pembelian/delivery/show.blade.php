@extends('layouts.app.frame')
@section('title', 'Form Pengiriman')
@section('description', '<div class="clearfix"></div>')
@section('breadcrumbs', Breadcrumbs::render('delivery.show', $order))
@section('button', '<a href="'.url("").'/admin/pembelian/delivery" class="btn btn-info btn-xs no-border">Back</a>')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Form Pengiriman</h3>
    </div>
    <div class="panel-body">
        <hr/>
        <div class="form-group col-md-6">
            <label>Nomor PO</label><br/>
            {{ $order->po_number }}
        </div>
        <div class="form-group col-md-6">
            <label>Supplier</label><br/>
            {{ $supplier[$order->supplier_id] }}
        </div>
    </div>
    <div class="panel-heading">
        <h3 class="panel-title">History Pengiriman</h3>
        <div class="clearfix"><hr/></div>
        <a class="btn btn-primary" href="{{ url('') }}/admin/pembelian/delivery/{{ $order->id }}/add">Tambah Pengiriman</a>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <th>Tanggal</th>
                    <th>Nomor DO</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach($detail as $dtl)
                    <tr>
                        <td>{{ $dtl->created_at }}</td>
                        <td>{{ $dtl->do_number }}</td>
                        <td>
                            <a class="btn btn-primary" href="{{ url('') }}/admin/pembelian/delivery/{{ $order->id }}/{{ $dtl->id }}">Detail</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push("script")
<script type="text/javascript">
</script>
@endpush
