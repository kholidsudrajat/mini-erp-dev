@extends('layouts.app.frame')
@section('title', 'Form Pengiriman #'.$do->do_number)
@section('description', '<div class="clearfix"></div>')
@section('breadcrumbs', Breadcrumbs::render('delivery.show', $do))
@section('button', '<a href="'.url("").'/admin/pembelian/delivery/'.$do->id.'" class="btn btn-info btn-xs no-border">Back</a>')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group col-md-6">
            <label>Nomor PO</label><br/>
            {{ $do->po_number }}
        </div>
        <div class="form-group col-md-6">
            <label>Supplier</label><br/>
            {{ $supplier[$do->supplier_id] }}
        </div>
        <div class="form-group col-md-6">
            <label>Nomor Do</label><br/>
            {{ $do->do_number }}
        </div>
        <div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <th>Produk</th>
                    <th>Qty</th>
                    <th>Kirim Ke</th>
                    <th>Retur</th>
                </thead>
                <tbody>
                @foreach($detail as $dtl)
                    <tr>
                        <td>{{ $dtl->produk }}</td>
                        <td>{{ $dtl->qty }}</td>
                        <td>{{ $dtl->shop }}</td>
                        <td>{{ $dtl->retur }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push("script")
<script type="text/javascript">
</script>
@endpush
