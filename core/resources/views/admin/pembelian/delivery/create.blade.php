@extends('layouts.app.frame')
@section('title', 'Tambah Pengiriman')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('bayar-po'))
@section('button', '<a onclick="backs()" class="btn btn-info btn-xs no-border">Back</a>')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Form Pengiriman</h3>
    </div>
    <div class="panel-body">
        <hr/>
        <div class="form-group col-md-6">
            <label>Nomor PO</label><br/>
            {{ $order->po_number }}
        </div>
        <div class="form-group col-md-6">
            <label>Supplier</label><br/>
            {{ $supplier[$order->supplier_id] }}
        </div>
        <div class="form-group col-md-6">
            <label>Nomor DO</label><br/>
            <input class="form-control" id="do_number" value="DO-{{ date('YmdHis') }}" placeholder="Masukkan Nomor DO" />
        </div>
        <div class="clearfix"></div>
        <hr/>
        <div class="table-responsive">
            <h5>List Produk</h5>
            <table class="table table-bordered">
                <thead>
                    <th>Produk</th>
                    <th width="15%">Qty</th>
                    <th width="30%">Kirim Ke</th>
                    <th width="15%">Retur</th>
                    <th width="15%">Belum Diterima</th>
                </thead>
                <tbody id="tab">
                    @foreach($detail as $dtl)
                    <tr id="row_{{ $dtl->model_id }}">
                        <td>{{ $dtl->name.' - ('.$dtl->model_name.')' }}</td>
                        <td>
                            <input class="form-control" onchange="changes({{ $dtl->model_id }})" id="qty_{{ $dtl->model_id }}" type="number" value="{{ $sisa[$dtl->model_id] }}" />
                            <input id="qtys_{{ $dtl->model_id }}" hidden value="{{ $sisa[$dtl->model_id] }}" />
                        </td>
                        <td>
                            {!! Form::select('shop_id', $shop, null, ['class' => 'form-control', 'data-init-plugin' => 'select2', 'id' => 'shop_'.$dtl->model_id]) !!}
                        </td>
                        <td><input class="form-control" onchange="changes({{ $dtl->model_id }})" id="retur_{{ $dtl->model_id }}" type="number" value="0" /></td>
                        <td id="sisa_{{ $dtl->model_id }}">0</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <div class="pull-right">
            <a class="btn btn-primary" id="submit">Submit</a>
        </div>
    </div>
</div>
@endsection

@push("script")
<script>
    $(document).ready(function() {
        $('#formValidate').validate();
    });
    
    function backs(){
        var back = window.location.href.replace('/add','');
        window.location.href = back;
    }
    
    function changes(id){
        Pace.restart();
        var qty = parseInt($('#qty_'+id).val());
        var retur = parseInt($('#retur_'+id).val());
        var qtys = parseInt($('#qtys_'+id).val());
        var sisa = qtys - (qty + retur);
        
        $('#sisa_'+id).html(sisa);
    }
    
    $('#submit').on('click', function(){
        var json = '';
        var do_number = $('#do_number').val();
        
        @foreach($detail as $dtl)
        var qty = $('#qty_{{ $dtl->model_id }}').val();
        var shop = $('#shop_{{ $dtl->model_id }} :selected').val();
        var retur = $('#retur_{{ $dtl->model_id }}').val();
        
        json += '{"model_id" : {{ $dtl->model_id }}, "qty" : '+qty+', "shop_id" : '+shop+', "retur" : '+retur+'},';
        @endforeach
        
        console.log(json);
        
        if(do_number != ""){
            $.ajax({
                type    : 'POST',
                url     : '{{ url("") }}/admin/pembelian/delivery/{{ $id }}/submit',
                data    : {
                    _token      : '{{ csrf_token() }}',
                    order_id    : '{{ $id }}',
                    do_number   : do_number,
                    json        : json
                },
                success : function(){
                    window.location.href = '{{ url("") }}/admin/pembelian/delivery/{{ $id }}';
                }
            });
        }
    });
</script>
@endpush
