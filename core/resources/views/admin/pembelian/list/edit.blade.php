@extends('layouts.app.frame')
@section('title', 'Edit Form Pembelian')
@section('description', 'Edit Form Pembelian')
@section('breadcrumbs', Breadcrumbs::render('order.edit', $order))
@section('button', '<a href="'.url('/admin/order').'" class="btn btn-info btn-xs no-border">Back</a>')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Edit Form Pembelian</h3>
    </div>
    <div class="panel-body">
        <hr/>
        <div class="form-group col-md-6">
            <label>Nomor PO</label>
            <input type="text" class="form-control" name="po_number" id="po_number" placeholder="Nomor PO" value="{{$order->po_number}}" />
        </div>
        <div class="form-group col-md-6">
            <label>Supplier</label>
            {!! Form::select('supplier_id', $supplier, $order->supplier_id, ['class' => 'form-control', 'data-init-plugin' => 'select2', 'id' => 'supplier_id']) !!}
        </div>
        <div class="clearfix"></div>
        <hr/>
        <div class="table-responsive">
            <h5>List Produk</h5>
            <table class="table table-bordered">
                <thead>
                    <th width="25%">Produk</th>
                    <th width="10%">Qty</th>
                    <th width="25%">Price</th>
                    <th width="25%">Total</th>
                    <th width="15%">Action</th>
                </thead>
                <tbody id="tab">
                    <tr>
                        <td>
                            {!! Form::select('model_id', $model, null, ['class' => 'form-control', 'data-init-plugin' => 'select2', 'id' => 'model_id']) !!}
                        </td>
                        <td>
                            {!! Form::number('qty', 0, ['class' => 'form-control', 'id' => 'qty', 'min' => '1']) !!}
                        </td>
                        <td>
                            {!! Form::text('price', 0, ['class' => 'form-control', 'id' => 'price']) !!}
                        </td>
                        <td>
                            <b id="total">0</b>
                        </td>
                        <td>
                            <a class="btn btn-primary" id="add"><i class="fa fa-plus"></i></a>
                        </td>
                    </tr>
                    @foreach($detail as $dtl)
                    <tr id="row_{{ $dtl->model_id }}">
                        <td>{{ $dtl->name.' - ('.$dtl->model_name.')' }}</td>
                        <td>{{ $dtl->qty }}</td>
                        <td>{{ str_replace(',','.',number_format($dtl->price)) }}</td>
                        <td id="total_{{ $dtl->model_id }}">{{ str_replace(',','.',number_format($dtl->total)) }}</td>
                        <td>
                            <a class="btn btn-primary" onclick="edit({{$dtl->model_id}})"><i class="fa fa-pencil"></i></a>
                            <a class="btn btn-danger" onclick="remove({{$dtl->model_id}})"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    <tr id="rows_{{ $dtl->model_id }}" style="display:none;">
                        <td>{{ $dtl->name.' - ('.$dtl->model_name.')' }}</td>
                        <td>
                            {!! Form::number('qty', $dtl->qty, ['class' => 'form-control', 'id' => 'qty'.$dtl->model_id, 'min' => '1', 'onchange' => 'change('.$dtl->model_id.')']) !!}
                        </td>
                        <td>
                            {!! Form::text('price', $dtl->price, ['class' => 'form-control price', 'id' => 'price'.$dtl->model_id, 'onchange' => 'change('.$dtl->model_id.')']) !!}
                        </td>
                        <td id="totals_{{ $dtl->model_id }}">{{ str_replace(',','.',number_format($dtl->total)) }}</td>
                        <td>
                            <a class="btn btn-success" onclick="update('{{ $dtl->model_id }}')"><i class="fa fa-check"></i></a>
                            <a class="btn btn-danger" onclick="cancel('{{ $dtl->model_id }}','{{$dtl->qty}}','{{$dtl->price}}')"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <textarea style="display:none;" id="json">
            @foreach($detail as $dtl)
            {"model_id" : {{ $dtl->model_id }}, "qty" : {{ $dtl->qty }}, "price" : {{ $dtl->price }}, "total" : {{ $dtl->total }}},
            @endforeach
            </textarea>
        </div>
        <div class="pull-right">
            <h5>Total : <b id="totals">{{ str_replace(',','.',number_format($order->total)) }}</b></h5>
        </div>
        <div class="clearfix"></div>
        <h5>Shipping</h5>
        <hr/>
        <div class="form-group col-md-12">
            <div class="radio radio-success">
                <input type="radio" {{ $order->shipping_type == 'pending' ? 'checked' : '' }} value="pending" name="method" id="pending">
                <label for="pending">Pending</label>
                <input type="radio" {{ $order->shipping_type == 'pickup' ? 'checked' : '' }} value="pickup" name="method" id="pickup">
                <label for="pickup">Pick-Up</label>
                <input type="radio" {{ $order->shipping_type == 'kirim' ? 'checked' : '' }} value="kirim" name="method" id="ship">
                <label for="ship">Kirim</label>
            </div>
        </div>
        <br/>
        <div id="shipm" style="display:none">
            <div class="form-group col-md-6">
                <label>Pilih Ekspedisi</label>
                <select class="full-width" data-init-plugin="select2" id="shipping">
                    @foreach($shipping as $shipp)
                        <option value="{{ $shipp->id }}">{{ $shipp->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="clearfix"></div>
        <br/>
        <h5>Payment</h5>
        <hr/>
        <div class="form-group col-md-12">
            <div class="radio radio-success">
                @php $i = 1; @endphp
                @foreach($payment as $pay)
                <input type="radio" {{ $order->payment_type == $pay->id ? 'checked' : '' }} value="{{ $pay->id }}" name="payment" id="{{ str_slug($pay->name) }}" />
                <label for="{{ str_slug($pay->name) }}">{{ $pay->name }}</label>
                @php $i++; @endphp
                @endforeach
            </div>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" onclick="save()">Save</a>
        </div>
    </div>
</div>
@endsection

@push("script")
<script type="text/javascript">
    $(document).ready(function(){
        if($('#ship').is(':checked'))
            $('#shipm').show();
        else
            $('#shipm').hide(); 
    });
    
    $('input[name=method]').on('change', function(){
        Pace.restart();
        if($('#ship').is(':checked'))
            $('#shipm').show();
        else
            $('#shipm').hide();
    });
    
    $('#price').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    
    $('.price').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    
    $('#model_id').on('change', function(){
        $('#qty').val(0);
        $('#price').val(0);
        $('#total').html(0);
    });
    
    $('#qty').add('#price').on('change', function(){
        var qty = parseInt($('#qty').val());
        var price = parseInt($('#price').val().replace(/,/g, ''));
        
        if(qty < 0)
            qty = 0;
        if(price < 0)
            price = 0;
        
        var total = qty * price;
        
        $('#qty').val(qty);
        $('#price').val(price);
        $('#total').html(total.toLocaleString(2).replace(/,/g, '.'));
    });
    
    $('#add').on('click', function(){
        var id = $('#model_id :selected').val();
        var qty = parseInt($('#qty').val());
        var price = parseInt($('#price').val().replace(/,/g, ''));
        
        if($('#row_'+id).length == 0 && id > 0 && qty > 0 && price > 0){
            $.ajax({
                type    : 'POST',
                url     : '{{ url("") }}/admin/order/adddetail/{{ $order->id }}',
                data    : {
                    _token      : '{{ csrf_token() }}',
                    model_id    : id,
                    qty         : qty,
                    price       : price,
                },
                success : function(){
                    location.reload();
                }
            });
        }
    });
    
    function edit(id){
        $('#row_'+id).hide();
        $('#rows_'+id).show();
    }
    
    function cancel(id,qty,price){
        var total = qty * price;
        
        $('#qty'+id).val(qty);
        $('#price'+id).val(price);
        $('#totals_'+id).html(total.toLocaleString(2).replace(/,/g, '.'));
        
        $('#rows_'+id).hide();
        $('#row_'+id).show();
    }
    
    function change(id){
        var qty = parseInt($('#qty'+id).val());
        var price = parseInt($('#price'+id).val().replace(/,/g, ''));
        
        if(qty < 0)
            qty = 0;
        if(price < 0)
            price = 0;
        
        var total = qty * price;
        
        $('#qty'+id).val(qty);
        $('#price'+id).val(price);
        $('#totals_'+id).html(total.toLocaleString(2).replace(/,/g, '.'));
    }
    
    function update(id){
        var qty = parseInt($('#qty'+id).val());
        var price = parseInt($('#price'+id).val().replace(/,/g, ''));
        
        $.ajax({
            type    : 'POST',
            url     : '{{ url("") }}/admin/order/updatedetail/{{ $order->id }}',
            data    : {
                _token  : '{{ csrf_token() }}',
                model_id: id,
                qty     : qty,
                price   : price,
            },
            success : function(){
                location.reload();
            }
        });
    }
    
    function remove(id){
        $.ajax({
            type    : 'POST',
            url     : '{{ url("") }}/admin/order/deletedetail/{{ $order->id }}',
            data    : {
                _token  : '{{ csrf_token() }}',
                model_id: id
            },
            success : function(){
                location.reload();
            }
        });
    }
    
    function save(){
        var nopo = $('#po_number').val();
        var supp = $('#supplier_id :selected').val();
        var json = $('#json').val();
        var total= parseInt($('#totals').html().replace(/\./g,''));
        var shid = $('input[name=method]').filter(":checked").val();
        var ship = $('#shipping :selected').val();
        if(shid != 'kirim')
            ship = 0;
        var paym = $('input[name=payment]').filter(":checked").val();
        
        if(nopo != "" && supp != "" && json != ""){
            $.ajax({
                type    : 'POST',
                url     : '{{ url("") }}/admin/order/update/{{ $order->id }}',
                data    : {
                    _token          : '{{ csrf_token() }}',
                    po_number       : nopo,
                    supplier_id     : supp,
                    shipping_id     : ship,
                    shipping_type   : shid,
                    payment_type    : paym,
                    total           : total,
                    json            : json
                },
                success : function(){
                    location.reload();
                }
            })
        }
    }
</script>
@endpush
