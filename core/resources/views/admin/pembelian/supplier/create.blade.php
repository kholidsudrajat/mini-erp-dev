@extends('layouts.app.frame')
@section('title', 'Create New Supplier')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('supplier_order.new'))
@section('button', '<a href="'.url('/admin/pembelian/supplier').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/pembelian/supplier', 'id' => 'formValidate', 'files' => true]) !!}

		@include ('admin.pembelian.supplier.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();

});
</script>
@endpush
