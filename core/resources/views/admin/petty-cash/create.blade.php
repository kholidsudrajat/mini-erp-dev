@extends('layouts.app.frame')
@section('title', 'Create New Ongkos')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('cost.new'))
@section('button', '<a href="'.url('/admin/cost').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/cost', 'id' => 'formValidate', 'files' => true, 'novalidate' => '-']) !!}

		@include ('admin.cost.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');
});
</script>
@endpush
