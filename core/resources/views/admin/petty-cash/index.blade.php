@extends('layouts.app.frame')
@section('title', 'Uang Kas')
@section('description', 'Uang Kas')
@section('breadcrumbs', Breadcrumbs::render('petty-cash'))
@section('button', '<div class="clearfix"></div>')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <form method="post" action="{{ url('') }}/admin/petty-cash" enctype="multipart/form-data">
                <input hidden name="_token" value="{{csrf_token()}}" />
                <div class="form-group form-group-default form-group-default-select2 required col-md-6">
                    <label>Tipe</label>
                    <select class="full-width" data-init-plugin="select2" name="type" id="tipe">
                        <option value="in">Pemasukkan</option>
                        <option value="out">Pengeluaran</option>
                    </select>
                </div>
                <div class="clearfix"></div>
                <div class="form-group form-group-default required col-md-3">
                    <label>Tanggal</label>
                    <input class="form-control" value="{{ date('Y/m/d') }}" type="text" id="date" name="date" />
                </div>
                <div class="form-group form-group-default required col-md-3">
                    <label>Biaya</label>
                    <input type="text" class="form-control" name="cost" id="cost" value="0" />
                </div>
                <div class="form-group form-group-default required col-md-3">
                    <label>Keterangan</label>
                    <input type="text" class="form-control" name="description" />
                </div>
                <div class="form-group form-group-default col-md-3" id="files" style="display:none; ">
                    <label>File</label>
                    <input type="file" class="form-control" name="files" />
                </div>
                <div class="clearfix"></div>
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
    <input type="hidden" id="drs" name="drange"/>
    <input type="hidden" id="did" name="did"/>
    <div class="form-group-attached">
        <div class="row clearfix">
            <div class="col-sm-6 col-xs-12">
                <div class="form-group form-group-default">
                    <label>Pencarian</label>
                    <form id="formsearch">
                        <input type="text" id="search-table" class="form-control" name="firstName" placeholder="Masukkan Keyword">
                    </form>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group form-group-default">
                    <label>Tanggal Mulai</label>
                    <input type="text" id="datepicker-start" class="form-control" name="firstName" placeholder="pilih tanggal">
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group form-group-default">
                    <label>Tanggal Berakhir</label>
                    <input type="text" id="datepicker-end" class="form-control" name="firstName" placeholder="pilih tanggal">
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <table class="table table-hover" id="petty-cash-table">
        <thead>
            <tr>
                <th> Tanggal </th><th> Pemasukkan </th><th> Pengeluaran </th><th width="20%"> Tindakan </th>
            </tr>
        </thead>
    </table>

@endsection


@push("script")
<script>

var oTable;
oTable = $('#petty-cash-table').DataTable({
    processing: true,
    serverSide: true,
    dom: 'lBfrtip',
    order:  [[ 0, "asc" ]],
    buttons: [
        {
            extend: 'print',
            autoPrint: true,
            customize: function ( win ) {
                $(win.document.body)
                    .css( 'padding', '2px' )
                    .prepend(
                        '<font style="font-size:14px;margin-top: 5px;margin-bottom:20px;"> Laporan Uang Kas </font><br/><br/><font style="font-size:8px;margin-top:15px;">{{date('Y-m-d h:i:s')}}</font></h5><br/><br/>'
                    );


                $(win.document.body).find( 'div' )
                    .css( {'padding': '2px', 'text-align': 'center', 'margin-top': '-50px'} )
                    .prepend(
                        ''
                    );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( { 'font-size': '9px', 'padding': '2px' } );


            },
            title: '',
            orientation: 'landscape',
            exportOptions: {columns: ':visible'} ,
            text: '<i class="fa fa-print" data-toggle="tooltip" title="" data-original-title="Print"></i>'
        },
        {extend: 'colvis', text: '<i class="fa fa-eye" data-toggle="tooltip" title="" data-original-title="Column visible"></i>'},
        {extend: 'csv', text: '<span>CSV</span>'}
    ],
    sDom: "<'table-responsive fixed't><'row'<p i>> B",
    sPaginationType: "bootstrap",
    destroy: true,
    responsive: true,
    scrollCollapse: true,
    oLanguage: {
        "sLengthMenu": "_MENU_ ",
        "sInfo": "Menampilkan <b>_START_ to _END_</b> dari _TOTAL_ entri"
    },
    lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
    ajax: {
    url: 
        '{!! route('petty-cash.data') !!}'
    ,
        data: function (d) {
            d.range = $('input[name=drange]').val();
        }
    },
    columns: [
        { data: "date", name: "date" },
        { data: "cost_in", name: "cost_in" },
        { data: "cost_out", name: "cost_out" },
        { data: "action", name: "action", orderable: false , searchable: false , printable : false},
    ],
}).on( 'processing.dt', function ( e, settings, processing ) {if(processing){Pace.start();} else {Pace.stop();}});

$("#petty-cash-table_wrapper > .dt-buttons").appendTo("div.export-options-container");


$('#datepicker-start').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
    $(this).datepicker('hide');
    if($('#datepicker-end').val() != ""){
        $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
        oTable.draw();
    }else{
        $('#datepicker-end').focus();
    }

});
$('#datepicker-end').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
    $(this).datepicker('hide');
    if($('#datepicker-start').val() != ""){
        $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
        oTable.draw();
    }else{
        $('#datepicker-start').focus();
    }

});

$('#formsearch').submit(function () {
    oTable.search( $('#search-table').val() ).draw();
    return false;
} );

oTable.page.len(25).draw();



function deleteData(id) {
    $('#modalDelete').modal('show');
    $('#did').val(id);
}

function hapus(){
    $('#modalDelete').modal('hide');
    var id = $('#did').val();
    $.ajax({
        url: '{{url("admin/petty-cash")}}' + "/" + id + '?' + $.param({"_token" : '{{ csrf_token() }}' }),
        type: 'DELETE',
        complete: function(data) {
            oTable.draw();
        }
    });
}

$('#date').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
    $(this).datepicker('hide');
});
    
$('#tipe').on('change', function(){
    var tipe = $('#tipe :selected').val();
    if(tipe == 'in')
        $('#files').hide();
    else
        $('#files').show();
});
    
$('#cost').inputmask("numeric", {
    radixPoint: ".",
    groupSeparator: ",",
    digits: 2,
    autoGroup: true,
    rightAlign: false,
    oncleared: function () { self.Value(''); }
});
</script>
@endpush
