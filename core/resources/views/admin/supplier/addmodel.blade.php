@extends('layouts.app.frame')
@section('title', 'Tambah Motif')
@section('breadcrumbs', Breadcrumbs::render('supplier.new'))

@section('content')
    {!! Form::hidden('did', '', ['id' => 'did']); !!}
    {!! Form::open(['url' => '/admin/savemodelsupplier/', 'id' => 'formValidate', 'files' => true]) !!}
    <div class="form-group form-group-default">
        {!! Form::label('name', 'Motif'); !!}
        {!! Form::text('name', '', ['class' => 'form-control', 'required']); !!}
        {!! Form::hidden('supplier_id', $id); !!}
    </div>

    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Submit', ['type' => 'submit', 'class' => 'btn btn-success']) !!}
	{!! Form::close() !!}
    
    <div class="clearfix"></div>
    <hr/>
    <div class="panel-title">List Motif</div>
    <table class="table table-striped table-hover table-bordered ss">
        <thead>
            <th>#</th>
            <th>Nama</th>
            <th>Action</th>
        </thead>
        <tbody>
            @if(count($model) > 0)
                @php $i = 1; @endphp
                @foreach($model as $bhn)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $bhn['name'] }}</td>
                    <td><a onclick="return deleteData({{$bhn['id']}})" class="btn btn-danger">Delete</a></td>
                </tr>
                @endforeach
            @endif
        </tbody>
    </table>
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    
    $('p').remove();
    $('body').contents().find(".container-fluid.relative").remove();

});
    
    function deleteData(id) {
        $('#modalDelete').modal('show');
        $('#did').val(id);
    }

    function hapus(){
        $('#modalDelete').modal('hide');
        var id = $('#did').val();
        $.ajax({
            url: '{{url("admin/deletemodelsupplier")}}' + "/" + id,
            type: 'GET',
            complete: function(data) {
                location.reload();
            }
        });
    }
</script>
@endpush
