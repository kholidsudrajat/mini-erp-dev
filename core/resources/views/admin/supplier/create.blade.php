@extends('layouts.app.frame')
@section('title', 'Create New Supplier')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('supplier.new'))
@section('button', '<a href="'.url('/admin/supplier').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/supplier', 'id' => 'formValidate', 'files' => true]) !!}

		@include ('admin.supplier.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();

});
</script>
@endpush
