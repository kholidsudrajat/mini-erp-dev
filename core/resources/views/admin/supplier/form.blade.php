<div aria-required="true" class="form-group required form-group-default {{ $errors->has('nama') ? 'has-error' : ''}}">
    {!! Form::label('nama', 'Nama') !!}
    {!! Form::text('nama', null, ['class' => 'form-control', 'required']) !!}
</div>
{!! $errors->first('nama', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group required form-group-default {{ $errors->has('no_telp') ? 'has-error' : ''}}">
    {!! Form::label('no_telp', 'No Telp') !!}
    {!! Form::text('no_telp', null, ['class' => 'form-control', 'required']) !!}
</div>
{!! $errors->first('no_telp', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group required form-group-default {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email') !!}
    {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
</div>
{!! $errors->first('email', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group required form-group-default {{ $errors->has('pic') ? 'has-error' : ''}}">
    {!! Form::label('pic', 'PIC') !!}
    {!! Form::text('pic', null, ['class' => 'form-control', 'required']) !!}
</div>
{!! $errors->first('pic', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group required form-group-default {{ $errors->has('alamat') ? 'has-error' : ''}}">
    {!! Form::label('alamat', 'Alamat') !!}
    {!! Form::text('alamat', null, ['class' => 'form-control', 'required']) !!}
</div>
{!! $errors->first('alamat', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group required form-group-default {{ $errors->has('keterangan') ? 'has-error' : ''}}">
    {!! Form::label('keterangan', 'Keterangan') !!}
    {!! Form::text('keterangan', null, ['class' => 'form-control', 'required']) !!}
</div>
{!! $errors->first('keterangan', '<label class="error">:message</label>') !!}

{{--<div class="pull-left">--}}
    {{--<div class="checkbox check-success">--}}
        {{--<input id="checkbox-agree" type="checkbox" required> <label for="checkbox-agree">Saya sudah mengecek data sebelum menyimpan</label>--}}
    {{--</div>--}}
{{--</div>--}}
<br/>

{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Save', ['type' => 'submit', 'class' => 'btn btn-success']) !!}
<button class="btn btn-default" type="reset"><i class="pg-close"></i> Clear</button>
