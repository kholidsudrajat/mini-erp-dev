@extends('layouts.app.frame')
@section('title', 'Penjualan')
@section('description', '')
@section('breadcrumbs', Breadcrumbs::render('cashier-2'))
@section('button', '')

@section('content')
   <div class="se-pre-con"></div>
    <div class="form-group col-md-12">
        <div class="row clearfix" {{ Auth::User()->shop_id != 0 ? 'style=display:none;' : '' }}>
            <div class="col-sm-4 col-xs-12">
                <div class="form-group form-group-default">
                    <label>Input Nomor Nota</label>
                    <input type="text" class="form-control" value="04{{ date('YmdHis') }}" id="noref" placeholder="Masukkan Nomor Nota" />
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="form-group form-group-default form-group-default-select2">
                    <label>Pilih Toko</label>
                    {!! Form::select('shop_id', $shop, $idshop, ['id' => 'shops', 'class' => 'full-width', 'data-init-plugin' => 'select2']) !!}
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="form-group form-group-default form-group-default-select2 required">
                    <label>Masukkan Nama Pelanggan</label>
                    {!! Form::select('customer_id', $cus, null, ['id' => 'cus', 'class' => 'full-width', 'data-init-plugin' => 'select2', 'required' => '']) !!}
                </div>
            </div>
        </div>
        @foreach($mods as $mods)
            <input hidden id="stk{{$mods['id']}}" value="{{ $mods['qty'] }}" />
        @endforeach
        <div class="row">
            <table class="table table-hover" id="cashier-table">
                <thead>
                    <tr>
                        <th width="5%"> # </th><th width="20%"> Nama Model </th><th width="20%"> Nama Barang </th><th width="10%"> Stock </th><th> QTY </th><th width="15%"> Harga / Pcs </th><th width="15%"> Tindakan </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="2">
                            <form id="searchmodel" method="get">
                            <input autocomplete="off" id="search-box" type="text" value="{{$model['model_name']}}" name="model" class="form-control" />
                            <input hidden value="{{$model['id']}}" id="model" />
                            <input hidden value="{{$model['price']}}" id="price" />
                            <button hidden type="submit">submit</button>
                            </form>
                            <div id="list_model" style="display:none;">
                            </div>
                        </td>
                        <td>
                            @if($model['name'] != "")
                                {{ $model['name'] }}
                            @endif
                        </td>
                        <td id="stocks{{ $model['id'] }}">
                            @if($model['qty'] != "")
                                {{ $model['qty'] }}
                            @endif
                        </td>
                        <td>
                            <input type="number" value="1" class="form-control" id="qty" />
                        </td>
                        <td>Rp {{ str_replace(',','.',number_format($model['price'])) }} </td>
                        <td><a class="btn btn-sm btn-primary" onclick="return addtocart()"><i class="fa fa-plus"></i></a></td>
                    </tr>
                    @php $s = array(); @endphp
                    @if($cart != "")
                        @php $o = 1; $i = 0; @endphp
                        @foreach($cart as $in)
                            @php $s[$i] = $in->subtotal; @endphp
                            <tr>
                                <td>{{ $o++ }}</td>
                                <td>{{ $mname[$in->model_id] }}</td>
                                <td colspan="2">{{ $name[$in->model_id] }}</td>
                                <td>{{ $in->qty }}</td>
                                <td>Rp {{ str_replace(',','.',number_format($s[$i])) }} </td>
                                <td><a class="btn btn-sm btn-danger" onclick="return hapus({{$i.','.$in->subtotal.','.$in->model_id}})"><i class="fa fa-times"></i></a></td>
                            </tr>
                            @php $i++; @endphp
                        @endforeach
                </tbody>
            </table>
            <h4><b>Total</b></h4>
            <hr>
            <table class="table table-hover" id="cashier-table">
                <tbody>
                        <tr>
                            <td colspan="2">
                                <label>Potongan</label>
                            </td>
                            <td>
                                <select class="form-control" data-init-plugin="select2" id="pot">
                                    <option value="0" {{ $pot=="0" ? 'selected' : '' }} >Tidak Ada</option>
                                    <option value="1" {{ $pot=="1" ? 'selected' : '' }} >Persen (%)</option>
                                    <option value="2" {{ $pot=="2" ? 'selected' : '' }} >Nominal</option>
                                </select>
                            </td>
                            <td>
                                <div class="input-group transparent" id="disco1" style="display:none;">
                                    <span class="input-group-addon">
                                        Rp
                                    </span>
                                    <input type="text" value="{{$discount}}" id="discount_nom" class="form-control" placeholder="Masukan Nominal Potongan" />
                                </div>
                                <div class="input-group transparent" id="disco2" style="display:none;">
                                    <input type="number" value="{{$discount}}" id="discount_per" class="form-control" placeholder="Masukan Prosentase Potongan" />
                                    <span class="input-group-addon">
                                        %
                                    </span>
                                </div>
                            </td>
                            <td>
                                <a id="disc" style="display:none;" onclick="return adddiscount()" class="btn btn-sm btn-primary"><i class="fa fa-check"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td><b>Subtotal</b></td>
                            <td colspan="2"><b>Rp {{ str_replace(',','.',number_format($subtotal)) }}</b></td>
                        </tr>
                        <tr style="{{ $pot == '0' || $pot == '' ? 'display:none;' : '' }}">
                            <td colspan="3"></td>
                            <td><b>Potongan</b></td>
                            <td colspan="2"><b>Rp {{ str_replace(',','.',number_format($subtotal - $total)) }}</b></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                            <td width="20%"><b>Total</b></td>
                            <td colspan="2"><b>Rp {{ str_replace(',','.',number_format($total)) }}</b></td>
                        </tr>
                        <tr id="hutang" style="display:none;">
                            <td colspan="3"></td>
                            <td width="20%"><b id="lbl_hutang"></b></td>
                            <td colspan="2">
                                <b id="hutangs"></b>  
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        @if($cart != "")
        <div class="col-md-12">
            <br/>
            <label>Metode Pembayaran</label>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <select class="full-width" data-init-plugin="select2" id="method">
                            @php $i = 1; @endphp
                            @foreach($method as $mtd => $mt)
                            <option {{ $i == 1 ? 'selected' : '' }} value="{{ $mtd }}">{{$mt}}</option>
                            @php $i++; @endphp
                            @endforeach
                        </select>
                    </div>
                </div>
                @php $i = 1; @endphp
                @foreach($method as $mtd => $mt)
                <div class="col-md-8">
                    <div id="pay{{$mtd}}" style="display:{{ $i == 1 ? 'block' : 'none' }};">
                        <div class="col-md-10" style="padding:0px;">
                            <div class="input-group transparent">
                                <span class="input-group-addon">
                                    Rp
                                </span>
                                <input type="text" value="0" id="pays{{$mtd}}" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <a onclick="return addpayment()" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>

                @php $i++; @endphp
                @endforeach
            </div>
            <textarea style="display: none;" id="method_pembayaran" name="method_pembayaran"></textarea>
            <div class="col-md-12">
                <table width="100%" id="tabss">
                </table>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-12" id="periods" style="display:none; margin-top:10px;">
            <hr/>
            <div class="form-group col-sm-12">
                <label>Periode Hutang (Hari)</label>
            </div>
            <div class="form-group col-sm-3">
                <b id="sisa" style="color:red"></b>
            </div>
            <div class="form-group col-sm-6">
                <label id="sisa"></label>
                <input id="period" type="number" class="form-control pull-right" value="7" />
            </div>
            <div class="col-sm-3">
                <a class="btn btn-success" onclick="return addperiod()">
                    <i class="fa fa-check"></i>
                </a>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-12">
            <hr/>
            <div class="form-group">
                <label>Catatan : </label>
                <textarea class="form-control" id="notes" placeholder="Catatan"></textarea>
            </div>
        </div>
        <div class="clearfix"></div>
        <br/>
        <div class="pull-right">
            <br/>
            <a onclick="return bayar()" class="btn btn-primary">Bayar</a>
        </div>
        @endif
        </div>
    </div>
@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('p').remove();
        var cus = localStorage.getItem('cust');
        if(cus != null)
            $('#cus').select2().select2('val', cus);
        @if($pot=="1")
            $('#disco1').hide();
            $('#discount_nom').hide();
            $('#disc2').show();
            $('#disco2').show();
        @elseif($pot=="2")
            $('#discount_per').hide();
            $('#disco2').hide();
            $('#disc2').show();
            $('#disco1').show();
        @else
            $('#disco1').hide();
            $('#disco2').hide();
        @endif
        
        @if(isset($model['qty']))
            if(localStorage.getItem('stocks_{{$model["id"]}}') < {{$model['qty']}} && localStorage.getItem('stocks_{{$model["id"]}}') != null)
                $('#stocks{{$model["id"]}}').html(localStorage.getItem('stocks_{{$model["id"]}}'));
            else
                $('#stocks{{$model["id"]}}').html({{$model['qty']}});
        @endif
    });
    
    $(document).ready(function(){
		$("#search-box").keyup(function(){
			if ($("#search-box").val().length > 2) {
                $.ajax({
                    type: "POST",
                    url: "{{url('admin/cashier-2/ajax_model')}}",
                    data: {
                        "_token"    : "{{ csrf_token() }}",
                        "keyword"   : $(this).val()
                    },
                    success: function(data){
				        $("#list_model").show();
                        $("#list_model").html("");
                        $("#list_model").html(data);
                    }
                });
			} else {
				$("#list_model").hide();
			}
		});
	});
    
    $('#cus').on('change', function(){
        localStorage.setItem('cust', $('#cus :selected').val());
    })
    
    function pilih(mod){
        var url = window.location.href.split('?')[0];
        window.location.href = url+'?model='+mod;
    }
    
    $('#shops').on('change', function(){
        var ids = $('#shops :selected').val();
        $.ajax({
            url : '{{ url("admin/cashier/shop") }}/'+ids+'/choose',
            type : 'GET',
            success : function(){
                window.location.href = window.location.href.split('?')[0];
            }
        })
    })
    
    $('#pot').on('change', function(){
        var pot = $('#pot :selected').val();
        $('#discount_nom').val('0');
        $('#discount_per').val('0');
        
        if(pot == 0){
            $('#disc').hide();
            $('#disc2').hide();
            
            var subtotal = {{ array_sum($s) }};
            var discount = 0;
            var total = subtotal - discount;

            var datanya = {
                'pot'       :   pot,
                'subtotal'  :   subtotal,
                'discount'  :   discount,
                'total'     :   total
            };

            $.ajax({
                url: '{{url("admin/cashier-2/adddisc")}}/',
                type: 'GET',
                data: datanya,
                success: function(){
                    window.location.href = window.location.href;
                }
            });
            
        }else if(pot == 1){
            $('#disc').show();
            $('#discount_nom').hide();
            $('#discount_per').show();
            $('#disco1').hide();
            $('#disc2').show();
            $('#disco2').show();
        }else if(pot == 2){
            $('#disc').show();
            $('#discount_per').hide();
            $('#discount_nom').show();
            $('#disco2').hide();
            $('#disc2').show();
            $('#disco1').show();
        }
    });
    
    $('#popup').fancybox({
        type: 'iframe'
    });
    
    function hapus(id, subtotal, mod){
        var stck = $('#stk'+mod).val();
        localStorage.setItem('stocks_'+mod, stck);
        $.ajax({
            url: '{{url("admin/cashier-2/hapusqty")}}/'+id+'?subtotal='+subtotal,
            type: 'GET',
            success: function(){
                window.location.href = window.location.href;
            }
        });
    };
    
    function addtocart(){
        var qty = $('#qty').val();
        var mod = $('#model').val();
        var pri = $('#price').val();
        var stocks = parseInt($('#stocks'+mod).html());
        var subtotal = pri * qty;
        
        var datanya = {
            'model_id'  :   mod,
            'qty'       :   qty,
            'subtotal'  :   subtotal
        };
        
        if(qty > stocks){
            alert('Stok Kurang!');
        }else{
            localStorage.setItem('stocks_'+mod, stocks - qty);
            if(mod != "" && qty > 0){
                $.ajax({
                    url: '{{url("admin/cashier-2/addqty")}}/',
                    type: 'GET',
                    data: datanya,
                    success: function(){
                        window.location.href = '{{url("admin/cashier-2")}}';
                    }
                });
            }
        }
    }
    
    function adddiscount(){
        var subtotal = {{ array_sum($s) }};
        var discount = 0;
        var disc = 0;
    
        var pot = $('#pot :selected').val();
        if(pot == 1){
            disc = parseInt($('#discount_per').val());
            discount = (disc / 100) * subtotal;
        }else if(pot == 2){
            disc = parseInt($('#discount_nom').val().replace(/,/g,''));
            discount = disc;
        }
    
        localStorage.setItem('tdiscounts', discount);
        var total = subtotal - discount;
    
        var datanya = {
            'pot'       :   pot,
            'subtotal'  :   subtotal,
            'discount'  :   disc,
            'total'     :   total
        };
    
        $.ajax({
            url: '{{url("admin/cashier-2/adddisc")}}/',
            type: 'GET',
            data: datanya,
            success: function(){
                window.location.href = window.location.href;
            }
        });
    }
    
    $('#method').on('change', function(){
        var pay = $('#method :selected').val();
        
        @foreach($method as $mtd => $mt)
            $('#pay{{$mtd}}').hide();
            $('#pays{{$mtd}}').hide();
        @endforeach
        
        $('#pay'+pay).show();
        $('#pays'+pay).show();
    });
    
    function addpayment(){
        var pay = $('#method :selected').val();
        var tpay = $('#method :selected').text();
        var payment = parseInt($('#pays'+pay).val().replace(/,/g, '')).toLocaleString().replace(/,/g, '.');

        var method = $('#method_pembayaran').val();
        if($('#row_'+pay).length == 0 && payment > 0){
            var tabs = '<tr id="row_'+pay+'"><th width="50%">Bayar dengan '+tpay+'</th><th width="5%"> : </th><td>'+payment+'</td><td width="5%"><a onclick="return hapuspay('+pay+')" href="javascript:;"><i class="fa fa-times"></i></a></td></tr>';

            method += pay+',' ;

            $('#tabss').append(tabs);
            $('#method_pembayaran').val(method);
            $('#periods').hide();
            var subtotal = {{ array_sum($s) }};
            var disc = localStorage.getItem('tdiscounts');
            var total = subtotal - disc;
            var pays = 0;
            @foreach($method as $mtd => $mt)
                pays += parseInt($('#pays{{$mtd}}').val().replace(/,/g, ''));
            @endforeach
        
            var cus = $('#cus :selected').text();
            if(total > pays){
                var hutang = total - pays;
                hutang = hutang.toLocaleString(2).replace(/,/g, '.');
                $('#hutang').show();
                $('#lbl_hutang').html('Hutang untuk '+cus);
                $('#hutangs').html('IDR '+hutang);
            }else{
                $('#hutang').hide();
            }
        }
    }
    
    function hapuspay(pay){
        @foreach($method as $mtd => $mt)
            $('#pay{{$mtd}}').hide();
            $('#pays{{$mtd}}').hide();
        @endforeach


        $('#pay'+pay).show();
        $('#pays'+pay).show();
        $('#pays'+pay).val(0);
        $('#pays'+pay).focus();
        $('#row_'+pay).remove();

        var a = $('#method_pembayaran').val().replace(pay+',', '');
        $('#method_pembayaran').val(a);

        var subtotal = {{ array_sum($s) }};
        var disc = localStorage.getItem('tdiscounts');
        var total = subtotal - disc;
        var pays = 0;
        @foreach($method as $mtd => $mt)
            pays += parseInt($('#pays{{$mtd}}').val().replace(/,/g, ''));
        @endforeach

        if(total > pays){
            var hutang = total - pays;
            hutang = hutang.toLocaleString(2).replace(/,/g, '.');
            $('#hutang').show();
            $('#hutangs').html('IDR '+hutang);
        }
    }
    
    function addperiod(){
        Pace.restart();
        localStorage.setItem('periods',$('#period').val());
    }
    
    function bayar(){
        var ids = $('#shops :selected').val();
        var subtotal = {{ array_sum($s) }};
        var discount = 0;
        var payment = '';
        var pay = 0;
    
        @foreach($method as $mtd => $mt)
            payment += ',{"{{$mtd}}":'+$('#pays{{$mtd}}').val().replace(/,/g, '')+"}";
            pay += parseInt($('#pays{{$mtd}}').val().replace(/,/g, ''));
        @endforeach
        var disc = localStorage.getItem('tdiscounts');
        var total = subtotal - disc;
        var debt  = total - pay;
        var period = 0 + localStorage.getItem('periods');
        period = parseFloat(period);
    
        console.log(debt+' '+period);
    
        if(debt > 0 && period < 1){
            $('#periods').show();
            $('#period').focus();
            $('#sisa').html('Sisa hutang : '+debt.toLocaleString(2));
            
            if(localStorage.getItem('period') > 0)
                return true;
            else
                return false;
        }
    
        var pot = $('#pot :selected').val();
        if(pot == 1){
            var disc = parseInt($('#discount_per').val());
            discount = (disc / 100) * subtotal;
        }else if(pot == 2){
            var disc = parseInt($('#discount_nom').val().replace(/,/g, ''));
            discount = disc;
        }
    
        var total = subtotal - discount;
        var method_pembayaran = $('#method_pembayaran').val();
    
        var datas = {
            'no_ref'            :   $('#noref').val(),
            'customer_id'       :   $('#cus :selected').val(),
            'pot'               :   pot,
            'discount'          :   disc,
            'subtotal'          :   subtotal,
            'total'             :   total,
            'payment'           :   payment,
            'shop_id'           :   ids,
            'method_pembayaran' :   method_pembayaran,
            'due_date'          :   localStorage.getItem('periods'),
            '_token'            :   '{{ csrf_token() }}'
        };
        var cus = $('#cus :selected').val();
    
        if(total != 0){
            $.ajax({
                url: '{{url("admin/cashier-2/bayar")}}',
                type: 'POST',
                data: datas,
                success: function(res){
                    @if(isset($model['qty']))
                        localStorage.removeItem('stocks_{{$model["id"]}}');
                    @endif
                    
                    window.location.href = '{{ url("admin/cashier-2/shipping") }}/'+res;
                }
            });
        }else{
            alert('Keranjang Belanja Masih Kosong!');
        }
    };
    
    $('#discount_nom').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    
    @foreach($method as $mtd => $mt)
    $('#pays{{ $mtd }}').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    @endforeach
</script>
@endpush
