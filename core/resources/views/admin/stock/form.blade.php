<div aria-required="true" class="form-group required form-group-default form-group-default-select2 {{ $errors->has('model_id') ? 'has-error' : ''}}">
    {!! Form::label('model_id', 'Produk') !!}
    <select name="model_id" class="full-width" required id="models" data-init-plugin="select2">
        <option>Pilih Produk</option>
        @foreach($model as $mod)
        <option {{ $mod['id'] == $mods ? 'selected' : '' }} value="{{ $mod['id'] }}">{{ $mod['names'] }}</option>
        @endforeach
    </select>
</div>
{!! $errors->first('model_id', '<label class="error">:message</label>') !!}

<div class="col-md-12">
    <table class="table table-bordered">
        <thead>
            <th>Toko / Gudang</th>
            <th colspan="2">QTY</th>
        </thead>
        <tbody id="tabs">
            <tr>
                <td width="45%">{!! Form::select('shop_id', $shop, null, ['id' => 'shop_id', 'class' => 'form-control', 'data-init-plugin' => 'select2']) !!}</td>
                <td width="45%">{!! Form::number('qty', null, ['id' => 'qty', 'class' => 'form-control', 'min' => '0', 'placeholder' => '0']) !!}</td>
                <td width="5%">
                    <a class="btn btn-primary" id="addstock"><i class="fa fa-plus"></i></a>
                </td>
            </tr>
        </tbody>
    </table>
    <textarea style="display:none;" id="sjson" name="sjson"></textarea>
</div>

{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Save', ['type' => 'submit', 'class' => 'btn btn-success']) !!}
<a href="{{ url('/admin/stock')}}" class="btn btn-info no-border">Cancel</a>

@push('script')
<script type="text/javascript">
    $('#addstock').on('click', function(){
        var shop = $('#shop_id :selected').val();
        var shops = $('#shop_id :selected').text();
        var qty = parseFloat($('#qty').val());
        
        if(qty > 0){
            var tab = '';
            if($('#row_'+shop).length < 1){                
                tab += '<tr id="row_'+ shop +'">';
                tab += '<td>'+ shops +'</td>';
                tab += '<td id="qty_'+ shop +'">'+ qty +'</td>';
                tab += '<td><a id="btn_'+ shop +'" class="btn btn-danger" onclick="hapusstock('+ shop +', '+ qty +')"><i class="fa fa-times"></i></a></td>';
                tab += '</tr>';

                $('#tabs').append(tab);
                
                var json = '{\"'+ shop +'\" : '+ qty +'},';
                $('#sjson').append(json);
            }else{
                var qtys = $('#qty_'+shop).html();
                var qt = parseFloat(qtys) + qty;
                
                $('#qty_'+shop).html(qt);
                $('#btn_'+shop).attr('onclick', 'hapusstock('+ shop +', '+ qt +')');
                
                var json = $('#sjson').val().replace('{\"'+ shop +'\" : '+ qtys +'},', '{\"'+ shop +'\" : '+ qt +'},');
                $('#sjson').val(json);
            }
            $('#qty').val(0);
            $('#qty').focus();
        }
        Pace.restart();
    })
    
    function hapusstock(shop, qty){
        Pace.restart();
        var json = $('#sjson').val().replace('{\"'+ shop +'\" : '+ qty +'},', '');
        $('#sjson').val(json);
        $('#row_'+shop).remove();
    }
</script>
@endpush