@extends('layouts.app.frame')
@section('title', 'Stock #' . $stock->id)
@section('description', 'Stock Details')
@section('breadcrumbs', Breadcrumbs::render('stock.show', $stock))
@section('button', '<a href="'.url('/root/stock').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
	<div class="panel panel-default">
		<div class="panel-body">
			<a href="{{ url('root/stock/' . $stock->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit Stock"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
			{!! Form::open([
				'method'=>'DELETE',
				'url' => ['root/stock', $stock->id],
				'style' => 'display:inline'
			]) !!}
				{!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
						'type' => 'submit',
						'class' => 'btn btn-danger btn-xs',
						'title' => 'Delete Stock',
						'onclick'=>'return confirm("Confirm delete?")'
				))!!}
			{!! Form::close() !!}
			<br/>
			<br/>

			<div class="table-responsive">
				<table class="table table-condensed">
					<tbody>
						<tr>
							<th>ID</th>
							<td>{{ $stock->id }}</td>
						</tr>
                        <tr>
                            <th>Nama</th>
                            <th>{{ $stock->nama }}</th>
                        </tr>
                        <tr>
                            <th>No Telp</th>
                            <th>{{ $stock->no_telp }}</th>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <th>{{ $stock->email }}</th>
                        </tr>
                        <tr>
                            <th>PIC</th>
                            <th>{{ $stock->pic }}</th>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <th>{{ $stock->alamat }}</th>
                        </tr>
                        <tr>
                            <th>Keterangan</th>
                            <th>{{ $stock->keterangan }}</th>
                        </tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
    
    <div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">List Bahan</h3>
		</div>
        <div class="panel-body table-responsive">
            <table class="table table-condensed">
                <thead>
                    <th width="10%">#</th>
                    <th>Nama</th>
                </thead>
                <tbody>
                    @php $i = 1; @endphp
                    @foreach($bahan as $bhn)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $bhn['name'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
    <div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">List Model</h3>
		</div>
        <div class="panel-body table-responsive">
            <table class="table table-condensed">
                <thead>
                    <th width="10%">#</th>
                    <th>Nama</th>
                </thead>
                <tbody>
                    @php $i = 1; @endphp
                    @foreach($model as $mdl)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $mdl['name'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('script')
<script type="text/javascript">
    function deleteData(idnya){
        var conf = confirm("Delete Data?");
        if(conf){
            $.ajax({
                type: "GET",
                url: "{{url('').'/admin/deletedata/'}}"+idnya+"/delete",
                success: function(){
                    location.reload();  
                }
            });
        }
    }
</script>
@endpush