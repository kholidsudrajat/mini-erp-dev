@extends('layouts.app.frame')
@section('title', 'Notice')
@section('breadcrumbs', Breadcrumbs::render('notice-monthly'))
@section('button', '<div class="clearfix"></div>')
@section('content')
<ul class="nav nav-tabs nav-justified">
  <li class="{{ $current == 1 ? 'active' : '' }}"><a onclick="totab(1)" data-toggle="tab" href="#tab_1">Omzet</a></li>
  <li class="{{ $current == 2 ? 'active' : '' }}"><a onclick="totab(2)" data-toggle="tab" href="#tab_2">Best Seller</a></li>
  <li class="{{ $current == 3 ? 'active' : '' }}"i><a onclick="totab(3)" data-toggle="tab" href="#tab_3">Rugi Laba</a></li>
</ul>
<div class="tab-content">
    <div id="tab_1" class="tab-pane fade {{ $current == 1 ? 'in active' : '' }}">
        <div class="panel panel-default" >
            <div class="panel-body">
                <div class="form-group-attached" style="text-align: center">
                    <div style="border: 1px solid rgba(0,0,0,.07);" class="form-group form-group-default col-md-12">
                        <label>Omzet</label>
                        <b>{{ str_replace(',', '.', number_format($omzet)) }}</b>
                    </div>
                    <div style="border: 1px solid rgba(0,0,0,.07);" class="form-group form-group-default col-md-6">
                        <label>Cash</label>
                        <b>{{ str_replace(',', '.', number_format($cash)) }}</b>
                    </div>
                    <div style="border: 1px solid rgba(0,0,0,.07);" class="form-group form-group-default col-md-6">
                        <label>Piutang</label>
                        <b>{{ str_replace(',', '.', number_format($debt * -1)) }}</b>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="tab_2" class="tab-pane fade {{ $current == 2 ? 'in active' : '' }}">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Product's Best Seller</h3>
            </div>
            <div class="panel-body">
                <div class="col-md-3 pull-right">
                    <div class="form-group">
                        <label>Jumlah Produk yang ditampilkan : </label>
                        <input id="prod" value="{{ $prod }}" type="number" class="form-control" />
                    </div>
                    <a onclick="config('product_best_seller')" class="btn btn-primary"><i class="fa fa-check"></i></a>
                </div>
                <div class="clearfix"></div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <th width="5%">#</th>
                            <th>Produk</th>
                            <th>Terjual</th>
                            <th>Harga</th>
                            <th>Total</th>
                        </thead>
                        <tbody id="tbs">
                            @php $i = 1; @endphp
                            @foreach($product as $prod)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $prod->names }}</td>
                                <td>{{ $prod->qty }}</td>
                                <td>{{ str_replace(',', '.', number_format($prod->harga / $prod->qty)) }}</td>
                                <td>{{ str_replace(',', '.', number_format($prod->harga)) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="tab_3" class="tab-pane fade {{ $current == 3 ? 'in active' : '' }}">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Rugi Laba</h3>
            </div>
            <div class="panel-body">
                <div class="form-group-attached col-md-12" style="text-align: center">
                    <div class="form-group form-group-default">
                        <label>Total Rugi Laba</label>
                        <b id="total">0</b>
                    </div>
                </div>
                <div class="col-md-12"><br/></div>
                <div class="form-group-attached">
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group form-group-default" style="border: 1px solid rgba(0,0,0,0.07);">
                            <label>Select date</label>
                            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                <span></span> <b class="caret"></b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-8 col-xs-12" style="padding-right:0px;">
                    <table class="table table-bordered" id="profit-loss-table">
                        <thead>
                            <tr>
                                <th> Toko </th><th> Omset </th><th> Modal </th><th> Profit </th>
                            </tr>
                        </thead>
                        <tbody id="profits">
                            @php
                                $tpen = 0;
                                $tmod = 0;
                                $tpro = 0;
                            @endphp
                            @foreach($profit as $prof)
                            @php
                                $tpr[$prof->shop_id] = 0;
                            @endphp
                            @endforeach
                            @foreach($profit as $prof)
                            <tr>
                                <td>{{ $prof->name }}</td>
                                <td>{{ str_replace(',', '.', number_format($prof->pendapatan)) }}</td>
                                <td>{{ str_replace(',', '.', number_format($prof->modal)) }}</td>
                                <td>{{ str_replace(',', '.', number_format($prof->profit)) }}</td>
                            </tr>
                            @php
                                $tpen += $prof->pendapatan;
                                $tmod += $prof->modal;
                                $tpro += $prof->profit;
                                $tpr[$prof->shop_id] += $prof->profit;
                            @endphp
                            @endforeach
                        </tbody>
                        <tfoot id="tprofit">
                            <th>Total</th>
                            <th>{{ str_replace(',', '.', number_format($tpen)) }}</th>
                            <th>{{ str_replace(',', '.', number_format($tmod)) }}</th>
                            <th>{{ str_replace(',', '.', number_format($tpro)) }}</th>
                        </tfoot>
                    </table>
                </div>
                <div class="col-md-4 col-xs-12" style="padding-left:0px;">
                    <table class="table table-bordered" id="profit-loss-table" style="border-left:0px;">
                        <thead>
                            <tr>
                                <th style="border-left:0px;"> Pengeluaran </th>
                                <th> Net Profit </th>
                            </tr>
                        </thead>
                        <tbody id="losss">
                            @php
                                $tpeng = 0;
                            @endphp
                            @foreach($loss as $los)
                            <tr>
                                <td style="border-left:0px;">{{ number_format($los->pengeluaran) }}</td>
                                <td>{{ number_format($tpr[$los->shop_id] - $los->pengeluaran) }}</td>
                            </tr>
                            @php
                                $tpeng += $los->pengeluaran;
                            @endphp
                            @endforeach
                        </tbody>
                        <tfoot id="tloss">
                            <th style="border-left:0px;">{{ number_format($tpeng) }}</th>
                            <th>{{ number_format($tpro - $tpeng) }}</th>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('p').remove(); 
        
        var total = {{ $tpro - $tpeng }};
    
        if(total > 0){
            $('#total').css('color','green');
            $('#total').html('Profit : '+total.toLocaleString().replace(/,/g, '.'));
        }else{
            $('#total').css('color','red');
            $('#total').html('Loss : '+total.toLocaleString().replace(/,/g, '.'));
        }
    });
    
    function totab(id){
        $.ajax({
            url     : '{{ url("") }}/admin/notice-monthly',
            type    : 'GET',
            data    : {
                current     : id
            }
        })
    }
    
    function config(type){
        if(type == 'low_stock')
            var value = $('#min').val();
        else if(type == 'high_stock')
            var value = $('#max').val();
        else if(type == 'product_best_seller')
            var value = $('#prod').val();
        
        $.ajax({
            url     : '{{ url("") }}/admin/config',
            type    : 'GET',
            data    : {
                type    : type,
                value   : value
            },
            complete    : function(){
                location.reload();
            }
        })
    }
    
    $(function() {
        var i = 1;
        var start = '01/01/1970';
        var end = moment();

        function cb(start, end) {
            if(i>1){
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                var datanya = {
                    mulai   :   start.format('YYYY-MM-DD')+' 00:00:00',    
                    sampai  :   end.format('YYYY-MM-DD')+' 23:59:59',    
                    _token  :   '{{csrf_token()}}',    
                };

                $.ajax({
                    type    :   'POST',
                    url     :   '{{ url("") }}/admin/report/profit-loss/filter',
                    data    :   datanya,
                    beforeSend  : function(){
                        Pace.start();
                    },
                    success : function(res){
                        $.each(res, function(index,value){
                            if(index == 'profit'){
                                var tpe = 0;
                                var tmo = 0;
                                var tpr = 0;
                                var tab = '';
                                var i = 0;

                                @foreach($shop as $sh)
                                    tab += '<tr>';
                                    tab += '<td>{{ $sh->name }}</td>';

                                    if(value.length > i){
                                        if(value[i]['name'] == ""){
                                            tab += '<td>0</td><td>0</td><td>0</td>';
                                        }else{
                                            var pendapatan = parseInt(value[i]['pendapatan']).toLocaleString().replace(/,/g, '.');
                                            var modal = parseInt(value[i]['modal']).toLocaleString().replace(/,/g, '.');
                                            var profit = parseInt(value[i]['profit']).toLocaleString().replace(/,/g, '.');

                                            tpe += parseInt(value[i]['pendapatan']);
                                            tmo += parseInt(value[i]['modal']);
                                            tpr += parseInt(value[i]['profit']);

                                            tab += '<td>'+pendapatan+'</td><td>'+modal+'</td><td>'+profit+'</td>';
                                        }
                                        i++;
                                    }else{
                                        tab += '<td>0</td><td>0</td><td>0</td>';
                                    }

                                    tab += '</tr>';
                                @endforeach

                                $('#profits').html(tab);

                                var tfoot = '<tr><th>Total</th><th>'+tpe.toLocaleString().replace(/,/g, '.')+'</th><th>'+tmo.toLocaleString().replace(/,/g, '.')+'</th><th>'+tpr.toLocaleString().replace(/,/g, '.')+'</th></tr>';

                                $('#tprofit').html(tfoot);
                            }else if(index == 'loss'){
                                var tpe = 0;
                                var tab = '';
                                var i = 0;

                                @foreach($shop as $sh)
                                    tab += '<tr>';

                                    if(value.length > i){
                                        if(value[i]['name'] == ""){
                                            tab += '<td style="border-left:0px;">0</td><td id="netprofit{{$sh->id}}">0</td>';
                                        }else{
                                            var pengeluaran = parseInt(value[i]['pengeluaran']).toLocaleString().replace(/,/g, '.');

                                            tpe += parseInt(value[i]['pengeluaran']);

                                            tab += '<td style="border-left:0px;">'+pengeluaran+'</td><td id="netprofit{{$sh->id}}"></td>';
                                        }
                                        i++;
                                    }else{
                                        tab += '<td style="border-left:0px;">0</td><td id="netprofit{{$sh->id}}">0</td>';
                                    }

                                    tab += '</tr>';
                                @endforeach

                                $('#losss').html(tab);

                                var tfoot = '<tr><th style="border-left:0px;">'+tpe.toLocaleString().replace(/,/g, '.')+'</th><th id="totalnet"></th></tr>';

                                $('#tloss').html(tfoot);
                            }else if(index == 'total'){
                                var total = value;

                                if(total > 0){
                                    $('#total').css('color','green');
                                    $('#total').html('Profit : '+total.toLocaleString().replace(/,/g, '.'));
                                }else{
                                    $('#total').css('color','red');
                                    $('#total').html('Loss : '+total.toLocaleString().replace(/,/g, '.'));
                                }
                            }else{
                                var netprofit = 0;
                                $.each(value, function(i, val){
                                    netprofit += val;
                                    $('#netprofit'+i).html(val.toLocaleString().replace(/,/g, '.'));
                                });
                                $('#totalnet').html(netprofit.toLocaleString().replace(/,/g, '.'));
                            }
                            Pace.stop();
                        });
                    }
                });
            }
            i++;
        }

        $('#reportrange').daterangepicker({
            autoUpdateInput: false,
            startDate: start,
            endDate: end,
            ranges: {
               'All Time': ['01/01/1970', moment()],
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 14 Days': [moment().subtract(13, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start,end)
    });
</script>
@endpush