@extends('layouts.app.frame')
@section('title', 'Edit Pengeluaran Konveksi')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('expense_konveksi.edit', $expense_konveksi))
@section('button', '<a href="'.url('/admin/expense_konveksi').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')


    {!! Form::model($expense_konveksi, [
            'method' => 'PATCH',
            'url' => ['/admin/expense_konveksi', $expense_konveksi->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}

        @include ('admin.expense_konveksi.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');

});
</script>
@endpush