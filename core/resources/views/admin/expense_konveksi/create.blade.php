@extends('layouts.app.frame')
@section('title', 'Create New Pengeluaran Konveksi')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('expense_konveksi.new'))
@section('button', '<a href="'.url('/admin/expense_konveksi').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/expense_konveksi', 'id' => 'formValidate', 'files' => true, 'novalidate' => '-']) !!}

		@include ('admin.expense_konveksi.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');
});
</script>
@endpush
