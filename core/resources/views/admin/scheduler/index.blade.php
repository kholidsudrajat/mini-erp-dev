@extends('layouts.app.frame')
@section('title', 'Notifikasi Penagihan ke Customer')
@section('description', 'Notifikasi Penagihan ke Customer')
@section('breadcrumbs', Breadcrumbs::render('scheduler'))
@section('button', '<div class="clearfix"></div>')

@section('content')
    <div class="panel panel-default col-md-3" style="min-height:360px;">
        <div class="panel-body" style="padding-left:5px; padding-right:5px;">
            <div aria-required="true" class="form-group required {{ $errors->has('tanggal') ? 'has-error' : ''}}" style="margin-bottom:0px;">
                {!! Form::label('tanggal', 'Tambah Tanggal Notifikasi Penagihan') !!}
                {!! Form::text('tanggal', null, ['class' => 'form-control', 'required' => '', 'id' => 'tanggal', 'placeholder' => 'Masukkan Tanggal']) !!}
            </div>
            <label class="error">*Tanggal maksimal 28</label>
            <a class="btn btn-success pull-right" style="margin-top: 0px;" onclick="tambah();" id="tambah">Tambah</a>
            <div class="clearfix"></div>
            <hr/>
            <div class="form-group">
                <label>Dikirim tembusan ke Email dan SMS (Customer)</label>
                <div class="checkbox check-success"><input {{ $emailcus == '1' ? 'checked' : '' }} id="emailcus" type="checkbox"><label for="emailcus">Email</label></div>
                <div class="checkbox check-success"><input {{ $phonecus == '1' ? 'checked' : '' }} id="phonecus" type="checkbox"><label for="phonecus">SMS</label></div>
            </div>
            <a class="btn btn-success pull-right" style="margin-top: 10px;" onclick="save('cus');" id="save">Simpan</a>
        </div>
    </div>
    <div class="panel panel-default col-md-9" style="min-height:360px;">
        <div class="panel-body">
            <div class="form-group col-md-5">
                <label>Dikirim tembusan ke Email dan SMS (admin)</label>
                <div class="checkbox check-success"><input {{ $email == '1' ? 'checked' : '' }} id="email" type="checkbox"><label for="email">Email</label></div>
                <div class="checkbox check-success"><input {{ $phone == '1' ? 'checked' : '' }} id="phone" type="checkbox"><label for="phone">SMS</label></div>
                <div class="clearfix"></div>
                <br/>
            </div>
            <div class="form-group col-md-7">
                <label>Email</label>
                <input type="text" value="{{ $emails }}" class="form-control" id="emails" placeholder="Masukkan Email" />
                <label class="error">*Pisah dengan koma. Contoh : email1@mail.com, email2@mail.com</label>
                <label>SMS</label>
                <input type="text" value="{{ $phones }}" class="form-control" id="phones" placeholder="Masukkan No. Handphone" />
                <label class="error">*Pisah dengan koma. Contoh : 08123456, 082145623</label>
                <a class="btn btn-success pull-right" style="margin-top: 10px;" onclick="save('admin');" id="save">Simpan</a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <input type="hidden" id="drs" name="drange"/>
    <input type="hidden" id="did" name="did"/>
    <div class="form-group-attached">
        <div class="row clearfix">
            <div class="col-sm-6 col-xs-12">
                <div class="form-group form-group-default">
                    <label>Pencarian</label>
                    <form id="formsearch">
                        <input type="text" id="search-table" class="form-control" name="firstName" placeholder="Masukkan Keyword">
                    </form>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group form-group-default">
                    <label>Tanggal Mulai</label>
                    <input type="text" id="datepicker-start" class="form-control" name="firstName" placeholder="pilih tanggal">
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group form-group-default">
                    <label>Tanggal Berakhir</label>
                    <input type="text" id="datepicker-end" class="form-control" name="firstName" placeholder="pilih tanggal">
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <table class="table table-hover" id="scheduler-table">
        <thead>
        <tr>
            <th> Tanggal </th><th> Action </th>
        </tr>
        </thead>
    </table>

@endsection

@push('script')
    <script>
        var oTable;
        oTable = $('#scheduler-table').DataTable({
            processing: true,
            serverSide: true,
            dom: 'lBfrtip',
            order:  [[ 0, "asc" ]],
            buttons: [],
            sDom: "<'table-responsive fixed't><'row'<p i>> B",
            sPaginationType: "bootstrap",
            destroy: true,
            responsive: true,
            scrollCollapse: true,
            oLanguage: {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Menampilkan <b>_START_ to _END_</b> dari _TOTAL_ entri"
            },
            lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
            ajax: {
                url:
                    '{!! route('scheduler.data') !!}'
                ,
                data: function (d) {
                    d.range = $('input[name=drange]').val();
                }
            },
            columns: [
                { data: "tanggal", name: "tanggal" },
                { data: "action", name: "action", orderable: false , searchable: false },
            ],
        }).on( 'processing.dt', function ( e, settings, processing ) {if(processing){Pace.start();} else {Pace.stop();}});

        $("#cost-table_wrapper > .dt-buttons").appendTo("div.export-options-container");


        $('#datepicker-start').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
            $(this).datepicker('hide');
            if($('#datepicker-end').val() != ""){
                $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
                oTable.draw();
            }else{
                $('#datepicker-end').focus();
            }

        });
        $('#datepicker-end').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
            $(this).datepicker('hide');
            if($('#datepicker-start').val() != ""){
                $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
                oTable.draw();
            }else{
                $('#datepicker-start').focus();
            }

        });

        $('#formsearch').submit(function () {
            oTable.search( $('#search-table').val() ).draw();
            return false;
        } );

        oTable.page.len(25).draw();

        function deleteData(id) {
            $('#modalDelete').modal('show');
            $('#did').val(id);
        }

        function hapus(){
            $('#modalDelete').modal('hide');
            var id = $('#did').val();
            $.ajax({
                url: '{{url("admin/scheduler/delete")}}' + "/" + id,
                type: 'GET',
                complete: function(data) {
                    oTable.draw();
                }
            });
        }

        $('#tanggal').keypress(function(e){
            if(e.which == 13){//Enter key pressed
                $('#tambah').click();//Trigger search button click event
                return false;
            }
        });
        
        function tambah()
        {
            if($('#tanggal').val() == ''){
                alert('Tanggal tidak boleh kosong');
                return false;
            }else if($('#tanggal').val() == 0) {
                alert('Tanggal tidak valid');
                return false;
            }else if($('#tanggal').val() > 28) {
                alert('Melebihi Tanggal Maksimal!');
                return false;
            } else {
                $.ajax({
                    url: '{{url('/')}}/admin/scheduler/tambah',
                    type: 'POST',
                    data: {
                        tanggal : $('#tanggal').val(),
                        _token  : '{{ csrf_token() }}',
                    },
                    success: function (data) {
                        window.location.reload();
                    }
                });
            }
        }
        
        function save(type)
        {
            if(type == 'cus'){
                var email = '0';
                if($('#emailcus').is(':checked'))
                    email = '1';

                var phone = '0';
                if($('#phonecus').is(':checked'))
                    phone = '1';

                $.ajax({
                    type    : 'POST',
                    url     : '{{ url("") }}/admin/configscheduler',
                    data    : {
                        _token      : '{{ csrf_token() }}',
                        emailcus    : email,
                        phonecus    : phone
                    },
                    complete: function(){
                        location.reload();
                    }
                })
            }else{                
                var email = '0';
                if($('#email').is(':checked'))
                    email = '1';

                var phone = '0';
                if($('#phone').is(':checked'))
                    phone = '1';

                var emails = $('#emails').val();
                var phones = $('#phones').val();

                $.ajax({
                    type    : 'POST',
                    url     : '{{ url("") }}/admin/configscheduler',
                    data    : {
                        _token  : '{{ csrf_token() }}',
                        email   : email,
                        phone   : phone,
                        emails  : emails,
                        phones  : phones
                    },
                    complete: function(){
                        location.reload();
                    }
                })
            }
        }
    </script>
@endpush
