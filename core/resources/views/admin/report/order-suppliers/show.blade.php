@extends('layouts.app.frame')
@section('title', 'Detail Pembelian Supplier #' . $supplier->nama)
@section('description', 'Detail Pembelian Supplier')
@section('breadcrumbs', Breadcrumbs::render('order-suppliers.show', $supplier))
@section('button', '<a href="'.url('/admin/report/order-suppliers').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
	<div class="panel panel-default">
		<div class="panel-body">
			<br/>
			<br/>

			<div class="table-responsive">
				<table class="table table-condensed">
					<tbody>
                        <tr>
                            <th>Nama</th>
                            <td>{{ $supplier->nama }}</td>
                        </tr>
                        <tr>
                            <th>No Telp</th>
                            <td>{{ $supplier->no_telp }}</td>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <td>{{ $supplier->alamat }}</td>
                        </tr>
                        <tr>
                            <th>Total Qty</th>
                            <td>{{ $supplier->qty }}</td>
                        </tr>
                        <tr>
                            <th>Total Pembelian</th>
                            <td>{{ str_replace(',', '.', number_format($supplier->total)) }}</td>
                        </tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">History Pembelian</h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <th width="5%">#</th>
                        <th>Nomor PO</th>
                        <th>Shipping</th>
                        <th>Payment</th>
                        <th>Qty</th>
                        <th>Total</th>
                    </thead>
                    <tbody id="tbo">
                        @php $i = 1; @endphp
                        @foreach($order as $orders)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td><a href="{{ $supplier->id.'/'.$orders->id }}">{{ $orders->po_number }}</a></td>
                            <td>{{ ($orders->ship != "") ? $orders->ship : $orders->shipping_type }}</td>
                            <td>{{ $orders->paym }}</td>
                            <td>{{ $orders->qty }}</td>
                            <td>{{ str_replace(',','.',number_format($orders->total)) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('script')
<script type="text/javascript">
</script>
@endpush