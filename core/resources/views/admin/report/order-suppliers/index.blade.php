@extends('layouts.app.frame')
@section('title', 'Laporan Pembelian per Supplier')
@section('description', 'Laporan Pembelian per Supplier')
@section('breadcrumbs', Breadcrumbs::render('order-suppliers'))
@section('button', '<div class="clearfix"></div>')

@section('content')
<div class="form-group-attached" style="text-align: center">
	<div class="col-md-4">
        <a style="width:100%; padding:15px;" class="btn btn-info" href="#">Supplier</a>
	</div>
	<div class="col-md-4">
        <a style="width:100%; padding:15px;" class="btn btn-success" href="{{ url('') }}/admin/report/order-po">Pembelian</a>
	</div>
	<div class="col-md-4">
        <a style="width:100%; padding:15px;" class="btn btn-success" href="{{ url('') }}/admin/report/order-payment">Pembayaran</a>
	</div>
    <div class="clearfix"></div>
    <br/>
	<div class="col-md-6">
        <a style="width:100%; padding:15px;" class="btn btn-success" href="{{ url('') }}/admin/report/order-delivery">Pengiriman</a>
	</div>
	<div class="col-md-6">
        <a style="width:100%; padding:15px;" class="btn btn-success" href="{{ url('') }}/admin/report/order-retur">Retur Pembelian</a>
	</div>
</div>
<div class="clearfix"></div>
<br/>
<input type="hidden" id="drs" name="drange"/>
<input type="hidden" id="did" name="did"/>
<div class="form-group-attached">
	<div class="row clearfix">
		<div class="col-sm-12 col-xs-12">
			<div class="form-group form-group-default">
				<label>Pencarian</label>
				<form id="formsearch">
					<input type="text" id="search-table" class="form-control" name="firstName" placeholder="Masukkan Keyword">
				</form>
			</div>
		</div>
	</div>
</div>

<div class="clearfix"></div>
<table class="table table-hover" id="order-suppliers-table">
	<thead>
		<tr>
			<th> Nama </th><th> No Telp </th><th> PIC </th><th> Qty </th><th> Total </th><th width="30%"> Tindakan </th>
		</tr>
	</thead>
</table>

@endsection


@push("script")
<script>

var oTable;
oTable = $('#order-suppliers-table').DataTable({
    processing: true,
    serverSide: true,
    dom: 'lBfrtip',
    order:  [[ 0, "asc" ]],
    buttons: [
        {
            extend: 'print',
            autoPrint: true,
            customize: function ( win ) {
                $(win.document.body)
                    .css( 'padding', '2px' )
                    .prepend(
                        '<h5 style="font-size: 9px;margin-top: 0px;"><br/><font style="font-size:14px;margin-top: 5px;margin-bottom:20px;">LAPORAN PEMBELIAN PER SUPPLIER</font><br/><br/><font style="font-size:8px;margin-top:15px;">{{date('Y-m-d h:i:s')}}</font></h5><br/><br/>'
                    );


                $(win.document.body).find( 'div' )
                    .css( {'padding': '2px', 'text-align': 'center', 'margin-top': '-50px'} )
                    .prepend(
                        ''
                    );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( { 'font-size': '9px', 'padding': '2px' } );


            },
            title: '',
            orientation: 'landscape',
            exportOptions: {columns: ':visible'} ,
            text: '<i class="fa fa-print" data-toggle="tooltip" title="" data-original-title="Print"></i>'
        },
        {extend: 'colvis', text: '<i class="fa fa-eye" data-toggle="tooltip" title="" data-original-title="Column visible"></i>'},
        {extend: 'csv', text: '<span>CSV</span>'}
    ],
    sDom: "<'table-responsive fixed't><'row'<p i>> B",
    sPaginationType: "bootstrap",
    destroy: true,
    responsive: true,
    scrollCollapse: true,
    oLanguage: {
        "sLengthMenu": "_MENU_ ",
        "sInfo": "Menampilkan <b>_START_ to _END_</b> dari _TOTAL_ entri"
    },
    lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
    ajax: {
    url: 
        '{!! route('order-suppliers.data') !!}'
    ,
        data: function (d) {
            d.range = $('input[name=drange]').val();
        }
    },
    columns: [
        { data: "nama", name: "nama"},
        { data: "no_telp", name: "no_telp"},
        { data: "pic", name: "pic"},
        { data: "qty", name: "order_details.qty"},
        { data: "total", name: "total", searchable: false},
        { data: "action", name: "action", orderable: false , searchable: false },
    ],
}).on( 'processing.dt', function ( e, settings, processing ) {if(processing){Pace.start();} else {Pace.stop();}});

$("#order-suppliers-table_wrapper > .dt-buttons").appendTo("div.export-options-container");


$('#datepicker-start').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
    $(this).datepicker('hide');
    if($('#datepicker-end').val() != ""){
        $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
        oTable.draw();
    }else{
        $('#datepicker-end').focus();
    }

});
$('#datepicker-end').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
    $(this).datepicker('hide');
    if($('#datepicker-start').val() != ""){
        $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
        oTable.draw();
    }else{
        $('#datepicker-start').focus();
    }

});

$('#formsearch').submit(function () {
    oTable.search( $('#search-table').val() ).draw();
    return false;
} );

oTable.page.len(10).draw();
</script>
@endpush
