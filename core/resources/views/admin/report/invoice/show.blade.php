@extends('layouts.app.frame')
@section('title', 'Detail Penjualan #' . $list_penjualan->id)
@section('description', 'Detail Penjualan')
@section('breadcrumbs', Breadcrumbs::render('list_penjualan.show', $list_penjualan))
@section('button', '<a href="'.url('/admin/report/sales_invoice').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
	<div class="panel panel-default">
		<div class="panel-body">
			<br/>
			<br/>

			<div class="table-responsive">
				<table class="table table-condensed">
					<tbody>
						<tr>
							<th>ID</th>
							<td>{{ $list_penjualan->id }}</td>
						</tr>
						<tr>
							<th>No Ref</th>
							<td>{{ $list_penjualan->no_ref }}</td>
						</tr>
						<tr>
							<th>Nama Pelanggan</th>
							<td>{{ $list_penjualan->nama }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

    <!--<div class="panel panel-default">
        <div class="form-group-attached">
            <div class="row clearfix">
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Pencarian</label>
                        <form id="formsearch">
                            <input type="text" id="search-table" class="form-control" name="firstName" placeholder="Cari berdasarkan nama barang/model">
                        </form>
                    </div>
                </div>
                <div class="clearfix"><br/></div>
                <div class="col-sm-4 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Urutkan Berdasarkan</label>
                        <select class="full-width" data-init-plugin="select2" id="sort">
                            <option value="">Default</option>
                            <option value="name">Nama Barang</option>
                            <option value="model_name">Kode</option>
                            <option value="qty">Qty</option>
                            <option value="cost">Modal</option>
                            <option value="price">Harga Jual</option>
                            <option value="diff">Profit</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>&nbsp;</label>
                        <select class="full-width" data-init-plugin="select2" id="asc">
                            <option value="0">Dari Terkecil</option>
                            <option value="1">Dari Terbesar</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Data Per Halaman</label>
                        <select class="full-width" data-init-plugin="select2" id="pagin">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>    
    </div>-->

    <div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Detail</h3><p></p>
            @php $o = count($detail); @endphp 
            <b class="pull-right">Data Ditemukan</b><b id="counter" class="pull-right">{{ $o }}&nbsp;</b>
            <br/>
		</div>
        <div class="panel-body table-responsive">
            <table class="table table-hover">
                <thead>
                    <th width="5%">#</th>
                    <th width="25%">Nama Barang</th>
                    <th>Kode</th>
                    <th width="10%">QTY</th>
                    <th>Modal</th>
                    <th>Harga Jual</th>
                    <th>Profit</th>
                </thead>
                <tbody id="tabs">
                    @php
                        $i = 1; 
                    @endphp
                    @foreach($detail as $dtl)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $dtl['name'] }}</td>
                        <td>{{ $dtl['model_name'] }}</td>
                        <td>{{ $dtl['qty'] }}</td>
                        <td>{{ str_replace(',', '.', number_format($dtl['qty'] * $dtl['total_cost'])) }}</td>
                        <td>{{ str_replace(',', '.', number_format($dtl['qty'] * $dtl['price'])) }}</td>
                        <td>{{ str_replace(',', '.', number_format($dtl['qty'] * $dtl['diff'])) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="pull-right">
                <ul class="pagination" id="paginations">
                    @for($a=1; $a<=$pagin; $a++)
                        <li><a href="javascript:;" onclick="return toPage({{$a}})">{{ $a }}</a></li>               
                    @endfor
                </ul>
                <input hidden id="pages" value="1" />
            </div>
        </div>
    </div>
@endsection
@push('script')
<script type="text/javascript">    
    $('#formsearch').submit(function () {
        var pages = 1;
        var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : pages,
            "keyword"   : $('#search-table').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/sales_invoice")}}/{{$list_penjualan->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });            
            }
        }); 
        
        return false;
    } );
    
    $('#sort').on('change', function(){
       var pages = 1;
       var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : pages,
            "keyword"   : $('#search-table').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/sales_invoice")}}/{{$list_penjualan->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });            
            }
        }); 
    });
    
    $('#asc').on('change', function(){
       var pages = 1;
       var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : pages,
            "keyword"   : $('#search-table').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/sales_invoice")}}/{{$list_penjualan->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });            
            }
        }); 
    });
    
    $('#pagin').on('change', function(){
       Pace.restart();
       var pages = 1;
       var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : pages,
            "range"     : $('#drs').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/sales_invoice")}}/{{$list_penjualan->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });
                
                if($('#pagin :selected').val() >= {{ $total }})
                    $('#paginations').html('');
                else{
                    var mods = {{ $total }} % $('#pagin :selected').val();
                    var pagination = {{ $total }} / $('#pagin :selected').val();
                    pagination = parseInt(pagination);
            
                    if(mods > 0)
                        pagination += 1;
            
                    var lists = "";
                    
                    for(var a=1; a<=pagination; a++){
                        lists += '<li><a href="javascript:;" onclick="return toPage('+a+')">'+a+'</a></li>'
                    }
               
                    $('#paginations').html(lists);
                }
            }
        }); 
    });
    
    function toPage(pages){
       Pace.restart();
        $('#pages').val(pages);
        var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : pages,
            "range"     : $('#drs').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/sales_invoice")}}/{{$list_penjualan->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });
            }
        }); 
    }
</script>
@endpush