@extends('layouts.app.frame')
@section('title', 'Detail Retur #' . $order->do_number)
@section('description', 'Detail Retur')
@section('breadcrumbs', Breadcrumbs::render('order-retur.show', $order))
@section('button', '<a href="'.url('/admin/report/order-retur').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <br/>
        <div class="table-responsive">
            <table class="table table-condensed">
                <tbody>
                    <tr>
                        <th>Nomor PO</th>
                        <td>{{ $order->po_number }}</td>
                    </tr>
                    <tr>
                        <th>Nomor DO</th>
                        <td>{{ $order->do_number }}</td>
                    </tr>
                    <tr>
                        <th>Nama Supplier</th>
                        <td>{{ $supplier->nama }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <th>Produk</th>
                    <th>Kirim Ke</th>
                    <th>Retur</th>
                </thead>
                <tbody>
                @foreach($detail as $dtl)
                    <tr>
                        <td>{{ $dtl->produk }}</td>
                        <td>{{ $dtl->shop }}</td>
                        <td>{{ $dtl->qty }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@push('script')
<script type="text/javascript">
</script>
@endpush