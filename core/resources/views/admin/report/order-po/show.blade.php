@extends('layouts.app.frame')
@section('title', 'Detail Pembelian #' . $order->po_number)
@section('description', 'Detail Pembelian')
@section('breadcrumbs', Breadcrumbs::render('order-po.show', $order))
@section('button', '<a href="'.url('/admin/report/order-po').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <br/>
        <div class="table-responsive">
            <table class="table table-condensed">
                <tbody>
                    <tr>
                        <th>Nomor PO</th>
                        <td>{{ $order->po_number }}</td>
                    </tr>
                    <tr>
                        <th>Nama Supplier</th>
                        <td>{{ $supplier->nama }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <th width="30%">Produk</th>
                    <th width="10%">Qty</th>
                    <th width="30%">Price</th>
                    <th width="30%">Total</th>
                </thead>
                <tbody>
                    @foreach($detail as $dtl)
                    <tr>
                        <td>{{ $dtl->name.' - ('.$dtl->model_name.')' }}</td>
                        <td>{{ $dtl->qty }}</td>
                        <td>{{ str_replace(',','.',number_format($dtl->price)) }}</td>
                        <td>{{ str_replace(',','.',number_format($dtl->total)) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">History Pembayaran</h3>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <th width="50%">Tanggal</th>
                    <th>Total</th>
                </thead>
                <tbody>
                @php $total = 0; @endphp
                @foreach($payment as $pay)
                    <tr>
                        <td>{{ $pay->created_at }}</td>
                        <td>{{ str_replace(',','.',number_format($pay->total)) }}</td>
                    </tr>
                @php $total += $pay->total; @endphp
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="pull-right col-md-4">
            <table width="100%">
                <tr>
                    <td width="50%"><h5>Total Pembelian</h5></td>
                    <td width="5%"> : </td>
                    <td width="45%" style="text-align:right;"><b id="totals">{{ str_replace(',','.',number_format($order->total)) }}</b></td>
                </tr>
                <tr>
                    <td width="50%"><h5>Total Bayar</h5></td>
                    <td width="5%"> : </td>
                    <td width="45%" style="text-align:right;"><b>{{ str_replace(',','.',number_format($total)) }}</b></td>
                </tr>
                @if($totalnya != 0)
                <tr>
                    <td width="50%"><h5>Sisa</h5></td>
                    <td width="5%"> : </td>
                    <td width="45%" style="text-align:right;"><b>{{ str_replace(',','.',number_format($totalnya)) }}</b></td>
                </tr>
                @else
                <tr>
                    <td width="50%"><h5>Status</h5></td>
                    <td width="5%"> : </td>
                    <td width="45%" style="text-align:right;"><b>Lunas</b></td>
                </tr>
                @endif
            </table>
        </div>
    </div>
</div>
@endsection
@push('script')
<script type="text/javascript">
</script>
@endpush