@extends('layouts.app.frame')
@section('title', 'Laporan Penjualan Produk')
@section('description', 'Laporan Penjualan Produk')
@section('breadcrumbs', Breadcrumbs::render('sales_product'))
@section('button', '<div class="clearfix"></div>')

@section('content')
<div class="form-group-attached" style="text-align: center">
	<div class="col-md-4">
        <a style="width:100%; padding:15px;" class="btn btn-success" href="{{ url('') }}/admin/report/sales_customer">Laporan Penjualan per Pelanggan</a>
	</div>
	<div class="col-md-4">
        <a style="width:100%; padding:15px;" class="btn btn-info" href="#">Laporan Penjualan per Produk</a>
	</div>
	<div class="col-md-4">
        <a style="width:100%; padding:15px;" class="btn btn-success" href="{{ url('') }}/admin/report/sales_invoice">Laporan Penjualan per Faktur</a>
	</div>
</div>
<div class="clearfix"></div>
<br/>
<div class="form-group-attached" style="text-align: center;">
	<div class="form-group form-group-default col-md-6">
		<label>Total Barang</label>
		<b>{{ $prod }}</b>
	</div>
	<div class="form-group form-group-default col-md-6">
		<label>Total QTY</label>
		<b>{{ str_replace(',', '.', number_format($report['tqty'])) }}</b>
	</div>
	<div class="form-group form-group-default col-md-4">
		<label>Total Modal</label>
		<b>{{ str_replace(',', '.', number_format($report['tmodal'])) }}</b>
	</div>
	<div class="form-group form-group-default col-md-4" style="border: 1px solid rgba(0,0,0,0.07);">
		<label>Total Harga</label>
		<b>{{ str_replace(',', '.', number_format($report['tharga'])) }}</b>
	</div>
	<div class="form-group form-group-default col-md-4">
		<label>Total Profit</label>
		<b>{{ str_replace(',', '.', number_format($report['tprofit'])) }}</b>
	</div>
</div>
<input hidden id="drs" name="drange" />
<input hidden id="did" name="did" />
<div class="form-group-attached">
    <div class="row clearfix">
        <div class="col-sm-5">
            <div class="form-group form-group-default" style="height:65px;">
                <label>Pencarian</label>
                <form id="formsearch">
                    <input type="text" id="search-table" class="form-control" name="firstName" placeholder="Masukkan Nama Barang / Model">
                </form>
            </div>
        </div>
        <div class="col-sm-5">
            <div class="form-group form-group-default">
                <label>Select date</label>
                <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                    <span></span> <b class="caret"></b>
                </div>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group form-group-default" style="text-align:center; padding-top:15px;">
                <a onclick="location.reload()" class="btn btn-primary" href="javascript:;"><i class="fa fa-times"></i> Clear Filter</a>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<table class="table table-hover" id="sales_product-table">
    <thead>
        <tr>
            <th> Nama Barang </th>
            <th> QTY </th>
            <th> Modal </th>
            <th> Harga Jual </th>
            <th> Profit </th>
            <th width="20%"> Action </th>
        </tr>
    </thead>
</table>
@endsection


@push("script")
<script>

var oTable;
oTable = $('#sales_product-table').DataTable({
    processing: true,
    serverSide: true,
    dom: 'lBfrtip',
    order:  [[ 0, "asc" ]],
    buttons: [
        {
            extend: 'print',
            autoPrint: true,
            customize: function ( win ) {
                $(win.document.body)
                    .css( 'padding', '2px' )
                    .prepend(
                        '<img src="{{asset('img/logo.png')}}" style="float:right; top:0; left:0;height: 40px;right: 10px;background: #101010;padding: 8px;border-radius: 4px" /><h5 style="font-size: 9px;margin-top: 0px;"><br/><font style="font-size:14px;margin-top: 5px;margin-bottom:20px;">LAPORAN PENJUALAN PRODUK</font><br/><br/><font style="font-size:8px;margin-top:15px;">{{date('Y-m-d h:i:s')}}</font></h5><br/><br/>'
                    );


                $(win.document.body).find( 'div' )
                    .css( {'padding': '2px', 'text-align': 'center', 'margin-top': '-50px'} )
                    .prepend(
                        ''
                    );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( { 'font-size': '9px', 'padding': '2px' } );


            },
            title: '',
            orientation: 'landscape',
            exportOptions: {columns: ':visible'} ,
            text: '<i class="fa fa-print" data-toggle="tooltip" title="" data-original-title="Print"></i>'
        },
        {extend: 'colvis', text: '<i class="fa fa-eye" data-toggle="tooltip" title="" data-original-title="Column visible"></i>'},
        {extend: 'csv', text: '<span>CSV</span>'}
    ],
    sDom: "<'table-responsive fixed't><'row'<p i>> B",
    sPaginationType: "bootstrap",
    destroy: true,
    responsive: true,
    scrollCollapse: true,
    oLanguage: {
        "sLengthMenu": "_MENU_ ",
        "sInfo": "Menampilkan <b>_START_ to _END_</b> dari _TOTAL_ entri"
    },
    lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
    ajax: {
    url: 
        '{!! route('sales_product.data') !!}'
    ,
        data: function (d) {
            d.range = $('input[name=drange]').val();
        }
    },
    columns: [
        { data: "names", name: "name" },
        { data: "tqty", name: "tqty"},
        { data: "tmodal", name: "tmodal" , searchable: false},
        { data: "tharga", name: "tharga" , searchable: false},
        { data: "tprofit", name: "tprofit" , searchable: false},
        { data: "action", name: "action", orderable: false , searchable: false },
    ],
}).on( 'processing.dt', function ( e, settings, processing ) {if(processing){Pace.start();} else {Pace.stop();}});

$("#sales_product-table_wrapper > .dt-buttons").appendTo("div.export-options-container");


$('#datepicker-start').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
    $(this).datepicker('hide');
    if($('#datepicker-end').val() != ""){
        $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
        oTable.draw();
    }else{
        $('#datepicker-end').focus();
    }

});
$('#datepicker-end').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
    $(this).datepicker('hide');
    if($('#datepicker-start').val() != ""){
        $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
        oTable.draw();
    }else{
        $('#datepicker-start').focus();
    }

});

$(function() {

    var start = '01/01/1970';
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#drs').val(start.format('YYYY/MM/DD')+':'+end.format('YYYY/MM/DD'));
        oTable.draw();
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'All Time': ['01/01/1970', moment()],
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 14 Days': [moment().subtract(13, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);
    
});
    
$('#formsearch').submit(function () {
    oTable.search( $('#search-table').val() ).draw();
    return false;
} );

oTable.page.len(10).draw();
    
    
</script>
@endpush
