@extends('layouts.app.frame')
@section('title', 'Laporan Retur Customer #' . $customer->customer_code)
@section('description', 'Laporan Retur Customer')
@section('breadcrumbs', Breadcrumbs::render('retur-customer.show', $customer))
@section('button', '<a href="'.url('/admin/report/retur-customer').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    <div class="panel panel-default">
        <div class="form-group-attached">
            <div class="row clearfix">
                <input hidden id="drs" />
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Tanggal Mulai</label>
                        <input type="text" id="datepicker-start" class="form-control" name="firstName" placeholder="pilih tanggal">
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Tanggal Berakhir</label>
                        <input type="text" id="datepicker-end" class="form-control" name="firstName" placeholder="pilih tanggal">
                    </div>
                </div>
                <div class="clearfix"><br/></div>
                <div class="col-sm-4 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Urutkan Berdasarkan</label>
                        <select class="full-width" data-init-plugin="select2" id="sort">
                            <option value="created_at">Tanggal</option>
                            <option value="no_retur">Nomor Retur</option>
                            <option value="no_ref">Nomor Ref</option>
                            <option value="qty">Qty</option>
                            <option value="cus">Pelanggan</option>
                            <option value="type_retur">Retur</option>
                            <option value="names">Produk</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>&nbsp;</label>
                        <select class="full-width" data-init-plugin="select2" id="asc">
                            <option value="1">Dari Terbesar</option>
                            <option value="0">Dari Terkecil</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Data Per Halaman</label>
                        <select class="full-width" data-init-plugin="select2" id="pagin">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>    
    </div>
    <div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Detail</h3><p></p>
            @php $o = count($report); @endphp 
            <b class="pull-right">Data Ditemukan</b><b id="counter" class="pull-right">{{ $o }}&nbsp;</b>
            <br/>
		</div>
        <div class="panel-body table-responsive">
            <table class="table table-hover">
                <thead>
                    <th>#</th>
                    <th>Tanggal</th>
                    <th>No Retur</th>
                    <th>No Ref</th>
                    <th>Pelanggan</th>
                    <th>Retur</th>
                    <th>Produk</th>
                    <th>QTY</th>
                    <th>Note</th>
                </thead>
                <tbody id="tabs">
                    @php
                        $i = 1; 
                    @endphp
                    @foreach($report as $rpt)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $rpt['created_at'] }}</td>
                        <td><a href="{{ url('') }}/admin/report/retur-customer/{{ $customer->id }}/{{ $rpt['no_retur'] }}">{{ $rpt['no_retur'] }}</a></td>
                        <td>{{ $rpt['no_ref'] }}</td>
                        <td>{{ $rpt['cus'] }}</td>
                        <td>{{ $rpt['type_retur'] }}</td>
                        <td>{{ $rpt['names'] }}</td>
                        <td>{{ $rpt['qty'] }}</td>
                        <td>{{ $rpt['note'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="pull-right">
                <ul class="pagination" id="paginations">
                    @for($a=1; $a<=$pagin; $a++)
                        <li><a href="javascript:;" onclick="return toPage({{$a}})">{{ $a }}</a></li>               
                    @endfor
                </ul>
                <input hidden id="pages" value="1" />
            </div>
        </div>
    </div>
@endsection
@push('script')
<script type="text/javascript">
    function deleteData(idnya){
        var conf = confirm("Delete Data?");
        if(conf){
            $.ajax({
                type: "GET",
                url: "{{url('').'/admin/deletedata/'}}"+idnya+"/delete",
                success: function(){
                    location.reload();  
                }
            });
        }
    }
    
    
    $('#datepicker-start').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
        Pace.restart();
        $(this).datepicker('hide');
        if($('#datepicker-end').val() != ""){
            $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
            var pages = 1;
            
            var datanya = {
                "_token"    : "{{ csrf_token() }}",
                "limit"     : $('#pagin :selected').val(),
                "page"      : pages,
                "range"     : $('#drs').val(),
                "sort"      : $('#sort :selected').val(),
                "asc"       : $('#asc :selected').val()
            };
            
            $.ajax({
                url     : '{{url("admin/report/retur-customer")}}/{{$customer->id}}/filter',
                type    : 'POST',
                data    : datanya,
                success : function(data){
                    $.each(data, function(i, item) {
                        if(i == 0)
                            $('#tabs').html(item);
                        else
                            $('#counter').html(item + '&nbsp;');
                    });
                }
            });
        }else{
            $('#datepicker-end').focus();
        }

    });
    
    $('#datepicker-end').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
        Pace.restart();
        $(this).datepicker('hide');
        if($('#datepicker-start').val() != ""){
            $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
            var pages = 1;
            
            var datanya = {
                "_token"    : "{{ csrf_token() }}",
                "limit"     : $('#pagin :selected').val(),
                "page"      : pages,
                "range"     : $('#drs').val(),
                "sort"      : $('#sort :selected').val(),
                "asc"       : $('#asc :selected').val()
            };
            
            $.ajax({
                url     : '{{url("admin/report/retur-customer")}}/{{$customer->id}}/filter',
                type    : 'POST',
                data    : datanya,
                success : function(data){
                    $.each(data, function(i, item) {
                        if(i == 0)
                            $('#tabs').html(item);
                        else
                            $('#counter').html(item + '&nbsp;');
                    });
                }
            });
        }else{
            $('#datepicker-start').focus();
        }

    });
    
    $('#sort').on('change', function(){
       Pace.restart();
       var pages = 1;
       var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : pages,
            "range"     : $('#drs').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/retur-customer")}}/{{$customer->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });
            }
        }); 
    });
    
    $('#asc').on('change', function(){
       Pace.restart();
       var pages = 1;
       var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : pages,
            "range"     : $('#drs').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/retur-customer")}}/{{$customer->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });
            }
        }); 
    });
    
    $('#pagin').on('change', function(){
       Pace.restart();
       var pages = 1;
       var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : pages,
            "range"     : $('#drs').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/retur-customer")}}/{{$customer->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });
                
                if($('#pagin :selected').val() >= {{ $total }})
                    $('#paginations').html('');
                else{
                    var mods = {{ $total }} % $('#pagin :selected').val();
                    var pagination = {{ $total }} / $('#pagin :selected').val();
                    pagination = parseInt(pagination);
            
                    if(mods > 0)
                        pagination += 1;
            
                    var lists = "";
                    
                    for(var a=1; a<=pagination; a++){
                        lists += '<li><a href="javascript:;" onclick="return toPage('+a+')">'+a+'</a></li>'
                    }
               
                    $('#paginations').html(lists);
                }
            }
        }); 
    });
    
    function toPage(pages){
       Pace.restart();
        $('#pages').val(pages);
        var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : pages,
            "range"     : $('#drs').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/retur-customer")}}/{{$customer->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });
            }
        }); 
    }
</script>
@endpush