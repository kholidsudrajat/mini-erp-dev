@extends('layouts.app.frame')
@section('title', 'Detail Pembayaran #' . $order->inv_number)
@section('description', 'Detail Pembayaran')
@section('breadcrumbs', Breadcrumbs::render('order-payment.show', $order))
@section('button', '<a href="'.url('/admin/report/order-payment').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <br/>
        <div class="table-responsive">
            <table class="table table-condensed">
                <tbody>
                    <tr>
                        <th>Nomor PO</th>
                        <td>{{ $order->po_number }}</td>
                    </tr>
                    <tr>
                        <th>Nomor Invoice</th>
                        <td>{{ $order->inv_number }}</td>
                    </tr>
                    <tr>
                        <th>Nama Supplier</th>
                        <td>{{ $supplier->nama }}</td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <td>{{ $order->dates }}</td>
                    </tr>
                    <tr>
                        <th>Bayar</th>
                        <td>{{ str_replace(',','.',number_format($order->total)) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@push('script')
<script type="text/javascript">
</script>
@endpush