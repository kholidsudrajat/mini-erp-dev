@extends('layouts.app.frame')
@section('title', 'Perpindahan Stok #' . $model->model_name)
@section('description', 'Perpindahan Stok')
@section('breadcrumbs', Breadcrumbs::render('stock_movement.show', $model))
@section('button', '<a href="'.url('/admin/report/stock_movement').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
	<div class="panel panel-default">
		<div class="panel-body">
			<br/>
			<br/>

			<div class="table-responsive">
				<table class="table table-condensed">
					<tbody>
						<tr>
							<th>Nama Barang</th>
							<td>{{ $model->name }}</td>
						</tr>
						<tr>
							<th>Nama Model</th>
							<td>{{ $model->model_name }}</td>
						</tr>
						<tr>
							<th>Stok di Toko</th>
							<td>{{ $stock->qtys }}</td>
						</tr>
						<tr>
							<th>Stok di Gudang</th>
							<td>{{ $stock->qtyw }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

    <div class="panel panel-default">
        <div class="form-group-attached">
            <div class="row clearfix">
                <input hidden id="drs" />
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Tanggal Mulai</label>
                        <input type="text" id="datepicker-start" class="form-control" name="firstName" placeholder="pilih tanggal">
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Tanggal Berakhir</label>
                        <input type="text" id="datepicker-end" class="form-control" name="firstName" placeholder="pilih tanggal">
                    </div>
                </div>
                <div class="clearfix"><br/></div>
                <div class="col-sm-3 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Urutkan Berdasarkan</label>
                        <select class="full-width" data-init-plugin="select2" id="sort">
                            <option value="created_at">Tanggal</option>
                            <option value="transfer_from">Dari</option>
                            <option value="transfer_to">Ke</option>
                            <option value="type">Keterangan</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>&nbsp;</label>
                        <select class="full-width" data-init-plugin="select2" id="asc">
                            <option value="1">Descending</option>
                            <option value="0">Ascending</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Pilih Data Berdasarkan</label>
                        <select class="full-width" data-init-plugin="select2" id="type">
                            <option value="semua">Semua Data</option>
                            <option value="in">Barang Masuk</option>
                            <option value="out">Barang Keluar</option>
                            <option value="toshop">Barang Masuk ke Toko</option>
                            <option value="towarehouse">Barang Masuk ke Gudang</option>
                            <option value="adjustment">Penyesuaian Stok</option>
                            <option value="retur">Retur</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Data Per Halaman</label>
                        <select class="full-width" data-init-plugin="select2" id="pagin">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>    
    </div>
    <div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Detail</h3><p></p>
            @php $o = count($report); @endphp 
            <b class="pull-right">Data Ditemukan</b><b id="counter" class="pull-right">{{ $o }}&nbsp;</b>
            <br/>
		</div>
        <div class="panel-body table-responsive">
            <table class="table table-hover">
                <thead>
                    <th width="5%">#</th>
                    <th>Tanggal</th>
                    <th>Dari</th>
                    <th>Ke</th>
                    <th>QTY</th>
                    <th width="25%">Keterangan</th>
                </thead>
                <tbody id="tabs">
                    @php $i=1; @endphp
                    @foreach($report as $rpt)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $rpt->created_at }}</td>
                            <td>
                                @if($rpt->transfer_from != 0)
                                    {{ $shop[$rpt->transfer_from] }}
                                @else
                                    Supplier / Produksi
                                @endif
                            </td>
                            <td>
                                @if($rpt->transfer_to != 0)
                                    {{ $shop[$rpt->transfer_to] }}
                                @else
                                    Penjualan
                                @endif
                            </td>
                            <td>
                                @if($rpt->type == "in")
                                    {{ $rpt->stock_in }}
                                @elseif($rpt->type == "out")
                                    {{ $rpt->stock_out }}
                                @elseif($rpt->type == "toshop")
                                    {{ $rpt->stock_shop }}
                                @elseif($rpt->type == "towarehouse")
                                    {{ $rpt->stock_warehouse }}
                                @elseif($rpt->type == "adjustment")
                                    {{ $rpt->stock_adjustment }}
                                @elseif($rpt->type == "retur")
                                    {{ $rpt->stock_retur }}
                                @endif
                            </td>
                            <td>
                                @if($rpt->type == "in")
                                    Barang Masuk 
                                @elseif($rpt->type == "out")
                                    Barang Keluar (Dibeli)
                                @elseif($rpt->type == "toshop")
                                    Barang Masuk ke Toko
                                @elseif($rpt->type == "towarehouse")
                                    Barang Masuk ke Gudang
                                @elseif($rpt->type == "adjustment")
                                    Penyesuaian Stok
                                @elseif($rpt->type == "retur")
                                    Retur
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="pull-right">
                <ul class="pagination" id="paginations">
                    @for($a=1; $a<=$pagin; $a++)
                        <li><a href="javascript:;" onclick="return toPage({{$a}})">{{ $a }}</a></li>               
                    @endfor
                </ul>
                <input hidden id="pages" value="1" />
            </div>
        </div>
    </div>
@endsection
@push('script')
<script type="text/javascript">
    function deleteData(idnya){
        var conf = confirm("Delete Data?");
        if(conf){
            $.ajax({
                type: "GET",
                url: "{{url('').'/admin/deletedata/'}}"+idnya+"/delete",
                success: function(){
                    location.reload();  
                }
            });
        }
    }
    
    
    $('#datepicker-start').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
        Pace.restart();
        $(this).datepicker('hide');
        if($('#datepicker-end').val() != ""){
            $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
            var pages = 1;
            
            var datanya = {
                "_token"    : "{{ csrf_token() }}",
                "limit"     : $('#pagin :selected').val(),
                "page"      : pages,
                "range"     : $('#drs').val(),
                "sort"      : $('#sort :selected').val(),
                "asc"       : $('#asc :selected').val(),
                "type"      : $('#type :selected').val()
            };
            
            $.ajax({
                url     : '{{url("admin/report/stock_movement")}}/{{$model->id}}/filter',
                type    : 'POST',
                data    : datanya,
                success : function(data){
                    $.each(data, function(i, item) {
                        if(i == 0)
                            $('#tabs').html(item);
                        else
                            $('#counter').html(item + '&nbsp;');
                    });       
                }
            });
        }else{
            $('#datepicker-end').focus();
        }

    });
    
    $('#datepicker-end').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
        Pace.restart();
        $(this).datepicker('hide');
        if($('#datepicker-start').val() != ""){
            $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
            var pages = 1;
            
            var datanya = {
                "_token"    : "{{ csrf_token() }}",
                "limit"     : $('#pagin :selected').val(),
                "page"      : pages,
                "range"     : $('#drs').val(),
                "sort"      : $('#sort :selected').val(),
                "asc"       : $('#asc :selected').val(),
                "type"      : $('#type :selected').val()
            };
            
            $.ajax({
                url     : '{{url("admin/report/stock_movement")}}/{{$model->id}}/filter',
                type    : 'POST',
                data    : datanya,
                success : function(data){
                    $.each(data, function(i, item) {
                        if(i == 0)
                            $('#tabs').html(item);
                        else
                            $('#counter').html(item + '&nbsp;');
                    });       
                }
            });
        }else{
            $('#datepicker-start').focus();
        }

    });
    
    $('#sort').on('change', function(){
       Pace.restart();
       var pages = 1;
       var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : pages,
            "range"     : $('#drs').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val(),
            "type"      : $('#type :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/stock_movement")}}/{{$model->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });       
            }
        }); 
    });
    
    $('#asc').on('change', function(){
       Pace.restart();
       var pages = 1;
       var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : pages,
            "range"     : $('#drs').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val(),
            "type"      : $('#type :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/stock_movement")}}/{{$model->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });       
            }
        }); 
    });
    
    $('#type').on('change', function(){
       Pace.restart();
       var pages = 1;
       var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : pages,
            "range"     : $('#drs').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val(),
            "type"      : $('#type :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/stock_movement")}}/{{$model->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });   
                
                if($('#pagin :selected').val() >= {{ $total }})
                    $('#paginations').html('');
                else{
                    var mods = {{ $total }} % $('#pagin :selected').val();
                    var pagination = {{ $total }} / $('#pagin :selected').val();
                    pagination = parseInt(pagination);
            
                    if(mods > 0)
                        pagination += 1;
            
                    var lists = "";
                    
                    for(var a=1; a<=pagination; a++){
                        lists += '<li><a href="javascript:;" onclick="return toPage('+a+')">'+a+'</a></li>'
                    }
               
                    $('#paginations').html(lists);
                }
            }
        }); 
    });
    
    $('#pagin').on('change', function(){
       Pace.restart();
       var pages = 1;
       var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : pages,
            "range"     : $('#drs').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val(),
            "type"      : $('#type :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/stock_movement")}}/{{$model->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });
                
                if($('#pagin :selected').val() >= {{ $total }})
                    $('#paginations').html('');
                else{
                    var mods = {{ $total }} % $('#pagin :selected').val();
                    var pagination = {{ $total }} / $('#pagin :selected').val();
                    pagination = parseInt(pagination);
            
                    if(mods > 0)
                        pagination += 1;
            
                    var lists = "";
                    
                    for(var a=1; a<=pagination; a++){
                        lists += '<li><a href="javascript:;" onclick="return toPage('+a+')">'+a+'</a></li>'
                    }
               
                    $('#paginations').html(lists);
                }
            }
        }); 
    });
    
    function toPage(pages){
        Pace.restart();
        $('#pages').val(pages);
        var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : pages,
            "range"     : $('#drs').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val(),
            "type"      : $('#type :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/stock_movement")}}/{{$model->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });
            }
        }); 
    }
</script>
@endpush