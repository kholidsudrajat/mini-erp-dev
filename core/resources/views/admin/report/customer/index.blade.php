@extends('layouts.app.frame')
@section('title', 'Laporan Penjualan per Pelanggan')
@section('description', 'Laporan Penjualan per Pelanggan')
@section('breadcrumbs', Breadcrumbs::render('sales_customer'))
@section('button', '<div class="clearfix"></div>')

@section('content')
<div class="form-group-attached" style="text-align: center">
	<div class="col-md-4">
        <a style="width:100%; padding:15px;" class="btn btn-info" href="#">Laporan Penjualan per Pelanggan</a>
	</div>
	<div class="col-md-4">
        <a style="width:100%; padding:15px;" class="btn btn-success" href="{{ url('') }}/admin/report/sales_product">Laporan Penjualan per Produk</a>
	</div>
	<div class="col-md-4">
        <a style="width:100%; padding:15px;" class="btn btn-success" href="{{ url('') }}/admin/report/sales_invoice">Laporan Penjualan per Faktur</a>
	</div>
</div>
<div class="clearfix"></div>
<br/>
<div class="form-group-attached" style="text-align: center">
	<div class="form-group form-group-default col-md-4">
		<label>Total Pelanggan</label>
		<b>{{ $cus }}</b>
	</div>
	<div class="form-group form-group-default col-md-4">
		<label>Total Penjualan</label>
		<b>{{ str_replace(',', '.', number_format($report['sale'])) }}</b>
	</div>
	<div class="form-group form-group-default col-md-4">
		<label>Total Piutang</label>
		<b>{{ str_replace(',', '.', number_format($report['debt'])) }}</b>
	</div>
</div>
<input type="hidden" id="drs" name="drange"/>
<input type="hidden" id="did" name="did"/>
<div class="form-group-attached">
	<div class="row clearfix">
		<div class="col-sm-10 col-xs-10">
			<div class="form-group form-group-default">
				<label>Pencarian</label>
				<form id="formsearch">
					<input type="text" id="search-table" class="form-control" name="firstName" placeholder="Masukkan Nama / Kode Pelanggan">
				</form>
			</div>
		</div>
        <div class="col-sm-2">
            <div class="form-group form-group-default" style="text-align:center; padding-top:15px;">
                <a onclick="location.reload()" class="btn btn-primary" href="javascript:;"><i class="fa fa-times"></i> Clear Filter</a>
            </div>
        </div>
	</div>
</div>

<div class="clearfix"></div>
<table class="table table-hover" id="sales_customer-table">
	<thead>
		<tr>
			<th width="5%"> # </th><th> Kode Pelanggan </th><th> Nama </th><th> No Telp </th><th> Alamat </th><th> Limit Piutang </th><th> Penjualan </th><th> Diskon </th><th> Piutang </th><th width="30%"> Tindakan </th>
		</tr>
	</thead>
</table>

@endsection


@push("script")
<script>

var oTable;
oTable = $('#sales_customer-table').DataTable({
    processing: true,
    serverSide: true,
    dom: 'lBfrtip',
    order:  [[ 0, "asc" ]],
    buttons: [
        {
            extend: 'print',
            autoPrint: true,
            customize: function ( win ) {
                $(win.document.body)
                    .css( 'padding', '2px' )
                    .prepend(
                        '<img src="{{asset('img/logo.png')}}" style="float:right; top:0; left:0;height: 40px;right: 10px;background: #101010;padding: 8px;border-radius: 4px" /><h5 style="font-size: 9px;margin-top: 0px;"><br/><font style="font-size:14px;margin-top: 5px;margin-bottom:20px;">LAPORAN PENJUALAN PRODUK</font><br/><br/><font style="font-size:8px;margin-top:15px;">{{date('Y-m-d h:i:s')}}</font></h5><br/><br/>'
                    );


                $(win.document.body).find( 'div' )
                    .css( {'padding': '2px', 'text-align': 'center', 'margin-top': '-50px'} )
                    .prepend(
                        ''
                    );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( { 'font-size': '9px', 'padding': '2px' } );


            },
            title: '',
            orientation: 'landscape',
            exportOptions: {columns: ':visible'} ,
            text: '<i class="fa fa-print" data-toggle="tooltip" title="" data-original-title="Print"></i>'
        },
        {extend: 'colvis', text: '<i class="fa fa-eye" data-toggle="tooltip" title="" data-original-title="Column visible"></i>'},
        {extend: 'csv', text: '<span>CSV</span>'}
    ],
    sDom: "<'table-responsive fixed't><'row'<p i>> B",
    sPaginationType: "bootstrap",
    destroy: true,
    scrollCollapse: true,
    oLanguage: {
        "sLengthMenu": "_MENU_ ",
        "sInfo": "Menampilkan <b>_START_ to _END_</b> dari _TOTAL_ entri"
    },
    lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
    ajax: {
    url: 
        '{!! route('sales_customer.data') !!}'
    ,
        data: function (d) {
            d.range = $('input[name=drange]').val();
        }
    },
    columns: [
        { data: "rownum", name: "rownum" },
        { data: "customer_code", name: "customer_code"},
        { data: "nama", name: "nama"},
        { data: "no_telp", name: "no_telp"},
        { data: "alamat", name: "alamat"},
        { data: "limit_debt", name: "limit_debt"},
        { data: "sales", name: "sales", searchable: false},
        { data: "discount", name: "discount", searchable: false},
        { data: "debt", name: "debt", searchable: false},
        { data: "action", name: "action", orderable: false , searchable: false },
    ],
}).on( 'processing.dt', function ( e, settings, processing ) {if(processing){Pace.start();} else {Pace.stop();}});

$("#sales_customer-table_wrapper > .dt-buttons").appendTo("div.export-options-container");


$('#datepicker-start').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
    $(this).datepicker('hide');
    if($('#datepicker-end').val() != ""){
        $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
        oTable.draw();
    }else{
        $('#datepicker-end').focus();
    }

});
$('#datepicker-end').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
    $(this).datepicker('hide');
    if($('#datepicker-start').val() != ""){
        $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
        oTable.draw();
    }else{
        $('#datepicker-start').focus();
    }

});

$('#formsearch').submit(function () {
    oTable.search( $('#search-table').val() ).draw();
    return false;
} );

oTable.page.len(10).draw();
</script>
@endpush
