@extends('layouts.app.frame')
@section('title', 'Detail Penjualan #' . $list_penjualan->id)
@section('description', 'Detail Penjualan')
@section('breadcrumbs', Breadcrumbs::render('sales_customer.shows', $list_penjualan))
@section('button', '<a href="'.url('/admin/report/sales_customer').'/'.$id.'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
	<div class="panel panel-default">
		<div class="panel-body">
			<br/>
			<br/>

			<div class="table-responsive">
				<table class="table table-condensed">
					<tbody>
						<tr>
							<th>ID</th>
							<td>{{ $list_penjualan->id }}</td>
						</tr>
						<tr>
							<th>No Ref</th>
							<td>{{ $list_penjualan->no_ref }}</td>
						</tr>
						<tr>
							<th>Nama Pelanggan</th>
							<td>{{ $list_penjualan->nama }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

    <div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Detail</h3>
		</div>
        <div class="panel-body table-responsive">
            <table class="table table-condensed">
                <thead>
                    <th width="10%">#</th>
                    <th>Nama Barang</th>
                    <th>Nama Model</th>
                    <th>QTY</th>
                    <th>Harga</th>
                    <th>Total</th>
                </thead>
                <tbody>
                    @php
                        if($list_penjualan->discount <= 100)
                            $discount = $list_penjualan->subtotal * ($list_penjualan->discount / 100);
                        else
                            $discount = $list_penjualan->discount;
                        $total = $list_penjualan->total + $discount;
                        $i = 1; 
                        $sub = 0; 
                    @endphp
                    @foreach($detail as $dtl)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $dtl['name'] }}</td>
                        <td>{{ $dtl['model_name'] }}</td>
                        <td>{{ $dtl['qty'] }}</td>
                        <td><span class="pull-right">{{ str_replace(',', '.', number_format($dtl['price']))  }}</span></td>
                        <td><span class="pull-right">{{ str_replace(',', '.', number_format($dtl['qty'] * $dtl['price']))  }}</span></td>
                    </tr>
                    @php $sub += $dtl['qty'] * $dtl['price'] @endphp
                    @endforeach
                    <tr>
                        <td colspan="5"><b class="pull-right">Subtotal</b></td>
                        <td><b class="pull-right">{{ str_replace(',', '.', number_format($sub)) }}</b></td>
                    </tr>
                    <tr>
                        <td colspan="5"><b class="pull-right">Potongan</b></td>
                        <td><b class="pull-right">{{ $list_penjualan->discount <= 100 ? '('.$list_penjualan->discount.'%)' : '' }} {{ str_replace(',', '.', number_format($discount)) }}</b></td>
                    </tr>
                    <tr>
                        <td colspan="5"><b class="pull-right">Total</b></td>
                        <td><b class="pull-right">{{ str_replace(',', '.', number_format($list_penjualan->total)) }}</b></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Pembayaran</h3>
		</div>
        <div class="panel-body table-responsive">
            <table class="table table-condensed">
                <thead>
                    <th>Tanggal</th>
                    <th>Keterangan</th>
                    <th width="25%">Bayar</th>
                </thead>
                <tbody>
                    @php $pays = 0; @endphp
                    @foreach($pay as $p)
                    <tr>
                        <td>{{ $p->created_at }}</td>
                        <td>
                            @if(isset($p->name))
                            Bayar dengan {{ $p->name }}
                            @else
                            Bayar Hutang
                            @endif
                        </td>
                        <td><span class="pull-right">{{str_replace(',', '.', number_format($p->pay)) }}</span></td>
                    </tr>
                    @php $pays += $p->pay; @endphp
                    @endforeach
                    <tr>
                        <td colspan="2"><b class="pull-right">Total</b></td>
                        <td><b class="pull-right">{{ str_replace(',', '.', number_format($pays)) }}</b></td>
                    </tr>
                    @if(isset($debt) && ($list_penjualan->total - $pays) > 0)
                    <tr>
                        <td colspan="2"><b class="pull-right">Hutang</b></td>
                        <td><b class="pull-right">{{str_replace(',', '.', number_format($debt->debt * -1)) }}</b></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td>
                            <div style="display:none;" id="debt">
                                <input type="number" value="{{ $debt->debt * -1 }}" id="debts" class="form-control" />
                                <br/>
                                <a class="btn btn-primary pull-right" id="pays"><i class="fa fa-check"></i></a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            <a class="btn btn-primary pull-right" id="bayar">Bayar Hutang</a>
                        </td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('script')
<script type="text/javascript">
    $('#bayar').on('click', function(){
        $('#debt').show();
        $('#debts').focus();
    });
    
    $('#pays').on('click', function(){
        $.ajax({
            type : 'POST',
            url : '{{ url("") }}/admin/list_penjualan/paydebt',
            data : {
                _token : '{{ csrf_token() }}',
                pay : $('#debts').val(),
                customer_id : '{{ $list_penjualan->customer_id }}',
                noref : '{{ $list_penjualan->no_ref }}'
            },
            success : function(){
                location.reload();
            }
        });
    });
</script>
@endpush