@extends('layouts.app.frame')
@section('title', 'Detail Penjualan Pelanggan #' . $customer->customer_code)
@section('description', 'Detail Penjualan Pelanggan')
@section('breadcrumbs', Breadcrumbs::render('sales_customer.show', $customer))
@section('button', '<a href="'.url('/admin/report/sales_customer').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
	<div class="panel panel-default">
		<div class="panel-body">
			<br/>
			<br/>

			<div class="table-responsive">
				<table class="table table-condensed">
					<tbody>
						<tr>
							<th>Kode Pelanggan</th>
							<td>{{ $customer->customer_code }}</td>
						</tr>
                        <tr>
                            <th>Nama</th>
                            <td>{{ $customer->nama }}</td>
                        </tr>
                        <tr>
                            <th>No Telp</th>
                            <td>{{ $customer->no_telp }}</td>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <td>{{ $customer->alamat }}</td>
                        </tr>
                        <tr>
                            <th>Penjualan</th>
                            <td>{{ str_replace(',', '.', number_format($customer->sales)) }}</td>
                        </tr>
                        <tr>
                            <th>Hutang</th>
                            <td>{{ str_replace(',', '.', number_format($customer->debt)) }}</td>
                        </tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

    <div class="panel panel-default">
        <div class="form-group-attached">
            <div class="row clearfix">
                <input hidden id="drs" />
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Tanggal Mulai</label>
                        <input type="text" id="datepicker-start" class="form-control" name="firstName" placeholder="pilih tanggal">
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Tanggal Berakhir</label>
                        <input type="text" id="datepicker-end" class="form-control" name="firstName" placeholder="pilih tanggal">
                    </div>
                </div>
                <div class="clearfix"><br/></div>
                <div class="col-sm-4 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Urutkan Berdasarkan</label>
                        <select class="full-width" data-init-plugin="select2" id="sort">
                            <option value="created_at">Tanggal</option>
                            <option value="no_ref">No Ref</option>
                            <option value="total">Total</option>
                            <option value="pay">Bayar</option>
                            <option value="debt">Hutang</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>&nbsp;</label>
                        <select class="full-width" data-init-plugin="select2" id="asc">
                            <option value="1">Dari Terbesar</option>
                            <option value="0">Dari Terkecil</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="form-group form-group-default">
                        <label>Data Per Halaman</label>
                        <select class="full-width" data-init-plugin="select2" id="pagin">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>    
    </div>
    <div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Detail</h3><p></p>
            @php $o = count($report); @endphp 
            <b class="pull-right">Data Ditemukan</b><b id="counter" class="pull-right">{{ $o }}&nbsp;</b>
            <br/>
		</div>
        <div class="panel-body table-responsive">
            <table class="table table-hover">
                <thead>
                    <th width="5%">#</th>
                    <th width="15%">No Ref</th>
                    <th width="20%">Tanggal</th>
                    <th>Total</th>
                    <th>Bayar</th>
                    <th>Hutang</th>
                </thead>
                <tbody id="tabs">
                    @php
                        $i = 1; 
                        $totals = 0;
                        $pay = 0;
                        $debt = 0;
                    @endphp
                    @foreach($report as $rpt)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td><a href="{{ url('').'/admin/report/sales_customer/'.$customer->id.'/'.$rpt->id }}/show">{{ $rpt->no_ref }}</a></td>
                        <td>{{ $rpt->created_at }}</td>
                        <td>{{ str_replace(',', '.', number_format($rpt->subtotal)) }}</td>
                        <td>{{ str_replace(',', '.', number_format($rpt->pay)) }}</td>
                        <td>{{ str_replace(',', '.', number_format($rpt->debt)) }}</td>
                    </tr>
                    @php
                        $totals += $rpt->subtotal;
                        $pay += $rpt->pay;
                        $debt += $rpt->debt;
                    @endphp
                    @endforeach
                    <tr>
                        <td colspan="3"><b class="pull-right">Total</b></td>
                        <td><b>{{ str_replace(',', '.', number_format($totals)) }}</b></td>
                        <td><b>{{ str_replace(',', '.', number_format($pay)) }}</b></td>
                        <td><b>{{ str_replace(',', '.', number_format($debt)) }}</b></td>
                    </tr>
                </tbody>
            </table>
            <div class="pull-right">
                <ul class="pagination" id="paginations">
                    @for($a=1; $a<=$pagin; $a++)
                        <li><a href="javascript:;" onclick="return toPage({{$a}})">{{ $a }}</a></li>               
                    @endfor
                </ul>
                <input hidden id="pages" value="1" />
            </div>
        </div>
    </div>
@endsection
@push('script')
<script type="text/javascript">
    $('#datepicker-start').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
        Pace.restart();
        $(this).datepicker('hide');
        if($('#datepicker-end').val() != ""){
            $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
            
            var datanya = {
                "_token"    : "{{ csrf_token() }}",
                "limit"     : $('#pagin :selected').val(),
                "page"      : 1,
                "range"     : $('#drs').val(),
                "sort"      : $('#sort :selected').val(),
                "asc"       : $('#asc :selected').val()
            };
            
            $.ajax({
                url     : '{{url("admin/report/sales_customer")}}/{{$customer->id}}/filter',
                type    : 'POST',
                data    : datanya,
                success : function(data){
                    $.each(data, function(i, item) {
                        if(i == 0)
                            $('#tabs').html(item);
                        else
                            $('#counter').html(item + '&nbsp;');
                    });
                }
            });
        }else{
            $('#datepicker-end').focus();
        }

    });
    
    $('#datepicker-end').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
        Pace.restart();
        $(this).datepicker('hide');
        if($('#datepicker-start').val() != ""){
            $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
            
            var datanya = {
                "_token"    : "{{ csrf_token() }}",
                "limit"     : $('#pagin :selected').val(),
                "page"      : 1,
                "range"     : $('#drs').val(),
                "sort"      : $('#sort :selected').val(),
                "asc"       : $('#asc :selected').val()
            };
            
            $.ajax({
                url     : '{{url("admin/report/sales_customer")}}/{{$customer->id}}/filter',
                type    : 'POST',
                data    : datanya,
                success : function(data){
                    $.each(data, function(i, item) {
                        if(i == 0)
                            $('#tabs').html(item);
                        else
                            $('#counter').html(item + '&nbsp;');
                    });
                }
            });
        }else{
            $('#datepicker-start').focus();
        }

    });
    
    $('#sort').on('change', function(){
       Pace.restart();
       var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : 1,
            "range"     : $('#drs').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/sales_customer")}}/{{$customer->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });
            }
        }); 
    });
    
    $('#asc').on('change', function(){
       Pace.restart();
       var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : 1,
            "range"     : $('#drs').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/sales_customer")}}/{{$customer->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });
            }
        }); 
    });
    
    $('#pagin').on('change', function(){
       Pace.restart();
       var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : 1,
            "range"     : $('#drs').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/sales_customer")}}/{{$customer->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });
                
                if($('#pagin :selected').val() >= {{ $total }})
                    $('#paginations').html('');
                else{
                    var mods = {{ $total }} % $('#pagin :selected').val();
                    var pagination = {{ $total }} / $('#pagin :selected').val();
                    pagination = parseInt(pagination);
            
                    if(mods > 0)
                        pagination += 1;
            
                    var lists = "";
                    
                    for(var a=1; a<=pagination; a++){
                        lists += '<li><a href="javascript:;" onclick="return toPage('+a+')">'+a+'</a></li>'
                    }
               
                    $('#paginations').html(lists);
                }
            }
        }); 
    });
    
    function toPage(pages){
       Pace.restart();
        var datanya = {
            "_token"    : "{{ csrf_token() }}",
            "limit"     : $('#pagin :selected').val(),
            "page"      : pages,
            "range"     : $('#drs').val(),
            "sort"      : $('#sort :selected').val(),
            "asc"       : $('#asc :selected').val()
        };

        $.ajax({
            url     : '{{url("admin/report/sales_customer")}}/{{$customer->id}}/filter',
            type    : 'POST',
            data    : datanya,
            success : function(data){
                $.each(data, function(i, item) {
                    if(i == 0)
                        $('#tabs').html(item);
                    else
                        $('#counter').html(item + '&nbsp;');
                });
            }
        }); 
    }
</script>
@endpush
