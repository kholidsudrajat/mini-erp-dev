@extends('layouts.app.frame')
@section('title', 'Detail Retur #' . $data->no_retur)
@section('description', 'Detail Retur')
@section('breadcrumbs', Breadcrumbs::render('retur-product.shows', $data))
@section('button', '<a href="'.url('/admin/report/retur-product').'/'.$id.'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
	<div class="panel panel-default">
		<div class="panel-body">
			<br/>
			<br/>

			<div class="table-responsive">
				<table class="table table-condensed">
					<tbody>
						<tr>
							<th>No Retur</th>
							<td>{{ $data->no_retur }}</td>
						</tr>
						<tr>
							<th>No Ref</th>
							<td>{{ $data->no_ref }}</td>
						</tr>
						<tr>
							<th>Tipe Retur</th>
							<td>{{ ucwords($data->type_retur) }}</td>
						</tr>
						<tr>
							<th>Nama Pelanggan</th>
							<td>{{ $data->cus }}</td>
						</tr>
						<tr>
							<th>Toko</th>
							<td>{{ $data->shopname }}</td>
						</tr>
						<tr>
							<th>Produk</th>
							<td>{{ $data->modelname }}</td>
						</tr>
						<tr>
							<th>QTY</th>
							<td>{{ $data->qty }}</td>
						</tr>
						<tr>
							<th>Total</th>
							<td>{{ ($data->type_retur != 'barang') ? number_format($data->cost) : '-' }}</td>
						</tr>
						<tr>
							<th>Note</th>
							<td>{{ $data->note }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
@push('script')
<script type="text/javascript">
    function deleteData(idnya){
        var conf = confirm("Delete Data?");
        if(conf){
            $.ajax({
                type: "GET",
                url: "{{url('').'/admin/deletedata/'}}"+idnya+"/delete",
                success: function(){
                    location.reload();  
                }
            });
        }
    }
</script>
@endpush