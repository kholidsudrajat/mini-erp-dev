@extends('layouts.app.frame')
@section('title', 'Input Biaya')
@section('description', '')
@section('breadcrumbs', Breadcrumbs::render('input-cost'))
@section('button', '<div class="clearfix"></div>')

@section('content')
<style>
    .panel-heading{
        display: none;
    }
    
    .panel .panel-heading + .panel-body{
        padding-top: 20px !important;
    }
</style>
<div id="forms">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group form-group-default required">
                <label>Tanggal</label>
                <input type="text" id="date" value="{{ date('Y/m/d') }}" class="form-control" name="date" placeholder="Masukkan Tanggal" required />
            </div>
        </div>
        <div class="col-md-6" {{ Auth::User()->shop_id != 0 ? 'style=display:none;' : '' }}>
            <div class="form-group form-group-default form-group-default-select2 required">
                <label>Toko / Gudang</label>
                {!! Form::select('shop_id', $shop, $idshop, ['data-init-plugin' => 'select2', 'id' => 'shops', 'class' => 'full-width']) !!}
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <table class="table table-bordered">
        <thead>
            <th>Deskripsi</th>
            <th>Biaya</th>
            <th width="15%">Tindakan</th>
        </thead>    
        <tbody id="tabBiaya">
            <tr>
                <td>
                    <input type="text" class="form-control" id="lbl" placeholder="Masukkan Deskripsi" />
                </td>
                <td>
                    <input type="text" class="form-control" id="biaya" placeholder="Masukkan Biaya" />
                </td>
                <td>
                    <a class="btn btn-xs btn-primary" id="tambah"><i class="fa fa-plus"></i></a>
                </td>
            </tr>
        </tbody>
    </table>
    <textarea style="display:none;" id="tampung"></textarea>
    <div class="pull-right">
        <a id="subm" class="btn btn-xs btn-primary" onclick="return submitform()">Submit</a>
        <a id="upd" style="display:none" class="btn btn-xs btn-primary" onclick="return editform()">Update</a>
        <a id="canc" style="display:none" class="btn btn-xs btn-danger" onclick="return cancels()">Cancel</a>
    </div>
    <br/>
    <hr/>
</div>
<div id="tables" class="row">
    <input type="hidden" id="drs" name="drange"/>
    <input type="hidden" id="did" name="did"/>
    <input type="hidden" id="ded" name="ded"/>
    <div class="form-group-attached col-md-12">
        <div class="row clearfix">
            <div class="col-sm-6 col-xs-6">
                <div class="form-group form-group-default">
                    <label>Tanggal Mulai</label>
                    <input type="text" id="datepicker-start" class="form-control" name="firstName" placeholder="pilih tanggal">
                </div>
            </div>
            <div class="col-sm-6 col-xs-6">
                <div class="form-group form-group-default">
                    <label>Tanggal Berakhir</label>
                    <input type="text" id="datepicker-end" class="form-control" name="firstName" placeholder="pilih tanggal">
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <table class="table table-hover" id="inputcost-table">
        <thead>
            <tr>
                <th> Tanggal </th><th> Toko </th><th> Total </th><th width="20%"> Action </th>
            </tr>
        </thead>
    </table>
</div>
@endsection


@push("script")
<script>
    var oTable;
    oTable = $('#inputcost-table').DataTable({
        processing: true,
        serverSide: true,
        dom: 'lBfrtip',
        order:  [[ 0, "asc" ]],
        buttons: [],
        sDom: "<'table-responsive fixed't><'row'<p i>> B",
        sPaginationType: "bootstrap",
        destroy: true,
        responsive: true,
        scrollCollapse: true,
        oLanguage: {
            "sLengthMenu": "_MENU_ ",
            "sInfo": "Menampilkan <b>_START_ to _END_</b> dari _TOTAL_ entri"
        },
        lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        ajax: {
        url: 
            '{!! route('input-cost.data') !!}'
        ,
            data: function (d) {
                d.range = $('input[name=drange]').val();
            }
        },
        columns: [
            { data: "date", name: "date" },
            { data: "name", name: "name" },
            { data: "cost", name: "cost" },
            { data: "action", name: "action", orderable: false , searchable: false },
        ],
    }).on( 'processing.dt', function ( e, settings, processing ) {if(processing){Pace.start();} else {Pace.stop();}});

    $("#inputcost-table_wrapper > .dt-buttons").appendTo("div.export-options-container");


    $('#datepicker-start').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
        $(this).datepicker('hide');
        if($('#datepicker-end').val() != ""){
            $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
            oTable.draw();
        }else{
            $('#datepicker-end').focus();
        }

    });
    $('#datepicker-end').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
        $(this).datepicker('hide');
        if($('#datepicker-start').val() != ""){
            $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
            oTable.draw();
        }else{
            $('#datepicker-start').focus();
        }

    });

    $('#formsearch').submit(function () {
        oTable.search( $('#search-table').val() ).draw();
        return false;
    } );

    oTable.page.len(25).draw();



    function deleteData(dates, id) {
        $('#modalDelete').modal('show');
        $('#did').val(dates);
        $('#ded').val(id);
    }

    function hapus(){
        $('#modalDelete').modal('hide');
        var datanya = {
            _token  :   '{{csrf_token()}}',
            dates   :   $('#did').val(),
            shop    :   $('#ded').val()
        }
        console.log($('#did').val());
        $.ajax({
            type    :   'POST',
            url     :   '{{ url("") }}/admin/inputcost/delete',
            data    :   datanya,
            success : function(){
                oTable.draw();
            }    
        });
    }

    $('#date').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
        $('#subm').show();
        $('#upd').hide();
        $('#canc').hide();
        $(this).datepicker('hide');
    });
    
    function submitform(){
        var jsonn = '['+$('#tampung').val().slice(0,-1)+']';

        var datanya = {
            _token  :   '{{csrf_token()}}',
            date    :   $('#date').val(),
            shop    :   $('#shops :selected').val(),
            json    :   jsonn
        }

        var validate = moment($('#date').val(),'YYYY/MM/DD',true).isValid();

        if(validate && $('#tampung').val() != ""){
            $.ajax({
                type    :   'POST',
                url     :   '{{ url("") }}/admin/inputcost/add',
                data    :   datanya,
                success : function(res){
                    if(res == 1){
                        $('.ro').remove();
                        $('#tampung').val('');
                        
                        alert('Sukses!');
                        oTable.draw();
                    }else
                        alert('Tidak Bisa Input Tanggal yang sama!');
                }
            });
        }else if(!validate){
            alert('Tanggal tidak boleh kosong!');
        }else{
            alert('Data masih kosong!');
        }
    }

    function editform(id){
        var jsonn = '['+$('#tampung').val().slice(0,-1)+']';

        var datanya = {
            _token  :   '{{csrf_token()}}',
            date    :   $('#date').val(),
            shop    :   $('#shops :selected').val(),
            shops   :   id,
            json    :   jsonn
        }

        var validate = moment($('#date').val(),'YYYY/MM/DD',true).isValid();

        if(validate){
            $.ajax({
                type    :   'POST',
                url     :   '{{ url("") }}/admin/inputcost/update',
                data    :   datanya,
                success : function(res){  
                    var dates = '{{ date("Y-m-d") }}';
                    $('.ro').remove();
                    $('#tampung').val('');
                    $('#subm').show();
                    $('#upd').hide();
                    $('#canc').hide();
                    $('#date').datepicker('setDate', dates);
                    $('#shops').select2().select2('val','{{ $idshop }}');
                    
                    alert('Sukses!');
                    oTable.draw();
                }
            });
        }
    }

    function reset(){
        location.reload();
    }    

    function edit(dates, shop){
        Pace.restart();
        $("html, body").animate({ scrollTop: 0 }, "slow");

        var datanya = {
            _token  :   '{{csrf_token()}}',
            dates   :   dates,
            shop    :   shop,
        }

        $.ajax({
            type    :   'POST',
            url     :   '{{ url("") }}/admin/inputcost/getbydate',
            data    :   datanya,
            success : function(res){
                $('.ro').remove();
                $('#tampung').val('');
                $('#lbl').val('');
                $('#biaya').val('');
                var string = '';
                
                $.each( res, function( index ) {
                    var lbl = res[index]['label'];
                    var cos = parseInt(res[index]['cost']);
                    var str = '{"label" : "'+lbl+'", "biaya" : '+cos+'},';

                    $('#tabBiaya').append('<tr class="ro" id="row_'+res[index]['id']+'"><td>'+lbl+'</td><td>'+cos.toLocaleString(2).replace(/,/g,'.')+'</td><td><a class="btn btn-xs btn-primary" onclick="return edititem('+res[index]['id']+',&#39;'+res[index]['label']+'&#39;,'+parseInt(res[index]['cost'])+')"><i class="fa fa-pencil"></i></a> &nbsp; <a class="btn btn-xs btn-danger" onclick="return removes('+res[index]['id']+',&#39;'+str.replace(/"/g,'&quot;')+'&#39;)"><i class="fa fa-times"></i></a></td></tr><tr class="ro" id="rows_'+res[index]['id']+'" style="display:none;"><td><input placeholder="Masukkan Label" type="text" class="form-control" id="lbl_'+res[index]['id']+'" value="'+res[index]['label']+'" /></td><td><input placeholder="Masukkan Biaya" type="text" class="form-control bia" id="biaya_'+res[index]['id']+'" value="'+res[index]['cost']+'" /></td><td><a class="btn btn-xs btn-success" onclick="return updateitem('+res[index]['id']+',&#39;'+str.replace(/"/g,'&quot;')+'&#39;)"><i class="fa fa-check"></i></a> &nbsp; <a class="btn btn-xs btn-danger" onclick="return cancel('+res[index]['id']+',&#39;'+res[index]['label']+'&#39;,'+parseInt(res[index]['cost'])+')"><i class="fa fa-ban"></i></a></td></tr>');
                    
                    string += str;
                });
                dates = dates.slice(0,10).replace(/\-/g, '/');
                $('#tampung').val(string);
                $('#date').datepicker('setDate', dates);
                $('#shops').select2().select2('val',shop);
                $('#subm').hide();
                $('#upd').attr('onclick','return editform('+shop+')');
                $('#upd').show();
                $('#canc').show();                
            }
        });
    }

    @foreach($ope as $op)         
    $('#field-{{ $op->id }}').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    @endforeach    

    $('#biaya').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });   
    
    var o = 1;    
    $('#tambah').on('click', function(){
        var lbl = $('#lbl').val();
        var biaya = $('#biaya').val().replace(/,/g, '.');
        
        if(lbl != "" && biaya != ""){
            var biayas = parseInt(biaya.replace(/\./g, ''));
            var tampung = $('#tampung').val();
            var strings = '{"label" : "'+lbl+'", "biaya" : '+biayas+'},';
            var str = strings.replace(/"/g,'&quot;');
            
            $('#tampung').val(tampung+strings);
            
            var tabs = '<tr class="ro" id="row_'+o+'"><td>'+lbl+'</td><td>'+biaya+'</td><td><a class="btn btn-xs btn-primary" onclick="return edititem('+o+',&#39;'+lbl+'&#39;,'+parseInt(biaya.replace(/\./g,''))+')"><i class="fa fa-pencil"></i></a> &nbsp; <a class="btn btn-xs btn-danger" onclick="return removes('+o+',&#39;'+str+'&#39;)"><i class="fa fa-times"></i></a></td></tr>';
            
            tabs += '<tr class="ro" id="rows_'+o+'" style="display:none;"><td><input placeholder="Masukkan Label" type="text" class="form-control" id="lbl_'+o+'" value="'+lbl+'" /></td><td><input placeholder="Masukkan Biaya" type="text" class="form-control bia" id="biaya_'+o+'" value="'+biaya+'" /></td><td><a class="btn btn-xs btn-success" onclick="return updateitem('+o+',&#39;'+str+'&#39;)"><i class="fa fa-check"></i></a> &nbsp; <a class="btn btn-xs btn-danger" onclick="return cancel('+o+',&#39;'+lbl+'&#39;,'+parseInt(biaya.replace(/\./g,''))+')"><i class="fa fa-ban"></i></a></td></tr>';

            $('#tabBiaya').append(tabs);
            o++;
        
            $('#lbl').val('');
            $('#biaya').val('');
        }
    });
       
    $('#lbl').add('#biaya').keypress(function(e){
        if(e.which == 13){
            $('#tambah').click();
        }
    });
        
    function edititem(id, lbl, biaya){
        $('#lbl_'+id).val(lbl);
        $('#biaya_'+id).val(biaya);
            
        $('#biaya_'+id).inputmask("numeric", {
            radixPoint: ".",
            groupSeparator: ",",
            digits: 2,
            autoGroup: true,
            rightAlign: false,
            oncleared: function () { self.Value(''); }
        });
        
        $('#row_'+id).hide();
        $('#rows_'+id).show();
    }   
        
    function cancel(id, lbl, biaya){
        $('#lbl_'+id).val(lbl);
        $('#biaya_'+id).val(biaya);
        $('#row_'+id).show();
        $('#rows_'+id).hide();
    }    
        
    function cancels(){
        location.reload();
    }
    
    function updateitem(id, str){
        var lbl = $('#lbl_'+id).val();
        var biaya = $('#biaya_'+id).val();
        var json = '{"label" : "'+lbl+'", "biaya" : '+parseInt(biaya.replace(/,/g,''))+'},';
        var olds = $('#tampung').val();
        var news = olds.replace(str, json);
        
        $('#row_'+id).html('<td>'+lbl+'</td><td>'+biaya.replace(/,/g,'.')+'</td><td><a class="btn btn-xs btn-primary" onclick="return edititem('+id+',&#39;'+lbl+'&#39;,'+parseInt(biaya.replace(/,/g,''))+')"><i class="fa fa-pencil"></i></a> &nbsp; <a class="btn btn-xs btn-danger" onclick="return removes('+id+',&#39;'+json.replace(/"/g,'&quot;')+'&#39;)"><i class="fa fa-times"></i></a></td>');
        $('#tampung').val(news);
        
        $('#row_'+id).show()
        $('#rows_'+id).hide()
    };
        
    function removes(id, str){
        var olds = $('#tampung').val();
        var news = olds.replace(str, '');
        
        $('#row_'+id).remove();
        $('#tampung').val(news);
    };
</script>
@endpush
