@extends('layouts.app.frame')
@section('title', 'Edit Salesman')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('salesman.edit', $salesman))
@section('button', '<a href="'.url('/admin/salesman').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')


    {!! Form::model($salesman, [
            'method' => 'PATCH',
            'url' => ['/admin/salesman', $salesman->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}

        @include ('admin.salesman.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');

});
</script>
@endpush