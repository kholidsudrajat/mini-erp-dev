@extends('layouts.app.frame')
@section('title', 'Create New Produk')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('model.new'))
@section('button', '<a href="'.url('/admin/model').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/model', 'id' => 'formValidate', 'files' => true, 'novalidate' => '-']) !!}

		@include ('admin.model.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
    $(document).ready(function() {
        $('#formValidate').validate();
        $('#formValidate').removeAttr('novalidate');
        $('#formValidate').validate();
    });
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#views').css('background-image', 'url('+e.target.result+')');
                $('#views').css('background-repeat', 'no-repeat');
                $('#views').css('background-size', 'contain');
                $('#views').css('background-position', '50% 50%');
                $('#views').css('height', '300px');
                $('#views').css('width', '200px');
                $('#views').css('margin', '5px auto');
                $('#views').css('padding', '5px');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgs").change(function() {
        Pace.restart();
        readURL(this);
    });
</script>
@endpush
