@extends('layouts.app.frame')
@section('title', 'Produk')
@section('description', 'Produk Management')
@section('breadcrumbs', Breadcrumbs::render('model'))
@section('button', '<a href="'.url('/admin').'/model/create'.'" class="btn btn-primary btn-xs no-border">Tambah Data Produk <i class="fa fa-plus"></i></a>')

@section('content')
<div id="desktop">
    <div class="dt-buttons btn-group pull-right">
        <a class="btn btn-warning buttons-collection buttons-colvis" tabindex="0" aria-controls="model-table" href="#" onclick="return togglelist(2)"><span><i class="fa fa-th-large"></i></span></a>
        <a class="btn btn-danger buttons-collection buttons-colvis" tabindex="0" aria-controls="model-table" href="#" onclick="return togglelist(1)"><span><i class="fa fa-list"></i></span></a>
    </div>
    <div class="clearfix"></div><br/>
    <input type="hidden" id="drs" name="drange"/>
    <input type="hidden" id="did" name="did"/>
    <div class="form-group-attached">
        <div class="row clearfix">
            <div class="col-sm-6 col-xs-12">
                <div class="form-group form-group-default">
                    <label>Pencarian</label>
                    <form id="formsearch">
                        <input type="text" id="search-table" class="form-control" name="firstName" placeholder="Masukkan Keyword">
                    </form>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group form-group-default">
                    <label>Tanggal Mulai</label>
                    <input type="text" id="datepicker-start" class="form-control" name="firstName" placeholder="pilih tanggal">
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group form-group-default">
                    <label>Tanggal Berakhir</label>
                    <input type="text" id="datepicker-end" class="form-control" name="firstName" placeholder="pilih tanggal">
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <table class="table table-hover" id="model-table">
        <thead>
            <tr>
                <th width="5%"> # </th><th> Gambar </th><th> Nama </th><th> Kode </th><th> Kategori/Variant </th><th> Harga Jual </th><th> Total Qty </th><th> Keterangan </th><th width="15%"> Tindakan </th>
            </tr>
        </thead>
    </table>
</div>
<div id="mobile">
    <div class="form-group-attached">
        <div class="row clearfix">
            <div class="col-sm-4">
                <div class="form-group form-group-default">
                    <label>Pencarian</label>
                    <form id="formsearch">
                        <input type="text" id="searchs" class="form-control" name="firstName" placeholder="pencarian"  style="height: 39px;">
                    </form>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group form-group-default">
                    <label>Urutkan Berdasarkan</label>
                    <select id="sort" class="full-width" data-init-plugin="select2">
                        <option value="default">Default</option>
                        <option value="name">Nama Barang</option>
                        <option value="model_name">Kode</option>
                        <option value="price">Harga</option>
                        <option value="qty">Stok</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group form-group-default">
                    <label>&nbsp;</label>
                    <select id="asc" class="full-width" data-init-plugin="select2">
                        <option value="asc">Dari Terkecil</option>
                        <option value="desc">Dari Terbesar</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th width="5%"> # </th><th width="10%"> Gambar </th><th> Keterangan </th>
            </tr>
        </thead>
        <tbody id="tabs">
            @foreach($model as $mdl)
                <tr>
                    <td>{{ $mdl->rownum }}</td>
                    <td>
                        <a href="{{url('admin/model').'/'.$mdl->id }}/changeimage" id="iframe">
                            <img src="{{url('files/models').'/'.$mdl->image}}" style="max-width:150px;" />
                        </a>
                    </td>
                    <td>
                        {{ $mdl->name }} - ( {{ $mdl->model_name }} )
                        <br/>
                        Harga : {{ number_format($mdl->price) }}
                        <br/>
                        Stok : <a href="{{ url('admin/stock') }}?kode={{ $mdl->model_name }}">{{ intval($mdl->qty) }}</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection


@push("script")
<script>

var oTable;
oTable = $('#model-table').DataTable({
    processing: true,
    serverSide: true,
    dom: 'lBfrtip',
    order:  [[ 0, "asc" ]],
    buttons: [],
    sDom: "<'table-responsive fixed't><'row'<p i>> B",
    sPaginationType: "bootstrap",
    destroy: true,
    responsive: true,
    scrollCollapse: true,
    oLanguage: {
        "sLengthMenu": "_MENU_ ",
        "sInfo": "Menampilkan <b>_START_ to _END_</b> dari _TOTAL_ entri"
    },
    lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
    ajax: {
    url: 
        '{!! route('model.data') !!}'
    ,
        data: function (d) {
            d.range = $('input[name=drange]').val();
        }
    },
    columns: [
        { data: "rownum", name: "rownum" },
        { data: "image", name: "image" },
        { data: "name", name: "name" },
        { data: "model_name", name: "model_name" },
        { data: "category", name: "category" },
        { data: "price", name: "price" },
        { data: "qty", name: "qty" },
        { data: "info", name: "info" },
        { data: "action", name: "action", orderable: false , searchable: false },
    ],
}).on( 'processing.dt', function ( e, settings, processing ) {if(processing){Pace.start();} else {Pace.stop();}});

$("#model-table_wrapper > .dt-buttons").appendTo("div.export-options-container");


$('#datepicker-start').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
    $(this).datepicker('hide');
    if($('#datepicker-end').val() != ""){
        $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
        oTable.draw();
    }else{
        $('#datepicker-end').focus();
    }

});
$('#datepicker-end').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
    $(this).datepicker('hide');
    if($('#datepicker-start').val() != ""){
        $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
        oTable.draw();
    }else{
        $('#datepicker-start').focus();
    }

});

$('#formsearch').submit(function () {
    oTable.search( $('#search-table').val() ).draw();
    return false;
} );

oTable.page.len(25).draw();

function togglelist(code){
    if(code == 1)
        oTable.column( 1 ).visible( false ).page.len(25).draw();
    else        
        oTable.column( 1 ).visible( true ).page.len(25).draw();
}

function deleteData(id) {
    $('#modalDelete').modal('show');
    $('#did').val(id);
}

function hapus(){
    $('#modalDelete').modal('hide');
    var id = $('#did').val();
    $.ajax({
        url: '{{url("admin/model")}}' + "/" + id + '?' + $.param({"_token" : '{{ csrf_token() }}' }),
        type: 'DELETE',
        complete: function(data) {
            oTable.draw();
        }
    });
}

$(oTable.body()).on('click', 'td', function () {
    var currentColumnIndex = oTable.cell(this).index().column;
    var row = oTable.row($(this).parents('tr'));
    var cell = oTable.cell(this).node();
    var oldValue = oTable.cell(this).data();
    var id = $(this).parents('tr').attr('id');
    
    if(currentColumnIndex == 2){
        $(cell).html('<input onblur="this.focus()" autofocus class="fokus form-control" id="input_'+currentColumnIndex+'" type="text" value="'+oldValue+'" /> <div style="padding:5px 0px;"> \n\
                     <a onclick="return updateData('+id+')" class="btn btn-xs btn-success"><i class="fa fa-check"></i></a> \n\
                     <a onclick="return cancelUpdate()" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a> \n\
    </div>');
    }
    
    $('#input_'+currentColumnIndex).focus();
});
    
$('input[type=text]').bind('touchstart click', function(){
    $(this).focus();
});
    
function updateData(id){
    if($('#input_1').length > 0){
        var datanya = {
            images : $('#input_1').val(),
            _token : '{{ csrf_token() }}'
        }
    }else if($('#input_2').length > 0){
        var datanya = {
            name : $('#input_2').val(),
            _token : '{{ csrf_token() }}'
        }
    }else if($('#input_3').length > 0){
        var datanya = {
            name : $('#input_3').val(),
            _token : '{{ csrf_token() }}'
        }
    }
    
    $.ajax({
        type : 'POST',
        url : '{{ url("") }}/admin/model/'+id+'/update',
        data : datanya,
        success : function(){
            oTable.draw();
        }
    });
}
    
function cancelUpdate(){
    oTable.draw();
}
    
$('#iframe').fancybox({
    type : 'iframe',
    afterLoad : function(){
        $('.fancybox-iframe').contents().find(".container-fluid.relative").remove();
    },
    afterClose : function(){
        oTable.draw();
    }
});
    
$('#sort').add('#asc').on('change', function(){
    var datanya = {
        keyword :   $('#searchs').val(),
        sort    :   $('#sort :selected').val(),
        asc     :   $('#asc :selected').val(),
        _token  :   '{{ csrf_token() }}'
    }
    
    $.ajax({
        type : 'POST',
        url  : '{{ url("") }}/admin/model/filter',
        data : datanya,
        success : function(datas){
            Pace.restart();
            $('#tabs').html(datas);
        }
    });
});
</script>
@endpush
