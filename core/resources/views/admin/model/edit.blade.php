@extends('layouts.app.frame')
@section('title', 'Edit Produk')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('model.edit', $model))
@section('button', '<a href="'.url('/admin/model').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')


    {!! Form::model($model, [
            'method' => 'PATCH',
            'url' => ['/admin/model', $model->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}

        @include ('admin.model.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
    $(document).ready(function() {
        $('#formValidate').validate();
    });
    
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        paste_data_images: true,
        height : 250,
        plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        image_advtab: true
    });
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#views').css('background-image', 'url('+e.target.result+')');
                $('#views').css('background-repeat', 'no-repeat');
                $('#views').css('background-size', 'contain');
                $('#views').css('background-position', '50% 50%');
                $('#views').css('height', '300px');
                $('#views').css('width', '200px');
                $('#views').css('margin', '5px auto');
                $('#views').css('padding', '5px');
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgs").change(function() {
        Pace.restart();
        readURL(this);
    });
</script>
@endpush