@extends('layouts.app.frame')
@section('title', 'Edit Produk')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('model.edit', $model))
@section('button', '<div class="clearfix"></div>')

@section('content')


    {!! Form::model($model, [
            'method' => 'PATCH',
            'url' => ['/admin/model', $model->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}

        @include ('admin.model.changes')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();

});
    
tinymce.init({
      selector: "textarea",
      theme: "modern",
      paste_data_images: true,
      height : 250,
      plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
      ],
      toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
      image_advtab: true
    });
</script>
@endpush