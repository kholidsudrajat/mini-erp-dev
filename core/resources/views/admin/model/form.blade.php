<div aria-required="true" class="form-group required form-group-default {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Nama') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
</div>
{!! $errors->first('name', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group required form-group-default {{ $errors->has('model_name') ? 'has-error' : ''}}">
    {!! Form::label('model_name', 'Kode') !!}
    {!! Form::text('model_name', null, ['class' => 'form-control', 'required']) !!}
</div>
{!! $errors->first('model_name', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group form-group-default required {{ $errors->has('category') ? 'has-error' : ''}}">
    {!! Form::label('category', 'Kategori/Variant') !!}
    {!! Form::text('category', null, ['class' => 'form-control', 'required']) !!}
</div>
{!! $errors->first('category', '<label class="error">:message</label>') !!}

@if(isset($model))
<div class="form-group form-group-default col-md-6">
    <label>QR Code</label>
    <div style="text-align:center; height:371px;">
        {!! QrCode::size(100)->generate('http://minierp.koorting.com/minierp/admin/cashier?keyword='.$model['model_name']); !!}
    </div>
</div>
@endif

<div aria-required="true" class="form-group form-group-default col-md-6 {{ $errors->has('images') ? 'has-error' : ''}}">
    {!! Form::label('images', 'Gambar') !!}
    <br/>
    <div>
        @if(isset($model))
            @if($model['image'] != "")
                <div id="views" style="margin:5px auto; padding:5px; background-image: url({{ url('files/models').'/'.$model['image'] }}); height:300px; width:200px; background-position: 50% 50%; background-size:contain; background-repeat: no-repeat;"></div>
            @endif
        @else
            <div id="views"></div>
        @endif
    </div>
    <div class="clearfix"></div>
    <br/>
    {!! Form::file('images',  ['class' => 'form-control', 'id' => 'imgs']) !!}
</div>
{!! $errors->first('images', '<label class="error">:message</label>') !!}
<div class="clearfix"></div>

<div aria-required="true" class="form-group form-group-default {{ $errors->has('info') ? 'has-error' : ''}}">
    {!! Form::label('info', 'Keterangan') !!}
    {!! Form::textarea('info', null, ['class' => 'full-width']) !!}
</div>
{!! $errors->first('info', '<label class="error">:message</label>') !!}

<div class="panel-heading" style="padding-left:4px;">
    <div class="panel-title">
        Ongkos
    </div>
</div>
<hr>
@foreach($cost as $costs)
<div style="display:none;" aria-required="true" class="col-md-6 form-group form-group-default {{ $errors->has($costs['id']) ? 'has-error' : ''}}">
    @php $mc = 0; @endphp
    @if(isset($modelcost))
        @foreach($modelcost as $mcs)
            @if($mcs['cost_id'] == $costs['id'])
                @php $mc = $mcs['costs']; @endphp
            @endif
        @endforeach
    @endif
    {!! Form::label($costs['id'], $costs['name']) !!}
    {!! Form::text($costs['id'], $mc, ['id' => 'cost'.$costs['id'], 'class' => 'form-control']) !!}
</div>
{!! $errors->first($costs['id'], '<label class="error">:message</label>') !!}
@endforeach
<br/>
<div class="col-md-6">
    <div aria-required="true" class="form-group required form-group-default {{ $errors->has('total_cost') ? 'has-error' : ''}}">
        {!! Form::label('total_cost', 'Modal Dasar') !!}
        {!! Form::text('total_cost', null, ['id' => 'total_cost', 'class' => 'form-control', 'required' => '']) !!}
    </div>
</div>
<div class="col-md-6">
    <div aria-required="true" class="form-group required form-group-default {{ $errors->has('price') ? 'has-error' : ''}}">
        {!! Form::label('price', 'Harga') !!}
        {!! Form::text('price', null, ['id' => 'price', 'class' => 'form-control', 'required' => '']) !!}
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-6">
    <div aria-required="true" class="form-group form-group-default {{ $errors->has('diff') ? 'has-error' : ''}}">
        {!! Form::label('diff', 'Selisih', ['id' => 'diff-style']) !!}
        {!! Form::text('diff', null, ['id' => 'diff', 'readonly' => '', 'class' => 'form-control']) !!}
    </div>
</div>
@if(!isset($model))
<div class="clearfix"></div>
<br/>
<div class="panel-heading" style="padding-left:4px;">
    <div class="panel-title">
        Stok
    </div>
    <hr/>
</div>
<div class="col-md-12">
    <table class="table table-bordered">
        <thead>
            <th>Toko / Gudang</th>
            <th colspan="2">QTY</th>
        </thead>
        <tbody id="tabs">
            <tr>
                <td width="45%">{!! Form::select('shop_id', $shop, null, ['id' => 'shop_id', 'class' => 'form-control', 'data-init-plugin' => 'select2']) !!}</td>
                <td width="45%">{!! Form::number('qty', null, ['id' => 'qty', 'class' => 'form-control', 'min' => '0', 'placeholder' => '0']) !!}</td>
                <td width="5%">
                    <a class="btn btn-primary" id="addstock"><i class="fa fa-plus"></i></a>
                </td>
            </tr>
        </tbody>
    </table>
    <textarea style="display:none;" id="sjson" name="sjson"></textarea>
</div>
@endif
<br/>
{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Save', ['type' => 'submit', 'class' => 'btn btn-success']) !!}
<a href="{{ url('/admin/model')}}" class="btn btn-info no-border">Cancel</a>

@push('script')
<script type="text/javascript">
    $('#addstock').on('click', function(){
        var shop = $('#shop_id :selected').val();
        var shops = $('#shop_id :selected').text();
        var qty = parseFloat($('#qty').val());
        
        if(qty > 0){
            var tab = '';
            if($('#row_'+shop).length < 1){                
                tab += '<tr id="row_'+ shop +'">';
                tab += '<td>'+ shops +'</td>';
                tab += '<td id="qty_'+ shop +'">'+ qty +'</td>';
                tab += '<td><a id="btn_'+ shop +'" class="btn btn-danger" onclick="hapusstock('+ shop +', '+ qty +')"><i class="fa fa-times"></i></a></td>';
                tab += '</tr>';

                $('#tabs').append(tab);
                
                var json = '{\"'+ shop +'\" : '+ qty +'},';
                $('#sjson').append(json);
            }else{
                var qtys = $('#qty_'+shop).html();
                var qt = parseFloat(qtys) + qty;
                
                $('#qty_'+shop).html(qt);
                $('#btn_'+shop).attr('onclick', 'hapusstock('+ shop +', '+ qt +')');
                
                var json = $('#sjson').val().replace('{\"'+ shop +'\" : '+ qtys +'},', '{\"'+ shop +'\" : '+ qt +'},');
                $('#sjson').val(json);
            }
            $('#qty').val(0);
            $('#qty').focus();
        }
        Pace.restart();
    })
    
    function hapusstock(shop, qty){
        Pace.restart();
        var json = $('#sjson').val().replace('{\"'+ shop +'\" : '+ qty +'},', '');
        $('#sjson').val(json);
        $('#row_'+shop).remove();
    }
    
    @foreach($cost as $costs)
    $('#cost{{$costs["id"]}}').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    
    $('#cost{{$costs["id"]}}').on('change', function(){
        @foreach($cost as $costs)
        var cost{{$costs['id']}} = parseInt($('#cost{{$costs["id"]}}').val().replace(/,/g, ''));
        @endforeach 
        
        var total_cost = @foreach($cost as $costs) cost{{$costs['id']}} + @endforeach 0;
        
        var price = parseInt($('#price').val().replace(/,/g, ''));
        var diff = price - total_cost;
         
        $('#total_cost').val(total_cost);
        $('#diff').val(diff);
    });
    @endforeach
    
    $('#price').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    $('#total_cost').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    $('#diff').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    
    $('#price').on('keyup', function(){
        var total_cost = $('#total_cost').val().replace(/,/g, '');
        var price = parseInt($('#price').val().replace(/,/g, ''));
        var diff = price - total_cost;

        if(diff > 0){
            $('#diff-style').html('profit');
            $('#diff-style').css('color', 'green');
        }else{
            $('#diff-style').html('loss');
            $('#diff-style').css('color', 'red');
        }

        $('#diff').val(diff);
    });

    var diff =  $('#diff').val();
    diff = 0 + diff.replace(/,/g, '');
    diff = parseFloat(diff);
    $(document).ready(function(){
        if(diff >= 0){
            $('#diff-style').html('profit');
            $('#diff-style').css('color', 'green');
        }else{
            $('#diff-style').html('loss');
            $('#diff-style').css('color', 'red');
        }
    });
</script>
@endpush
