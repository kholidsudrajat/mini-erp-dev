<div aria-required="true" class="form-group required form-group-default {{ $errors->has('images') ? 'has-error' : ''}}">
    {!! Form::label('images', 'Gambar') !!}
    @if(isset($model))
    @if($model['image'] != "")
        <img src="{{ url('files/models').'/'.$model['image'] }}" style="max-width:200px; margin: 10px 0px;" />
        {!! Form::file('images', null, ['class' => 'form-control', 'required' => '']) !!}
    @endif
    @else
        {!! Form::file('images', null, ['class' => 'form-control', 'required' => '']) !!}
    @endif
</div>
{!! $errors->first('images', '<label class="error">:message</label>') !!}

{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Save', ['type' => 'submit', 'class' => 'btn btn-success']) !!}