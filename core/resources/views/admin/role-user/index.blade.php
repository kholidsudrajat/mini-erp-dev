@extends('layouts.app.frame')
@section('title', 'Role User')
@section('description', 'Role User')
@section('breadcrumbs', Breadcrumbs::render('role-user'))
@section('button', '<a href="'.url('/admin').'/user'.'" class="btn btn-info btn-xs no-border">Back</a>')

@section('content')

    <form id="formValidate" method="post">
        <div class="col-sm-3 col-xs-6">
            <div aria-required="true" class="form-group required form-group-default {{ $errors->has('role') ? 'has-error' : ''}}">
                {!! Form::label('role', 'Role') !!}
                <input type="text" name="role" id="role" class="form-control" required>
            </div>
            {!! $errors->first('role', '<label class="error">:message</label>') !!}
        </div>
        <div class="col-span1">
            <a class="btn btn-success" style="margin-top: 10px;" id="tambah" onclick="tambah();">Tambah</a>
        </div>
    </form>
    <hr>
    <input type="hidden" id="drs" name="drange"/>
    <input type="hidden" id="did" name="did"/>
    <div class="form-group-attached">
        <div class="row clearfix">
            <div class="col-sm-6 col-xs-12">
                <div class="form-group form-group-default">
                    <label>Pencarian</label>
                    <form id="formsearch">
                        <input type="text" id="search-table" class="form-control" name="firstName" placeholder="Masukkan Keyword">
                    </form>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group form-group-default">
                    <label>Tanggal Mulai</label>
                    <input type="text" id="datepicker-start" class="form-control" name="firstName" placeholder="pilih tanggal">
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group form-group-default">
                    <label>Tanggal Berakhir</label>
                    <input type="text" id="datepicker-end" class="form-control" name="firstName" placeholder="pilih tanggal">
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <table class="table table-hover" id="roleuser-table">
        <thead>
        <tr>
            <th> Role </th><th> Action </th>
        </tr>
        </thead>
    </table>

@endsection

@push('script')
    <script>
        var oTable;
        oTable = $('#roleuser-table').DataTable({
            processing: true,
            serverSide: true,
            dom: 'lBfrtip',
            order:  [[ 0, "asc" ]],
            buttons: [],
            sDom: "<'table-responsive fixed't><'row'<p i>> B",
            sPaginationType: "bootstrap",
            destroy: true,
            responsive: true,
            scrollCollapse: true,
            oLanguage: {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Menampilkan <b>_START_ to _END_</b> dari _TOTAL_ entri"
            },
            lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
            ajax: {
                url:
                    '{!! route('role-user.data') !!}'
                ,
                data: function (d) {
                    d.range = $('input[name=drange]').val();
                }
            },
            columns: [
                { data: "role", name: "role" },
                { data: "action", name: "action", orderable: false , searchable: false },
            ],
        }).on( 'processing.dt', function ( e, settings, processing ) {if(processing){Pace.start();} else {Pace.stop();}});

        $("#cost-table_wrapper > .dt-buttons").appendTo("div.export-options-container");


        $('#datepicker-start').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
            $(this).datepicker('hide');
            if($('#datepicker-end').val() != ""){
                $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
                oTable.draw();
            }else{
                $('#datepicker-end').focus();
            }

        });
        $('#datepicker-end').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
            $(this).datepicker('hide');
            if($('#datepicker-start').val() != ""){
                $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
                oTable.draw();
            }else{
                $('#datepicker-start').focus();
            }

        });

        $('#formsearch').submit(function () {
            oTable.search( $('#search-table').val() ).draw();
            return false;
        } );

        oTable.page.len(25).draw();

        function deleteData(id) {
            $('#modalDelete').modal('show');
            $('#did').val(id);
        }

        function hapus(){
            $('#modalDelete').modal('hide');
            var id = $('#did').val();
            $.ajax({
                url: '{{url("admin/role-user/delete")}}' + "/" + id,
                type: 'GET',
                complete: function(data) {
                    oTable.draw();
                }
            });
        }

        $('#role').keypress(function(e){
            if(e.which == 13){
                $('#tambah').click();
                return false;
            }
        });

        function tambah()
        {
            if($('#role').val() == ''){
                alert('Role tidak boleh kosong');
                return false;
            } else {
                $.ajax({
                    url: '{{url('/')}}/admin/role-user/tambah',
                    type: 'POST',
                    data: {
                        role: $('#role').val(),
                        _token: '{{ csrf_token() }}'
                    },
                    success: function (data) {
                        console.log(data);
                        window.location.reload();
                    }
                });
            }
        }
    </script>
@endpush
