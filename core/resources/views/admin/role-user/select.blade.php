@extends('layouts.app.frame')
@section('title', 'Master Role User')
@section('description', 'Choose Menu Role User')
@section('breadcrumbs', Breadcrumbs::render('role-user'))
@section('button', '<a href="'.url('/admin').'/user'.'" class="btn btn-info btn-xs no-border">Back</a>')

@section('content')

    <form id="formValidate" method="POST" action="{{ url('/admin/insert/role-menu') }}">
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $user->id }}">
        @php
            if(isset($user)){
                $all_data = explode(",", $user->role_menu);
            }else{
                $all_data = array();
            }
        @endphp
        <div aria-required="true" class="form-group required form-group-default {{ $errors->has('role') ? 'has-error' : ''}}">
            {!! Form::label('role', 'Role Menu') !!}
            <div class="row">
                <div class="col-sm-4">
                    <ul style="list-style: none;">
                        @foreach($role as $roles)
                            @if($roles->id == 1)
                                <li class="parent1">
                                    {{ Form::checkbox('role_menu[]', $roles->id, in_array($roles->id, $all_data)) }} {{ $roles->menu }}
                                </li>
                            @elseif($roles->parent_menu == 1)
                                <ul style="list-style: none;">
                                <li class="child1">
                                    {{ Form::checkbox('role_menu[]', $roles->id, in_array($roles->id, $all_data)) }} {{ $roles->menu }}
                                </li>
                                </ul>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-4">
                    <ul style="list-style: none;">
                        @foreach( $role as $roles)
                            @if($roles->id == 2)
                                <li class="parent2">
                                    {{ Form::checkbox('role_menu[]', $roles->id, in_array($roles->id, $all_data)) }} {{ $roles->menu }}
                                </li>
                            @elseif($roles->parent_menu == 2)
                                <ul style="list-style: none;">
                                <li class="child2">
                                    {{ Form::checkbox('role_menu[]', $roles->id, in_array($roles->id, $all_data)) }} {{ $roles->menu }}
                                </li>
                                </ul>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-4">
                    <ul style="list-style: none;">
                        @foreach( $role as $roles)
                            @if($roles->id == 3)
                                <li class="parent3">
                                    {{ Form::checkbox('role_menu[]', $roles->id, in_array($roles->id, $all_data)) }} {{ $roles->menu }}
                                </li>
                            @elseif($roles->parent_menu == 3)
                                <ul style="list-style: none;">
                                    <li class="child3">
                                        {{ Form::checkbox('role_menu[]', $roles->id, in_array($roles->id, $all_data)) }} {{ $roles->menu }}
                                    </li>
                                </ul>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <div class="clearfix"></div>
                <hr/>
                <div class="col-sm-4">
                    <ul style="list-style: none;">
                        @foreach( $role as $roles)
                            @if($roles->id == 32)
                                <li class="parent3">
                                    {{ Form::checkbox('role_menu[]', $roles->id, in_array($roles->id, $all_data)) }} {{ $roles->menu }}
                                </li>
                            @elseif($roles->parent_menu == 32)
                                <ul style="list-style: none;">
                                    <li class="child3">
                                        {{ Form::checkbox('role_menu[]', $roles->id, in_array($roles->id, $all_data)) }} {{ $roles->menu }}
                                    </li>
                                </ul>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-4">
                    <ul style="list-style: none;">
                        @foreach( $role as $roles)
                            @if($roles->id == 35)
                                <li class="parent3">
                                    {{ Form::checkbox('role_menu[]', $roles->id, in_array($roles->id, $all_data)) }} {{ $roles->menu }}
                                </li>
                            @elseif($roles->parent_menu == 35)
                                <ul style="list-style: none;">
                                    <li class="child3">
                                        {{ Form::checkbox('role_menu[]', $roles->id, in_array($roles->id, $all_data)) }} {{ $roles->menu }}
                                    </li>
                                </ul>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-4">
                    <ul style="list-style: none;">
                        @foreach( $role as $roles)
                            @if($roles->id == 39)
                                <li class="parent3">
                                    {{ Form::checkbox('role_menu[]', $roles->id, in_array($roles->id, $all_data)) }} {{ $roles->menu }}
                                </li>
                            @elseif($roles->parent_menu == 39)
                                <ul style="list-style: none;">
                                    <li class="child3">
                                        {{ Form::checkbox('role_menu[]', $roles->id, in_array($roles->id, $all_data)) }} {{ $roles->menu }}
                                    </li>
                                </ul>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        {!! $errors->first('role', '<label class="error">:message</label>') !!}
        <div class="col-span1">
            <button class="btn btn-success" style="margin-top: 10px;">Tambah</button>
        </div>
    </form>
@endsection

@push('script')
    <script>
        $('li :checkbox').on('click', function () {
            var $chk = $(this),
                $li = $chk.closest('li'),
                $ul, $parent;
            if ($li.has('ul')) {
                $li.find(':checkbox').not(this).prop('checked', this.checked)
            }
            do {
                $ul = $li.parent();
                $parent = $ul.siblings(':checkbox');
                if ($chk.is(':checked')) {
                    $parent.prop('checked', $ul.has(':checkbox:not(:checked)').length == 0)
                } else {
                    $parent.prop('checked', false)
                }
                $chk = $parent;
                $li = $chk.closest('li');
            } while ($ul.is(':not(.someclass)'));
        });
    </script>
@endpush
