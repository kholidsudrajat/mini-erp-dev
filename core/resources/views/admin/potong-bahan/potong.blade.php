@extends('layouts.app.frame')
@section('title', 'Potong Bahan')
@section('description', 'Potong Bahan')
@section('breadcrumbs', Breadcrumbs::render('potong-bahan.potong'))
@section('button', '<a href="'.url("").'/admin/potong-bahan" class="btn btn-info btn-xs no-border">Back</a>')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Form</h3>
    </div>
    <div class="panel-body">
        <form>
            <div class="form-group col-md-6">
                <div class="form-group">
                    <label>Tanggal Potong</label>
                    <input id="tanggal" style="color:black;" value="{{ date('Y-m-d') }}" class="form-control" readonly />
                </div>
                <div class="form-group">
                    <label>Supplier</label>
                    {!! Form::select('supplier', $supplier, null, ['class' => 'form-control', 'data-init-plugin' => 'select2', 'id' => 'suppliers']) !!}
                </div>
            </div>
            <div class="form-group col-md-6">
                <div class="form-group">
                    <label>Bahan</label>
                    {!! Form::select('bahan', array('-'), null, ['data-init-plugin' => 'select2', 'class' => 'form-control', 'id' => 'bahans']) !!}
                </div>
                <div class="form-group">
                    <label>Motif</label>
                    {!! Form::select('motif', array('-'), null, ['data-init-plugin' => 'select2', 'class' => 'form-control', 'id' => 'motifs']) !!}
                </div>
            </div>
        </form>
        <div class="pull-right">
            <a class="btn btn-success" id="cekstock">Cek Stock</a>
        </div>
    </div>
    <div class="panel-body" id="stocks" style="display:none;">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <th width="5%">#</th>
                    <th>Warna</th>
                    <th>Roll</th>
                    <th width="30%">Kg</th>
                </thead>
                <tbody id="list_stock">
                </tbody>
            </table>
        </div>
        <a class="btn btn-success" id="submit">Submit</a>
        <textarea style="display:none;0" id="bahandetail"></textarea>
    </div>
</div>
@endsection

@push("script")
<script type="text/javascript">
    $('#suppliers').on('change', function(){
        var supp = $('#suppliers :selected').val();
        
        if(supp > 0){
            $('#bahandetail').val('');
            $('#supp').val(supp);
            $.ajax({
                type    : 'GET',
                url     : '{{ url("") }}/admin/pilihbahan/'+supp,
                success : function(res){
                    $.each(res, function(index, value){
                        if(index == 'bahan'){
                            $('#bahans').html('').select2({data: [{id: '', text: ''}]});
                            var data = [];
                            $.each(value, function(val){
                                data.push({id : value[val]['id'], text : value[val]['name']});
                            })
                            $('#bahans').html('').select2({
                                data: data
                            });
                        }else if(index == 'motif'){
                            $('#motifs').html('').select2({data: [{id: '', text: ''}]});
                            var data = [];
                            $.each(value, function(val){
                                data.push({id : value[val]['id'], text : value[val]['name']});
                            })
                            $('#motifs').html('').select2({
                                data: data
                            });
                        }
                    })
                }
            })
        }
    });
    
    $('#cekstock').on('click', function(){
        $.ajax({
            type    : 'POST',
            url     : '{{ url("") }}/admin/potong-bahan/cekstock',
            data    : {
                _token  : '{{ csrf_token() }}',
                supplier: $('#suppliers :selected').val(),
                bahan   : $('#bahans :selected').val(),
                motif   : $('#motifs :selected').val()
            },
            beforeSend: function(){
                $('#stocks').hide();
                Pace.restart();
            },
            success : function(res){
                $('#list_stock').html(res);
                $('#stocks').show();
            }
        })
    });
    
    function addbahan(id){
        var teks = $('#bahandetail').val();
        
        if($('#chk_'+id).is(':checked'))
            teks = teks + ',' + id;
        else
            teks = teks.replace(','+id, '');
        
        $('#bahandetail').val(teks);
    }
    
    $('#submit').on('click', function(){
        var detail = $('#bahandetail').val();
        
        if(detail != ""){
            detail = detail.substring(1);
            
            $.ajax({
                type    : 'POST',
                url     : '{{ url("") }}/admin/potong-bahan/submit',
                data    : {
                    _token  : '{{ csrf_token() }}',
                    detail  : detail,
                    tanggal : $('#tanggal').val()
                },
                success : function(){
                    window.location.href = '{{ url("") }}/admin/potong-bahan';
                }
            })
        }
    });
</script>
@endpush
