@extends('layouts.app.frame')
@section('title', 'Potong Bahan')
@section('description', 'Potong Bahan')
@section('breadcrumbs', Breadcrumbs::render('potong-bahan'))
@section('button', '<div class="clearfix"></div>')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Header</h3>
        <div class="pull-right">
            <a style="margin-bottom:10px;" class="btn btn-primary" href="{{ url('') }}/admin/potong-bahan/potong">Potong Bahan</a>
        </div>
    </div>
    <div class="panel-body">
        <div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table table-bordered" id="potong">
                <thead>
                    <th width="5%">No</th>
                    <th>Supplier</th>
                    <th>Bahan</th>
                    <th>Motif</th>
                    <th>Warna</th>
                    <th>Kg</th>
                    <th width="15%">Action</th>
                </thead>
                <tbody>
                    @php $i = 1; @endphp
                    @foreach($data as $datas)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $datas->supp }}</td>
                        <td>{{ $datas->bahan }}</td>
                        <td>{{ $datas->motif }}</td>
                        <td>{{ $datas->warna }}</td>
                        <td>{{ $datas->kg }}</td>
                        <td><a class="btn btn-danger" onclick="remove({{ $datas->id }})">Remove</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@if($i > 1)
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Detail Potong Bahan</h3>
        <div class="pull-right">
            <a style="margin-bottom:10px;" class="btn btn-primary" href="{{ url('') }}/admin/potong-bahan/potongmodel">Potong Model</a>
        </div>
    </div>
    <div class="panel-body">
        <div class="clearfix"></div>
        <div class="table-responsive">
            <table class="table table-bordered" id="potong">
                <thead>
                    <th width="5%">No</th>
                    <th>Nama</th>
                    <th>QTY</th>
                    <th>Keterangan</th>
                    <th width="15%">Action</th>
                </thead>
                <tbody>
                    @php $u = 1; @endphp
                    @foreach($model as $mdl)
                    <tr>
                        <td>{{ $u++ }}</td>
                        <td>{{ $mdl->name }}</td>
                        <td>{{ $mdl->qty }}</td>
                        <td><input class="form-control" id="ket_{{ $mdl->id }}" /></td>
                        <td><a class="btn btn-danger" onclick="deletes({{ $mdl->id }})">Delete</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @if($u > 1)
        <a class="btn btn-success" onclick="simpan()">Submit</a>
        @endif
    </div>
</div>
@endif
@endsection

@push("script")
<script type="text/javascript">
    function remove(id){
        $.ajax({
            type    : 'POST',
            url     : '{{ url("") }}/admin/potong-bahan/remove',
            data    : {
                _token  : '{{ csrf_token() }}',
                id      : id
            },
            success : function(){
                location.reload();
            }
        });
    }
    
    function deletes(id){
        $.ajax({
            type    : 'POST',
            url     : '{{ url("") }}/admin/potong-bahan/delete',
            data    : {
                _token  : '{{ csrf_token() }}',
                id      : id
            },
            success : function(){
                location.reload();
            }
        });
    }
    
    function simpan(){
        var keterangan = {
            @foreach($model as $mdl)
            '{{ $mdl->id }}'    : $('#ket_{{ $mdl->id }}').val(),
            @endforeach
        }
        
        $.ajax({
            type    : 'POST',
            url     : '{{ url("") }}/admin/potong-bahan/submitall',
            data    : {
                _token      : '{{ csrf_token() }}',
                keterangan  : keterangan
            },
            success : function(){
                location.reload();
            }
        });
    }
</script>
@endpush
