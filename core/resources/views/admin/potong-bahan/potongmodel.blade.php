@extends('layouts.app.frame')
@section('title', 'Potong Model')
@section('description', 'Potong Model')
@section('breadcrumbs', Breadcrumbs::render('potong-bahan.potongmodel'))
@section('button', '<a href="'.url("").'/admin/potong-bahan" class="btn btn-info btn-xs no-border">Back</a>')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Form Add Potong Model</h3>
    </div>
    <div class="panel-body">
        <form method="post" action="{{ url('') }}/admin/potong-bahan/submitmodel">
            <div class="form-group">
                <label>Model</label>
                {!! Form::hidden('_token', csrf_token()) !!}
                {!! Form::select('model_id', $model, null, ['class' => 'form-control', 'data-init-plugin' => 'select2', 'id' => 'model']) !!}
            </div>
            <div class="form-group">
                <label>QTY</label>
                {!! Form::number('qty', 0, ['class' => 'form-control', 'min' => 0]) !!}
            </div>
            <div class="form-group">
                <label>Keterangan</label>
                <div class="col-sm-12" style="padding:10px;" id="keterangan"></div>
            </div>
            <button class="btn btn-success" type="submit">Submit</button>
        </form>
    </div>
</div>
@endsection

@push("script")
<script type="text/javascript">
    $('#model').on('change', function(){
        $.ajax({
            type    : 'GET',
            url     : '{{ url("") }}/admin/potong-bahan/getmodel',
            data    : {
                model   : $('#model :selected').val()
            },
            success : function(res){
                $('#keterangan').html(res);
            }
        }) 
    });
</script>
@endpush
