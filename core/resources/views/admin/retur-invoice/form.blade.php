@extends('layouts.app.frame')
@section('title', 'Retur dengan Faktur')
@section('description', 'Retur dengan Faktur')
@section('breadcrumbs', Breadcrumbs::render('retur-invoice'))
@section('button', '<a href="'.url('/admin').'/retur-invoice'.'" class="btn btn-info btn-xs no-border">Back</a>')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <form>
                <div class="form-group col-md-12" {{ Auth::User()->shop_id != 0 ? 'style=display:none;' : '' }}>
                    <label>Pilih Toko</label> 
                    {!! Form::select('shops_id', $shop, $idshop, ['class' => 'full-width', 'data-init-plugin' => 'select2', 'onchange' => 'return changeshop()', 'disabled']) !!}
                    <input hidden id="shops" value="{{ $idshop }}" />
                </div>
                <div class="form-group col-md-6">
                    <label>Retur</label>
                    <select data-init-plugin="select2" class="full-width" id="retur">
                        <option value="0">Pilih Retur</option>
                        <option value="cash">Cash</option>
                        <option value="deposit">Deposit</option>
                        <option value="barang">Barang</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label>Customer</label>
                    <select data-init-plugin="select2" class="full-width" disabled>
                        <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                    </select>
                    <input hidden id="customers" value="{{ $cashier->customer_id }}" />
                </div>
                <div class="form-group col-md-4">
                    <label>Produk</label>
                    <select data-init-plugin="select2" class="full-width" id="produk" onchange="return changeProduk()">
                        <option value="0">Pilih Produk</option>
                        @foreach($model as $mod)
                        <option value="{{ $mod->id }}">{{ $mod->model_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label>QTY</label>
                    <input type="number" min="1" value="1" class="form-control" id="qty" />
                </div>
                <div class="form-group col-md-4" id="show_cash" style="display:none;">
                    <label>Total</label> 
                    <input type="text" disabled class="form-control" id="total_cash" style="color:black;" />
                    <input name="total_cash" hidden id="total_cashs" />
                </div>
                <div class="form-group col-md-4" id="show_deposit" style="display:none;">
                    <label>Total Deposit</label> 
                    <input type="text" disabled class="form-control" id="total_deposit" style="color:black;" />
                    <input name="total_deposit" hidden id="total_deposits" />
                </div>
                <div class="form-group col-md-4" id="show_barang" style="display:none;">
                    <label>Ambil Barang dari Toko/Gudang</label> 
                    {!! Form::select('shops_id', $shop, null, ['class' => 'full-width', 'data-init-plugin' => 'select2', 'id' => 'ambil']) !!}
                </div>
                <div class="clearfix"></div>
                <div class="form-group col-md-6">
                    <label>Keterangan</label>
                    <textarea rows="3" placeholder="Keterangan Retur" class="full-width" id="note"></textarea>
                </div>
                <div class="clearfix"></div>
                <div class="pull-right">
                    <a onclick="return saveRetur()" class="btn btn-primary">Simpan</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push("script")
<script type="text/javascript">

    $('#shops').on('change', function(){
        var ids = $('#shops :selected').val();
        
        $.ajax({
            url : '{{url("/")}}/admin/changeshop',
            type : 'POST',
            data : {
                _token : '{{ csrf_token() }}',
                shop_id : ids
            },
            success: function() {
                window.location.href = window.location.href;
            }
        })
    });
    
    $('#retur').on('change', function(){
        var ret = $('#retur :selected').val();
        if(ret != 0){
            Pace.restart();
            changeProduk();
            $('#show_cash').hide();
            $('#show_barang').hide();
            $('#show_deposit').hide();
            
            if(ret == 'cash')
                $('#show_cash').show();
            else if(ret == 'deposit')
                $('#show_deposit').show();
            else if(ret == 'barang')
                $('#show_barang').show();
        }
    });
    
    function changeProduk(){
        var pro = $('#produk :selected').val();
        var ret = $('#retur :selected').val();
        
        if(pro != 0){
            $.ajax({
                type : 'POST',
                url : '{{ url("") }}/admin/getProduk',
                data : {
                    _token : '{{ csrf_token() }}',
                    type : ret,
                    model_id : pro,
                    noref : '{{ $noref }}'
                },
                success : function(res){
                    Pace.restart();
                    $('#qty').val(1);
                    $('#qty').attr('max',res['qty']);
                    localStorage.setItem('max', res['qty']);
                    
                    if(ret == 'cash'){
                        $('#total_cash').val(res['price']);
                        $('#total_cashs').val(res['price']);
                    }else if(ret == 'deposit'){
                        $('#total_deposit').val(res['price']);
                        $('#total_deposits').val(res['price']);
                    }else if(ret == 'barang'){
                        function initSelect(){
                          $("#ambil").select2({});
                        }
                        $("#ambil").select2("destroy");
                        
                        var opt = "";
                        $.each(res, function(index, value){
                            if(index != 'qty'){
                                $.each(value, function(ind, val){
                                    opt += "<option value='"+val.shop_id+"'>"+val.name+"</option>"; 
                                })
                            }
                        })
                        $("#ambil").html(opt);
                        
                        initSelect();
                    }
                }
            });
        }
    }
    
    $('#qty').on('change', function(){
        var qty = $('#qty').val();
        var ret = $('#retur :selected').val();
        var maks = localStorage.getItem('max');
        if(maks == "undefined")
            maks = 0;
        
        Pace.restart();
        if(qty > 0 && qty <= maks){
            if(ret == 'cash')
                var harga = $('#total_cashs').val();
            else if(ret == 'deposit')
                var harga = $('#total_deposits').val();
            
            var total = qty * harga;
            
            if(ret == 'cash'){
                $('#total_cash').val(total);
            }else if(ret == 'deposit'){
                $('#total_deposit').val(total);
            }
        }else if(qty > maks){
            alert('Maksimal QTY produk ini adalah '+maks+'!')
            $('#qty').val(maks);
            if(ret == 'cash'){
                var harga = $('#total_cashs').val();
                $('#total_cash').val(harga*maks);
            }else if(ret == 'deposit'){
                var harga = $('#total_deposits').val();
                $('#total_deposit').val(harga*maks);
            }
        }else{
            $('#qty').val(1);
            if(ret == 'cash'){
                var harga = $('#total_cashs').val();
                $('#total_cash').val(harga);
            }else if(ret == 'deposit'){
                var harga = $('#total_deposits').val();
                $('#total_deposit').val(harga);
            }
        }
    });
    
    function saveRetur(){
        var cus = $('#customers').val();
        var type = $('#retur').val();
        var mod = $('#produk :selected').val();
        var qty = $('#qty').val();
        var note = $('#note').val();
        var ids = $('#shops').val();
        var shopid = $('#ambil :selected').val();
        
        if(type == 'cash')
            var total = $('#total_cash').val().replace(/,/g, '');
        else if(type == 'deposit')
            var total = $('#total_deposit').val().replace(/,/g, '');
        
        var data = {
            _token : '{{ csrf_token() }}',
            customer_id : cus,
            type_retur : type,
            model_id : mod,
            shop_id : ids,
            shops_id : shopid,
            qty : qty,
            cost : total,
            note : note,
            noref : '{{ $noref }}'
        }
        
        if(cus != '0' && type != '0' && mod != '0'){
            $.ajax({
                type : 'POST',
                url : '{{ url("") }}/admin/retur-general/save',
                data : data,
                success : function(res){
                    Pace.restart();
                    if(res){
                        alert('Transaksi Berhasil!');
                        window.location.href = '{{ url("") }}/admin/retur-invoice';
                    }else
                        alert('Gagal!');
                }
            });
        }else if(type == '0')
            alert('Pilih Retur Terlebih Dahulu!');
        else if(cus == '0')
            alert('Pilih Pelanggan Terlebih Dahulu!');
        else if(mod == '0')
            alert('Pilih Produk Terlebih Dahulu!');
    }
    
    $('#total_cash').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
    
    $('#total_deposit').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ",",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        oncleared: function () { self.Value(''); }
    });
</script>
@endpush
