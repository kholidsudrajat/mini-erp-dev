@extends('layouts.app.frame')
@section('title', 'Create New Biaya Operasional')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('operasional.new'))
@section('button', '<a href="'.url('/admin/operasional').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/operasional', 'id' => 'formValidate', 'files' => true, 'novalidate' => '-']) !!}

		@include ('admin.operational.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');
});
</script>
@endpush
