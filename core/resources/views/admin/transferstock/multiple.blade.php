@extends('layouts.app.frame')
@section('title', 'Kirim Stock')
@section('description', 'Kirim Multiple Stock')
@section('breadcrumbs', Breadcrumbs::render('kirim-stock'))
@section('button', '')

@section('content')
    <div>
        <a class="btn btn-success" href="{{ url('') }}/admin/kirim-stock">Single Transfer Stock</a>
        <a class="btn btn-primary pull-right" onclick="return transfer()">Kirim</a>
    </div>
    <div class="clearfix"></div>
    <hr/>
    <table class="table table-hover">
        <thead>
            <tr>
                <th width="5%">#</th><th> Nama Model </th><th> Stock </th><th> Toko/Gudang </th><th width="10%">QTY</th><th width="25%"> Tansfer To </th>
            </tr>
        </thead>
        <tbody>
            @foreach($stock as $st)
            <tr>
                <td>
                    <div class="checkbox check-success">
                        <input type="checkbox" id="chk_{{$st->id}}" />
                        <label for="chk_{{$st->id}}"></label>
                    </div>
                </td>
                <td>{{ $st->model_name }}</td>
                <td>{{ $st->qty }}</td>
                <td>{{ $st->shop_name }}</td>
                <td><input type="number" value="0" min="0" max="{{ $st->qty }}" class="form-control" id="qty_{{$st->id}}" /></td>
                <td>
                    <select class="form-control" data-init-plugin="select2" id="shop_{{$st->id}}">
                        <option value="0">Pilih Toko/Gudang yang dituju</option>
                        @foreach($shop as $sh)
                            @if($st->shop_id != $sh->id)
                                <option value="{{ $sh->id }}">{{ $sh->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@push("script")
<script type="text/javascript">
    @foreach($stock as $st)
        $('#qty_{{$st->id}}').on('change', function(){
            var qty = $('#qty_{{$st->id}}').val();
            if(qty > {{$st->qty}}){
                alert('Stok Kurang!');
                $('#qty_{{$st->id}}').val({{$st->qty}});
            }
        });
    @endforeach
    
    function transfer(){
        var json = "{";
        @foreach($stock as $st)
            if($('#chk_{{$st->id}}').is(':checked')){
                if($('#qty_{{$st->id}}').val() > 0 && $('#shop_{{$st->id}} :selected').val() > 0){
                    var qty = $('#qty_{{$st->id}}').val();
                    var stock_id = {{ $st->id }};
                    var shop_id = $('#shop_{{$st->id}} :selected').val();

                    json += '"{{$st->id}}":{"qty" : '+qty+', "shop_id" : '+shop_id+'},';
                }
            }
        @endforeach
        
        json = json.slice(0,-1)+'}';
        
        if(json != '}'){
            confirm('Proses Transfer?');
            if(confirm){
                $.ajax({
                    url : '{{ url("") }}/admin/kirim-stock/multiple/transfer',
                    type : 'POST',
                    data : {
                        _token : '{{ csrf_token() }}',
                        json : json
                    },
                    success : function(res){
                        res = res.replace(/<br>/g , "\n");
                        alert(res);
                        location.reload();
                    }
                });
            }
        }
    }
</script>
@endpush
