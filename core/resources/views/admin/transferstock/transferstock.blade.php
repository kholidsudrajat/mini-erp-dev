@extends('layouts.app.frame')
@section('title', 'Transfer Stok <br/><br/> '.$model['name'].'( '.$model['model_name'].' ) - '.$shops['name'].' <br/>')
@section('breadcrumbs', Breadcrumbs::render('kirim-stock.transfer', $id))

@section('content')
    {!! Form::open(['url' => '/admin/prosestransferstock/', 'id' => 'formValidate', 'files' => true]) !!}
    <div class="form-group form-group-default required">
        {!! Form::label('qtyl', 'QTY'); !!}
        {!! Form::number('qtyl', $qty, ['class' => 'form-control', 'disabled']); !!}
    </div>
    <div class="form-group form-group-default required">
        {!! Form::label('qty', 'QTY Transfer'); !!}
        {!! Form::number('qty', null, ['id' => 'qty', 'class' => 'form-control', 'placeholder' => '0', 'max' => $qty, 'required']); !!}
        {!! Form::hidden('stock_id', $id, ['id' => 'stock_id']); !!}
    </div>
    <div class="form-group form-group-default form-group-default-select2 required">
        {!! Form::label('shop_id', 'Kirim Ke'); !!}
        {!! Form::select('shop_id', $shop, null, ['id' => 'shop_id', 'class' => 'full-width', 'data-init-plugin' => 'select2', 'required' => '']); !!}
    </div>

    <a class="btn btn-success" id="submit">Submit</a>
	{!! Form::close() !!}
    
    <div class="clearfix"></div>
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');
    
    $('p').remove();

});
    
$('#submit').on('click', function(){
    var datanya = {
        qty       : $('#qty').val(),
        shop_id   : $('#shop_id :selected').val(),
        stock_id  : {{ $id }}
    };
    var qty = $('#qty').val();            
    var model = "{{ $model['name'] }}";
    var shop = $('#shop_id :selected').text();

    $.ajax({
        type : 'GET',
        url  : '{{url("/")}}/admin/prosestransferstock/',
        data : datanya,
        success : function(datas){
            alert(qty +' '+ model + ' berhasil ditransfer ke '+ shop +'!');
            parent.$.fancybox.close();
        }
    }); 
});
</script>
@endpush
