@extends('layouts.app.frame')
@section('title', 'Edit Master Ongkos')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('cost.edit', $cost))
@section('button', '<a href="'.url('/admin/cost').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')


    {!! Form::model($cost, [
            'method' => 'PATCH',
            'url' => ['/admin/cost', $cost->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}

        @include ('admin.cost.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');

});
</script>
@endpush