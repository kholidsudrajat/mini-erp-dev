@extends('layouts.app.frame')
@section('title', 'Create New User')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('user.new'))
@section('button', '<a href="'.url('/admin/user').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/user', 'id' => 'formValidate', 'files' => true, 'novalidate' => '-']) !!}

		@include ('admin.user.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');
});
</script>
@endpush
