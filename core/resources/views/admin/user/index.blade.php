@extends('layouts.app.frame')
@section('title', 'Role & User')
@section('description', 'Role & User Management')
@section('breadcrumbs', Breadcrumbs::render('user'))
@section('button', '<a href="'.url('/admin').'/user/create'.'" class="btn btn-primary btn-xs no-border">Tambah Data Pengguna <i class="fa fa-plus"></i></a> <a data-toggle="collapse" data-target="#rolesection" class="btn btn-info btn-xs no-border">Tambah Data Peran</a>')

@section('content')
    <div id="rolesection" class="collapse">
        <form id="formValidate" method="post">
            <div class="col-sm-3 col-xs-6" style="padding-left:0px;">
                <div aria-required="true" class="form-group required form-group-default {{ $errors->has('role') ? 'has-error' : ''}}">
                    {!! Form::label('role', 'Role') !!}
                    <input type="text" name="role" id="role" class="form-control" placeholder="Role" required>
                </div>
                {!! $errors->first('role', '<label class="error">:message</label>') !!}
            </div>
            <div class="col-span1">
                <a class="btn btn-success" style="margin-top: 10px;" id="tambah" onclick="tambah();">Tambah</a>
            </div>
        </form>
        <hr>

        <div class="clearfix"></div>
        <table class="table table-hover display nowrap">
            <thead>
                <tr>
                    <th width="10%"> # </th><th> Peran </th><th width="20%"> Tindakan </th>
                </tr>
            </thead>
            <tbody>
                @php $i = 1; @endphp
                @foreach($role as $rol)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $rol['role'] }}</td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="role-user/select/{{ $rol['id'] }}"><i class="fa fa-pencil"></i></a>
                        <a class="btn btn-xs btn-danger" onclick="deleteData('role-user', {{ $rol['id'] }})"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="clearfix"></div>
        <hr/>
    </div>

    <input type="hidden" id="drs" name="drange"/>
    <input type="hidden" id="did" name="did"/>
    <div class="form-group-attached">
        <div class="row clearfix">
            <div class="col-sm-6 col-xs-12">
                <div class="form-group form-group-default">
                    <label>Pencarian</label>
                    <form id="formsearch">
                        <input type="text" id="search-table" class="form-control" name="firstName" placeholder="Masukkan Keyword">
                    </form>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group form-group-default">
                    <label>Tanggal Mulai</label>
                    <input type="text" id="datepicker-start" class="form-control" name="firstName" placeholder="pilih tanggal">
                </div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="form-group form-group-default">
                    <label>Tanggal Berakhir</label>
                    <input type="text" id="datepicker-end" class="form-control" name="firstName" placeholder="pilih tanggal">
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <table class="table table-hover display nowrap" id="user-table">
        <thead>
            <tr>
                <th width="10%"> # </th><th> Photo </th><th> Nama </th><th> Nama Pengguna </th><th> Email </th><th width="20%"> Tindakan </th>
            </tr>
        </thead>
    </table>

@endsection


@push("script")
<script>

var oTable;
oTable = $('#user-table').DataTable({
    processing: true,
    serverSide: true,
    dom: 'lBfrtip',
    order:  [[ 0, "asc" ]],
    buttons: [
        {
            extend: 'print',
            autoPrint: true,
            customize: function ( win ) {
                $(win.document.body)
                    .css( 'padding', '2px' )
                    .prepend(
                        '<img src="{{asset('img/logo.png')}}" style="float:right; top:0; left:0;height: 40px;right: 10px;background: #101010;padding: 8px;border-radius: 4px" /><h5 style="font-size: 9px;margin-top: 0px;"><br/><font style="font-size:14px;margin-top: 5px;margin-bottom:20px;"> Laporan Kategori</font><br/><br/><font style="font-size:8px;margin-top:15px;">{{date('Y-m-d h:i:s')}}</font></h5><br/><br/>'
                    );


                $(win.document.body).find( 'div' )
                    .css( {'padding': '2px', 'text-align': 'center', 'margin-top': '-50px'} )
                    .prepend(
                        ''
                    );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( { 'font-size': '9px', 'padding': '2px' } );


            },
            title: '',
            orientation: 'landscape',
            exportOptions: {columns: ':visible'} ,
            text: '<i class="fa fa-print" data-toggle="tooltip" title="" data-original-title="Print"></i>'
        },
        {extend: 'colvis', text: '<i class="fa fa-eye" data-toggle="tooltip" title="" data-original-title="Column visible"></i>'},
        {extend: 'csv', text: '<span>CSV</span>'}
    ],
    sDom: "<'table-responsive fixed't><'row'<p i>> B",
    sPaginationType: "bootstrap",
    destroy: true,
    responsive: true,
    scrollCollapse: true,
    oLanguage: {
        "sLengthMenu": "_MENU_ ",
        "sInfo": "Menampilkan <b>_START_ to _END_</b> dari _TOTAL_ entri"
    },
    lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
    ajax: {
    url: 
        '{!! route('user.data') !!}'
    ,
        data: function (d) {
            d.range = $('input[name=drange]').val();
        }
    },
    columns: [
        { data: "rownum", name: "rownum" },
        { data: "photo", name: "photo" },
        { data: "name", name: "name" },
        { data: "username", name: "username" },
        { data: "email", name: "email" },
        { data: "action", name: "action", orderable: false , searchable: false },
    ],
}).on( 'processing.dt', function ( e, settings, processing ) {if(processing){Pace.start();} else {Pace.stop();}});

$("#user-table_wrapper > .dt-buttons").appendTo("div.export-options-container");
    
$('#datepicker-start').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
    $(this).datepicker('hide');
    if($('#datepicker-end').val() != ""){
        $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
        oTable.draw();
    }else{
        $('#datepicker-end').focus();
    }

});
$('#datepicker-end').datepicker({format: 'yyyy/mm/dd'}).on('changeDate', function (ev) {
    $(this).datepicker('hide');
    if($('#datepicker-start').val() != ""){
        $('#drs').val($('#datepicker-start').val()+":"+$('#datepicker-end').val());
        oTable.draw();
    }else{
        $('#datepicker-start').focus();
    }

});

$('#formsearch').submit(function () {
    oTable.search( $('#search-table').val() ).draw();
    return false;
} );

oTable.page.len(25).draw();



function deleteData(type, id) {
    $('#modalDelete').modal('show');
    $('#did').val(id);
    localStorage.setItem('types', type);
}

function hapus(){
    var type = localStorage.getItem('types');
    $('#modalDelete').modal('hide');
    var id = $('#did').val();
    if(type == 'user'){
        $.ajax({
            url: '{{url("admin/user")}}' + "/" + id + '?' + $.param({"_token" : '{{ csrf_token() }}' }),
            type: 'DELETE',
            complete: function(data) {
                oTable.draw();
            }
        });
    }else if(type == 'role-user'){
        $.ajax({
            url: '{{url("admin/role-user/delete")}}' + "/" + id,
            type: 'GET',
            complete: function(data) {
                location.reload();
            }
        });
    }
}

    function tambah(){
        if($('#role').val() == ''){
            alert('Role tidak boleh kosong');
            return false;
        } else {
            $.ajax({
                url: '{{url('/')}}/admin/role-user/tambah',
                type: 'POST',
                data: {
                    role: $('#role').val(),
                    _token: '{{ csrf_token() }}'
                },
                success: function (data) {
                    console.log(data);
                    window.location.reload();
                }
            });
        }
    }
    
</script>
@endpush
