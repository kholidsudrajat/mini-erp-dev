@if(isset($shop))
<div aria-required="true" class="form-group required form-group-default form-group-default-select2 {{ $errors->has('shop_id') ? 'has-error' : ''}}">
    {!! Form::label('shop_id', 'Toko') !!}
    {!! Form::select('shop_id', $shop, null, ['class' => 'full-width', 'required' => '', 'data-init-plugin' => 'select2']) !!}
</div>
{!! $errors->first('shop_id', '<label class="error">:message</label>') !!}
@endif

@if($id == Auth::user()->id)
<div aria-required="true" class="form-group required form-group-default {{ $errors->has('photos') ? 'has-error' : ''}}">
    {!! Form::label('photos', 'Photo') !!}
    {!! Form::file('photos', ['class' => 'form-control', 'required' => '']) !!}
</div>
{!! $errors->first('photos', '<label class="error">:message</label>') !!}
@endif

<div aria-required="true" class="form-group required form-group-default {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Nama') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => '']) !!}
</div>
{!! $errors->first('name', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group required form-group-default {{ $errors->has('username') ? 'has-error' : ''}}">
    {!! Form::label('username', 'Username') !!}
    {!! Form::text('username', null, ['class' => 'form-control', 'required' => '']) !!}
</div>
{!! $errors->first('username', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group required form-group-default {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email') !!}
    {!! Form::email('email', null, ['class' => 'form-control', 'required' => '']) !!}
</div>
{!! $errors->first('email', '<label class="error">:message</label>') !!}

<div aria-required="true" class="form-group required form-group-default {{ $errors->has('password') ? 'has-error' : ''}}">
    {!! Form::label('password', 'Password') !!}
    @if(isset($user))
    {!! Form::password('password', ['class' => 'form-control']) !!}
    @else
    {!! Form::password('password', ['class' => 'form-control']) !!}
    @endif
</div>
{!! $errors->first('password', '<label class="error">:message</label>') !!}

@if(isset($role_user))
    <div aria-required="true" class="form-group required form-group-default form-group-default-select2 {{ $errors->has('role_user') ? 'has-error' : ''}}">
        {!! Form::label('role_user', 'Role User') !!}
        {!! Form::select('role', $role_user, null, ['class' => 'full-width', 'required' => '', 'data-init-plugin' => 'select2']) !!}
    </div>
    {!! $errors->first('shop_id', '<label class="error">:message</label>') !!}
@endif

@if(isset($role))
    @php $all_data = explode(",", $user->role_menu); @endphp
    <div aria-required="true" class="form-group required form-group-default {{ $errors->has('role') ? 'has-error' : ''}}">
        {!! Form::label('role', 'Role Menu') !!}
        <ul style="list-style: none; display: inline-flex;">
            @foreach( $role as $roles)
                <li style="padding-right: 10px;">
                    {{ Form::checkbox('role_menu[]', $roles->id, in_array($roles->id, $all_data)) }} {{ $roles->menu }}
                </li>
            @endforeach
        </ul>
    </div>
    {!! $errors->first('role', '<label class="error">:message</label>') !!}
@endif

{{--<div class="pull-left">--}}
    {{--<div class="checkbox check-success">--}}
        {{--<input id="checkbox-agree" type="checkbox" required> <label for="checkbox-agree">Saya sudah mengecek data sebelum menyimpan</label>--}}
    {{--</div>--}}
{{--</div>--}}
<br/>

{!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Save', ['type' => 'submit', 'class' => 'btn btn-success']) !!}
<button class="btn btn-default" type="reset"><i class="pg-close"></i> Clear</button>

@push('script')
    <script>
        function tambah()
        {

        }
    </script>
@endpush
