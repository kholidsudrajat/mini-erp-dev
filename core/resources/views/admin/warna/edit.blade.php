@extends('layouts.app.frame')
@section('title', 'Edit Warna')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('warna.edit', $warna))
@section('button', '<a href="'.url('/admin/warna').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')


    {!! Form::model($warna, [
            'method' => 'PATCH',
            'url' => ['/admin/warna', $warna->id],
            'files' => true,
            'id' => 'formValidate',
        ]) !!}

        @include ('admin.warna.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');

});
</script>
@endpush