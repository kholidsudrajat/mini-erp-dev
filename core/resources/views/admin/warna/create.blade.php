@extends('layouts.app.frame')
@section('title', 'Create New Warna')
@section('description', 'Please make sure to check all input')
@section('breadcrumbs', Breadcrumbs::render('warna.new'))
@section('button', '<a href="'.url('/admin/warna').'" class="btn btn-info btn-xs no-border">Back
</a>')

@section('content')
    {!! Form::open(['url' => '/admin/warna', 'id' => 'formValidate', 'files' => true, 'novalidate' => '-']) !!}

		@include ('admin.warna.form')

	{!! Form::close() !!}
@endsection

@push("script")
<script>
$(document).ready(function() {

    $('#formValidate').validate();
    $('#formValidate').removeAttr('novalidate');
});
</script>
@endpush
