-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `cashier`;
CREATE TABLE `cashier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_ref` varchar(20) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT 0,
  `discount` bigint(20) NOT NULL,
  `subtotal` bigint(20) NOT NULL,
  `total` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `cashier` (`id`, `no_ref`, `customer_id`, `discount`, `subtotal`, `total`, `created_at`, `updated_at`) VALUES
(1,	'140617015818',	1,	0,	120000,	120000,	'2017-06-14 06:58:18',	'2017-06-14 06:58:18'),
(2,	'150617102905',	1,	0,	385000,	385000,	'2017-06-15 03:29:05',	'2017-06-15 03:29:05'),
(3,	'160617104229',	1,	88000,	440000,	352000,	'2017-06-16 03:42:29',	'2017-06-16 03:42:29'),
(4,	'160617105046',	1,	10,	125000,	112500,	'2017-06-16 03:50:46',	'2017-06-16 03:50:46'),
(5,	'160617105446',	1,	5,	75000,	71250,	'2017-06-16 03:54:46',	'2017-06-16 03:54:46'),
(6,	'160617105835',	1,	10,	100000,	90000,	'2017-06-16 03:58:35',	'2017-06-16 03:58:35'),
(7,	'160617023128',	1,	25000,	250000,	225000,	'2017-06-16 07:31:28',	'2017-06-16 07:31:28');

DROP TABLE IF EXISTS `cashier_details`;
CREATE TABLE `cashier_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cashier_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `cashier_details` (`id`, `cashier_id`, `model_id`, `qty`, `created_at`, `updated_at`) VALUES
(1,	1,	2,	1,	'2017-06-14 06:58:18',	'2017-06-14 06:58:18'),
(2,	1,	4,	1,	'2017-06-14 06:58:18',	'2017-06-14 06:58:18'),
(3,	2,	4,	3,	'2017-06-15 03:29:05',	'2017-06-15 03:29:05'),
(4,	2,	3,	2,	'2017-06-15 03:29:05',	'2017-06-15 03:29:05'),
(5,	2,	2,	2,	'2017-06-15 03:29:05',	'2017-06-15 03:29:05'),
(6,	3,	2,	4,	'2017-06-16 03:42:29',	'2017-06-16 03:42:29'),
(7,	3,	4,	2,	'2017-06-16 03:42:29',	'2017-06-16 03:42:29'),
(8,	3,	3,	1,	'2017-06-16 03:42:29',	'2017-06-16 03:42:29'),
(9,	4,	2,	1,	'2017-06-16 03:50:46',	'2017-06-16 03:50:46'),
(10,	4,	3,	1,	'2017-06-16 03:50:46',	'2017-06-16 03:50:46'),
(11,	5,	2,	1,	'2017-06-16 03:54:46',	'2017-06-16 03:54:46'),
(12,	6,	3,	2,	'2017-06-16 03:58:35',	'2017-06-16 03:58:35'),
(13,	7,	2,	2,	'2017-06-16 07:31:28',	'2017-06-16 07:31:28'),
(14,	7,	3,	2,	'2017-06-16 07:31:28',	'2017-06-16 07:31:28');

DROP TABLE IF EXISTS `costs`;
CREATE TABLE `costs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `costs` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1,	'Jahit',	'2017-06-07 19:52:29',	'2017-06-07 19:52:29'),
(2,	'Obras',	'2017-06-07 19:52:34',	'2017-06-07 19:52:34'),
(3,	'Seleting / Acc',	'2017-06-07 19:52:44',	'2017-06-07 19:52:44'),
(4,	'Karet / Benang',	'2017-06-07 19:52:52',	'2017-06-07 19:52:52');

DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_code` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no_telp` varchar(30) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `customers` (`id`, `customer_code`, `nama`, `no_telp`, `alamat`, `created_at`, `updated_at`) VALUES
(1,	'C1706080001',	'Fahmi Abdurahman',	'085691194459',	'Bogor',	'2017-06-07 18:58:21',	'2017-06-07 18:58:21');

DROP TABLE IF EXISTS `expense_konveksi`;
CREATE TABLE `expense_konveksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `expense_konveksi` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1,	'DLL',	'2017-06-07 19:51:13',	'2017-06-07 19:51:13'),
(2,	'Gaji',	'2017-06-07 19:51:19',	'2017-06-07 19:51:19'),
(3,	'Invest',	'2017-06-07 19:51:25',	'2017-06-07 19:51:25');

DROP TABLE IF EXISTS `expense_toko`;
CREATE TABLE `expense_toko` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `category` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `expense_toko` (`id`, `name`, `category`, `created_at`, `updated_at`) VALUES
(1,	'Chandra',	'gaji',	'2017-06-07 20:02:54',	'2017-06-07 20:02:54'),
(2,	'Hendri',	'gaji',	'2017-06-07 20:03:03',	'2017-06-07 20:03:03'),
(3,	'Listrik',	'utilities',	'2017-06-07 20:03:11',	'2017-06-07 20:03:11');

DROP TABLE IF EXISTS `models`;
CREATE TABLE `models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `model_name` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `info` text NOT NULL,
  `total_cost` bigint(20) NOT NULL,
  `price` bigint(20) NOT NULL,
  `diff` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `models` (`id`, `name`, `model_name`, `image`, `info`, `total_cost`, `price`, `diff`, `created_at`, `updated_at`) VALUES
(2,	'Celana Jeans',	'levis-701',	'53175.jpg',	'-',	25000,	75000,	50000,	'2017-06-13 03:32:59',	'2017-06-12 20:32:59'),
(3,	'Kemeja Lengan Panjang',	'kmj-01',	'94020.jpg',	'-',	20000,	50000,	30000,	'2017-06-12 20:36:19',	'2017-06-12 20:36:19'),
(4,	'Kemeja Lengan Pendek',	'klp-201',	'92862.jpg',	'-',	20000,	45000,	25000,	'2017-06-12 22:31:54',	'2017-06-12 22:31:54');

DROP TABLE IF EXISTS `models_cost`;
CREATE TABLE `models_cost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `cost_id` int(11) NOT NULL,
  `costs` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `models_cost` (`id`, `model_id`, `cost_id`, `costs`, `created_at`, `updated_at`) VALUES
(1,	2,	1,	10000,	'2017-06-13 03:33:00',	'2017-06-12 20:33:00'),
(2,	2,	2,	5000,	'2017-06-13 03:33:00',	'2017-06-12 20:33:00'),
(3,	2,	3,	5000,	'2017-06-13 03:33:00',	'2017-06-12 20:33:00'),
(4,	2,	4,	5000,	'2017-06-13 03:33:00',	'2017-06-12 20:33:00'),
(5,	3,	1,	10000,	'2017-06-12 20:36:19',	'2017-06-12 20:36:19'),
(6,	3,	2,	5000,	'2017-06-12 20:36:19',	'2017-06-12 20:36:19'),
(7,	3,	3,	3000,	'2017-06-12 20:36:19',	'2017-06-12 20:36:19'),
(8,	3,	4,	2000,	'2017-06-12 20:36:19',	'2017-06-12 20:36:19'),
(9,	4,	1,	10000,	'2017-06-12 22:31:54',	'2017-06-12 22:31:54'),
(10,	4,	2,	0,	'2017-06-12 22:31:54',	'2017-06-12 22:31:54'),
(11,	4,	3,	5000,	'2017-06-12 22:31:54',	'2017-06-12 22:31:54'),
(12,	4,	4,	5000,	'2017-06-12 22:31:54',	'2017-06-12 22:31:54');

DROP TABLE IF EXISTS `payment_method`;
CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `payment_method` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1,	'Giro',	'2017-06-07 20:26:21',	'2017-06-07 20:26:21');

DROP TABLE IF EXISTS `shops`;
CREATE TABLE `shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `is_shop` enum('0','1') NOT NULL DEFAULT '0',
  `is_warehouse` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `shops` (`id`, `name`, `is_shop`, `is_warehouse`, `created_at`, `updated_at`) VALUES
(2,	'Toko 1',	'1',	'0',	'2017-06-06 21:53:17',	'2017-06-06 21:53:17'),
(3,	'Gudang 1',	'0',	'1',	'2017-06-06 21:58:58',	'2017-06-06 21:58:58');

DROP TABLE IF EXISTS `stocks`;
CREATE TABLE `stocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `stocks` (`id`, `shop_id`, `model_id`, `qty`, `created_at`, `updated_at`) VALUES
(1,	2,	2,	6,	'2017-06-12 07:47:12',	'2017-06-12 00:47:12'),
(2,	3,	2,	3,	'2017-06-12 07:47:12',	'2017-06-12 00:47:12');

DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `email` varchar(65) DEFAULT NULL,
  `pic` varchar(30) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `suppliers` (`id`, `nama`, `no_telp`, `email`, `pic`, `alamat`, `keterangan`, `created_at`, `updated_at`) VALUES
(1,	'YKK',	'123456',	'admin@ykk.co.id',	'Fahmi Abdurahman',	'Bogor',	'-',	'2017-06-06 08:02:28',	'2017-06-06 01:02:28');

DROP TABLE IF EXISTS `suppliers_bahan`;
CREATE TABLE `suppliers_bahan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `suppliers_bahan` (`id`, `supplier_id`, `name`, `created_at`, `updated_at`) VALUES
(4,	1,	'Kain Katun',	'2017-06-06 01:54:42',	'2017-06-06 01:54:42'),
(5,	1,	'Kain Flanel',	'2017-06-06 01:55:25',	'2017-06-06 01:55:25');

DROP TABLE IF EXISTS `suppliers_model`;
CREATE TABLE `suppliers_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `suppliers_model` (`id`, `supplier_id`, `name`, `created_at`, `updated_at`) VALUES
(1,	1,	'Polos',	'2017-06-06 02:14:45',	'2017-06-06 02:14:45');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` tinyint(4) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'Administrator',	'admin',	'fhmabdurahman@gmail.com',	'$2a$06$Frgkq3miTAgcsA7FQbZLNOdVTgQnUnExEl9Eu1sOoQbrEqDeNAMV6',	2,	'Dnf58ENZAMcm7h3j4R9icqgU5Eq3JtmLlZZBETSPGKHU8AKz5aAeTreezNah',	'2017-06-05 21:56:41',	'2017-06-13 02:42:38'),
(2,	'Chandra',	'chandra',	'chandrathen@gmail.com',	'$2y$10$RSWIdGtyRTQWC7NIbf6Jo.BUjVchUBDqmFkNjm79ZuJXf23JajtlK',	1,	NULL,	'2017-06-07 19:06:48',	'2017-06-07 19:09:24');

DROP TABLE IF EXISTS `warnas`;
CREATE TABLE `warnas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `warnas` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1,	'Merah',	'2017-06-07 19:18:44',	'2017-06-07 19:18:44'),
(2,	'Jingga',	'2017-06-07 19:18:50',	'2017-06-07 19:18:50');

-- 2017-06-16 08:02:00
