<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempPotong extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'temp_potong';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tanggal', 'stock_bahan_detail_id'];

    
}
