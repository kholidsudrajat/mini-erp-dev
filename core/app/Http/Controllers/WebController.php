<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Contact;
use App\Slider;
use App\Menu;
use App\Page;
use App\PageDetail;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class WebController extends Controller
{
    public function index()
    {
        $slider = Slider::orderBy('order')->get();
        
        $about = Page::join('menu','menu','=','menu.id')->whereName('about')->first();
        
        $join = Page::join('menu','menu','=','menu.id')->whereName('join-us')->first();
        
        $class = Page::select(['page.id', 'title', 'subtitle', 'description', 'image'])
            ->join('menu','menu','=','menu.id')->whereName('classes')->first();
        
        $classes = PageDetail::wherePageId($class['id'])->orderBy('order')->get();
        
        $trainer = Page::select(['page.id', 'title', 'subtitle', 'description', 'image'])
            ->join('menu','menu','=','menu.id')->whereName('trainer')->first();
        
        $trainers = PageDetail::wherePageId($trainer['id'])->orderBy('order')->get();
        
        return view('web.index', compact('slider', 'about', 'class', 'classes', 'trainer', 'trainers', 'join'));
    }
    
    public function joinus(Request $request)
    {
        $req['firstname'] = $request['firstname'];
        $req['lastname'] = $request['lastname'];
        $req['email'] = $request['email'];
        
        Contact::create($req);
        
        return redirect('gym');
    }
}
