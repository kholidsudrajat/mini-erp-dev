<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\RoleUser;
use App\Cashier;
use App\Customer;
use App\Debt;
use App\User;
use App\Payment;
use App\Scheduler;
use App\Stock;
use App\Notification;
use App\NotificationDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Session;
use Datatables;
use DB;
use Auth;
use View;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = DB::Table('v_sales')
            ->leftJoin('customers', 'customers.id', '=', 'customer_id')
            ->where('sales','!=','')
            ->orderBy('sales','DESC')
            ->limit(10)
            ->get();
            
        $product = DB::Table('v_product')
            ->select(['names', DB::Raw('SUM(tqty) AS qty')])
            ->groupBy('id')
            ->orderBy('qty','DESC')
            ->limit(10)
            ->get();
        
        $getrange = Cashier::select(DB::Raw('MIN(YEAR(created_at)) AS mins'), DB::Raw('MAX(YEAR(created_at)) AS maks'))->first();
        $zz = $getrange['maks'];
        $yy = $getrange['mins'];
        $years = array();
        for($a=$yy; $a<=$zz; $a++){
            $years[$a] = $a;
        }
        
        $year = $zz;        
        for($i=1; $i<=12; $i++){
            $sales[$i] = Cashier::select([
                DB::Raw('SUM(subtotal) AS totals')
            ])->whereRaw('MONTH(created_at) = "'.$i.'" AND YEAR(created_at) = "'.$year.'"')->first();
        }
        
        return view('admin.dashboard', compact('customer', 'product', 'sales', 'years'));
    }
  
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $role_user = RoleUser::pluck('role', 'id');

        return view('admin.user.edit', compact('user', 'role_user', 'id'));
    }
    
    public function changemonthsales($year){
        for($i=1; $i<=12; $i++){
            $sales[$i] = Cashier::select([
                DB::Raw('SUM(subtotal) AS totals')
            ])->whereRaw('MONTH(created_at) = "'.$i.'" AND YEAR(created_at) = "'.$year.'"')->first();
        }
        
        return $sales;
    }
    
    public function reminder(){
        $cek = Scheduler::get();
        foreach($cek as $c){
            $a = intval(date('d'));
            $b = intval($c->tanggal);
            if($a == $b){
                $date = date('Y-m-d 00:00:00');
            }
        }
        
        if(!isset($date))
            return 0;
        
        $data = DB::Table('v_report_cus')
            ->join('customers', 'customers.id', '=', 'customer_id')
            ->whereRaw('debt <> 0')
            ->get();
        
        $customer = Customer::get();
        foreach($customer as $cu){
            $cus[$cu->id] = '';    
        }
        
        foreach($data as $dd){
            $debt = Debt::whereRaw('customer_id = '.$dd->customer_id.' AND due_date < "'.$date.'"')->get();
            foreach($debt as $de){
                $cashier = Cashier::whereNoRef($de->no_ref)->first();                    
                $payment = Payment::whereNoRef($de->no_ref)
                    ->select([DB::Raw('SUM(pay) as pay')])
                    ->first();
                
                $debt = $cashier->total - $payment->pay;
                $due = strtotime($de->due_date);
                $now = time();
                
                if($debt > 0 && $now > $due)
                    $cus[$dd->customer_id] .= 'No Ref : '.$de->no_ref.'<br/> Hutang : '.$debt.'<br/> Jatuh Tempo : '.$de->due_date.'<br/><br/>';
            }
            $cus[$dd->customer_id] .= '<br/>';
        }
        
        foreach($cus as $key => $val) 
        {
            if($val !== ''){
                $get = Customer::findOrFail($key);
                if($get->email != ""){
                    $arr['email'] = $get->email;
                    $arr['val'] = $val;
                    Mail::send('mail.debt', $arr, function ($message) use ($arr){
                        $message->from('no-reply@minierp.koorting.com', 'MINI ERP ' );
                        $message->to($arr['email'])->subject("Tagihan");
                    });
                }
            }
        }
        
        return 1;
    }
    
    public function analytic()
    {
        $stock = Stock::join('shops', 'shops.id', '=', 'shop_id')
            ->join('models', 'models.id', '=', 'model_id')
            ->select(['model_id', 'shop_id', 'shops.name AS shop_name', 'models.name AS model_name', 'qty'])
            ->whereRaw('qty < 10')
            ->orderBy('models.id')
            ->get();
        $i = 0;
        $id = '';
        foreach($stock as $st){
            if($i==0){
                $dd['title'] = 'Stock Barang Per '.date('d/m/Y H:i:s');
                $id .= Notification::insertGetId($dd);
            }
            
            $data['notif_id'] = $id;
            $data['model_id'] = $st->model_id;
            $data['shop_id'] = $st->shop_id;
            $data['qty'] = $st->qty;
            
            NotificationDetail::create($data);
            
            $i++;
        }
        return redirect('admin/notification');
    }


    public function whatsapp()
    {
        $number = '6281384376223'; # Number with country code
        $type = 'sms'; # This can be either sms or voice


        // Create a instance of Registration class.
        $r = new \Registration($number, $type);

        $r->codeRequest('sms', $number); // could be 'voice' too

        return back();

    }
}
