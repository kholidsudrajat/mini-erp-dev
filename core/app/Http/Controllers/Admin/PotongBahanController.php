<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\ModalPcs;
use App\ModalPcsBahan;
use App\ModalPcsModel;
use App\Models;
use App\Warna;
use App\StockBahan;
use App\StockBahanDetail;
use App\StockBahanLog;
use App\TrxBahan;
use App\TrxBahanDetail;
use App\TempPotong;
use App\TempPotongModel;
use App\Supplier;
use App\SupplierBahan;
use App\SupplierModel;
use Session;
use Datatables;
use DB;
use Auth;
use View;

class PotongBahanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::Table('v_temp_potong')->get();
        $model = TempPotongModel::join('models','models.id','=','model_id')
            ->select([
                'temp_potong_model.id', DB::Raw('CONCAT(name, " - (", model_name, ")") AS name'), 'qty', 'info'
            ])
            ->get();
        
        return view('admin.potong-bahan.index', compact('data', 'model'));
    }
    
    public function submitall(Request $req)
    {
        $date = date('Y-m-d H:i:s');
        $id = ModalPcs::insertGetId(['tanggal' => $date]);
        
        $bahan = TempPotong::get();
        $data['modal_pcs_id'] = $id;
        foreach($bahan as $ba){
            $data['stock_bahan_detail_id'] = $ba->stock_bahan_detail_id;
            
            ModalPcsBahan::create($data);
        }
        TempPotong::truncate();
        
        $keterangan = $req['keterangan'];
        $model = TempPotongModel::get();
        $datas['modal_pcs_id'] = $id;
        foreach($model as $mo){
            $datas['model_id'] = $mo->model_id;
            $datas['qty'] = $mo->qty;
            $datas['keterangan'] = $keterangan[$mo->id];
            
            ModalPcsModel::create($datas);
        }
        TempPotongModel::truncate();
    }
    
    public function potong()
    {
        $supplier = Supplier::pluck('nama','id')->prepend('Pilih Supplier',0);
        
        return view('admin.potong-bahan.potong', compact('supplier'));
    }
    
    public function potongmodel()
    {
        $model = Models::select(['id', DB::Raw('CONCAT(name, " - (", model_name, ")") AS name')])
            ->pluck('name','id')
            ->prepend('Pilih Produk',0);
        
        return view('admin.potong-bahan.potongmodel', compact('model'));
    }
    
    public function cekstock(Request $req)
    {
        $stock = StockBahan::whereSupplierId($req['supplier'])
            ->whereBahanId($req['bahan'])
            ->whereMotifId($req['motif'])
            ->first();
        
        $cek = TempPotong::select('stock_bahan_detail_id')->get()->toArray();
        $detail = StockBahanDetail::join('warnas', 'warnas.id', '=', 'warna_id')
            ->select([
                'warna_id', 'name', DB::Raw('COUNT(warna_id) AS roll'), 'stock_bahan_details.created_at'
            ])
            ->whereStockId($stock->id)
            ->whereNotIn('stock_bahan_details.id',$cek)
            ->groupBy('warna_id')
            ->get();
        
        $table = '';
        $i = 1;
        foreach($detail as $dtl){
            $table .= '<tr><td>'.$i++.'</td><td>'.$dtl->name.'</td><td>'.$dtl->roll.'</td>';
            $table .= '<td>';
            
            $kg = StockBahanDetail::whereStockId($stock->id)
                ->whereWarnaId($dtl->warna_id)
                ->whereNotIn('stock_bahan_details.id',$cek)
                ->get();
            foreach($kg as $kgs){
                $table .= '<div class="checkbox check-success"><input onclick="addbahan('.$kgs->id.')" type="checkbox" id="chk_'.$kgs->id.'" /><label for="chk_'.$kgs->id.'">'.$kgs->kg.' ('.substr($kgs->created_at,0,10).')</label></div>';
            }
            
            $table .= '</td>';
            $table .='</tr>';
        }
        
        return $table;
    }
    
    public function submit(Request $req)
    {
        $detail = explode(',',$req['detail']);
        
        foreach($detail as $in => $val){
            $data['tanggal'] = $req['tanggal'];
            $data['stock_bahan_detail_id'] = $val;
            
            TempPotong::create($data);
        }
    }
    
    public function remove(Request $req)
    {
        $delete = TempPotong::findOrFail($req['id']);
        $delete->delete();
    }
    
    public function delete(Request $req)
    {
        $delete = TempPotongModel::findOrFail($req['id']);
        $delete->delete();
    }
    
    public function submitmodel(Request $req)
    {
        TempPotongModel::create($req->all());
        
        return redirect('admin/potong-bahan');
    }
    
    public function getmodel(Request $req)
    {
        $id = $req['model'];
        $data = Models::findOrFail($id);
        
        return $data->info;
    }
}