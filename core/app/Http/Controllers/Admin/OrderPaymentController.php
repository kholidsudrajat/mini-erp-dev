<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Order;
use App\OrderDetail;
use App\OrderPayment;
use App\SupplierOrder;
use App\Shipping;
use App\Models;
use App\PaymentMethod;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class OrderPaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('admin.pembelian.payment.index');
    }
    
    public function add($id)
    {
        $total = Order::findOrFail($id);
        $bayar = OrderPayment::select([DB::Raw('SUM(total) AS total')])->whereOrderId($id)->first();
        
        $totalnya = $total->total - (0 + $bayar->total);
        
        return view('admin.pembelian.payment.create', compact('id', 'totalnya'));
    }
    
    public function show($id)
    {
        $total = Order::findOrFail($id);
        $bayar = OrderPayment::select([DB::Raw('SUM(total) AS total')])->whereOrderId($id)->first();
        
        $totalnya = $total->total - (0 + $bayar->total);
        
        $order = Order::findOrFail($id);
        $detail = OrderDetail::join('models', 'models.id', '=', 'model_id')->whereOrderId($id)->get();
        $payments = OrderPayment::whereOrderId($id)->get();
        
        $supplier = SupplierOrder::pluck('nama', 'id')->prepend('Pilih Supplier',0);
        $model = Models::select(['id', DB::Raw('CONCAT(name, " - (", model_name, ")") AS name')])
            ->pluck('name','id')
            ->prepend('Pilih Produk',0);
        
        $shipping = Shipping::get();
        $payment = PaymentMethod::get();

        return view('admin.pembelian.payment.show', compact('order', 'detail', 'supplier', 'model', 'shipping', 'payment', 'payments', 'totalnya'));
    }
    
    public function submit(Request $req)
    {
        OrderPayment::create($req->all());
        $order_id = $req['order_id'];
        Order::findOrFail($order_id)->update(['status' => '1']);
    }
    
    public function edit($id)
    {
        $order = Order::findOrFail($id);
        $detail = OrderDetail::join('models', 'models.id', '=', 'model_id')->whereOrderId($id)->get();
        
        $supplier = SupplierOrder::pluck('nama', 'id')->prepend('Pilih Supplier',0);
        $model = Models::select(['id', DB::Raw('CONCAT(name, " - (", model_name, ")") AS name')])
            ->pluck('name','id')
            ->prepend('Pilih Produk',0);
        
        $shipping = Shipping::get();
        $payment = PaymentMethod::get();

        return view('admin.pembelian.payment.edit', compact('order', 'detail', 'supplier', 'model', 'shipping', 'payment'));
    }
    
    public function update($id, Request $request)
    {
        $requestData = $request->all();

        $order = Order::findOrFail($id);
        $order->update($requestData);

        Session::flash('flash_message', 'Order updated!');

        return 1;
    }
    
    public function destroy($id)
    {
        Order::destroy($id);

        Session::flash('flash_message', 'Order deleted!');

        return redirect('admin/pembelian/bayar');
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $order = Order::leftjoin('supplier_orders', 'supplier_orders.id', 'supplier_id')
            ->leftjoin('shipping', 'shipping.id', '=', 'shipping_id')
            ->leftjoin('payment_method', 'payment_method.id', '=', 'payment_type')
            ->select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'orders.id', 'po_number', 'supplier_orders.nama AS supp', 'shipping.name AS ship', 
                'payment_method.name AS paym', 'payment_type', 'shipping_type', 'total'
            ]);

         $datatables = app('datatables')->of($order)
            ->addColumn('action', function ($order) {
                return '<a href="bayar/'.$order->id.'" class="bb btn btn-primary">Detail</a>';
            })
            ->editColumn('total', function ($order){
                return str_replace(',','.',number_format($order->total));
            })
            ->editColumn('ship', function ($order){
                if($order->ship != "")
                    return $order->ship;
                else
                    return $order->shipping_type;
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
}