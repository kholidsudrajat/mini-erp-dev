<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\StockBahan;
use App\StockBahanLog;
use App\StockBahanDetail;
use App\Supplier;
use App\SupplierBahan;
use App\SupplierModel;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class StockBahanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.stock-bahan.index');
    }
    
    public function show($id)
    {
        $stock = StockBahan::join('suppliers', 'suppliers.id', '=', 'supplier_id')
            ->join('suppliers_bahan', 'suppliers_bahan.id', '=', 'bahan_id')
            ->join('suppliers_model', 'suppliers_model.id', '=', 'motif_id')
            ->selectRaw('stock_bahans.id, suppliers.nama AS supplier_name, suppliers_bahan.name AS bahan_name, suppliers_model.name AS motif_name, roll, qty, total')
            ->whereRaw('stock_bahans.id = '.$id)
            ->first();

        $detail = StockBahanDetail::join('warnas', 'warnas.id', '=', 'warna_id')
            ->select([
                'warna_id', DB::Raw('COUNT(warna_id) AS roll'), DB::Raw('SUM(kg) AS kg'), DB::Raw('SUM(total) AS total'), 'name'
            ])
            ->whereStockId($id)
            ->groupBy('warna_id')
            ->get();
        
        return view('admin.stock-bahan.show', compact('stock', 'detail'));
    }
    
    public function shows($id, $ids)
    {
        $stock = StockBahan::join('stock_bahan_details', 'stock_bahans.id', '=', 'stock_id')
            ->join('warnas', 'warnas.id', '=', 'warna_id')
            ->join('suppliers', 'suppliers.id', '=', 'supplier_id')
            ->join('suppliers_bahan', 'suppliers_bahan.id', '=', 'bahan_id')
            ->join('suppliers_model', 'suppliers_model.id', '=', 'motif_id')
            ->selectRaw('stock_bahans.id, warnas.name AS warna_name, suppliers.nama AS supplier_name, suppliers_bahan.name AS bahan_name, 
            suppliers_model.name AS motif_name, roll, qty, stock_bahans.total')
            ->whereRaw('stock_bahans.id = '.$id.' AND warna_id = '.$ids)
            ->first();
        
        $detail = StockBahanLog::join('warnas', 'warnas.id', '=', 'warna_id')
            ->whereStockId($id)
            ->whereWarnaId($ids)
            ->orderBy('tanggal')
            ->get();
        
        return view('admin.stock-bahan.shows', compact('id', 'stock', 'detail'));
    }
    
    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $stock = StockBahan::join('suppliers', 'suppliers.id', '=', 'supplier_id')
            ->join('suppliers_bahan', 'suppliers_bahan.id', '=', 'bahan_id')
            ->join('suppliers_model', 'suppliers_model.id', '=', 'motif_id')
            ->selectRaw('@rownum  := @rownum  + 1 AS rownum, stock_bahans.id, suppliers.nama AS supplier_name, suppliers_bahan.name AS bahan_name, suppliers_model.name AS motif_name, roll, qty, total');

         $datatables = app('datatables')->of($stock)
             ->editColumn('total', function($stock){
                return str_replace(',', '.', number_format($stock->total)); 
             })
             ->addColumn('action', function($stock){
                return '<a href="stock-bahan/'.$stock->id.'" class="btn btn-primary">Detail</a>';
             });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('stock_bahans.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('stock_bahans.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
}
