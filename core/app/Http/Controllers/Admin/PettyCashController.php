<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PettyCash;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class PettyCashController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.petty-cash.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.petty-cash.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $type = $request['type'];
        $data['date'] = $request['date'];
        $data['type'] = $type;
        $data['cost'] = str_replace(',', '', $request['cost']);
        $data['description'] = $request['description'];
        if($type == 'out'){
            $files = $request['files'];
            if($files != ""){
                $path = 'files/petty-cash/';
                $name = rand(10000,99999).'.'.$files->getClientOriginalExtension();
                $files->move($path,$name);
                $data['file'] = $name;
            }
        }

        $pettycash = PettyCash::create($data);
        
        Session::flash('flash_message', 'PettyCash added!');

        return redirect('admin/petty-cash');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $pettycash = DB::table('v_petty_cash')
            ->where('date', $id)
            ->select([ DB::Raw('SUM(cost_in) AS cost_in'), DB::Raw('SUM(cost_out) AS cost_out'), 'date' ])
            ->groupBy('date')
            ->first();

        $petty = PettyCash::where('date', $id)->get();
        
        return view('admin.petty-cash.show', compact('pettycash', 'petty'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $pettycash = PettyCash::findOrFail($id);

        return view('admin.petty-cash.edit', compact('pettycash'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $requestData = $request->all();

        $pettycash = PettyCash::findOrFail($id);
        $pettycash->update($requestData);

        Session::flash('flash_message', 'PettyCash updated!');

        return redirect('admin/petty-cash');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PettyCash::destroy($id);

        Session::flash('flash_message', 'PettyCash deleted!');

        return redirect('admin/petty-cash');
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $pettycash = DB::table('v_petty_cash')
            ->select([
                'date', DB::Raw('SUM(cost_in) AS cost_in'), DB::Raw('SUM(cost_out) AS cost_out')
            ])
            ->groupBy('date');

         $datatables = app('datatables')->of($pettycash)
             ->editColumn('date', function($inputcost){
                 return date_format(date_create($inputcost->date), 'd M Y');
             })
             ->editColumn('cost_in', function ($pettycash) {
                 if($pettycash->cost_in == "")
                     return '-';
                 else
                     return str_replace(',','.',number_format($pettycash->cost_in));
             })
             ->editColumn('cost_out', function ($pettycash) {
                 if($pettycash->cost_out == "")
                     return '-';
                 else
                     return str_replace(',','.',number_format($pettycash->cost_out));
             })
             ->addColumn('action', function ($pettycash) {
                 return '<a href="petty-cash/'.$pettycash->date.'" class="bb btn btn-primary">Lihat Detail</a>';
             });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('petty_cash.date', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('petty_cash.date', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
}
