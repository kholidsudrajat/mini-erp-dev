<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Stock;
use App\StockLog;
use App\Shop;
use App\Models;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class TransferStockController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.transferstock.index');
    }
    
    public function multiple()
    {
        $stock = DB::table('v_stock')
            ->select([
                'shop_name', 'model_name', 'id', 'qty', 'shop_id'
            ])
            ->where('qty','!=','0')
            ->get();
        
        $shop = Shop::get();
        
        return view('admin.transferstock.multiple', compact('stock', 'shop'));
    }

    public function transfermultiple(Request $req)
    {
        $json = json_decode($req['json']);
        $text = "";
        foreach($json as $index => $value){
            $data = Stock::findOrFail($index);
            $shop = Stock::whereShopId($value->shop_id)->whereModelId($data['model_id'])->first();

            $qty = $value->qty;

            if($qty <= $data['qty']){
                $qty_new = $data['qty'] - $qty;

                $old = Stock::findOrFail($data['id']);
                $old->update(['qty' => $qty_new]);

                if(count($shop) != 0){
                    $new = Stock::findOrFail($shop['id']);
                    $qty_new = $new['qty'] + $qty;
                    $new->update(['qty' => $qty_new]);
                }else{
                    Stock::create(['model_id' => $data['model_id'], 'qty' => $qty, 'shop_id' => $value->shop_id]);
                }
            }

            $cek = Shop::findOrFail($value->shop_id);
            if($cek['is_shop'] == '1')
                $type = 'toshop';
            else
                $type = 'towarehouse';

            $datas = array(
                'model_id'      =>  $data['model_id'],
                'qty'           =>  $value->qty,
                'transfer_from' =>  $data['shop_id'],
                'transfer_to'   =>  $value->shop_id,
                'type'          =>  $type,
            );

            $this->createlog($datas);
            $model = Models::findOrFail($data['model_id']);
            $shop = Shop::findOrFail($data['shop_id']);
            $shops = Shop::findOrFail($value->shop_id);
            $text .= '- '. $value->qty.' : '.$model->name.' ('.$model->model_name.') dari '.$shop->name.' ke '.$shops->name.' berhasil dikirim! <br>';
        }
        
        return $text;
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $shop = Shop::pluck('name','id')->prepend('Pilih Toko / Gudang', '');
        
        return view('admin.transferstock.create', compact('shop'));
    }
    
    public function transfer($id)
    {
        $cek = Stock::findOrFail($id);
        $qty = $cek['qty'];
        $shop = Shop::where('id','!=',$cek['shop_id'])->pluck('name','id')->prepend('Pilih Toko / Gudang', '');
        $shops = Shop::findOrFail($cek['shop_id']);
        $model = Models::findOrFail($cek['model_id']);
        
        return view('admin.transferstock.transferstock', compact('id', 'shop', 'qty', 'shops', 'model'));
    }
    
    public function prosestransfer(Request $req)
    {
        $data = Stock::findOrFail($req['stock_id']);
        $shop = Stock::whereShopId($req['shop_id'])->whereModelId($data['model_id'])->first();

        $qty = $req['qty'];
        
        if($qty <= $data['qty']){
            $qty_new = $data['qty'] - $qty;
            
            $old = Stock::findOrFail($data['id']);
            $old->update(['qty' => $qty_new]);
            
            if(count($shop) != 0){
                $new = Stock::findOrFail($shop['id']);
                $qty_new = $new['qty'] + $qty;
                $new->update(['qty' => $qty_new]);
            }else{
                Stock::create(['model_id' => $data['model_id'], 'qty' => $qty, 'shop_id' => $req['shop_id']]);
            }
        }
            
        $cek = Shop::findOrFail($req['shop_id']);
        if($cek['is_shop'] == '1')
            $type = 'toshop';
        else
            $type = 'towarehouse';
            
        $datas = array(
            'model_id'      =>  $data['model_id'],
            'qty'           =>  $req['qty'],
            'transfer_from' =>  $data['shop_id'],
            'transfer_to'   =>  $req['shop_id'],
            'type'          =>  $type,
        );
        
        $this->createlog($datas);

        Session::flash('success', 'Transfer Stok dari ... ke .... berhasil!');

        return $data['qty'] - $req['qty'];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $transferstock = Stock::create($requestData);

        Session::flash('flash_message', 'Mtransferstock added!');

        return redirect('admin/transferstock');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $transferstock = Stock::findOrFail($id);

        return view('admin.transferstock.show', compact('transferstock'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $transferstock = Stock::findOrFail($id);

        return view('admin.transferstock.edit', compact('transferstock'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $requestData = $request->all();

        $transferstock = Stock::findOrFail($id);
        $transferstock->update($requestData);

        Session::flash('flash_message', 'Mtransferstock updated!');

        return redirect('admin/transferstock');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Stock::destroy($id);

        Session::flash('flash_message', 'Mtransferstock deleted!');

        return redirect('admin/transferstock');
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $stock = DB::table('v_stock')
            ->select([
                'shop_name', 'model_name', 'id', 'qty'
            ])
            ->where('qty','!=','0');

         $datatables = app('datatables')->of($stock)
            ->addColumn('action', function ($transferstock) {
                return '<a id="transfer" href="kirim-stock/'.$transferstock->id.'/transfer" class="btn btn-xs btn-primary rounded">Transfer Stok</a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        return $datatables->make(true);
    }
    
    public function createlog($data)
    {
        StockLog::create($data);
        
        return 1;
    }
}
