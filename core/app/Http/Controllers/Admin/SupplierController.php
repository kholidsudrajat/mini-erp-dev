<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Supplier;
use App\SupplierBahan;
use App\SupplierModel;
use App\SupplierOrder;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class SupplierController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.supplier.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $Supplier = Supplier::create($requestData);

        Session::flash('flash_message', 'Supplier added!');

        return redirect('admin/supplier');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $supplier = Supplier::findOrFail($id);
        $bahan = SupplierBahan::whereSupplierId($id)->get();
        $model = SupplierModel::whereSupplierId($id)->get();

        return view('admin.supplier.show', compact('supplier', 'bahan', 'model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $supplier = Supplier::findOrFail($id);

        return view('admin.supplier.edit', compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $requestData = $request->all();

        $supplier = Supplier::findOrFail($id);
        $supplier->update($requestData);

        Session::flash('flash_message', 'Supplier updated!');

        return redirect('admin/supplier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        SupplierBahan::whereSupplierId($id)->delete();
        SupplierModel::whereSupplierId($id)->delete();
        Supplier::destroy($id);

        Session::flash('flash_message', 'Supplier deleted!');

        return redirect('admin/supplier');
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $supplier = Supplier::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'nama', 'no_telp', 'email', 'pic', 'alamat', 'keterangan']);

         $datatables = app('datatables')->of($supplier)
            ->addColumn('action', function ($supplier) {
                return '<a id="addbahan" href="'. url('admin/addbahansupplier') .'/'. $supplier->id.'" class="bb btn btn-success">Tambah Bahan</a> 
                <a id="addmodel" href="'. url('admin/addmodelsupplier') .'/'. $supplier->id.'" class="bb btn btn-primary">Tambah Motif</a> 
                <a href="supplier/'.$supplier->id.'" class="bb btn btn-info">Detail</a> 
                <a href="supplier/'.$supplier->id.'/edit" class="bb btn btn-warning">Edit</a> 
                <a onclick="return deleteData('.$supplier->id.')" class="bb btn btn-danger">Delete</a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('suppliers.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('suppliers.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
    
    public function addbahan($id)
    {
        $bahan = SupplierBahan::whereSupplierId($id)->get();
        
        return view('admin.supplier.addbahan', compact('id', 'bahan'));
    }
    
    public function savebahan(Request $req)
    {
        $id = $req['supplier_id'];
        SupplierBahan::create($req->all());
        
        return redirect('admin/addbahansupplier/'.$id);
    }
    
    public function deletebahan($id)
    {
        SupplierBahan::destroy($id);
        
        return 1;
    }
    
    public function addmodel($id)
    {
        $model = SupplierModel::whereSupplierId($id)->get();
        
        return view('admin.supplier.addmodel', compact('id', 'model'));
    }
    
    public function savemodel(Request $req)
    {
        $id = $req['supplier_id'];
        SupplierModel::create($req->all());
        
        return redirect('admin/addmodelsupplier/'.$id);
    }
    
    public function deletemodel($id)
    {
        SupplierModel::destroy($id);
        
        return 1;
    }
}
