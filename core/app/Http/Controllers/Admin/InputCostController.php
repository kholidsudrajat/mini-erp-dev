<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\OperationalCost;
use App\Operational;
use App\Shop;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;
use PDF;

class InputCostController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('admin');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index() {
        if(Auth::User()->shop_id == 0){
            $idshop = Session::get('shop');
            $ids = Shop::whereIsShop('1')->first();

            if($idshop == ""){
                Session::put('shop', $ids['id']);
                $idshop = Session::get('shop');
            }
        }else
            $idshop = Auth::User()->shop_id;
        
        $shop = Shop::pluck('name', 'id');
        $ope = Operational::get();
        
		return view('admin.inputcost.index', compact('ope', 'shop', 'idshop'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create() {
		return view('admin.inputcost.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
    
    public function add(Request $req){
        $date = str_replace('/','-',$req['date']);
        
        $json = json_decode($req['json']);
        $data['date'] = $date;
        $data['shop_id'] = $req['shop'];
        foreach($json as $index => $value){
            $data['label']  = $value->label;
            $data['cost']   = str_replace(',', '', $value->biaya);

            if($value->biaya > 0)
                OperationalCost::create($data);
        }

        return 1;
    }
    
    public function updates(Request $req){
        $date = str_replace('/','-',$req['date']);
        
        $json = json_decode($req['json']);
        $data['date'] = $date;
        $data['shop_id'] = $req['shop'];
        
        OperationalCost::whereRaw('date LIKE "%'.$date.'%" AND shop_id = "'.$req['shops'].'"')->delete();
        foreach($json as $index => $value){
            $data['label']  = $value->label;
            $data['cost']   = str_replace(',', '', $value->biaya);

            if($value->biaya > 0)
                OperationalCost::create($data);
        }

        return 1;
    }
    
    public function getbydate(Request $req){
        $date = $req['dates'];
        
        $data = OperationalCost::whereRaw('date LIKE "%'.$date.'%" AND shop_id = "'.$req['shop'].'"')->get();
        
        return $data;
    }
    
	public function destroys(Request $req) {
        $date = $req['dates'];
		OperationalCost::whereRaw('date LIKE "%'.$date.'%" AND shop_id = "'.$req['shop'].'"')->delete();

		Session::flash('flash_message', 'OperationalCost deleted!');

		return redirect('admin/operasional');
	}
    
	public function store(Request $request) {
		$requestData = $request->all();

		$inputcost = OperationalCost::create($requestData);

		Session::flash('flash_message', 'OperationalCost added!');

		return redirect('admin/operasional');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function show($id) {
		$inputcost = OperationalCost::findOrFail($id);

		return view('admin.inputcost.show', compact('inputcost'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function edit($id) {
		$inputcost = OperationalCost::findOrFail($id);

		return view('admin.inputcost.edit', compact('inputcost'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function update($id, Request $request) {
		$requestData = $request->all();

		$inputcost = OperationalCost::findOrFail($id);
		$inputcost->update($requestData);

		Session::flash('flash_message', 'OperationalCost updated!');

		return redirect('admin/operasional');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
    
	public function destroy($id) {
		OperationalCost::destroy($id);

		Session::flash('flash_message', 'OperationalCost deleted!');

		return redirect('admin/operasional');
	}

	public function anyData(Request $request) {
		DB::statement(DB::raw('set @rownum=0'));
		$inputcost = OperationalCost::join('shops', 'shops.id', '=', 'shop_id')
            ->select([
					DB::raw('SUM(cost) AS cost'), 'operational_costs.id', 'date', 'shops.name', 'shop_id'
            ])
            ->groupBy('date','shop_id');

		$datatables = app('datatables')->of($inputcost)
                ->editColumn('date', function($inputcost){
                    return date_format(date_create($inputcost->date), 'd M Y');
                })
                ->editColumn('cost', function($inputcost){
                    return str_replace(',','.',number_format($inputcost->cost));
                })
				->addColumn('action', function ($inputcost) {
                    return '<a onclick="return edit(&quot;'.$inputcost->date.'&quot;,&quot;'.$inputcost->shop_id.'&quot;)" class="btn-xs btn btn-primary"><i class="fa fa-pencil"></i></a>
                    <a onclick="return deleteData(&quot;'.$inputcost->date.'&quot;, '.$inputcost->shop_id.')" class="btn-xs btn btn-danger"><i class="fa fa-times"></i></a>';
                });

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('date', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('date', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}
	
	public function exportpdf(Request $req){
	    $range = explode(":", $req['date']);
	    
        $report = OperationalCost::join('shops', 'shops.id', '=', 'shop_id')
            ->select([
					DB::raw('SUM(cost) AS cost'), 'operational_costs.id', 'date', 'shops.name', 'shop_id'
            ]);
        
        $clause = '';
        if($req['date'] != "")
            $clause .= 'date BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->groupBy('date','shop_id')->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Tanggal</th><th>Toko</th><th>Total</th></tr>';
        
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->date.'</td>';
            $html .= '<td>'.$rep->name.'</td>';
            $html .= '<td>'.str_replace(',','.',number_format($rep->cost)).'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/input-cost/'))
                mkdir('upload/input-cost/', 0777, true);
            
            file_put_contents('upload/input-cost/'.$filename.'.pdf', $pdf);
            $file = 'upload/input-cost/'.$filename.'.pdf';
            
            return $file;
        }
    }

}
