<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cashier;
use App\CashierDetail;
use App\Shop;
use App\Models;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ReportProductController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('admin');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index() {
		$prod = Models::join('cashier_details', 'models.id', '=', 'model_id')
				->groupBy('model_id')
				->get();
		$prod = count($prod);
		
		$report = Models::join('cashier_details', 'cashier_details.model_id', '=', 'models.id')
				->select([
					DB::Raw('SUM(qty) AS tqty'), DB::Raw('SUM(qty * total_cost) AS tmodal'), 
					DB::Raw('SUM(qty * price) AS tharga'), DB::Raw('SUM(qty * diff) AS tprofit')])
				->first();
				
		return view('admin.report.product.index', compact('prod', 'report'));
	}

	public function show($id, Request $req) {
		$limit = 5;
		$model = Models::findOrFail($id);
		$report = CashierDetail::join('models', 'models.id', '=', 'model_id')
				->join('cashier', 'cashier_id', '=', 'cashier.id')
				->select([
					'models.id', DB::raw('CONCAT(name, " (", model_name,")") AS names'), 'cashier_id', 'qty',
					'no_ref', DB::Raw('qty * total_cost AS cost'), DB::Raw('qty * price AS price'),
					DB::Raw('qty * diff AS diff'), 'cashier_details.created_at'])
				->whereModelId($id);

		$total = count($report->get());

		$report = $report->limit($limit)->orderBy('created_at', 'desc');
		$report = $report->get();

		$mod = $total % $limit;
		$pagin = intval($total / $limit);
		if ($mod > 0)
			$pagin = $pagin + 1;

		if ($pagin == 1)
			$pagin = 0;

		return view('admin.report.product.show', compact('model', 'report', 'pagin', 'total'));
	}

	public function shows($id, $idm) {
		$list_penjualan = Cashier::join('customers', 'customers.id', '=', 'customer_id')->select('cashier.id', 'cashier.created_at', 'no_ref', 'nama', 'total', 'subtotal', 'discount')->where('cashier.id', $id)->first();

		$detail = CashierDetail::join('models', 'models.id', '=', 'model_id')->select('cashier_details.id', 'model_name', 'name', 'cashier_details.created_at', 'qty', 'price')->whereCashierId($id)->get();

		return view('admin.report.product.shows', compact('list_penjualan', 'detail', 'idm'));
	}

	public function filter($id, Request $req) {
		$limit = $req['limit'];
		$page = ($req['page'] - 1) * $limit;

		$table = "";

		$report = CashierDetail::join('models', 'models.id', '=', 'model_id')
				->join('cashier', 'cashier_id', '=', 'cashier.id')
				->select([
					'models.id', DB::raw('CONCAT(name, " (", model_name,")") AS names'), 'cashier_id', 'qty',
					'no_ref', DB::Raw('qty * total_cost AS cost'), DB::Raw('qty * price AS price'),
					DB::Raw('qty * diff AS diff'), 'cashier_details.created_at'])
				->whereModelId($id)
				->limit($limit)
				->offset($page);

		if ($req['range'] != "") {
			$rang = explode(":", $req['range']);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$report->whereBetween('cashier_details.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$report->whereBetween('cashier_details.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		if ($req['sort'] != "") {
			if ($req['asc'] == 1)
				$report->orderBy($req['sort'], 'DESC');
			else
				$report->orderBy($req['sort'], 'ASC');
		}else {
			$report->orderBy('no_ref', 'ASC');
		}

		$report = $report->get();

		$i = $page + 1;
		$qty = 0;
		$cost = 0;
		$price = 0;
		$diff = 0;
		foreach ($report as $rpt) {
			$table .= '
                <tr><td>' . $i++ . '</td>
                    <td><a href="' . $rpt['cashier_id'] . '/' . $rpt['id'] . '/show">' . $rpt['no_ref'] . '</a></td>
                    <td>' . $rpt['created_at'] . '</td>
                    <td>' . $rpt['qty'] . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt['cost'])) . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt['price'])) . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt['diff'])) . '</td>
                </tr>
                ';

			$qty += $rpt['qty'];
			$cost += $rpt['cost'];
			$price += $rpt['price'];
			$diff += $rpt['diff'];
		}

		$table .= ' 
            <tr>
                <td colspan="3"><b class="pull-right">Total</b></td>
                <td><b>' . $qty . '</b></td>
                <td><b>' . str_replace(',', '.', number_format($cost)) . '</b></td>
                <td><b>' . str_replace(',', '.', number_format($price)) . '</b></td>
                <td><b>' . str_replace(',', '.', number_format($diff)) . '</b></td>
            </tr>
            ';

		$data[0] = $table;
		$data[1] = count($report);

		return $data;
	}

	public function anyData(Request $request) {
		DB::statement(DB::raw('set @rownum=0'));
		$report = DB::Table('v_product')
            ->select(['id', 'names', DB::Raw('SUM(tqty) AS tqty'), DB::Raw('SUM(tmodal) AS tmodal'), DB::Raw('SUM(tharga) AS tharga'), DB::Raw('SUM(tprofit) AS tprofit')])
            ->groupBy('id');

		$datatables = app('datatables')->of($report)
				->addColumn('action', function ($report) {
					return '<a href="sales_product/' . $report->id . '" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="' . trans('systems.edit') . '">Lihat Detail</a>';
				})
				->editColumn('tmodal', function($report) {
					return str_replace(',', '.', number_format($report->tmodal));
				})
				->editColumn('tharga', function($report) {
					return str_replace(',', '.', number_format($report->tharga));
				})
				->editColumn('tprofit', function($report) {
                    return str_replace(',', '.', number_format($report->tprofit));
                });

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}

}
