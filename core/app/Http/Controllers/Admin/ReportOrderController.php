<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDetail;
use App\OrderPayment;
use App\SupplierOrder;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ReportOrderController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index() {
        return view('admin.report.order-suppliers.index');
	}
    
	public function show($id) {
        $supplier = SupplierOrder::leftjoin('orders', 'supplier_orders.id', '=', 'supplier_id')
            ->leftjoin('order_details', 'order_details.order_id', '=', 'orders.id')
            ->select([
                'supplier_orders.id', 'nama', 'no_telp', 'email', 'pic', 'alamat', DB::Raw('COUNT(po_number) AS transaksi'), 
                DB::Raw('SUM(order_details.total) AS total'), DB::Raw('SUM(order_details.qty) AS qty')
            ])
            ->where('supplier_orders.id',$id)
            ->groupBy('supplier_orders.id')
            ->first();
        
        $order = Order::leftjoin('supplier_orders', 'supplier_orders.id', 'supplier_id')
            ->leftjoin('order_details', 'order_details.order_id', '=', 'orders.id')
            ->leftjoin('shipping', 'shipping.id', '=', 'shipping_id')
            ->leftjoin('payment_method', 'payment_method.id', '=', 'payment_type')
            ->select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), DB::Raw('SUM(order_details.qty) AS qty'), 'orders.id', 'po_number', 'supplier_orders.nama AS supp', 
                'shipping.name AS ship', 'payment_method.name AS paym', 'payment_type', 'shipping_type', 'orders.total', 'status'
            ])
            ->whereSupplierId($id)
            ->groupBy('orders.id')
            ->get();
        
        
        return view('admin.report.order-suppliers.show', compact('supplier', 'order'));
	}

    public function shows($id, $idm){
        $supplier = SupplierOrder::findOrFail($id);
        $order = Order::findOrFail($idm);
        $payment = OrderPayment::whereOrderId($idm)->get();
        $bayar = OrderPayment::select([DB::Raw('SUM(total) AS total')])->whereOrderId($idm)->first();
        $totalnya = $order->total - (0 + $bayar->total);
        
        $detail = OrderDetail::join('models', 'models.id', '=', 'model_id')
            ->select(['model_id', 'name', 'model_name', 'qty', 'order_details.price', 'total'])
            ->whereOrderId($idm)
            ->get();
        
        return view('admin.report.order-suppliers.shows', compact('supplier', 'order', 'detail', 'payment', 'totalnya'));
    }
    
	public function anyData(Request $request) {
		$report = SupplierOrder::leftjoin('orders', 'supplier_orders.id', '=', 'supplier_id')
            ->leftjoin('order_details', 'order_id', '=', 'orders.id')
            ->select([
                'supplier_orders.id', 'nama', 'no_telp', 'pic', DB::Raw('COUNT(po_number) AS transaksi'), DB::Raw('SUM(order_details.total) AS total'), 
                DB::Raw('SUM(qty) AS qty')
            ])
            ->whereRaw('order_details.total > 0')
            ->groupBy('supplier_orders.id');

		$datatables = app('datatables')->of($report)
            ->addColumn('action', function ($report) {
				return '<a href="order-suppliers/' . $report->id . '" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="' . trans('systems.edit') . '">Lihat Detail</a>';
            })
            ->editColumn('qty', function($report){
                return str_replace(',','',number_format($report->qty));
            })
            ->editColumn('total', function($report){
                return str_replace(',','.',number_format($report->total));
            });

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		return $datatables->make(true);
	}

}
