<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cashier;
use App\CashierDetail;
use App\Shop;
use App\Retur;
use App\Models;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ReturNoretController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('admin');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index() {
		return view('admin.report.retur-noret.index');
	}
    
	public function show($id) {
		$data = Retur::join('models', 'models.id', '=', 'model_id')
            ->join('customers', 'customers.id', '=', 'customer_id')
            ->join('shops', 'shops.id', '=', 'shop_id')
            ->select(['returs.id', 'no_retur', 'no_ref', DB::Raw('CONCAT(customers.nama, " (", customer_code, ")") AS cus'), 'type_retur', 'qty', 'returs.note',
                      DB::Raw('CONCAT(models.name, " (", model_name, ")") AS modelname'), DB::Raw('shops.name AS shopname'), DB::Raw('price * qty AS cost')])
            ->where(['returs.id' => $id])
            ->first();

		return view('admin.report.retur-noret.shows', compact('data', 'id'));
	}

	public function anyData(Request $request) {
		$report = Retur::select([
            'id', 'no_retur', 'no_ref', 'type_retur', 'created_at'
        ]);

		$datatables = app('datatables')->of($report)
				->addColumn('action', function ($report) {
					return '<a href="retur-noret/' . $report->id . '" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="' . trans('systems.edit') . '">Lihat Detail</a>';
				});

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('returs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('returs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}

}
