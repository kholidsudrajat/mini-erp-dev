<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Warna;
use App\StockBahan;
use App\StockBahanDetail;
use App\StockBahanLog;
use App\TrxBahan;
use App\TrxBahanDetail;
use App\TempBahan;
use App\TempBahanDetail;
use App\Supplier;
use App\SupplierBahan;
use App\SupplierModel;
use Session;
use Datatables;
use DB;
use Auth;
use View;

class BahanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sup = Supplier::pluck('nama','id')->prepend('Pilih Supplier', 0);
        $cek = TempBahan::count();
        $temp = TempBahan::first();
        if($cek > 0){
            $bahan = SupplierBahan::whereId($temp->bahan_id)->pluck('name', 'id');
            $motif = SupplierModel::whereId($temp->motif_id)->pluck('name', 'id');
        }
        $warna = Warna::pluck('name', 'id');
        $detail = TempBahanDetail::join('warnas', 'warnas.id', '=', 'warna_id')
            ->select(['temp_id', 'name', DB::Raw('COUNT(temp_bahan_details.id) AS roll'), DB::Raw('SUM(kg) AS kg')])
            ->groupBy('temp_id')
            ->get();
        
        return view('admin.bahan.index',compact('sup', 'cek', 'temp', 'bahan', 'motif', 'warna', 'detail'));
    }
    
    public function pilihbahan($sup)
    {
        $bahan = SupplierBahan::whereSupplierId($sup)->get();
        $motif = SupplierModel::whereSupplierId($sup)->get();
        
        $data['bahan'] = $bahan;
        $data['motif'] = $motif;
        
        return $data;
    }
    
    public function createtemp(Request $req)
    {
        $cek = TempBahan::count();
        if($cek == 1){
            $upd = TempBahan::first();
            $upd->update($req->all());
        }else{
            TempBahan::create($req->all());
        }
        
        return 1;
    }
    
    public function createtempdetail(Request $req)
    {
        $kg = $req['kg'];
        $cek = TempBahanDetail::count();
        if($cek > 0){
            $ceks = TempBahanDetail::select([DB::Raw('MAX(temp_id) AS temp_id')])->first();
            $id = $ceks['temp_id'] + 1;
            foreach($kg as $index => $val){
                $data['temp_id'] = $id;
                $data['warna_id'] = $req['warna_id'];
                $data['kg'] = $val;
                
                TempBahanDetail::create($data);
            }
        }else{
            $id = 1;
            foreach($kg as $index => $val){
                $data['temp_id'] = $id;
                $data['warna_id'] = $req['warna_id'];
                $data['kg'] = $val;
                
                TempBahanDetail::create($data);
            }
        }
        
        return 1;
    }
    
    public function submitbahan(Request $req)
    {
        $temp = TempBahan::first();
        $detail = TempbahanDetail::get();
        $get = TempBahanDetail::selectRaw('SUM(kg) as total, COUNT(id) as counts')->first();
        
        $data['tanggal'] = $temp['tanggal'];
        $data['supplier_id'] = $temp['supplier_id'];
        $data['bahan_id'] = $temp['bahan_id'];
        $data['motif_id'] = $temp['motif_id'];
        $data['total'] = $get['total'] * $temp['harga'];
        
        $id = TrxBahan::insertGetId($data);
        
        foreach($detail as $dtl){
            $datas['trx_id'] = $id;
            $datas['warna_id'] = $dtl['warna_id'];
            $datas['kg'] = $dtl['kg'];
            $datas['total'] = $dtl['kg'] * $temp['harga'];
            
            TrxBahanDetail::create($datas);
        }
        
        $cek = StockBahan::whereSupplierId($temp['supplier_id'])
            ->whereBahanId($temp['bahan_id'])
            ->whereMotifId($temp['motif_id'])
            ->first();
        
        if(!isset($cek)){
            $dd['supplier_id'] = $temp['supplier_id'];
            $dd['bahan_id'] = $temp['bahan_id'];
            $dd['motif_id'] = $temp['motif_id'];
            $dd['roll'] = $get['counts'];
            $dd['qty'] = $get['total'];
            $dd['total'] = $get['total'] * $temp['harga'];
            
            $ids = StockBahan::insertGetId($dd);
        }else{
            $dd['roll'] = $cek['roll'] + $get['counts'];
            $dd['qty'] = $cek['qty'] + $get['total'];
            $dd['total'] = $cek['total'] + ($get['total'] * $temp['harga']);
            
            $cek->update($dd);
            
            $ids = $cek->id;
        }
        
        foreach($detail as $dtl){
            $ddd['stock_id'] = $ids;
            $ddd['warna_id'] = $dtl['warna_id'];
            $ddd['kg'] = $dtl['kg'];
            $ddd['total'] = $dtl['kg'] * $temp['harga'];
            
            StockBahanDetail::create($ddd);
            
            $de['tanggal'] = $temp['tanggal'];
            $de['transaksi'] = 'beli';
            $de['stock_id'] = $ids;
            $de['warna_id'] = $dtl['warna_id'];
            $de['kg'] = $dtl['kg'];
            $de['harga'] = $temp['harga'];
            
            StockBahanLog::create($de);
        }
        
        TempBahan::truncate();
        TempBahanDetail::truncate();
        
        return 1;
    }
    
    public function cleartemp()
    {
        TempBahan::truncate();
        TempBahanDetail::truncate();
        
        return 1;
    }
    
    public function detailkg($id)
    {
        $data = TempBahanDetail::whereTempId($id)->get();
        
        return $data;
    }
}