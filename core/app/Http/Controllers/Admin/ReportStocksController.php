<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cashier;
use App\CashierDetail;
use App\Shop;
use App\Models;
use App\StockMovement;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ReportStocksController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index() {
		$prod = Models::count();

		$report = Models::join('stocks', 'model_id', '=', 'models.id')
				->select(['model_id', 'name', DB::Raw('SUM(qty) AS tqty'), 
					DB::Raw('SUM(qty * price) AS tharga')])
				->first();

		return view('admin.report.stocks.index', compact('prod', 'report'));
	}

	public function show($id, Request $req) {
		$limit = 5;
		$model = Models::findOrFail($id);

		$stock = DB::table('stock_by_shop')->select([DB::Raw('SUM(qtys) AS qtys'), DB::Raw('SUM(qtyw) AS qtyw')])->whereModelId($id)->first();

		$report = StockMovement::select(['type', 'transfer_from', 'transfer_to', DB::Raw('IFNULL(stock_in,0) AS stock_in'), DB::Raw('IFNULL(stock_out,0) AS stock_out'), DB::Raw('IFNULL(stock_shop,0) AS stock_shop'), DB::Raw('IFNULL(stock_warehouse,0) AS stock_warehouse'), 'created_at'])->whereModelId($id);

		$total = count($report->get());

		$report = $report->limit($limit);
		$report = $report->get();

		$mod = $total % $limit;
		$pagin = intval($total / $limit);
		if ($mod > 0)
			$pagin = $pagin + 1;

		if ($pagin == 1)
			$pagin = 0;

		$shop = Shop::pluck('name', 'id');

		return view('admin.report.stocks.show', compact('model', 'report', 'stock', 'shop', 'pagin', 'total'));
	}

	public function filter($id, Request $req) {
		$limit = $req['limit'];
		$page = ($req['page'] - 1) * $limit;

		$table = "";

		$report = StockMovement::select(['type', 'transfer_from', 'transfer_to', DB::Raw('IFNULL(stock_in,0) AS stock_in'),
					DB::Raw('IFNULL(stock_out,0) AS stock_out'), DB::Raw('IFNULL(stock_shop,0) AS stock_shop'),
					DB::Raw('IFNULL(stock_warehouse,0) AS stock_warehouse'), 'created_at'])
				->whereModelId($id)
				->limit($limit)
				->offset($page);

		$shop = Shop::pluck('name', 'id');

		if ($req['range'] != "") {
			$rang = explode(":", $req['range']);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$report->whereBetween('stock_movement.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$report->whereBetween('stock_movement.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		if ($req['type'] != "") {
			if ($req['type'] != "semua")
				$report->whereType($req['type']);
		}

		if ($req['sort'] != "") {
			if ($req['asc'] == 1)
				$report->orderBy($req['sort'], 'DESC');
			else
				$report->orderBy($req['sort'], 'ASC');
		}else {
			$report->orderBy('created_at', 'ASC');
		}

		$report = $report->get();

		$i = $page + 1;
		foreach ($report as $rpt) {
			$to = $rpt['transfer_to'];
			$from = $rpt['transfer_from'];

			$table .= '
                <tr>
                    <td>' . $i++ . '</td>
                    <td>' . $rpt['created_at'] . '</td>';

			if ($from != 0)
				$table .= '<td>' . $shop[$from] . '</td>';
			else
				$table .= '<td> Supplier / Produksi </td>';

			if ($to != 0)
				$table .= '<td>' . $shop[$to] . '</td>';
			else
				$table .= '<td> Penjualan </td>';

			$table .= '<td>';
			if ($rpt['type'] == "in")
				$table .= $rpt['stock_in'];
			elseif ($rpt['type'] == "out")
				$table .= $rpt['stock_out'];
			elseif ($rpt['type'] == "toshop")
				$table .= $rpt['stock_shop'];
			elseif ($rpt['type'] == "towarehouse")
				$table .= $rpt['stock_warehouse'];
			$table .= '</td>';

			$table .= '<td>';
			if ($rpt['type'] == "in")
				$table .= 'Barang Masuk';
			elseif ($rpt['type'] == "out")
				$table .= 'Barang Keluar (Dibeli)';
			elseif ($rpt['type'] == "toshop")
				$table .= 'Barang Masuk ke Toko';
			elseif ($rpt['type'] == "towarehouse")
				$table .= 'Barang Masuk ke Gudang';
			$table .= '</td>';

			$table .= '</tr>';
		}

		$data[0] = $table;
		$data[1] = count($report);

		return $data;
	}

	public function anyData(Request $request) {
		DB::statement(DB::raw('set @rownum=0'));
		$report = Models::join('stocks', 'model_id', '=', 'models.id')
				->select(['model_id', 'name', DB::Raw('SUM(qty) AS tqty'), 
					DB::Raw('SUM(qty * price) AS tharga')])
                ->where('qty', '>', '0')
				->groupBy('model_id');

		$datatables = app('datatables')->of($report)
				->editColumn('tharga', function($report){
					return str_replace(',','.',number_format($report->tharga));
				});

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('stocks.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('stocks.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}

}
