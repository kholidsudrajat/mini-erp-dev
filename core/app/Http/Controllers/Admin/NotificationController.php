<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Notification;
use App\NotificationDetail;
use App\Stock;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.notification.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.notification.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $Notification = Notification::create($requestData);

        Session::flash('flash_message', 'Notification added!');

        return redirect('admin/notification');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $notification = Notification::findOrFail($id);
        
        $notification->update(['isread' => '1']);
        
        $stock = Stock::join('shops', 'shops.id', '=', 'shop_id')
            ->join('models', 'models.id', '=', 'model_id')
            ->select(['model_id', 'shop_id', 'shops.name AS shop_name', 'models.name AS model_name', 'qty'])
            ->whereRaw('qty < 10')
            ->orderBy('models.id')
            ->get();

        return view('admin.notification.show', compact('notification', 'stock'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $notification = Notification::findOrFail($id);

        return view('admin.notification.edit', compact('notification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $requestData = $request->all();

        $notification = Notification::findOrFail($id);
        $notification->update($requestData);

        Session::flash('flash_message', 'Notification updated!');

        return redirect('admin/notification');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Notification::destroy($id);

        Session::flash('flash_message', 'Notification deleted!');

        return redirect('admin/notification');
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $notification = Notification::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'title']);

         $datatables = app('datatables')->of($notification)
            ->addColumn('action', function ($notification) {
                return '<a href="'.url("").'/admin/notification/'.$notification->id.'" class="btn btn-primary">Lihat Detail</a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('notifications.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('notifications.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
}
