<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cashier;
use App\CashierDetail;
use App\PaymentMethod;
use App\Shop;
use App\Models;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ReportInvoiceController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('admin');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index() {
		$cas = Cashier::count();

		$report = Cashier::join('cashier_details', 'cashier.id', '=', 'cashier_id')
				->join('models', 'models.id', '=', 'model_id')
				->select([DB::Raw('SUM(qty * total_cost) AS tmodal'), DB::Raw('SUM(qty * price) AS tharga'),
					DB::Raw('SUM(qty * diff) AS tprofit')])
				->first();

		return view('admin.report.invoice.index', compact('cas', 'report'));
	}

	public function show($id) {
		$limit = 5;
		$list_penjualan = Cashier::join('customers', 'customers.id', '=', 'customer_id')->select('cashier.id', 'cashier.created_at', 'no_ref', 'nama', 'total', 'subtotal', 'discount')->where('cashier.id', $id)->first();

		$detail = CashierDetail::join('models', 'models.id', '=', 'model_id')->select('cashier_details.id', 'model_name', 'name', 'cashier_details.created_at', 'qty', 'price', 'total_cost', 'diff')->whereCashierId($id);

		$total = count($detail->get());

		$detail = $detail->limit($limit);
		$detail = $detail->get();

		$mod = $total % $limit;
		$pagin = intval($total / $limit);
		if ($mod > 0)
			$pagin = $pagin + 1;

		if ($pagin == 1)
			$pagin = 0;

		return view('admin.report.invoice.show', compact('list_penjualan', 'detail', 'pagin', 'total'));
	}

	public function filter($id, Request $req) {
		$limit = $req['limit'];
		$page = ($req['page'] - 1) * $limit;

		$table = "";

		$report = CashierDetail::join('models', 'models.id', '=', 'model_id')
				->select([
					'models.id', 'name', 'model_name', 'cashier_id', 'qty',
					DB::Raw('qty * total_cost AS cost'), DB::Raw('qty * price AS price'),
					DB::Raw('qty * diff AS diff'), 'cashier_details.created_at'])
				->whereCashierId($id)
				->limit($limit)
				->offset($page);

		if ($req['keyword'] != "") {
			$report->whereRaw('(model_name LIKE "%' . $req['keyword'] . '%" OR name LIKE "%' . $req['keyword'] . '%")');
		}

		if ($req['sort'] != "") {
			if ($req['asc'] == 1)
				$report->orderBy($req['sort'], 'DESC');
			else
				$report->orderBy($req['sort'], 'ASC');
		}

		$report = $report->get();

		$i = $page + 1;
		$qty = 0;
		$cost = 0;
		$price = 0;
		$diff = 0;
		foreach ($report as $rpt) {
			$table .= '
                <tr><td>' . $i++ . '</td>
                    <td>' . $rpt['name'] . '</td>
                    <td>' . $rpt['model_name'] . '</td>
                    <td>' . $rpt['qty'] . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt['cost'])) . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt['price'])) . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt['diff'])) . '</td>
                </tr>
                ';

			$qty += $rpt['qty'];
			$cost += $rpt['cost'];
			$price += $rpt['price'];
			$diff += $rpt['diff'];
		}

		$data[0] = $table;
		$data[1] = count($report);

		return $data;
	}

	public function anyData(Request $request)
    {
        $payment = PaymentMethod::pluck('name', 'id');

		DB::statement(DB::raw('set @rownum=0'));
		$list_penjualan = Cashier::join('cashier_details', 'cashier.id', '=', 'cashier_id')
            ->join('models', 'models.id', '=', 'model_id')
            ->select(['cashier.id', 'no_ref', 'cashier.created_at', 'method_pembayaran',DB::Raw('SUM(qty * total_cost) AS costs'), DB::Raw('SUM(discount) AS discount'),
                DB::Raw('SUM(qty * price) AS prices'), DB::Raw('(SUM(qty * price) - SUM(qty * total_cost) - SUM(discount)) AS diffs')])
            ->groupBy('cashier_id');

		$datatables = app('datatables')->of($list_penjualan)
            ->addColumn('action', function ($list_penjualan) {
                return '<a href="sales_invoice/' . $list_penjualan->id . '" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="' . trans('systems.edit') . '">Lihat Detail</a>';
            })
            ->editColumn('costs', function($report) {
                return str_replace(',', '.', number_format($report->costs));
            })
            ->editColumn('prices', function($report) {
                return str_replace(',', '.', number_format($report->prices));
            })
            ->editColumn('discount', function($report) {
                return str_replace(',', '.', number_format($report->discount));
            })
            ->editColumn('diffs', function($report) {
                return str_replace(',', '.', number_format($report->diffs));
            })
            ->editColumn('method_pembayaran', function($list_penjualan) use ($payment) {
                if ($list_penjualan->method_pembayaran != ''){
                    $data = explode(',', substr($list_penjualan->method_pembayaran, 0, -1));

                    $result = "";
                    foreach ($data as $datas) {
                        $result .= '<a style="color: green ">'.$payment[$datas].'</a> ';
                    }

                    return $result;
                }
            });

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('cashier.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('cashier.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}

}
