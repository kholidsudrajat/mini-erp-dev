<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ExpenseKonveksi;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ExpenseKonveksiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.expense_konveksi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.expense_konveksi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $ExpenseKonveksi = ExpenseKonveksi::create($requestData);

        Session::flash('flash_message', 'ExpenseKonveksi added!');

        return redirect('admin/expense_konveksi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $expense_konveksi = ExpenseKonveksi::findOrFail($id);

        return view('admin.expense_konveksi.show', compact('expense_konveksi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $expense_konveksi = ExpenseKonveksi::findOrFail($id);

        return view('admin.expense_konveksi.edit', compact('expense_konveksi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $requestData = $request->all();

        $expense_konveksi = ExpenseKonveksi::findOrFail($id);
        $expense_konveksi->update($requestData);

        Session::flash('flash_message', 'ExpenseKonveksi updated!');

        return redirect('admin/expense_konveksi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ExpenseKonveksi::destroy($id);

        Session::flash('flash_message', 'ExpenseKonveksi deleted!');

        return redirect('admin/expense_konveksi');
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $expense_konveksi = ExpenseKonveksi::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'name']);

         $datatables = app('datatables')->of($expense_konveksi)
            ->addColumn('action', function ($expense_konveksi) {
                return '<a href="expense_konveksi/'.$expense_konveksi->id.'/edit" class="bb btn btn-primary"><i class="fa fa-pencil"></i></a> 
                <a onclick="return deleteData('.$expense_konveksi->id.')" class="bb btn btn-danger"><i class="fa fa-times"></i></a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('expense_konveksi.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('expense_konveksi.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
}
