<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Retur;
use App\Models;
use App\Shop;
use App\Stock;
use App\Customer;
use App\PettyCash;
use App\Deposit;
use App\Cashier;
use App\CashierDetail;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ReturInvoiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.retur-invoice.index');
    }
    
    public function show($noref)
    {
        $cashier = Cashier::whereNoRef($noref)->first();
        $detail = CashierDetail::whereCashierId($cashier->id)->get();
        
        $idshop = $cashier->shop_id;
        
        $shop = Shop::pluck('name','id');
        $customer = Customer::select([DB::Raw('CONCAT(nama, " - (", customer_code, ")") AS name'), 'id'])
            ->whereId($cashier->customer_id)
            ->first();
        
        $model = Models::join('stocks', 'models.id', '=', 'stocks.model_id')
            ->join('cashier_details', 'models.id', '=', 'cashier_details.model_id')
            ->select([DB::Raw('CONCAT(name, " - (", model_name, ")") AS model_name'), 'models.id'])
            ->whereCashierId($cashier->id)
            ->groupBy('models.id')
            ->get();
        
        return view('admin.retur-invoice.form', compact('noref', 'model', 'shop', 'customer', 'idshop', 'cashier', 'cashier_detail'));
    }
}
