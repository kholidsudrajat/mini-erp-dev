<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\RoleMenu;
use App\RoleUser;
use App\User;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class RoleUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.role-user.index');
    }

    public function tambah(Request $request)
    {
        $data['role'] = $request['role'];
        $data['slug'] = str_slug($request['role'], '-');

        $post = RoleUser::create($data);

        return $post;
    }

    public function select($id)
    {
        $user = User::rightJoin('role_user', 'users.role', '=', 'role_user.id')
            ->where('role_user.id', $id)
            ->first();
        
        $role = RoleMenu::get();

        return view('admin.role-user.select', compact('role', 'user'));
    }

    public function update(Request $request)
    {
        $user = RoleUser::findOrFail($request->id);

        if(isset($request->role_menu)) {
            $role_menu = $request->role_menu;
            $request['role_menu'] = implode(",", $role_menu);
        }

        $requestData = $request->all();

        $user->update($requestData);

        Session::flash('flash_message', 'User updated!');

        return back();
    }

    public function delete($id)
    {
        RoleUser::destroy($id);

        Session::flash('flash_message', 'Cost deleted!');

        return redirect('admin/role-menu');
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $roleuser = RoleUser::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'role']);

        $datatables = app('datatables')->of($roleuser)
            ->addColumn('action', function ($roleuser) {
                return '<a href="role-user/select/'.$roleuser->id.'" class="bb btn btn-primary"><i class="fa fa-pencil"></i></a> 
                <a onclick="return deleteData('.$roleuser->id.')" class="bb btn btn-danger"><i class="fa fa-times"></i></a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('costs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('costs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }

}