<?php

namespace App\Http\Controllers\Admin;

use App\CustomerAddress;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Customer;
use App\Shipping;
use App\Deposit;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.customer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $shipping = Shipping::pluck('name', 'id');
        
        return view('admin.customer.create',compact('shipping'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $count = Customer::max('id');
        $code = $count + 1;
        if(strlen($code)==1)
            $code = "000".$code;
        elseif(strlen($code)==2)
            $code = "00".$code;
        elseif(strlen($code)==3)
            $code = "0".$code;
        elseif(strlen($code)==4)
            $code = $code;
        
        $request['customer_code'] = 'C'.date('ymd').$code;
        $requestData = $request->except(['_token', 'shipping']);

        $customer = Customer::insertGetId($requestData);

        $data['customer_id'] = $customer;
        $data['alamat'] = $request['alamat'];
        $data['shipping_id'] = $request['shipping'];
        
        CustomerAddress::create($data);

        Session::flash('flash_message', 'Mcustomer added!');

        return redirect('admin/customer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $customer = Customer::findOrFail($id);
        
        $deposit = Deposit::whereCustomerId($id)->get();

        return view('admin.customer.show', compact('customer', 'deposit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);

        $shipping = Shipping::pluck('name', 'id')->prepend('Pilih Shipping', '-');

        $address = CustomerAddress::leftJoin('customers', 'customer_address.customer_id', '=', 'customers.id')
            ->leftJoin('shipping', 'shipping.id', '=', 'customer_address.shipping_id')
            ->select([
                'customer_address.id', 'customer_address.alamat', 'name'
            ])
            ->where('customer_address.customer_id', $id)
            ->get();

        return view('admin.customer.edit', compact('customer', 'shipping', 'address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $requestData = $request->all();
        $customer = Customer::findOrFail($id);
        $customer->update($requestData);

        Session::flash('flash_message', 'Mcustomer updated!');

        return redirect('admin/customer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Customer::destroy($id);

        Session::flash('flash_message', 'Mcustomer deleted!');

        return redirect('admin/customer');
    }

    public function sms($id)
    {
        $customer = Customer::findOrFail($id);

        $nexmo = app('Nexmo\Client');
        $nexmo->message()->send([
            'to' => '6281284376223',
            'from' => 'JARANGUDA',
            'text' => "Nama saya $customer->nama",
        ]);

        return back();
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $customer = Customer::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'customer_code', 'nama', 'no_telp', 'limit_debt'])
            ->where('id','!=','0');

         $datatables = app('datatables')->of($customer)
            ->addColumn('action', function ($customer) {
                return '<a href="customer/'.$customer->id.'/edit" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.edit') .'"><i class="fa fa-pencil"></i></a>
                <a onclick="deleteData('.$customer->id.')" class="btn btn-xs btn-danger rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.delete') .'"><i class="fa fa-trash"></i></a>';
                //'<a href="sent/sms/'.$customer->id.'" class="btn btn-xs btn-success rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.send') .'"><i class="fa fa-send"></i></a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('customers.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('customers.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }

    public function tambah(Request $request)
    {
        $data['customer_id'] = $request['customer_id'];
        $data['alamat'] = $request['alamat'];
        $data['shipping_id'] = $request['shipping'];

        $post = CustomerAddress::create($data);

        return $post;
    }

    public function hapus($id)
    {
        CustomerAddress::destroy($id);

        Session::flash('flash_message', 'Cost deleted!');

        return redirect('admin/scheduler');

    }
}
