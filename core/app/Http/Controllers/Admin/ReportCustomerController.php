<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Cashier;
use App\CashierDetail;
use App\Shop;
use App\Models;
use App\Payment;
use App\Debt;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ReportCustomerController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index() {
		$cus = Customer::count();
		$report = Customer::leftJoin('v_sale', 'v_sale.customer_id', '=', 'customers.id')
				->select([DB::Raw('SUM(subtotal) as sale'), DB::Raw('SUM(debt) as debt')])
				->first();
		
		return view('admin.report.customer.index', compact('report', 'cus'));
	}

	public function show($id, Request $req) {
		$limit = 5;
		$customer = Customer::leftJoin('v_report_cus', 'v_report_cus.customer_id', '=', 'customers.id')
				->select([
					DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'customers.id', 'customer_code', 'nama', 'no_telp', 'alamat', 'limit_debt', 'sales', 'debt'
				])
				->where('customers.id', $id)
				->first();

		$report = DB::table('v_sale')
            ->select(['id', 'no_ref', 'subtotal', DB::Raw('SUM(pays) AS pay'), DB::Raw('SUM(debt) AS debt'), 'created_at'])
            ->where('customer_id', $id);

		$total = $report->count();
        
		$report = $report->limit($limit)
            ->groupBy('no_ref')
            ->orderBy('created_at', 'desc');
		$report = $report->get();
        
		$mod = $total % $limit;
		$pagin = intval($total / $limit);
		if ($mod > 0)
			$pagin = $pagin + 1;

		if ($pagin == 1)
			$pagin = 0;

		return view('admin.report.customer.show', compact('customer', 'report', 'pagin', 'total'));
	}

	public function shows($id, $idm) {
		$list_penjualan = Cashier::join('customers', 'customers.id', '=', 'customer_id')->select(['cashier.id', 'cashier.created_at', 'no_ref', 'nama', 'total', 'subtotal', 'discount'])->where('cashier.id', $idm)->first();

		$detail = CashierDetail::join('models', 'models.id', '=', 'model_id')->select('cashier_details.id', 'model_name', 'name', 'cashier_details.created_at', 'qty', 'price')->whereCashierId($idm)->get();

        $pay = Payment::leftjoin('payment_method', 'pm_id', '=', 'payment_method.id')->select('payments.created_at', 'name', 'pay')->whereNoRef($list_penjualan->no_ref)->get();
        
        $debt = Debt::whereNoRef($list_penjualan->no_ref)->first();
        
		return view('admin.report.customer.shows', compact('list_penjualan', 'detail', 'id', 'pay', 'debt'));
	}

	public function filter($id, Request $req) {
		$limit = $req['limit'];
        $page = 1;
        if($req['page'] > 0)
            $page = ($req['page'] - 1) * $limit;
        
		$table = "";

		$report = DB::table('v_sale')
            ->select(['id', 'no_ref', 'subtotal', DB::Raw('SUM(pays) AS pay'), DB::Raw('SUM(debt) AS debt'), 'created_at'])
            ->where('customer_id', $id)
            ->limit($limit)
            ->offset($page);

		if ($req['range'] != "") {
			$rang = explode(":", $req['range']);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$report->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$report->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		if ($req['sort'] != "") {
			if ($req['asc'] == 1)
				$report->orderBy($req['sort'], 'DESC');
			else
				$report->orderBy($req['sort'], 'ASC');
		}else {
			$report->orderBy('no_ref', 'ASC');
		}

		$report = $report->groupBy('no_ref')->get();
        
		$i = $page + 1;
		$total = 0;
		$pay = 0;
		$debt = 0;
		foreach ($report as $rpt) {
			$table .= '
                <tr><td>' . $i++ . '</td>
                    <td><a href="' . url("") . '/admin/report/sales_customer/' . $id . '/' . $rpt->id . '/show">' . $rpt->no_ref . '</a></td>
                    <td>' . $rpt->created_at . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt->subtotal)) . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt->pay)) . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt->debt)) . '</td>
                </tr>
                ';

			$total += $rpt->subtotal;
			$pay += $rpt->pay;
			$debt += $rpt->debt;
		}

		$table .= ' 
            <tr>
                <td colspan="3"><b class="pull-right">Total</b></td>
                <td><b>' . str_replace(',', '.', number_format($total)) . '</b></td>
                <td><b>' . str_replace(',', '.', number_format($pay)) . '</b></td>
                <td><b>' . str_replace(',', '.', number_format($debt)) . '</b></td>
            </tr>
            ';

		$data[0] = $table;
		$data[1] = count($report);

		return $data;
	}

	public function anyData(Request $request) {
		DB::statement(DB::raw('set @rownum=0'));
		$report = Customer::leftJoin('v_report_cus', 'v_report_cus.customer_id', '=', 'customers.id')
            ->select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'customer_code', 'nama', 'no_telp', 'alamat', 'limit_debt', 'sales', 'debt', 'discount'
            ]);

		$datatables = app('datatables')->of($report)
				->addColumn('action', function ($report) {
					return '<a href="sales_customer/' . $report->id . '" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="' . trans('systems.edit') . '">Lihat Detail</a>';
				})
				->editColumn('limit_debt', function($report) {
					return str_replace(',', '.', number_format($report->limit_debt));
				})
				->editColumn('sales', function($report) {
					return str_replace(',', '.', number_format($report->sales));
				})
				->editColumn('discount', function($report) {
					return str_replace(',', '.', number_format($report->discount));
				})
				->editColumn('debt', function($report) {
                    return str_replace(',', '.', number_format($report->debt));
                });

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		return $datatables->make(true);
	}

}
