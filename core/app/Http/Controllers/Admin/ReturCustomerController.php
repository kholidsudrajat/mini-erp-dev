<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Cashier;
use App\CashierDetail;
use App\Shop;
use App\Retur;
use App\Models;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ReturCustomerController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//$this->middleware('admin');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index() {
		return view('admin.report.retur-customer.index');
	}

	public function show($id, Request $req) {
		$limit = 5;
		$customer = Customer::findOrFail($id);
		$report = Retur::join('models', 'models.id', '=', 'model_id')
                ->join('customers', 'customer_id', '=', 'customers.id')
				->select([
					'models.id', DB::raw('CONCAT(name, " (", model_name,")") AS names'), 'qty', 'no_ref', 'no_retur', 'type_retur', 'returs.note',
                    DB::Raw('qty * total_cost AS cost'), DB::Raw('qty * price AS price'), DB::raw('CONCAT(nama, " (", customer_code,")") AS cus'),
					DB::Raw('qty * diff AS diff'), 'returs.created_at', 'customer_code'])
				->whereCustomerId($id);

		$total = count($report->get());

		$report = $report->limit($limit)->orderBy('created_at', 'desc');
		$report = $report->get();

		$mod = $total % $limit;
		$pagin = intval($total / $limit);
		if ($mod > 0)
			$pagin = $pagin + 1;

		if ($pagin == 1)
			$pagin = 0;

		return view('admin.report.retur-customer.show', compact('customer', 'report', 'pagin', 'total'));
	}

	public function shows($id, $noret) {
		$data = Retur::join('models', 'models.id', '=', 'model_id')
            ->join('customers', 'customers.id', '=', 'customer_id')
            ->join('shops', 'shops.id', '=', 'shop_id')
            ->select(['returs.id', 'no_retur', 'no_ref', DB::Raw('CONCAT(customers.nama, " (", customer_code, ")") AS cus'), 'type_retur', 'qty', 'returs.note',
                      DB::Raw('CONCAT(models.name, " (", model_name, ")") AS modelname'), DB::Raw('shops.name AS shopname'), DB::Raw('price * qty AS cost')])
            ->whereNoRetur($noret)
            ->first();

		return view('admin.report.retur-customer.shows', compact('data', 'id'));
	}

	public function filter($id, Request $req) {
		$limit = $req['limit'];
		$page = ($req['page'] - 1) * $limit;

		$table = "";

		$report = Retur::join('models', 'models.id', '=', 'model_id')
                ->join('customers', 'customer_id', '=', 'customers.id')
				->select([
					'models.id', DB::raw('CONCAT(name, " (", model_name,")") AS names'), 'qty', 'no_ref', 'no_retur', 'type_retur', 'returs.note',
                    DB::Raw('qty * total_cost AS cost'), DB::Raw('qty * price AS price'), DB::raw('CONCAT(nama, " (", customer_code,")") AS cus'),
					DB::Raw('qty * diff AS diff'), 'returs.created_at'])
				->whereCustomerId($id)
				->limit($limit)
				->offset($page);

		if ($req['range'] != "") {
			$rang = explode(":", $req['range']);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$report->whereBetween('returs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$report->whereBetween('returs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		if ($req['sort'] != "") {
			if ($req['asc'] == 1)
				$report->orderBy($req['sort'], 'DESC');
			else
				$report->orderBy($req['sort'], 'ASC');
		}else {
			$report->orderBy('no_ref', 'ASC');
		}

		$report = $report->get();

		$i = $page + 1;
		foreach ($report as $rpt) {
			$table .= '
                <tr>
                    <td>'.$i++.'</td>
                    <td>'.$rpt['created_at'].'</td>
                    <td><a href="'.url("").'/admin/report/retur-customer/'.$id.'/'.$rpt['no_retur'].'">'.$rpt['no_retur'].'</a></td>
                    <td>'.$rpt['no_ref'].'</td>
                    <td>'.$rpt['cus'].'</td>
                    <td>'.$rpt['type_retur'].'</td>
                    <td>'.$rpt['names'].'</td>
                    <td>'.$rpt['qty'].'</td>
                    <td>'.$rpt['note'].'</td>
                </tr>
                ';
		}

		$data[0] = $table;
		$data[1] = count($report);

		return $data;
	}

	public function anyData(Request $request) {
		$report = Retur::join('customers', 'customers.id', '=', 'customer_id')
            ->select(['customers.id', 'customer_code', 'nama', DB::Raw('count(no_retur) AS total')])
            ->groupBy('customer_id');

		$datatables = app('datatables')->of($report)
				->addColumn('action', function ($report) {
					return '<a href="retur-customer/' . $report->id . '" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="' . trans('systems.edit') . '">Lihat Detail</a>';
				});

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('returs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('returs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}

}
