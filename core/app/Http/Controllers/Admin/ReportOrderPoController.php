<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDetail;
use App\OrderPayment;
use App\SupplierOrder;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ReportOrderPoController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index() {
        return view('admin.report.order-po.index');
	}
    
	public function show($id) {
        $order = Order::findOrFail($id);
        $supplier = SupplierOrder::findOrFail($order->supplier_id);
        $payment = OrderPayment::whereOrderId($id)->get();
        $bayar = OrderPayment::select([DB::Raw('SUM(total) AS total')])->whereOrderId($id)->first();
        $totalnya = $order->total - (0 + $bayar->total);
        
        $detail = OrderDetail::join('models', 'models.id', '=', 'model_id')
            ->select(['model_id', 'name', 'model_name', 'qty', 'order_details.price', 'total'])
            ->whereOrderId($id)
            ->get();
        
        return view('admin.report.order-po.show', compact('supplier', 'order', 'detail', 'payment', 'totalnya'));
	}

    public function shows($id, $idm){
        $supplier = SupplierOrder::findOrFail($id);
        $order = Order::findOrFail($idm);
        $payment = OrderPayment::whereOrderId($idm)->get();
        $bayar = OrderPayment::select([DB::Raw('SUM(total) AS total')])->whereOrderId($idm)->first();
        $totalnya = $order->total - (0 + $bayar->total);
        
        $detail = OrderDetail::join('models', 'models.id', '=', 'model_id')
            ->select(['model_id', 'name', 'model_name', 'qty', 'order_details.price', 'total'])
            ->whereOrderId($idm)
            ->get();
        
        return view('admin.report.order-po.shows', compact('supplier', 'order', 'detail', 'payment', 'totalnya'));
    }
    
	public function anyData(Request $request) {
        $order = Order::leftjoin('supplier_orders', 'supplier_orders.id', 'supplier_id')
            ->leftjoin('payment_method', 'payment_method.id', '=', 'payment_type')
            ->select([
                'orders.id', 'po_number', 'supplier_orders.nama AS supp', 'payment_method.name AS paym', 'payment_type', 'shipping_type', 'total'
            ]);

         $datatables = app('datatables')->of($order)
            ->addColumn('action', function ($order) {
                return '<a href="order-po/'.$order->id.'" class="bb btn btn-primary">Detail</a>';
            })
            ->editColumn('total', function ($order){
                return str_replace(',','.',number_format($order->total));
            });
        
		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}
        
        if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}

}
