<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Stock;
use App\Shop;
use Session;
use Datatables;
use DB;
use Auth;

class NoticeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function daily(Request $req)
    {
        if(isset($req['current']))
            Session::put('currentss', $req['current']);

        if(Session::get('currentss') == ''){
            $current = 1;
        }else{
            $current = Session::get('currentss');
        }
        
        ///////////omzet
        $omzet = DB::table('v_profit')->select([DB::Raw('SUM(pendapatan) AS pendapatan')])->first();
        $omzet = $omzet->pendapatan;
        $debt = DB::table('v_debt')->select([DB::Raw('SUM(debt) AS debt')])->first();
        $debt = $debt->debt;
        $cash = $omzet + $debt;
        
        /////////best seller
        $prods = DB::table('config')->whereType('product_best_seller')->first();
        $prod = $prods->value;
        $product = DB::Table('v_product')
            ->select(['names', DB::Raw('SUM(tqty) AS qty'), DB::Raw('SUM(tharga) AS harga')])
            ->groupBy('id')
            ->orderBy('qty','DESC')
            ->limit($prod)
            ->get();
        
        ////////low stock
        $mins = DB::table('config')->whereType('low_stock')->first();
        $min = $mins->value;
        $low = Stock::join('shops', 'shops.id', '=', 'shop_id')
            ->join('models', 'models.id', '=', 'model_id')
            ->select([DB::Raw('concat(models.name," (",models.model_name,")") AS names'), 'model_id', 'shop_id', 'shops.name AS shop_name', 'models.name AS model_name', 'qty'])
            ->whereRaw('qty < '.$min)
            ->orderBy('models.id')
            ->get();
        
        ////////high stock
        $maks = DB::table('config')->whereType('high_stock')->first();
        $max = $maks->value;
        $high = Stock::join('shops', 'shops.id', '=', 'shop_id')
            ->join('models', 'models.id', '=', 'model_id')
            ->select([DB::Raw('concat(models.name," (",models.model_name,")") AS names'), 'model_id', 'shop_id', 'shops.name AS shop_name', 'models.name AS model_name', 'qty'])
            ->whereRaw('qty > '.$max)
            ->orderBy('models.id')
            ->get();
        
        return view('admin.notice-daily', compact('omzet', 'debt', 'cash', 'product', 'low', 'high', 'current', 'prod', 'min', 'max'));
    }
    
    public function monthly(Request $req)
    {
        if(isset($req['current']))
            Session::put('currents', $req['current']);

        if(Session::get('currents') == ''){
            $current = 1;
        }else{
            $current = Session::get('currents');
        }
        
        ///////////omzet
        $omzet = DB::table('v_profit')->select([DB::Raw('SUM(pendapatan) AS pendapatan')])->first();
        $omzet = $omzet->pendapatan;
        $debt = DB::table('v_debt')->select([DB::Raw('SUM(debt) AS debt')])->first();
        $debt = $debt->debt;
        $cash = $omzet + $debt;
        
        /////////best seller
        $prods = DB::table('config')->whereType('product_best_seller')->first();
        $prod = $prods->value;
        $product = DB::Table('v_product')
            ->select(['names', DB::Raw('SUM(tqty) AS qty'), DB::Raw('SUM(tharga) AS harga')])
            ->groupBy('id')
            ->orderBy('qty','DESC')
            ->limit($prod)
            ->get();
        
        ////////rugi laba
        $mulai = $req['mulai'];
        $sampai = $req['sampai'];
        
        $profit = DB::Table('v_profit')
            ->select([
                DB::Raw('SUM(pendapatan) AS pendapatan'), DB::Raw('SUM(modal) AS modal'), DB::Raw('SUM(profit) AS profit'), 
                'name', 'shop_id'
            ]);
        
        if($mulai != '' && $sampai != '')
            $profit = $profit->whereRaw('tanggal BETWEEN "'.$mulai.'" AND "'.$sampai.'"');
        
        $profit = $profit->groupBy('shop_id')->get();
        
        $loss = DB::Table('v_loss')
            ->select([
                DB::Raw('SUM(pengeluaran) AS pengeluaran'), 'name', 'date', 'shop_id'
            ]);
        
        if($mulai != '' && $sampai != '')
            $loss = $loss->whereRaw('date BETWEEN "'.$mulai.'" AND "'.$sampai.'"');
        
        $loss = $loss->groupBy('shop_id')->get();
        
        $shop = Shop::whereIsShop('1')->get();
        
        return view('admin.notice-monthly', compact('omzet', 'debt', 'cash', 'product', 'current', 'prod', 'profit', 'loss', 'shop'));
    }
    
    public function config(Request $req)
    {
        $type = $req['type'];
        $value = $req['value'];
        
        $config = DB::table('config')
            ->whereType($type)
            ->update(['value' => $value]);
        
        return 1;
    }
}