<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cashier;
use App\CashierDetail;
use App\Shop;
use App\Models;
use App\StockMovement;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ReportStockController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index() {
		$prod = Models::count();

		$report = StockMovement::select([
					DB::Raw('IFNULL(SUM(stock_in),0) AS tmasuk'), DB::Raw('IFNULL(SUM(stock_out),0) AS tkeluar')
				])
				->first();

		return view('admin.report.stock.index', compact('prod', 'report'));
	}

	public function show($id, Request $req) {
		$limit = 5;
		$model = Models::findOrFail($id);

		$stock = DB::table('stock_by_shop')->select([DB::Raw('SUM(qtys) AS qtys'), DB::Raw('SUM(qtyw) AS qtyw')])->whereModelId($id)->first();

		$report = StockMovement::select(['type', 'transfer_from', 'transfer_to', DB::Raw('IFNULL(stock_in,0) AS stock_in'), DB::Raw('IFNULL(stock_out,0) AS stock_out'), DB::Raw('IFNULL(stock_shop,0) AS stock_shop'), DB::Raw('IFNULL(stock_warehouse,0) AS stock_warehouse'), DB::Raw('IFNULL(stock_adjustment,0) AS stock_adjustment'), DB::Raw('IFNULL(stock_retur,0) AS stock_retur'), 'created_at'])->whereModelId($id);

		$total = count($report->get());

		$report = $report->limit($limit);
		$report = $report->orderBy('created_at','DESC');
		$report = $report->get();

		$mod = $total % $limit;
		$pagin = intval($total / $limit);
		if ($mod > 0)
			$pagin = $pagin + 1;

		if ($pagin == 1)
			$pagin = 0;

		$shop = Shop::pluck('name', 'id');

		return view('admin.report.stock.show', compact('model', 'report', 'stock', 'shop', 'pagin', 'total'));
	}

	public function filter($id, Request $req) {
		$limit = $req['limit'];
		$page = ($req['page'] - 1) * $limit;

		$table = "";

		$report = StockMovement::select(['type', 'transfer_from', 'transfer_to', DB::Raw('IFNULL(stock_in,0) AS stock_in'),
					DB::Raw('IFNULL(stock_out,0) AS stock_out'), DB::Raw('IFNULL(stock_shop,0) AS stock_shop'),
					DB::Raw('IFNULL(stock_warehouse,0) AS stock_warehouse'), DB::Raw('IFNULL(stock_adjustment,0) AS stock_adjustment'),
                    DB::Raw('IFNULL(stock_retur,0) AS stock_retur'), 'created_at'])
				->whereModelId($id)
				->limit($limit)
				->offset($page);

		$shop = Shop::pluck('name', 'id');

		if ($req['range'] != "") {
			$rang = explode(":", $req['range']);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$report->whereBetween('stock_movement.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$report->whereBetween('stock_movement.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		if ($req['type'] != "") {
			if ($req['type'] != "semua")
				$report->whereType($req['type']);
		}

		if ($req['sort'] != "") {
			if ($req['asc'] == 1)
				$report->orderBy($req['sort'], 'DESC');
			else
				$report->orderBy($req['sort'], 'ASC');
		}else {
			$report->orderBy('created_at', 'ASC');
		}

		$report = $report->get();

		$i = $page + 1;
		foreach ($report as $rpt) {
			$to = $rpt['transfer_to'];
			$from = $rpt['transfer_from'];

			$table .= '
                <tr>
                    <td>' . $i++ . '</td>
                    <td>' . $rpt['created_at'] . '</td>';

			if ($from != 0 )
				$table .= '<td>' . $shop[$from] . '</td>';
			elseif($rpt['type'] == "adjustment")
				$table .= '<td> Penyesuaian Stok </td>';
            else
				$table .= '<td> Supplier / Produksi </td>';

			if ($to != 0)
				$table .= '<td>' . $shop[$to] . '</td>';
			else
				$table .= '<td> Penjualan </td>';

			$table .= '<td>';
			if ($rpt['type'] == "in")
				$table .= $rpt['stock_in'];
			elseif ($rpt['type'] == "out")
				$table .= $rpt['stock_out'];
			elseif ($rpt['type'] == "toshop")
				$table .= $rpt['stock_shop'];
			elseif ($rpt['type'] == "towarehouse")
				$table .= $rpt['stock_warehouse'];
			elseif ($rpt['type'] == "adjustment")
				$table .= $rpt['stock_adjustment'];
			elseif ($rpt['type'] == "retur")
				$table .= $rpt['stock_retur'];
			$table .= '</td>';

			$table .= '<td>';
			if ($rpt['type'] == "in")
				$table .= 'Barang Masuk';
			elseif ($rpt['type'] == "out")
				$table .= 'Barang Keluar (Dibeli)';
			elseif ($rpt['type'] == "toshop")
				$table .= 'Barang Masuk ke Toko';
			elseif ($rpt['type'] == "towarehouse")
				$table .= 'Barang Masuk ke Gudang';
			elseif ($rpt['type'] == "adjustment")
				$table .= 'Penyesuaian Stok';
			elseif ($rpt['type'] == "retur")
				$table .= 'Retur';
			$table .= '</td>';

			$table .= '</tr>';
		}

		$data[0] = $table;
		$data[1] = count($report);

		return $data;
	}

	public function anyData(Request $request) {
		DB::statement(DB::raw('set @rownum=0'));
		$report = StockMovement::select(['name', 'model_id', DB::Raw('IFNULL(SUM(stock_in),0) AS stock_in'),
					DB::Raw('IFNULL(SUM(stock_out),0) AS stock_out')])
				->groupBy('model_id');

		$datatables = app('datatables')->of($report)
				->addColumn('action', function ($report) {
			return '<a href="stock_movement/' . $report->model_id . '" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="' . trans('systems.edit') . '">Lihat Detail</a>';
		});

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('stock_movement.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('stock_movement.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}

}
