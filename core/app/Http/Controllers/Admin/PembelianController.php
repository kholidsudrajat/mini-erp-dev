<?php

namespace App\Http\Controllers\Admin;

use App\Cost;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ModelCost;
use App\Order;
use App\OrderDetail;
use App\SupplierOrder;
use App\Shipping;
use App\Models;
use App\PaymentMethod;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class PembelianController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function form()
    {
        $supplier = SupplierOrder::pluck('nama', 'id');
        $model = Models::select(['id', DB::Raw('CONCAT(name, " - (", model_name, ")") AS name')])
            ->pluck('name','id')
            ->prepend('Pilih Model', '-');
        
        $shipping = Shipping::get();
        $payment = PaymentMethod::get();
        $cost = Cost::get();
        
        return view('admin.pembelian.form', compact('supplier', 'model', 'shipping', 'payment', 'cost'));
    }
    
    public function submit(Request $req)
    {
        $cek = Order::wherePoNumber($req['po_number'])->count();
        
        if($cek != 0)
            return 0;
        
        $data['po_number'] = $req['po_number'];
        $data['supplier_id'] = $req['supplier_id'];
        $data['shipping_type'] = $req['shipping_type'];
        $data['shipping_id'] = $req['shipping_id'];
        $data['payment_type'] = $req['payment_type'];
        $data['total'] = $req['total'];
        $data['dates'] = date('Y-m-d H:i:s');
        
        $id = Order::insertGetId($data);
        
        $json = '['.substr($req['json'],0,-1).']';
        $json = json_decode($json);
        foreach($json as $js){
            $datas['order_id'] = $id;
            $datas['model_id'] = $js->model_id;
            $datas['qty'] = $js->qty;
            $datas['price'] = $js->price;
            $datas['total'] = $js->total;
            
            OrderDetail::create($datas);
        }
        return 1;
    }

    public function tambah(Request $request)
    {
        $requestData = $request->all();

        $Supplier = SupplierOrder::create($requestData);

        return $Supplier;
    }

    public function tambah_produk(Request $request)
    {
        $this->validate($request, [
            'model_name'    =>  'required|max:255|unique:models|alpha_dash'
        ]);

        $request['image'] = '';
        $files = $request['images'];
        if($files!=""){
            $path = 'files/models/';
            $name = rand(10000,99999).'.'.$files->getClientOriginalExtension();
            $files->move($path,$name);
            $request['image'] = $name;
        }

        $requestData = array(
            'name'          =>  $request['name'],
            'model_name'    =>  $request['model_name'],
            'image'         =>  $request['image'],
            'category'      =>  $request['category'],
            'info'          =>  $request['info'],
            'total_cost'    =>  str_replace(',', '', $request['total_cost']),
            'price'         =>  str_replace(',', '', $request['price']),
            'diff'          =>  str_replace(',', '', $request['diff']),
            'created_at'    =>  date('Y-m-d H:i:s'),
            'updated_at'    =>  date('Y-m-d H:i:s'),
        );

        $id = Models::insertGetId($requestData);

        $cost = Cost::get();
        foreach($cost as $costs){
            $c = $costs['id'];
            $data['model_id'] = $id;
            $data['cost_id'] = $costs['id'];
            $data['costs'] = str_replace(',', '', $request[$c]);

            if($request[$c] > 0)
                ModelCost::create($data);
        }

        return $requestData;
    }
    
    public function getmodel(Request $req)
    {
        $model = Models::findOrFail($req['model']);
        
        return $model->price;
    }
}