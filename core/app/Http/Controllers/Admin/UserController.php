<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\RoleUser;
use App\User;
use App\Shop;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $role = RoleUser::all();
        
        return view('admin.user.index', compact('role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $shop = Shop::whereIsShop('1')->pluck('name', 'id');

        $role_user = RoleUser::pluck('role', 'id');
        
        return view('admin.user.create', compact('shop', 'role_user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name' => 'required|max:255',
          'email' => 'required|email|max:255|unique:users',
          'username' => 'required|max:255|unique:users|alpha_dash',
          'password' => 'required|min:6',
        ]);
        $request["password"] = bcrypt($request->password);
        $request["role"] = $request->role;
        $requestData = $request->all();

        $user = User::create($requestData);

        Session::flash('flash_message', 'User added!');

        return redirect('admin/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $shop = Shop::whereIsShop('1')->pluck('name', 'id');

        return view('admin.user.show', compact('user', 'shop'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $shop = Shop::whereIsShop('1')->pluck('name', 'id');
        $role_user = RoleUser::pluck('role', 'id');

        return view('admin.user.edit', compact('user', 'role_user', 'shop', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);
        $request["password"] = ($request->password == "") ? $user['password'] : bcrypt($request->password);

        $files = $request['photos'];
        if($files!=""){
            if(!is_dir('files/profile/'))
                mkdir('files/profile/', 0777, true);
            
            $path = 'files/profile/';
            $name = rand(10000,99999).'.'.$files->getClientOriginalExtension();
            $files->move($path,$name);
            $request['photo'] = $name;
        }
        
        $this->validate($request, [
                'name' => 'required|max:255'
            ]);

        if($request['email'] != $user['email'])
        {
             $this->validate($request, [
                'email' => 'required|email|max:255|unique:users'
            ]);
        }

        if($request['username'] != $user['username'])
        {
             $this->validate($request, [
                'username' => 'required|max:255|unique:users|alpha_dash'
            ]);
        }
        $requestData = $request->all();

        $user->update($requestData);

        Session::flash('flash_message', 'User updated!');

        return redirect('admin/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        User::destroy($id);

        Session::flash('flash_message', 'User deleted!');

        return redirect('admin/user');
    }

    public function anyData(Request $request)
    {
        $id_user = Auth::user()->id;
        DB::statement(DB::raw('set @rownum=0'));
        $user = User::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'users.id', 'users.name',
            'email', 'username', 'photo'])->where('id','!=',$id_user);

         $datatables = app('datatables')->of($user)
             ->editColumn('photo', function($user){
                 if($user->photo != '')
                     return '<img src="'.url('files/profile').'/'.$user->photo.'" style="max-width:100px;" />';
                 else
                     return '<img src="'.url('img/profiles').'/avatar.jpg" style="max-width:100px;" />';
             })
            ->addColumn('action', function ($user) {
                return '<a href="user/'.$user->id.'/edit" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.edit') .'"><i class="fa fa-pencil"></i></a> <a onclick="deleteData(\'user\', '.$user->id.')" class="btn btn-xs btn-danger rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.delete') .'"><i class="fa fa-trash"></i></a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('users.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('users.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
}
