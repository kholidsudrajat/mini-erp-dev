<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Salesman;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class SalesmanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.salesman.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.salesman.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $salesman = Salesman::create($requestData);

        Session::flash('flash_message', 'Salesman added!');

        return redirect('admin/salesman');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $salesman = Salesman::findOrFail($id);

        return view('admin.salesman.show', compact('salesman'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $salesman = Salesman::findOrFail($id);

        return view('admin.salesman.edit', compact('salesman'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $requestData = $request->all();

        $salesman = Salesman::findOrFail($id);
        $salesman->update($requestData);

        Session::flash('flash_message', 'Salesman updated!');

        return redirect('admin/salesman');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Salesman::destroy($id);

        Session::flash('flash_message', 'Salesman deleted!');

        return redirect('admin/salesman');
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $salesman = Salesman::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'name', 'telp']);

         $datatables = app('datatables')->of($salesman)
            ->addColumn('action', function ($salesman) {
                return '<a href="salesman/'.$salesman->id.'/edit" class="bb btn btn-primary"><i class="fa fa-pencil"></i></a> 
                <a onclick="return deleteData('.$salesman->id.')" class="bb btn btn-danger"><i class="fa fa-times"></i></a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('salesmans.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('salesmans.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
}
