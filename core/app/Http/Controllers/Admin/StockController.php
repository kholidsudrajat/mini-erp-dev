<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Stock;
use App\StockLog;
use App\Shop;
use App\Models;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class StockController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.stock.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $req)
    {
        $mods = '';
        if($req['model'] != '')
            $mods = $req['model'];
        
        $shop = Shop::pluck('name','id')->prepend('Pilih Toko / Gudang', 0);
        $model = Models::join('stocks', 'model_id', '=', 'models.id')
            ->select([
                DB::Raw('CONCAT(models.name, " - ", models.category, " (Stock : ", SUM(qty), ")") AS names'), 'models.id'
            ])
            ->groupBy('models.id')
            ->get();
        
        return view('admin.stock.create', compact('shop', 'model', 'mods'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $id = $request['model_id'];
        $json = '['.substr($request['sjson'],0,-1).']';
        $json = json_decode($json);
        
        foreach($json as $index){
            foreach($index as $in => $val){
                $datas['model_id'] = $id;
                $datas['shop_id'] = $in;
                $datas['qty'] = $val;

                $cek = Stock::whereModelId($id)->whereShopId($in);
                if($cek->count() > 0){
                    $olddata = $cek->first();
                    $oldqty = $olddata['qty'];
                    $newqty = $oldqty + $val;

                    $cek->update(['qty' => $newqty]);
                }else
                    $stock = Stock::create($datas);

                $data = array(
                    'model_id'      =>  $id,
                    'qty'           =>  $val,
                    'transfer_to'   =>  $in,
                    'type'          =>  'in',
                );

                $this->createlog($data);
            }            
        }
        
        return redirect('admin/stock');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $stock = Stock::findOrFail($id);

        return view('admin.stock.show', compact('stock'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $stock = Stock::findOrFail($id);

        return view('admin.stock.edit', compact('stock'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $requestData = $request->all();

        $stock = Stock::findOrFail($id);
        $stock->update($requestData);

        Session::flash('flash_message', 'Mstock updated!');

        return redirect('admin/stock');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Stock::destroy($id);

        Session::flash('flash_message', 'Mstock deleted!');

        return redirect('admin/stock');
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $stock = DB::table('v_stock')
            ->select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'shop_name', 'models.id AS mod', 'v_stock.model_name','models_name', 'v_stock.id', 'qty',
                'models.category'
            ])
            ->leftJoin('models', 'models.name', '=', 'v_stock.model_name');

         $datatables = app('datatables')->of($stock)
             ->editColumn('model_name', function($stock){
                 return '<a href="stock/create?model='.$stock->mod.'">'.$stock->model_name.'</a>';
             });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('stocks.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('stocks.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
    
    public function getstock(Request $req)
    {
        $stock = Stock::select([
            DB::Raw('SUM(qty) AS qty')
        ])
            ->whereModelId($req['model'])
            ->first();
        
        return $stock['qty'];
    }
    
    public function createlog($data)
    {
        StockLog::create($data);
        
        return 1;
    }
}
