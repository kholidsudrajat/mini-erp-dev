<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Order;
use App\OrderDetail;
use App\OrderPayment;
use App\OrderDelivery;
use App\OrderDeliveryDetail;
use App\OrderRetur;
use App\SupplierOrder;
use App\Shipping;
use App\Models;
use App\Shop;
use App\Stock;
use App\StockLog;
use App\PaymentMethod;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class OrderDeliveryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $shop = Shop::pluck('name', 'id');
        $default = Shop::whereDefault('1')->first();
        $default = $default->id;
        
        $shops = Shop::first();
        if(!isset($default->id))
            $default = $shops->id;
        
        return view('admin.pembelian.delivery.index', compact('shop', 'default'));
    }
    
    public function add($id)
    {
        $order = Order::findOrFail($id);
        $supplier = SupplierOrder::pluck('nama', 'id')->prepend('Pilih Supplier',0);
        
        $sisa = array();
        $data1 = Order::join('order_details', 'orders.id', '=', 'order_id')
            ->where('orders.id',$id)
            ->get();
        $data2 = OrderDelivery::join('order_delivery_details', 'order_delivery.id', '=', 'order_delivery_id')
            ->where('order_id',$id)
            ->get();
        
        foreach($data1 as $dat){
            $sisa[$dat->model_id] = $dat->qty;
        }
        foreach($data2 as $dat){
            $sisa[$dat->model_id] -= $dat->qty + $dat->retur;
        }
        
        $detail = OrderDetail::join('models', 'models.id', '=', 'model_id')->whereOrderId($id)->get();
        $shop = Shop::pluck('name','id');
        
        return view('admin.pembelian.delivery.create', compact('id', 'detail', 'shop', 'order', 'supplier', 'sisa'));
    }
    
    public function show($id)
    {        
        $order = Order::findOrFail($id);
        $detail = OrderDelivery::whereOrderId($id)->get();
        
        $supplier = SupplierOrder::pluck('nama', 'id')->prepend('Pilih Supplier',0);
        $model = Models::select(['id', DB::Raw('CONCAT(name, " - (", model_name, ")") AS name')])
            ->pluck('name','id')
            ->prepend('Pilih Produk',0);

        return view('admin.pembelian.delivery.show', compact('order', 'detail', 'supplier', 'model'));
    }
    
    public function detail($id, $oid)
    {    
        $do = OrderDelivery::join('orders', 'orders.id', '=', 'order_id')
            ->select([
                'orders.id', 'supplier_id', 'po_number', 'do_number'
            ])
            ->where(['order_delivery.id' => $oid])
            ->first();
            
        $supplier = SupplierOrder::pluck('nama', 'id')->prepend('Pilih Supplier',0);
        
        $detail = OrderDeliveryDetail::join('models', 'models.id', '=', 'model_id')
            ->join('shops', 'shops.id', '=', 'shop_id')
            ->select([
                DB::Raw('CONCAT(models.name, " - (", model_name, ")") AS produk'), 'qty', DB::Raw('shops.name AS shop'), 'retur'
            ])
            ->whereOrderDeliveryId($oid)
            ->get();
        
        return view('admin.pembelian.delivery.detail', compact('do', 'supplier', 'detail'));
    }
    
    public function submit(Request $req)
    {
        $data['order_id'] = $req['order_id'];
        $data['do_number'] = $req['do_number'];
        
        $id = OrderDelivery::insertGetId($data);
        
        $order_id = $req['order_id'];
        Order::findOrFail($order_id)->update(['status' => '1']);
        
        $json = json_decode('['.substr($req['json'],0,-1).']');
        foreach($json as $js){
            $datas['order_delivery_id'] = $id;
            $datas['model_id'] = $js->model_id;
            $datas['qty'] = $js->qty;
            $datas['retur'] = $js->retur;
            $datas['shop_id'] = $js->shop_id;
            
            OrderDeliveryDetail::create($datas);
            
            if($js->retur > 0){
                $dt['order_delivery_id'] = $id;
                $dt['model_id'] = $js->model_id;
                $dt['qty'] = $js->retur;
                
                OrderRetur::create($dt);
            }
            
            if($js->qty > 0){
                $stock = Stock::whereShopId($js->shop_id)->whereModelId($js->model_id)->first();
                
                if(!isset($stock)){
                    $dd['shop_id'] = $js->shop_id;
                    $dd['model_id'] = $js->model_id;
                    $dd['qty'] = $js->qty;
                    
                    Stock::create($dd);
                }else{
                    $qty = $stock->qty;
                    $qty = $qty + $js->qty;
                    $dd['qty'] = $qty;
                    
                    $stock->update($dd);
                }
                
                $de = array(
                    'model_id'      =>  $js->model_id,
                    'qty'           =>  $js->qty,
                    'transfer_to'   =>  $js->shop_id,
                    'type'          =>  'in',
                );

                $this->createlog($de);
            }
        }
    }
    
    public function takeall($order_id)
    {
        $shop = Shop::whereDefault('1')->first();
        $data['order_id'] = $order_id;
        $data['do_number'] = 'DO-'.date('YmdHis');
        
        $id = OrderDelivery::insertGetId($data);
        
        Order::findOrFail($order_id)->update(['status' => '2']);
        
        $sisa = array();
        $data1 = Order::join('order_details', 'orders.id', '=', 'order_id')
            ->where('orders.id',$order_id)
            ->get();
        $data2 = OrderDelivery::join('order_delivery_details', 'order_delivery.id', '=', 'order_delivery_id')
            ->where('order_id',$order_id)
            ->get();
        
        foreach($data1 as $dat){
            $sisa[$dat->model_id] = $dat->qty;
        }
        foreach($data2 as $dat){
            $sisa[$dat->model_id] -= $dat->qty + $dat->retur;
        }
        
        foreach($sisa as $index => $value){
            $datas['order_delivery_id'] = $id;
            $datas['model_id'] = $index;
            $datas['qty'] = $value;
            $datas['retur'] = 0;
            $datas['shop_id'] = $shop->id;
            
            if($value > 0){
                OrderDeliveryDetail::create($datas);

                $stock = Stock::whereShopId($shop->id)->whereModelId($index)->first();

                if(!isset($stock)){
                    $dd['shop_id'] = $shop->id;
                    $dd['model_id'] = $index;
                    $dd['qty'] = $value;

                    Stock::create($dd);
                }else{
                    $qty = $stock->qty;
                    $qty = $qty + $value;
                    $dd['qty'] = $qty;

                    $stock->update($dd);
                }

                $de = array(
                    'model_id'      =>  $index,
                    'qty'           =>  $value,
                    'transfer_to'   =>  $shop->id,
                    'type'          =>  'in',
                );

                $this->createlog($de);
            }
        }
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $order = Order::leftjoin('supplier_orders', 'supplier_orders.id', 'supplier_id')
            ->leftjoin('shipping', 'shipping.id', '=', 'shipping_id')
            ->leftjoin('payment_method', 'payment_method.id', '=', 'payment_type')
            ->select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'orders.id', 'po_number', 'supplier_orders.nama AS supp', 'shipping.name AS ship', 
                'payment_method.name AS paym', 'payment_type', 'shipping_type', 'total', 'status'
            ]);

         $datatables = app('datatables')->of($order)
            ->addColumn('action', function ($order) {
                if($order->status != '2')
                    return '<a href="delivery/'.$order->id.'" class="bb btn btn-primary">Detail</a><a onclick="takeall('.$order->id.')" class="bb btn btn-success">Terima Barang Penuh</a>';
                else
                    return '<a href="delivery/'.$order->id.'" class="bb btn btn-primary">Detail</a>';
            })
            ->editColumn('total', function ($order){
                return str_replace(',','.',number_format($order->total));
            })
            ->editColumn('ship', function ($order){
                if($order->ship != "")
                    return $order->ship;
                else
                    return $order->shipping_type;
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
    
    public function createlog($data)
    {
        StockLog::create($data);
        
        return 1;
    }
}