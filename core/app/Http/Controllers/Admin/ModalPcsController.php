<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\ModalPcs;
use App\ModalPcsBahan;
use App\ModalPcsModel;
use Session;
use Datatables;
use DB;
use Auth;
use View;

class PotongBahanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::Table('v_temp_potong')->get();
        $model = TempPotongModel::join('models','models.id','=','model_id')
            ->select([
                'temp_potong_model.id', DB::Raw('CONCAT(name, " - (", model_name, ")") AS name'), 'qty', 'info'
            ])
            ->get();
        
        return view('admin.potong-bahan.index', compact('data', 'model'));
    }