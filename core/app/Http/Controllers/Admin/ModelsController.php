<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models;
use App\Cost;
use App\Shop;
use App\Stock;
use App\StockLog;
use App\ModelCost;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ModelsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        DB::statement(DB::raw('set @rownum=0'));
        $model = DB::table('v_model')
            ->select([DB::raw('@rownum  := @rownum + 1 AS rownum'), 'qty', 'id', 'name', 'model_name', 'image', 'info', 
                      'total_cost', 'price', 'diff'])
            ->get();
        
        return view('admin.model.index', compact('model'));
    }

    public function filter(Request $req)
    {   
        $table = "";
        DB::statement(DB::raw('set @rownum=0'));
        $model = DB::table('v_model')
            ->select([DB::raw('@rownum  := @rownum + 1 AS rownum'), 'qty', 'id', 'name', 'model_name', 'image', 'info', 
                      'total_cost', 'price', 'diff']);
        
        if($req['keyword'] != ""){
            $model->where('model_name', '%'.$req['keyword'].'%')->orWhere('name', '%'.$req['keyword'].'%');
        }

        if($req['sort'] != 'default')
            $model->orderBy($req['sort'], $req['asc']);
        else
            $model->orderBy('id', $req['asc']);
        
        $model = $model->get();
        '';
        foreach($model as $mdl){
            $table .= '
            <tr>
            <td>'.$mdl->rownum.'</td>
            <td>
            <a href="'.url('admin/model').'/'.$mdl->id.'/changeimage" id="iframe">
            <img src="'.url('files/models').'/'.$mdl->image.'" style="max-width:150px;" />
            </a>
            </td>
            <td>
            '. $mdl->name .' - ( '. $mdl->model_name .' )
            <br/>
            Harga : '. number_format($mdl->price) .'
            <br/>
            Stok : <a href="'. url('admin/stock') .'?kode='. $mdl->model_name .'">'. intval($mdl->qty) .'</a>
            </td>
            </tr>';
        }
        
        return $table;
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $cost = Cost::get();
        $shop = Shop::pluck('name', 'id');
        
        return view('admin.model.create', compact('cost', 'shop'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {        
        $this->validate($request, [
            'model_name'    =>  'required|max:255|alpha_dash|unique:models,model_name,NULL,id,category,'.$request->category
        ]);
        
        $request['image'] = '';
        $files = $request['images'];
        if($files!=""){
            $path = 'files/models/';
            $name = rand(10000,99999).'.'.$files->getClientOriginalExtension();
            $files->move($path,$name);
            $request['image'] = $name;
        }
        
        $requestData = array(
            'name'          =>  $request['name'],
            'model_name'    =>  $request['model_name'],
            'image'         =>  $request['image'],
            'category'      =>  $request['category'],
            'info'          =>  $request['info'],
            'total_cost'    =>  str_replace(',', '', $request['total_cost']),
            'price'         =>  str_replace(',', '', $request['price']),
            'diff'          =>  str_replace(',', '', $request['diff']),
            'created_at'    =>  date('Y-m-d H:i:s'),
            'updated_at'    =>  date('Y-m-d H:i:s'),
        );

        $id = Models::insertGetId($requestData);

        $cost = Cost::get();
        foreach($cost as $costs){
            $c = $costs['id'];
            $data['model_id'] = $id;
            $data['cost_id'] = $costs['id'];
            $data['costs'] = str_replace(',', '', $request[$c]);
            
            if($request[$c] > 0)
                ModelCost::create($data);
        }
        
        $json = '['.substr($request['sjson'],0,-1).']';
        $json = json_decode($json);
        
        foreach($json as $index){
            foreach($index as $in => $val){
                $datas['model_id'] = $id;
                $datas['shop_id'] = $in;
                $datas['qty'] = $val;

                $cek = Stock::whereModelId($id)->whereShopId($in);
                if($cek->count() > 0){
                    $olddata = $cek->first();
                    $oldqty = $olddata['qty'];
                    $newqty = $oldqty + $val;

                    $cek->update(['qty' => $newqty]);
                }else
                    $stock = Stock::create($datas);

                $data = array(
                    'model_id'      =>  $id,
                    'qty'           =>  $val,
                    'transfer_to'   =>  $in,
                    'type'          =>  'in',
                );

                $this->createlog($data);
            }            
        }

        return redirect('admin/model');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $model = Models::findOrFail($id);

        return view('admin.model.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $cost = Cost::get();
        $modelcost = ModelCost::whereModelId($id)->get();
        $model = Models::findOrFail($id);

        return view('admin.model.edit', compact('model', 'cost', 'modelcost'));
    }
    public function changeimage($id)
    {
        $cost = Cost::get();
        $modelcost = ModelCost::whereModelId($id)->get();
        $model = Models::findOrFail($id);

        return view('admin.model.change', compact('model', 'cost', 'modelcost'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    { 
        $this->validate($request, [
            'model_name'    =>  'required|max:255|alpha_dash|unique:models,model_name,NULL,id,category,'.$request->category
        ]);
        
        $files = $request['images'];
        if($files!=""){
            $path = 'files/models/';
            $name = rand(10000,99999).'.'.$files->getClientOriginalExtension();
            $files->move($path,$name);
            $request['image'] = $name;
        }
        $request['price'] = str_replace(',', '', $request['price']);
        $request['total_cost'] = str_replace(',', '', $request['total_cost']);
        $request['diff'] = str_replace(',', '', $request['diff']);
        $requestData = $request->all();

        $model = Models::findOrFail($id);
        $model->update($requestData);
        
        $cost = Cost::get();
        foreach($cost as $costs){
            $c = $costs['id'];
            $modelcost = ModelCost::whereModelId($id)->whereCostId($c)->first();
            
            $data['model_id'] = $id;
            $data['cost_id'] = $costs['id'];
            $data['costs'] = str_replace(',', '', $request[$c]);
            
            if($modelcost['id'] != ""){
                $mcst = ModelCost::findOrFail($modelcost['id']);
                $mcst->update($data);
            }else
                ModelCost::create($data);
        }

        Session::flash('flash_message', 'Mmodel updated!');

        return redirect('admin/model');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Models::destroy($id);

        Session::flash('flash_message', 'Mmodel deleted!');

        return redirect('admin/model');
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $model = DB::table('v_model')
            ->select([DB::raw('@rownum  := @rownum + 1 AS rownum'), 'qty', 'id', 'name', 'model_name', 'image', 'info', 
                      'total_cost', 'price', 'diff', 'category']);

         $datatables = app('datatables')->of($model)
            ->editColumn('image', function($model){
                if($model->image != "")
                    return '<a href="'.url('admin/model').'/'.$model->id.'/changeimage" id="iframe"><img src="'.url('files/models').'/'.$model->image.'" style="max-width:150px;" /></a>';
                else
                    return '-';
            })
            ->editColumn('price', function($model){
                return str_replace(',','.',number_format($model->price));
            })
            ->editColumn('qty', function($model){
                return '<a href="'.url('admin/stock').'?kode='.$model->model_name.'">'.intval($model->qty).'</a>';
            })
            ->addColumn('action', function ($model) {
                return '<a href="model/'.$model->id.'/edit" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.edit') .'"><i class="fa fa-pencil"></i></a> '
                        . '<a onclick="deleteData('.$model->id.')" class="btn btn-xs btn-danger rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.delete') .'"><i class="fa fa-trash"></i></a> ';
            })
            ->addColumn('DT_RowId', function($model){
                return $model->id;
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('v_model.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('v_model.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
    
    public function createlog($data)
    {
        StockLog::create($data);
        
        return 1;
    }
}
