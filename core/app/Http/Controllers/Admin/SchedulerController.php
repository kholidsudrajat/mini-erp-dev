<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Scheduler;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class SchedulerController extends Controller
{
    public function __construct()
    {
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $email = DB::Table('config')->whereType('email_checklist')->first();
        $email = $email->value;
        $phone = DB::Table('config')->whereType('phone_checklist')->first();
        $phone = $phone->value;
        $emails = DB::Table('config')->whereType('email_cus_checklist')->first();
        $emailcus = $emails->value;
        $phones = DB::Table('config')->whereType('phone_cus_checklist')->first();
        $phonecus = $phones->value;
        $emails = DB::Table('config')->whereType('email_list')->first();
        $emails = $emails->value;
        $phones = DB::Table('config')->whereType('phone_list')->first();
        $phones = $phones->value;
        
        return view('admin.scheduler.index', compact('email', 'phone', 'emailcus', 'phonecus', 'emails', 'phones'));
    }

    public function tambah(Request $request)
    {
        $validators = \Validator::make($request->all(), [
			'tanggal' => 'required|unique:scheduler_notification'
        ]);
        
        if($validators->fails()){
            return 0;
        }else{
            $data['tanggal'] = $request['tanggal'];

            $post = Scheduler::create($data);

            return $post;
        }
    }

    public function delete($id)
    {
        Scheduler::destroy($id);

        Session::flash('flash_message', 'Cost deleted!');

        return redirect('admin/scheduler');
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $scheduler = Scheduler::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'tanggal']);

        $datatables = app('datatables')->of($scheduler)
            ->addColumn('action', function ($scheduler) {
                return '<!--<a href="cost/'.$scheduler->id.'/edit" class="bb btn btn-primary"><i class="fa fa-pencil"></i></a>--> 
                <a onclick="return deleteData('.$scheduler->id.')" class="bb btn btn-danger"><i class="fa fa-times"></i></a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('costs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('costs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }

    public function config(Request $req)
    {
        if($req['email'] != '' && $req['phone']){
            /////email checklist
            $cek1 = DB::Table('config')->whereType('email_checklist');
            $data1 = $cek1->first();
            if($data1->value != $req['email'])
                $cek1->update(['value' => $req['email']]);

            /////phone checklist
            $cek2 = DB::Table('config')->whereType('phone_checklist');
            $data2 = $cek2->first();
            if($data2->value != $req['phone'])
                $cek2->update(['value' => $req['phone']]);

            /////email list
            $cek3 = DB::Table('config')->whereType('email_list');
            $cek3->update(['value' => $req['emails']]);

            /////phone list
            $cek4 = DB::Table('config')->whereType('phone_list');
            $cek4->update(['value' => $req['phones']]);
        }else{
            $cek5 = DB::Table('config')->whereType('phone_cus_checklist');
            $cek5->update(['value' => $req['phonecus']]);
            
            $cek6 = DB::Table('config')->whereType('email_cus_checklist');
            $cek6->update(['value' => $req['emailcus']]);
        }
        
        return 1;
    }
    
}
