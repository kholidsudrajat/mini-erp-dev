<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDelivery;
use App\OrderDeliveryDetail;
use App\OrderPayment;
use App\SupplierOrder;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ReportOrderDeliveryController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index() {
        return view('admin.report.order-delivery.index');
	}
    
	public function show($id) {
        $order = OrderDelivery::join('orders', 'orders.id', '=', 'order_id')
            ->where('order_delivery.id',$id)
            ->first();
        
        $supplier = SupplierOrder::findOrFail($order->supplier_id);
        
        $do = OrderDelivery::join('orders', 'orders.id', '=', 'order_id')
            ->select([
                'orders.id', 'supplier_id', 'po_number', 'do_number'
            ])
            ->where('order_delivery.id', $id)
            ->first();
        
        $detail = OrderDeliveryDetail::join('models', 'models.id', '=', 'model_id')
            ->join('shops', 'shops.id', '=', 'shop_id')
            ->select([
                DB::Raw('CONCAT(models.name, " - (", model_name, ")") AS produk'), 'qty', DB::Raw('shops.name AS shop'), 'retur'
            ])
            ->whereOrderDeliveryId($id)
            ->get();
        
        return view('admin.report.order-delivery.show', compact('supplier', 'order', 'do', 'detail'));
	}
    
	public function anyData(Request $request) {
        $order = Order::join('order_delivery', 'order_delivery.order_id', '=', 'orders.id')
            ->join('order_delivery_details', 'order_delivery_details.order_delivery_id', '=', 'order_delivery.id')
            ->join('supplier_orders', 'supplier_orders.id', '=', 'supplier_id')
            ->select([
                'order_delivery.id', 'do_number', 'po_number', 'nama', DB::Raw('SUM(order_delivery_details.qty) AS qty')
            ])
            ->groupBy('order_delivery.id');

         $datatables = app('datatables')->of($order)
            ->addColumn('action', function ($order) {
                return '<a href="order-delivery/'.$order->id.'" class="bb btn btn-primary">Detail</a>';
            });
        
		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		return $datatables->make(true);
	}

}
