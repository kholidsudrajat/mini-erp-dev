<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ExpenseToko;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ExpenseTokoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.expense_toko.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $category = array(
            ''          =>  'Pilih Kategori',
            'gaji'      =>  'gaji',
            'komisi'    =>  'komisi',
            'utilities' =>  'utilities',
            'lain-lain' =>  'lain-lain',
        );
        
        return view('admin.expense_toko.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $ExpenseToko = ExpenseToko::create($requestData);

        Session::flash('flash_message', 'ExpenseToko added!');

        return redirect('admin/expense_toko');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $expense_toko = ExpenseToko::findOrFail($id);

        return view('admin.expense_toko.show', compact('expense_toko'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $category = array(
            ''          =>  'Pilih Kategori',
            'gaji'      =>  'gaji',
            'komisi'    =>  'komisi',
            'utilities' =>  'utilities',
            'lain-lain' =>  'lain-lain',
        );
        $expense_toko = ExpenseToko::findOrFail($id);

        return view('admin.expense_toko.edit', compact('expense_toko', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $requestData = $request->all();

        $expense_toko = ExpenseToko::findOrFail($id);
        $expense_toko->update($requestData);

        Session::flash('flash_message', 'ExpenseToko updated!');

        return redirect('admin/expense_toko');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ExpenseToko::destroy($id);

        Session::flash('flash_message', 'ExpenseToko deleted!');

        return redirect('admin/expense_toko');
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $expense_toko = ExpenseToko::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'name', 'category']);

         $datatables = app('datatables')->of($expense_toko)
            ->addColumn('action', function ($expense_toko) {
                return '<a href="expense_toko/'.$expense_toko->id.'/edit" class="bb btn btn-primary"><i class="fa fa-pencil"></i></a> 
                <a onclick="return deleteData('.$expense_toko->id.')" class="bb btn btn-danger"><i class="fa fa-times"></i></a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('expense_toko.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('expense_toko.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
}
