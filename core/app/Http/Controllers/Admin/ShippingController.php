<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Shipping;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ShippingController extends Controller
{
    public function __construct()
    {
        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.shipping.index');
    }

    public function tambah(Request $request)
    {
        $data['name'] = $request['name'];

        $post = Shipping::create($data);

        return $post;
    }

    public function delete($id)
    {
        Shipping::destroy($id);

        Session::flash('flash_message', 'Cost deleted!');

        return redirect('admin/shipping');
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $shipping = Shipping::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'name']);

        $datatables = app('datatables')->of($shipping)
            ->addColumn('action', function ($shipping) {
                return '<!--<a href="cost/'.$shipping->id.'/edit" class="bb btn btn-primary"><i class="fa fa-pencil"></i></a>--> 
                <a onclick="return deleteData('.$shipping->id.')" class="bb btn btn-danger"><i class="fa fa-times"></i></a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('costs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('costs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }

}