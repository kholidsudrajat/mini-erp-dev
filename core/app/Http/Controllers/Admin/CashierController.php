<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cashier;
use App\CashierDetail;
use App\CashierStatus;
use App\Customer;
use App\CustomerAddress;
use App\RoleMenu;
use App\Salesman;
use App\Models;
use App\Shop;
use App\Stock;
use App\StockLog;
use App\Debt;
use App\Payment;
use App\PaymentMethod;
use App\Discount;
use App\Shipping;
use App\User;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;
use PDF;

class CashierController extends Controller
{
    public function getnotes($id)
    {
        $data = Customer::findOrFail($id);
        $result = $data['note'];
        
        return $result;
    }
    
    public function index(Request $req)
    {
        $current = Session::get('current');
        if($current == '')
            $current = 1;
        $limit = 6;
        $subtotal = Session::get('subtotal');
        $total = Session::get('total');
        $discount = Session::get('discount');
        $pot = Session::get('pot');
		$shop = Shop::whereIsShop('1')->pluck('name','id');
        
        $method = PaymentMethod::pluck('name','id');
        
        if(Auth::User()->shop_id < 1){
            $idshop['1'] = Session::get('shop1');
            $idshop['2'] = Session::get('shop2');
            $idshop['3'] = Session::get('shop3');
            $ids = Shop::whereIsShop('1')->first();

            if(Session::get('shop1') == ""){
                Session::put('shop1', $ids['id']);
                $idshop['1'] = Session::get('shop1');
            }

            if(Session::get('shop2') == ""){
                Session::put('shop2', $ids['id']);
                $idshop['2'] = Session::get('shop2');
            }

            if(Session::get('shop3') == ""){
                Session::put('shop3', $ids['id']);
                $idshop['3'] = Session::get('shop3');
            }
        }else{ 
            $idshop['1'] = Session::get('shop1');
            $idshop['2'] = Session::get('shop2');
            $idshop['3'] = Session::get('shop3');
            
            if(Session::get('shop1') == ""){
                Session::put('shop1', Auth::user()->shop_id);
                $idshop['1'] = Session::get('shop1');
            }

            if(Session::get('shop2') == ""){
                Session::put('shop2', Auth::user()->shop_id);
                $idshop['2'] = Session::get('shop2');
            }

            if(Session::get('shop3') == ""){
                Session::put('shop3', Auth::user()->shop_id);
                $idshop['3'] = Session::get('shop3');
            }
        }
        $key = $req['keyword'];
        
        for($u = 1; $u <= 3; $u++){
            $clause = 'qty > 0 AND is_shop = "1" AND shop_id = "'.$idshop[$u].'"';
            
            if($current == $u)
                $clause .= ' AND (model_name LIKE "%'.$key.'%" OR models.name LIKE "%'.$key.'%")';

            $models = Models::select(['models.id', 'models.name', 'qty', 'price', 'model_name', 'image'])
                ->leftjoin('stocks', 'model_id', '=', 'models.id')
                ->leftjoin('shops', 'shops.id', '=', 'shop_id')
                ->whereRaw($clause);

            $totals = count($models->get());
            
            $model[$u] = $models->limit($limit)->orderBy('models.id', 'DESC')->get();

            $mod = $totals % $limit;
            $pagin[$u] = intval($totals / $limit);

            if($mod > 0)
                $pagin[$u] = $pagin[$u] + 1;

            if($pagin[$u] == 1)
                $pagin[$u] = 0;
        }
            
        $mod = Models::all();
        $cus = Customer::pluck('nama','id');
        $sales = User::whereRole('3')->pluck('name','id')->prepend('Pilih Sales','0');
        
        if(Session::get('cart1') !== "")
           $cart['1'] = json_decode('['.substr(Session::get('cart1'),0,-1).']');
        else
           $cart['1'] = "";
        
        if(Session::get('cart2') !== "")
           $cart['2'] = json_decode('['.substr(Session::get('cart2'),0,-1).']');
        else
           $cart['2'] = "";
        
        if(Session::get('cart3') !== "")
           $cart['3'] = json_decode('['.substr(Session::get('cart3'),0,-1).']');
        else
           $cart['3'] = "";
        
        $name = array();
        $mname = array();
        $price = array();
        $allqty = array();
        foreach($mod as $models){
            $id = $models['id'];
            $name[$id] = $models['name'];
            $mname[$id] = $models['model_name'];
            $price[$id] = $models['price'];
            
            $getallqty = Models::leftJoin('stocks', 'models.id', '=', 'model_id')
            ->leftJoin('shops', 'shops.id', '=', 'shop_id')
            ->select(['models.id', DB::Raw('SUM(qty) AS qtys')])
            ->whereRaw('models.id = '.$id.' AND deleted_at IS NULL')
            ->first();
            
            $allqty[$id] = $getallqty['qtys'];
        }
        
        return view('admin.cashier.index', compact('allqty', 'sales', 'model', 'cus', 'name', 'mname', 'price', 'shop', 'idshop', 'pagin', 'totals', 'method', 'cart', 'subtotal', 'discount', 'pot', 'total', 'current', 'key'));
    } 
    
    public function details($tab, Request $req)
    {
        $limit = 6;
        $subtotal = Session::get('subtotal'.$tab);
        $total = Session::get('total'.$tab);
        $discount = Session::get('discount'.$tab);
        $pot = Session::get('pot'.$tab);
        $shop = Shop::whereIsShop('1')->pluck('name','id');
        
        $method = PaymentMethod::pluck('name','id');
        
        if(Auth::User()->shop_id < 1){
            $idshop = Session::get('shop'.$tab);
            $ids = Shop::whereIsShop('1')->first();

            if($idshop == ""){
                Session::put('shop', $ids['id']);
                $idshop = Session::get('shop'.$tab);
            }
        }else
            $idshop = Auth::User()->shop_id;
        
        $model = Models::select(['models.id', 'models.name', 'qty', 'price', 'model_name', 'image'])->join('stocks', 'model_id', '=', 'models.id')->join('shops', 'shops.id', '=', 'shop_id');
            
        $totals = count($model->get());
            
        $model = $model->limit($limit)->groupBy('models.id')->get();
        
        $mod = Models::all();
        
        if(Session::get('cart'.$tab) != "")
           $cart = json_decode('['.substr(Session::get('cart'.$tab),0,-1).']');
        else
           $cart = "";
        
        $name = array();
        $mname = array();
        $price = array();
        foreach($mod as $models){
            $id = $models['id'];
            $name[$id] = $models['name'];
            $mname[$id] = $models['model_name'];
            $price[$id] = $models['price'];
        }
        
        $mod = $totals % $limit;
        $pagin = intval($totals / $limit);
        
        if($mod > 0)
            $pagin = $pagin + 1;
        
        if($pagin == 1)
            $pagin = 0;
        
        $notes = $this->getnotes($req['cus']);
        $cus = Customer::pluck('nama','id');
        $cu = $req['cus'];
        
        return view('admin.cashier.details', compact('model', 'cart', 'name', 'mname', 'price', 'subtotal', 'discount', 'pot', 'total', 'shop', 'idshop', 'pagin', 'totals', 'method', 'tab', 'notes', 'cus', 'cu'));
    } 
    
    public function shipping($noref)
    {
        $data = Cashier::whereNoRef($noref)->first();
        $cus = $data['customer_id'];
        $addr = CustomerAddress::whereCustomerId($cus)->first();
        $address = CustomerAddress::whereCustomerId($cus)->get();
        
        if($cus == 0 || !isset($addr))
            return redirect('admin/cashier/print/'.$noref);
        
        $ship = Shipping::findOrFail($addr->shipping_id);
        $shipping = Shipping::get();
        
        return view('admin.cashier.shipping', compact('address', 'shipping', 'addr', 'ship', 'noref'));    
    }
    
    public function changeaddress(Request $req)
    {
        $address = CustomerAddress::findOrFail($req['address']);
        
        return $address['shipping_id'];
    }
    
    public function saveshipping(Request $req)
    {
        $data['no_ref'] = $req['no_ref'];
        $data['status'] = $req['status'];
        if($data['status'] == 'dikirim'){
            $data['address_id'] = $req['address_id'];
            $data['shipping_id'] = $req['shipping_id'];
        }else{
            $data['address_id'] = 0;
            $data['shipping_id'] = 0;
        }
        
        CashierStatus::create($data);
        
        return 1;
    }
    
    public function prints($noref)
    {
        $cashier = Cashier::join('shops', 'shop_id', '=', 'shops.id')
            ->leftjoin('customers', 'customer_id', '=', 'customers.id')
            ->select([
                'cashier.id', 'no_ref', 'cashier.created_at', DB::Raw('shops.name AS shop_name'), DB::Raw('customers.nama AS customer_name'), 'subtotal', 
                'discount', 'total', 'cashier.note', 'signature'
            ])
            ->whereNoRef($noref)
            ->first();
        
        $detail = CashierDetail::join('models', 'models.id', '=', 'model_id')
            ->select(['model_name', 'name', 'price', 'qty'])
            ->whereCashierId($cashier['id'])
            ->get();
        
        $payment = Payment::join('payment_method', 'payment_method.id', '=', 'pm_id')
                ->select(['name', 'pay'])
                ->whereNoRef($noref)
                ->where('pay', '>', '0')
                ->get();
        
        $debt = Debt::whereNoRef($noref)->first();
        
        return view('admin.cashier.prints', compact('cashier', 'detail', 'payment', 'debt'));    
    }
    
    public function prints_(Request $req)
    {
        $noref = $req['noref'];
        $cashier = Cashier::join('shops', 'shop_id', '=', 'shops.id')
            ->join('customers', 'customer_id', '=', 'customers.id')
            ->select([
                'cashier.id', 'no_ref', 'cashier.created_at', DB::Raw('shops.name AS shop_name'), 
                DB::Raw('customers.nama AS customer_name'), 'subtotal', 'discount', 'total', 'cashier.note', 'signature'
            ])
            ->whereNoRef($noref)
            ->first();
        
        $detail = CashierDetail::join('models', 'models.id', '=', 'model_id')
            ->select(['model_name', 'name', 'price', 'qty'])
            ->whereCashierId($cashier['id'])
            ->get();
        
        $payment = Payment::join('payment_method', 'payment_method.id', '=', 'pm_id')
                ->select(['name', 'pay'])
                ->whereNoRef($noref)
                ->where('pay', '>', '0')
                ->get();
        
        $debt = Debt::whereNoRef($noref)->first();
        
        $reprint = '';
        if($req['reprint'] == 1)
            $reprint = 'Re-Printed';
        
        $data['cashier'] = $cashier;
        $data['detail'] = $detail;
        $data['payment'] = $payment;
        $data['debt'] = $debt;
        $data['reprint'] = $reprint;
        $pdf = PDF::loadView('admin.cashier.print', $data);
        
        $output = $pdf->output();
        if($req['reprint'] == 1){
            file_put_contents('upload/invoice/TRX-'.$noref.' (Re-Printed).pdf', $output);
            return $pdf->download('TRX-'.$noref.' (Re-Printed).pdf');
        }else{
            file_put_contents('upload/invoice/TRX-'.$noref.'.pdf', $output);
            return $pdf->download('TRX-'.$noref.'.pdf');
        }
    }
    
    public function shop($id, Request $req)
    {
        $tab = $req['tab'];
        if($tab != ''){
            $shop = Session::get('shop'.$tab);
            $cart = Session::get('cart'.$tab);
            $json = "[".substr($cart,0,-1)."]";
            $json = json_decode($json);
            
            foreach($json as $in => $val){
                $stock = Session::get('model_'.$shop.'_'.$val->model_id);
                $stocks = intval($stock) + intval($val->qty);
                
                Session::put('model_'.$shop.'_'.$val->model_id, $stocks);
            }
            
            Session::put('current',$tab);
            Session::put('shop'.$tab,$id);
            Session::put('cart'.$tab,'');
            Session::put('pot'.$tab, '0');
            Session::put('subtotal'.$tab, '0');
            Session::put('discount'.$tab, '0');
            Session::put('total'.$tab, '0');
        }
        Session::put('shop',$id);
        Session::put('cart_','');
        Session::put('pot_', '0');
        Session::put('subtotal_', '0');
        Session::put('discount_', '0');
        Session::put('total_', '0');
        
        
        return 1;
    }
    
    public function tabs(Request $req)
    {
        $tab = $req['tab'];
        Session::put('current',$tab);
        
        return 1;
    }
    
    public function modelDetail($id)
    {
        $model = Models::findOrFail($id);
        
        $shop = Shop::whereIsShop('1')->get();
        foreach($shop as $shops){
            $dd = $shops['id'];
            $getallqty = Models::leftJoin('stocks', 'models.id', '=', 'model_id')
                ->leftJoin('shops', 'shops.id', '=', 'shop_id')
                ->select(['models.id', DB::Raw('SUM(qty) AS qtys')])
                ->whereRaw('models.id = '.$id.' AND shops.id = "'.$dd.'"')
                ->first();
            
            $qty[$dd] = $getallqty['qtys'];
        }
        
        return view('admin.cashier.mod-detail', compact('model', 'shop', 'qty'));
    }
    
    public function addqty(Request $req)
    {
        $tab = $req['tab'];
        $cart = Session::get('cart'.$tab);
        $sub = $req['subtotal'];
        
        $stocks = $req['stocks'];
        $mo_id = $req['model_id'];
        $qt = $req['qty'];
        $shop = Session::get('shop'.$tab);
        $ses = Session::get('model_'.$shop.'_'.$mo_id);
        if(!isset($ses))
            $ses = $stocks;
            
        $ses = $ses - $qt;        
        if($ses < 0){
            return response()->json([
                'status'    => '400',
                'message'   => 'Stok Kurang!'
            ]);
        }else
            Session::put('model_'.$shop.'_'.$mo_id, $ses);
        
        if(Session::get('cart'.$tab) != ""){
            $cart = Session::get('cart'.$tab);
            $json = "[".substr($cart,0,-1)."]";
            $json = json_decode($json);
            
            $new_cart = $cart;
            $i = 1;
            foreach($json as $index)
            {
                $mod = $index->model_id;
                $qty = $index->qty;
                $subtotal = $index->subtotal;
                if($index->model_id == $req['model_id']){
                    $old = '{"model_id" : "'.$mod.'", "qty" : "'.$qty.'", "subtotal" : "'.$subtotal.'"},';
                    $qtys = $qty + $req['qty'];
                    $subs = $req['subtotal'];
                    $subs = $subtotal + $subs;
                    $new = '{"model_id" : "'.$mod.'", "qty" : "'.$qtys.'", "subtotal" : "'.$subs.'"},';
                    $new_cart = str_replace($old, $new, $cart);
                    $i++;
                }else{
                    $i++;
                    if($i<=2)
                        $new_cart = $new_cart.'{"model_id" : "'.$req['model_id'].'", "qty" : "'.$req['qty'].'", "subtotal" : "'.$sub.'"},';
                }
            }
        }else{
            $new_cart = $cart.'{"model_id" : "'.$req['model_id'].'", "qty" : "'.$req['qty'].'", "subtotal" : "'.$sub.'"},';
        }
        
        $subtotal = Session::get('subtotal'.$tab);
        $discount = 0;
        $total = 0;
        $discount = Session::get('discount'.$tab);
        $pot = Session::get('pot'.$tab);
        if($subtotal != ""){
            $subtotal += $req['subtotal'];
            if($pot == '1')
                $disc = $discount / 100 * $subtotal;
            else
                $disc = $discount;
            $total = $subtotal - $disc;
        }
        
        Session::put('cart'.$tab,$new_cart);
        Session::put('subtotal'.$tab, $subtotal);
        Session::put('discount'.$tab, $discount);
        Session::put('total'.$tab, $total);
        Session::put('current',$tab);
        
        return 1;
    }
    
    public function adddisc(Request $req)
    {        
        Session::put('pot', $req['pot']);
        Session::put('subtotal', $req['subtotal']);
        Session::put('discount', $req['discount']);
        Session::put('total', $req['total']);
        
        return 1;
    }
    
    public function hapusqty($id, Request $req)
    {
        $tab = $req['tab'];
        $shop = Session::get('shop'.$tab);
        $old = Session::get('cart'.$tab);
        $old = substr($old,0,-1);
        $json = json_decode('['.$old.']');
        foreach($json as $in => $val){
            if($val->model_id == $req['mod']){
                $stock = Session::get('model_'.$shop.'_'.$req['mod']);
                $stock = intval($stock) + intval($val->qty);
                
                Session::put('model_'.$shop.'_'.$req['mod'], $stock);
            }
        }
        $ex = str_replace(array('{','}'),'',$old);
        $ex = explode(',"',$ex);
        
        foreach($ex as $in){
            $dd = $in;
        }
        
        $mod = intval(substr($dd,14));
        
        unset($ex[$id]);
        
        $new = "";
        foreach($ex as $in => $val){
            if(substr($val,0,1)=='"')
                $val = substr($val,1,200);
            $new .= '{"'.$val.'},';
        }
        
        $subtotal = Session::get('subtotal'.$tab);
        if($subtotal > 0)
            $subtotal = $subtotal - $req['subtotal'];
        
        $pot = Session::get('pot'.$tab);
        $discount = Session::get('discount'.$tab);
        $disc = $discount;
        if($pot == '1')
            $disc = $discount / 100 * $subtotal;
        $total = $subtotal - $disc;
        
        if(count($ex) > 0){
            Session::put('cart'.$tab, $new);
            Session::put('subtotal'.$tab, $subtotal);
            Session::put('discount'.$tab, $discount);
            Session::put('total'.$tab, $total);
        }else{
            Session::put('cart'.$tab, '');
            Session::put('subtotal'.$tab, '0');
            Session::put('discount'.$tab, '0');
            Session::put('total'.$tab, '0');
        }
        Session::put('current', $tab);
        
        return 1;
    }
    
    public function ajaxmodel(Request $req)
    {
        $u = Session::get('current');
        if($u == '')
            $u = 1;
        $tab = $req['tab'];
        $limit = 6;
        $page = ($req['page'] - 1) * $limit;
        
        $ids = Shop::whereIsShop('1')->first();
    
        if(Auth::User()->shop_id < 1){
            $idshop['1'] = Session::get('shop1');
            $idshop['2'] = Session::get('shop2');
            $idshop['3'] = Session::get('shop3');
            $ids = Shop::whereIsShop('1')->first();

            if(Session::get('shop1') == ""){
                Session::put('shop1', $ids['id']);
                $idshop['1'] = Session::get('shop1');
            }

            if(Session::get('shop2') == ""){
                Session::put('shop2', $ids['id']);
                $idshop['2'] = Session::get('shop2');
            }

            if(Session::get('shop3') == ""){
                Session::put('shop3', $ids['id']);
                $idshop['3'] = Session::get('shop3');
            }
        }else{
            $idshop['1'] = Auth::User()->shop_id;
            $idshop['2'] = Auth::User()->shop_id;
            $idshop['3'] = Auth::User()->shop_id;
        }
        
        $list = "";
        $key = $req['keyword'];
        
        $clause = 'qty > 0 AND shop_id = "'.$idshop[$tab].'" AND (model_name LIKE "%'.$key.'%" OR models.name LIKE "%'.$key.'%")';
        $model = Models::select(['models.id', 'models.name', 'qty', 'price', 'model_name', 'image'])
            ->join('stocks', 'model_id', '=', 'models.id')->join('shops', 'shops.id', '=', 'shop_id')
            ->whereRaw($clause)
            ->limit($limit)
            ->offset($page)
            ->orderBy('models.id', 'DESC')
            ->get();
        
        foreach($model as $mod)
        {
            $getallqty = Models::leftJoin('stocks', 'models.id', '=', 'model_id')
            ->leftJoin('shops', 'shops.id', '=', 'shop_id')
            ->select(['models.id', DB::Raw('SUM(qty) AS qtys')])
            ->whereRaw('models.id = '.$mod['id'].' AND deleted_at IS NULL')
            ->first();
            
            $allqty = $getallqty['qtys'];
            
            $list .= '<div class="form-group col-lg-2 col-md-3 col-sm-4 col-xs-4" id="mods'. $u .'_'. $mod['id'] .'">
                    <div class="form-group-default">
                        <div class="displaytable">
                            <div class="displaytablecell">
                                <label class="title-prod">'. $mod['name'] .'</label>
                            </div>
                        </div>
                        <a class="col-md-12 gmb" style="cursor:pointer;" id="popup" href="'. url('').'/admin/model_detail/'.$mod['id'] .'">
                            <div class="frame-image">
                                <img src="'. url('files/models').'/'.$mod['image'] .'" />
                                <!--img src="'. url('files/models').'/'.$mod['image'] .'" style="max-height:200px; margin-bottom:10px;" /-->
                                <div class="text-overlay">
                                    <span><i class="font-10">IDR '. str_replace(',','.',number_format($mod['price'])) .'</i></span>
                                    <span class="font-10">
                                        ('. $mod['qty'] .' / '.$allqty.' Pcs)
                                    </span>
                                </div>
                            </div>
                        </a>
                        <span class="font-10">'. $mod['model_name'] .'</span>
                        <br/>
                        <div class="clearfix" style="padding-bottom: 3px;"></div>
                        <input name="qty" type="number" value="1" style="width:70%" id="qty'.$mod['id'].'_'.$u.'" />
                        <input style="display:none;" id="stk'.$mod['id'].'" value="'. $mod['qty'] .'" />
                        <input style="display:none;" id="stocks'.$mod['id'].'" value="'. $mod['qty'] .'" />
                        <a onclick="addtocart('.$u.','.$mod['id'].','.$mod['price'].')" class="btn btn-add-produk btn-xs btn-primary"><i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>';
        }
        
        return $list;
    }
    
    public function bayar(Request $req)
    {
        define('UPLOAD_DIR', 'files/signature/');
        $img = $req['tanda_tangan'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data_img = base64_decode($img);
        $file = UPLOAD_DIR . uniqid() . '.png';
        $success = file_put_contents($file, $data_img);

        $tab = $req['tab'];
        $count = Cashier::count();
        if ($count < 10)
            $count = '0' . $count;

        $noref = $req['noref'];

        $data['no_ref'] = $noref;
        $data['shop_id'] = $req['shop_id'];
        $data['customer_id'] = $req['customer_id'];
        $data['salesman_id'] = $req['salesman_id'];
        $data['note'] = $req['notes'];
        $data['discount'] = 0 + $req['discount'];
        $data['subtotal'] = 0 + $req['subtotal'];
        $data['total'] = 0 + $req['total'];
        $data['signature'] = $file;
        $data['method_pembayaran'] = $req['method_pembayaran'];
        $data['updated_at'] = date('Y-m-d H:i:s');

        $id = Cashier::insertGetId($data);
        
        if(Session::get('cart'.$tab) != "")
           $cart = json_decode('['.substr(Session::get('cart'.$tab),0,-1).']');
        else
           $cart = "";
        
        $datas = array();
        $bb = array();
        $shop = Session::get('shop'.$tab);
        foreach($cart as $in => $val)
        {
            $datas['cashier_id'] = $id;
            $datas['model_id'] = $val->model_id;
            $bb[$val->model_id] = $val->qty;
            $datas['qty'] = $val->qty;
            
            $this->removestock($datas['model_id'], $datas['qty'], $data['shop_id']);
            $datas['transfer_from'] = $data['shop_id'];
            $datas['type'] = 'out';
            $this->createlog($datas);
            
            Session::remove('model_'.$shop.'_'.$val->model_id);
            CashierDetail::create($datas);
        }
            
        /////////INSERT INTO TABLE DISCOUNT
        $json = '['.substr($req['jdisc'],0,-1).']';
        $json = json_decode($json);
        
        $value['no_ref'] = $noref;
        foreach($json as $index){
            foreach($index as $in => $val){
                foreach($val as $i => $o){
                    $value['type'] = $in;
                    $value['model_id'] = $i;
                    $value['discount'] = $o;
                    
                    if($i == 0)
                        $value['qty'] = 1;
                    else
                        $value['qty'] = $bb[$i];
                    
                    Discount::create($value);
                }
            }
        }
        
        /////////INSERT INTO TABLE PAYMENT
        $payment = '['.substr($req['payment'],1).']';
        
        $payment = json_decode($payment);
        $dt['no_ref'] = $noref;
        $dt['customer_id'] = $req['customer_id'];
        $debt = $data['total'];
        foreach($payment as $index){
            foreach($index as $in => $val){
                $dt['pm_id'] = $in;
                $dt['pay']   = $val;
                
                $debt -= $val;
            }
    
            if($dt['pay'] > 0)
                Payment::create($dt);
        }
        
        /////INSERT INTO TABLE DEBT IF TOTAL > PAY
        if($debt > 0){
            $due = strtotime('+'.$req['due_date'].' days');
            $duedate = date('Y-m-d 23:59:59',$due);
            
            $do['no_ref'] = $noref;
            $do['customer_id'] = $req['customer_id'];
            $do['debt'] = $debt * -1;
            $do['due_date'] = $duedate;
            
            Debt::create($do);
        }
        
        Session::put('cart'.$tab,'');
        Session::put('pot'.$tab, '0');
        Session::put('subtotal'.$tab, '0');
        Session::put('discount'.$tab, '0');
        Session::put('total'.$tab, '0');
        
        return $noref;
    }
    
    /////////////////CASHIER-2
    public function index2(Request $req)
    {
        $subtotal = Session::get('subtotal_');
        $total = Session::get('total_');
        $discount = Session::get('discount_');
        $pot = Session::get('pot_');
        
        $shop = Shop::whereIsShop('1')->pluck('name','id');
        $idshop = Session::get('shop');
        $ids = Shop::whereIsShop('1')->first();
    
        if(Auth::User()->shop_id < 1){
            $idshop = Session::get('shop');
            $ids = Shop::whereIsShop('1')->first();

            if($idshop == ""){
                Session::put('shop', $ids['id']);
                $idshop = Session::get('shop');
            }
        }else
            $idshop = Auth::User()->shop_id;
        
        $method = PaymentMethod::pluck('name','id');
        
        $mod = $req['model'];
        $mods = Models::join('stocks', 'model_id', '=', 'models.id')
            ->join('shops', 'shops.id', '=', 'shop_id')
            ->select([
                'models.id', 'models.name', 'qty', 'price', 'model_name', 'image'
            ])
            ->whereRaw('qty > 0 AND shop_id="'.$idshop.'"')
            ->groupBy('models.id')
            ->get();
        
        $clause = 'qty > 0 AND shop_id = "'.$idshop.'" AND models.id = "'.$mod.'"';
        $model = Models::join('stocks', 'model_id', '=', 'models.id')
            ->join('shops', 'shops.id', '=', 'shop_id')
            ->select([
                'models.id', 'models.name', 'qty', 'price', 'model_name', 'image'
            ])
            ->whereRaw($clause)
            ->groupBy('models.id')
            ->first();
        
        $mod = Models::all();
        $cus = Customer::pluck('nama','id');
        
        if(Session::get('cart_') != "")
           $cart = json_decode('['.substr(Session::get('cart_'),0,-1).']');
        else
           $cart = "";
        
        $name = array();
        $mname = array();
        $price = array();
        foreach($mod as $models){
            $id = $models['id'];
            $name[$id] = $models['name'];
            $mname[$id] = $models['model_name'];
            $price[$id] = $models['price'];
        }
        
        return view('admin.cashier-2.index', compact('model','cus','cart','name','mname','price','subtotal','discount','pot','total','mods','shop','idshop','method'));
    } 
    
    public function addqty2(Request $req)
    {
        $cart = Session::get('cart_');
        $sub = $req['subtotal'];
        if(Session::get('cart_') != "")
        {
            $cart = Session::get('cart_');
            $json = "[".substr($cart,0,-1)."]";
            $json = json_decode($json);
            
            $new_cart = $cart;
            $i = 1;
            if(Session::get('cart_') != ""){
                $cart = Session::get('cart_');
                $json = "[".substr($cart,0,-1)."]";
                $json = json_decode($json);

                $new_cart = $cart;
                $i = 1;
                foreach($json as $index)
                {
                    $mod = $index->model_id;
                    $qty = $index->qty;
                    $subtotal = $index->subtotal;
                    if($index->model_id == $req['model_id']){
                        $old = '{"model_id" : "'.$mod.'", "qty" : "'.$qty.'", "subtotal" : "'.$subtotal.'"},';
                        $qtys = $qty + $req['qty'];
                        $subs = $req['subtotal'];
                        $subs = $subtotal + $subs;
                        $new = '{"model_id" : "'.$mod.'", "qty" : "'.$qtys.'", "subtotal" : "'.$subs.'"},';
                        $new_cart = str_replace($old, $new, $cart);
                        $i++;
                    }else{
                        $i++;
                        if($i<=2)
                            $new_cart = $new_cart.'{"model_id" : "'.$req['model_id'].'", "qty" : "'.$req['qty'].'", "subtotal" : "'.$sub.'"},';
                    }
                }
            }else{
                $new_cart = $cart.'{"model_id" : "'.$req['model_id'].'", "qty" : "'.$req['qty'].'", "subtotal" : "'.$sub.'"},';
            }
        }
        else
           $new_cart = $cart.'{"model_id" : "'.$req['model_id'].'", "qty" : "'.$req['qty'].'", "subtotal" : "'.$req['subtotal'].'"},';
        
        $subtotal = Session::get('subtotal_');
        $discount = 0;
        $total = 0;
        $discount = Session::get('discount_');
        $pot = Session::get('pot_');
        if($subtotal != ""){
            $subtotal += $req['subtotal'];
            if($pot == '1')
                $disc = $discount / 100 * $subtotal;
            else
                $disc = $discount;
            $total = $subtotal - $disc;
        }else{
            $subtotal = $sub;
            $total = $sub;
        }
        
        Session::put('cart_',$new_cart);
        Session::put('subtotal_', $subtotal);
        Session::put('discount_', $discount);
        Session::put('total_', $total);
        
        return 1;
    }
    
    public function adddiscs(Request $req)
    {        
        Session::put('pot_', $req['pot']);
        Session::put('subtotal_', $req['subtotal']);
        Session::put('discount_', $req['discount']);
        Session::put('total_', $req['total']);
        
        return 1;
    }
    
    
    
    public function hapusqty2($id, Request $req)
    {
        $old = Session::get('cart_');
        $old = substr($old,0,-1);
        $ex = str_replace(array('{','}'),'',$old);
        $ex = explode(',"',$ex);
        
        foreach($ex as $in){
            $dd = $in;
        }
        
        unset($ex[$id]);
        
        $new = "";
        foreach($ex as $in => $val){
            if(substr($val,0,1)=='"')
                $val = substr($val,1,200);
            $new .= '{"'.$val.'},';
        }
        
        $subtotal = Session::get('subtotal_');
        if($subtotal > 0)
            $subtotal = $subtotal - $req['subtotal'];
        
        $pot = Session::get('pot_');
        $discount = Session::get('discount_');
        $disc = $discount;
        if($pot == '1')
            $disc = $discount / 100 * $subtotal;
        $total = $subtotal - $disc;
        if(count($ex) > 0){
            Session::put('cart_',$new);
            Session::put('subtotal_', $subtotal);
            Session::put('discount_', $discount);
            Session::put('total_', $total);
        }else{
            Session::put('cart_', '');
            Session::put('subtotal_', '0');
            Session::put('discount_', '0');
            Session::put('total_', '0');
        }
        
        return 1;
    }
    
    public function ajaxmodels(Request $req)
    {
        $idshop = Session::get('shop');
        $ids = Shop::whereIsShop('1')->first();
    
        if(Auth::User()->shop_id < 1){
            $idshop = Session::get('shop');
            $ids = Shop::whereIsShop('1')->first();

            if($idshop == "")
                Session::put('shop', $ids['id']);
        }else
            $idshop = Auth::User()->shop_id;
        
        $list = '<ul id="listnone">';
        $key = $req['keyword'];
        $clause = 'qty > 0 AND shop_id="'.$idshop.'" AND (model_name LIKE "%'.$key.'%" OR models.name LIKE "%'.$key.'%")';
        $model = Models::select(['models.id', 'models.name', 'qty', 'price', 'model_name', 'category'])
            ->join('stocks', 'model_id', '=', 'models.id')
            ->join('shops', 'shops.id', '=', 'shop_id')
            ->whereRaw($clause)
            ->groupBy('models.id')
            ->get();
        
        $i = 0;
        foreach($model as $mod)
        {
            $i++;
            $list .= '<li onclick="return pilih(&#39;'.$mod->id.'&#39;)">'.$mod->name.' - ('.$mod->model_name.') - '.$mod->category.'</li>';
        }
        
        $list .= '</ul>';
        
        if($i < 1)
            $list = "";
        
        return $list;
    }
    
    public function bayars(Request $req)
    {      
        $noref = $req['no_ref'];
        $data['no_ref'] = $noref;
        $data['customer_id'] = $req['customer_id'];
        $data['shop_id'] = $req['shop_id'];
        $data['discount'] = 0 + $req['discount'];
        $data['subtotal'] = 0 + $req['subtotal'];
        $data['total'] = 0 + $req['total'];
        $data['method_pembayaran'] = $req['method_pembayaran'];
        $data['updated_at'] = date('Y-m-d H:i:s');
        
        $id = Cashier::insertGetId($data);
        
        if(Session::get('cart_') != "")
           $cart = json_decode('['.substr(Session::get('cart_'),0,-1).']');
        else
           $cart = "";
        
        $datas = array();
        foreach($cart as $in => $val)
        {
            $datas['cashier_id'] = $id;
            $datas['model_id'] = $val->model_id;
            $datas['qty'] = $val->qty;
            
            $this->removestock($datas['model_id'], $datas['qty'], $data['shop_id']);
            $datas['transfer_from'] = $data['shop_id'];
            $datas['type'] = 'out';
            $this->createlog($datas);
            
            CashierDetail::create($datas);
        }
        
        Discount::create([
            'no_ref'    => $noref,
            'type'      => $req['pot'],
            'model_id'  => 0,
            'qty'       => 1,
            'discount'  => 0 + $req['discount']
        ]);
        
        /////////INSERT INTO TABLE PAYMENT
        $payment = '['.substr($req['payment'],1).']';
        
        $payment = json_decode($payment);
        $dt['no_ref'] = $noref;
        $dt['customer_id'] = $req['customer_id'];
        $debt = $data['total'];
        foreach($payment as $index){
            foreach($index as $in => $val){
                $dt['pm_id'] = $in;
                $dt['pay']   = $val;
                
                $debt -= $val;
            }
    
            if($dt['pay'] > 0)
                Payment::create($dt);
        }
        
        /////INSERT INTO TABLE DEBT IF TOTAL > PAY
        if($debt > 0){
            $due = strtotime('+'.$req['due_date'].' days');
            $duedate = date('Y-m-d 23:59:59',$due);
            
            $do['no_ref'] = $noref;
            $do['customer_id'] = $req['customer_id'];
            $do['debt'] = $debt * -1;
            $do['due_date'] = $duedate;
            
            Debt::create($do);
        }
        
        Session::put('cart_','');
        Session::put('pot_', '0');
        Session::put('subtotal_', '0');
        Session::put('discount_', '0');
        Session::put('total_', '0');
        
        return $noref;
    }     
    
    public function shipping2($noref)
    {
        $data = Cashier::whereNoRef($noref)->first();
        $cus = $data['customer_id'];
        $addr = CustomerAddress::whereCustomerId($cus)->first();
        $address = CustomerAddress::whereCustomerId($cus)->get();
        
        if($cus == 0 || !isset($addr))
            return redirect('admin/cashier-2/print/'.$noref);
        
        $ship = Shipping::findOrFail($addr->shipping_id);
        $shipping = Shipping::get();
        
        return view('admin.cashier-2.shipping', compact('address', 'shipping', 'addr', 'ship', 'noref'));    
    }
    
    public function changeaddress2(Request $req)
    {
        $address = CustomerAddress::findOrFail($req['address']);
        
        return $address['shipping_id'];
    }
    
    public function saveshipping2(Request $req)
    {
        $data['no_ref'] = $req['no_ref'];
        $data['status'] = $req['status'];
        if($data['status'] == 'dikirim'){
            $data['address_id'] = $req['address_id'];
            $data['shipping_id'] = $req['shipping_id'];
        }else{
            $data['address_id'] = 0;
            $data['shipping_id'] = 0;
        }
        
        CashierStatus::create($data);
        
        return 1;
    }
    
    public function prints2($noref)
    {
        $cashier = Cashier::join('shops', 'shop_id', '=', 'shops.id')
            ->leftjoin('customers', 'customer_id', '=', 'customers.id')
            ->select([
                'cashier.id', 'no_ref', 'cashier.created_at', DB::Raw('shops.name AS shop_name'), DB::Raw('customers.nama AS customer_name'), 'subtotal', 
                'discount', 'total', 'cashier.note', 'signature'
            ])
            ->whereNoRef($noref)
            ->first();
        
        $detail = CashierDetail::join('models', 'models.id', '=', 'model_id')
            ->select(['model_name', 'name', 'price', 'qty'])
            ->whereCashierId($cashier['id'])
            ->get();
        
        $payment = Payment::join('payment_method', 'payment_method.id', '=', 'pm_id')
                ->select(['name', 'pay'])
                ->whereNoRef($noref)
                ->where('pay', '>', '0')
                ->get();
        
        $debt = Debt::whereNoRef($noref)->first();
        
        return view('admin.cashier-2.prints', compact('cashier', 'detail', 'payment', 'debt'));    
    }
    
    public function prints_2($noref)
    {
        $cashier = Cashier::join('shops', 'shop_id', '=', 'shops.id')
            ->join('customers', 'customer_id', '=', 'customers.id')
            ->select([
                'cashier.id', 'no_ref', 'cashier.created_at', DB::Raw('shops.name AS shop_name'), DB::Raw('customers.nama AS customer_name'), 'subtotal', 
                'discount', 'total', 'cashier.note'
            ])
            ->whereNoRef($noref)
            ->first();
        
        $detail = CashierDetail::join('models', 'models.id', '=', 'model_id')
            ->select(['model_name', 'name', 'price', 'qty'])
            ->whereCashierId($cashier['id'])
            ->get();
        
        $payment = Payment::join('payment_method', 'payment_method.id', '=', 'pm_id')
                ->select(['name', 'pay'])
                ->whereNoRef($noref)
                ->where('pay', '>', '0')
                ->get();
        
        $debt = Debt::whereNoRef($noref)->first();
        
        $reprint = '';
        if($req['reprint'] == 1)
            $reprint = 'Re-Printed';
        
        $data['cashier'] = $cashier;
        $data['detail'] = $detail;
        $data['payment'] = $payment;
        $data['debt'] = $debt;
        $data['reprint'] = $reprint;
        
        $pdf = PDF::loadView('admin.cashier-2.print', $data);
        
        $output = $pdf->output();
        if($req['reprint'] == 1){
            file_put_contents('upload/invoice/TRX-'.$noref.' (Re-Printed).pdf', $output);
            return $pdf->download('TRX-'.$noref.' (Re-Printed).pdf');
        }else{
            file_put_contents('upload/invoice/TRX-'.$noref.'.pdf', $output);
            return $pdf->download('TRX-'.$noref.'.pdf');
        }
    }    
    
    public function removestock($model_id, $qty, $shop_id)
    {
        $stock = Stock::join('shops', 'shops.id', '=', 'shop_id')->where(['shop_id' => $shop_id])->whereModelId($model_id)->first();
        $old_stock = $stock->qty;
        $new_stock = $old_stock - $qty;
        
        $update = Stock::whereShopId($stock->shop_id)->whereModelId($model_id)->update(['qty' => $new_stock]);
        
        return $update;
    }
    
    public function createlog($data)
    {
        StockLog::create($data);
        
        return 1;
    }
}
