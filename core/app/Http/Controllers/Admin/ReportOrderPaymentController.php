<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDetail;
use App\OrderPayment;
use App\SupplierOrder;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ReportOrderPaymentController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index() {
        return view('admin.report.order-payment.index');
	}
    
	public function show($id) {
        $order = OrderPayment::join('orders', 'orders.id', '=', 'order_id')
            ->select([
                'order_payments.total', 'po_number', 'inv_number', 'dates', 'supplier_id'
            ])
            ->where('order_payments.id',$id)
            ->first();
        
        $supplier = SupplierOrder::findOrFail($order->supplier_id);
        
        return view('admin.report.order-payment.show', compact('order', 'supplier'));
	}
    
	public function anyData(Request $request) {
        $order = OrderPayment::join('orders','orders.id','=','order_id')
            ->join('supplier_orders','supplier_orders.id','=','supplier_id')
            ->join('payment_method','payment_method.id','=','payment_type')
            ->select([
                'order_payments.id', 'inv_number', 'po_number', 'nama', 'name', 'order_payments.total'
            ]);

         $datatables = app('datatables')->of($order)
            ->addColumn('action', function ($order) {
                return '<a href="order-payment/'.$order->id.'" class="bb btn btn-primary">Detail</a>';
            })
            ->editColumn('total', function ($order){
                return str_replace(',','.',number_format($order->total));
            });
        
		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		return $datatables->make(true);
	}

}
