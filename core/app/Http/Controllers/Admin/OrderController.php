<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Order;
use App\OrderDetail;
use App\SupplierOrder;
use App\Shipping;
use App\Models;
use App\PaymentMethod;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('admin.pembelian.list.index');
    }
    
    public function show($id)
    {
        $order = Order::findOrFail($id);
        $detail = OrderDetail::join('models', 'models.id', '=', 'model_id')
            ->select(['model_id', 'name', 'model_name', 'qty', 'order_details.price', 'total'])
            ->whereOrderId($id)
            ->get();
        
        $supplier = SupplierOrder::pluck('nama', 'id')->prepend('Pilih Supplier',0);
        $model = Models::select(['id', DB::Raw('CONCAT(name, " - (", model_name, ")") AS name')])
            ->pluck('name','id')
            ->prepend('Pilih Produk',0);
        
        $shipping = Shipping::get();
        $payment = PaymentMethod::get();

        return view('admin.pembelian.list.show', compact('order', 'detail', 'supplier', 'model', 'shipping', 'payment'));
    }
    
    public function edit($id)
    {
        $order = Order::findOrFail($id);
        $detail = OrderDetail::join('models', 'models.id', '=', 'model_id')
            ->select(['model_id', 'name', 'model_name', 'qty', 'order_details.price', 'total'])
            ->whereOrderId($id)
            ->get();
        
        $supplier = SupplierOrder::pluck('nama', 'id')->prepend('Pilih Supplier',0);
        $model = Models::select(['id', DB::Raw('CONCAT(name, " - (", model_name, ")") AS name')])
            ->pluck('name','id')
            ->prepend('Pilih Produk',0);
        
        $shipping = Shipping::get();
        $payment = PaymentMethod::get();

        return view('admin.pembelian.list.edit', compact('order', 'detail', 'supplier', 'model', 'shipping', 'payment'));
    }
    
    public function update($id, Request $request)
    {
        $requestData = $request->all();

        $order = Order::findOrFail($id);
        $order->update($requestData);

        Session::flash('flash_message', 'Order updated!');

        return 1;
    }
    
    public function adddetail($id, Request $request)
    {
        $total = $request->qty * $request->price;
        $requestData = [
            'order_id'  =>  $id,
            'model_id'  =>  $request->model_id,
            'qty'       =>  $request->qty,
            'price'     =>  $request->price,
            'total'     =>  $total
        ];

        $order = OrderDetail::create($requestData);
        
        $summ = OrderDetail::selectRaw('SUM(total) AS totals')->whereOrderId($id)->first();
        $sum = $summ->totals;
        
        $orders = Order::findOrFail($id);
        $orders->update(['total' => $sum]);

        return 1;
    }
    
    public function updatedetail($id, Request $request)
    {
        $total = $request->qty * $request->price;
        $requestData = [
            'qty'   =>  $request->qty,
            'price' =>  $request->price,
            'total' =>  $total
        ];

        $order = OrderDetail::whereOrderId($id)->whereModelId($request->model_id)->first();
        $order->update($requestData);
        
        $summ = OrderDetail::selectRaw('SUM(total) AS totals')->whereOrderId($id)->first();
        $sum = $summ->totals;
        
        $orders = Order::findOrFail($id);
        $orders->update(['total' => $sum]);

        return 1;
    }
    
    public function deletedetail($id, Request $request)
    {
        $order = OrderDetail::whereOrderId($id)->whereModelId($request->model_id)->first();
        $order->delete();
        
        $summ = OrderDetail::selectRaw('SUM(total) AS totals')->whereOrderId($id)->first();
        $sum = $summ->totals;
        
        $orders = Order::findOrFail($id);
        $orders->update(['total' => $sum]);

        return 1;
    }
    
    public function destroy($id)
    {
        OrderDetail::whereOrderId($id)->delete();
        Order::destroy($id);

        Session::flash('flash_message', 'Order deleted!');

        return redirect('admin/order');
    }

    public function anyData(Request $request)
    {
        $order = Order::leftjoin('supplier_orders', 'supplier_orders.id', 'supplier_id')
            ->leftjoin('shipping', 'shipping.id', '=', 'shipping_id')
            ->leftjoin('payment_method', 'payment_method.id', '=', 'payment_type')
            ->leftjoin('order_payments', 'orders.id', '=', 'order_id')
            ->select([
                'orders.id', 'po_number', 'supplier_orders.nama AS supp', 'shipping.name AS ship', 'payment_method.name AS paym', 'payment_type', 
                'shipping_type', 'orders.total', DB::Raw('SUM(order_payments.total) AS pay'), 'status'
            ])
            ->groupBy('orders.id');

         $datatables = app('datatables')->of($order)
            ->addColumn('action', function ($order) {
                if($order->status != 0){
                    return '<a href="order/'.$order->id.'" class="bb btn btn-success"><i class="fa fa-eye"></i></a>';
                }else{
                    return '<a href="order/'.$order->id.'" class="bb btn btn-success"><i class="fa fa-eye"></i></a>
                    <a href="order/'.$order->id.'/edit" class="bb btn btn-primary"><i class="fa fa-pencil"></i></a> 
                    <a onclick="return deleteData('.$order->id.')" class="bb btn btn-danger"><i class="fa fa-times"></i></a>';
                }
                    
            })
            ->editColumn('total', function ($order){
                return str_replace(',','.',number_format($order->total));
            })
            ->editColumn('pay', function ($order){
                return str_replace(',','.',number_format($order->total - $order->pay));
            })
            ->editColumn('ship', function ($order){
                if($order->ship != "")
                    return $order->ship;
                else
                    return $order->shipping_type;
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
}