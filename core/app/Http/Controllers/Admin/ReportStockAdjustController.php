<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Stock;
use App\StockAdjustment;
use App\StockLog;
use App\Shop;
use App\Models;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class ReportStockAdjustController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.report.stock-adjustment.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $shop = Shop::pluck('name','id')->prepend('Pilih Toko / Gudang', '0');
        $model = Models::pluck('name','id')->prepend('Pilih Model', '0');
        
        return view('admin.stock.create', compact('shop', 'model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $req['date'] = $request['date'];
        $req['shop_id'] = $request['shop_id'];
        $req['model_id'] = $request['model_id'];
        
        $qty = $request['qty'] - $request['qtys'];
        if($qty > 0)
            $req['type'] = 'plus';
        else
            $req['type'] = 'min';
        
        $req['qty'] = intval($qty);
        $req['note'] = $request['note'];

        StockAdjustment::create($req);
        
        if($req['type'] == 'min')
            $req['qty'] = $req['qty'] * -1;
        
        $cek = Stock::whereModelId($req['model_id'])->whereShopId($req['shop_id']);
        if($cek->count() > 0){
            $olddata = $cek->first();
            $oldqty = $olddata['qty'];
            $newqty = $oldqty + $req['qty'];
            
            $cek->update(['qty' => $newqty]);
        }else
            $stock = Stock::create($requestData);

        $data = array(
            'model_id'      =>  $req['model_id'],
            'qty'           =>  $req['qty'],
            'transfer_to'   =>  $req['shop_id'],
            'type'          =>  'adjustment',
        );
        
        $this->createlog($data);

        return redirect('admin/stock-adjustment');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $stock = Stock::findOrFail($id);

        return view('admin.stock.show', compact('stock'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $stockadjustment = StockAdjustment::findOrFail($id);

        return view('admin.stock-adjustment.edit', compact('stockadjustment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $requestData = $request->all();

        $stock = StockAdjustment::findOrFail($id);
        $stock->update($requestData);

        Session::flash('flash_message', 'Mstock updated!');

        return redirect('admin/stock-adjustment');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Stock::destroy($id);

        Session::flash('flash_message', 'Mstock deleted!');

        return redirect('admin/stock');
    }

    public function anyData(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $stock = StockAdjustment::join('shops', 'shops.id', '=', 'shop_id')
            ->join('models', 'models.id', '=', 'model_id')
            ->select([
                'stock_adjustments.id', 'stock_adjustments.date', DB::Raw('shops.name AS shop_name'), DB::Raw('models.name AS model_name'), 
                'stock_adjustments.shop_id', 'stock_adjustments.model_id', 'stock_adjustments.type', 'stock_adjustments.qty', 'stock_adjustments.note'
            ]);

        $datatables = app('datatables')->of($stock)
            ->editColumn('qty', function($stock){
                if($stock->type == 'min')
                    return $stock->qty * -1;
                else
                    return $stock->qty;
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('stock_adjustments.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('stock_adjustments.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
    
    public function getstock(Request $req)
    {
        $shop = Shop::findOrFail($req['shop']);
        $model = Models::findOrFail($req['model']);
        $allstock = Stock::select([
            DB::Raw('SUM(qty) AS qty')
        ])
            ->whereModelId($req['model'])
            ->first();
        $stocks = Stock::whereModelId($req['model'])
            ->whereShopId($req['shop'])
            ->first();
        
        $data['shop'] = $shop;
        $data['model'] = $model;
        $data['allstock'] = $allstock;
        $data['stocks'] = $stocks;
        
        return $data;
    }
    
    public function createlog($data)
    {
        StockLog::create($data);
        
        return 1;
    }
}