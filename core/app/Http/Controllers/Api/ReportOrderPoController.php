<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Order;
use App\OrderDetail;
use App\OrderPayment;
use App\SupplierOrder;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use PDF;
use Auth;

class ReportOrderPoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index(Request $request) {
        $order = Order::leftjoin('supplier_orders', 'supplier_orders.id', 'supplier_id')
            ->leftjoin('payment_method', 'payment_method.id', '=', 'payment_type')
            ->select([
                'orders.id', 'po_number', 'supplier_orders.nama AS supp', 'payment_method.name AS paym', 'payment_type', 'shipping_type', 'total'
            ]);

         $datatables = app('datatables')->of($order)
            ->addColumn('action', function ($order) {
                return '<a onclick="orderPo('.$order->id.')" href="javascript:;;" class="bb btn btn-primary">Detail</a>';
            })
            ->editColumn('total', function ($order){
                return str_replace(',','.',number_format($order->total));
            });
        
		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}
        
	        if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}
	
	public function exportpdf(Request $req){
	    $keyword = $req['keyword'];
	    $range = explode(":", $req['date']);
	    
        $report = Order::leftjoin('supplier_orders', 'supplier_orders.id', 'supplier_id')
            ->leftjoin('payment_method', 'payment_method.id', '=', 'payment_type')
            ->select([
                'orders.id', 'po_number', 'supplier_orders.nama AS supp', 'payment_method.name AS paym', 'payment_type', 'shipping_type', 'total'
            ]);
        
        if($keyword != "" && $req['date'] != "")
            $clause = '(supplier_orders.nama LIKE "%'.$keyword.'%" OR po_number LIKE "%'.$keyword.'%") AND orders.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        elseif($keyword != "" && $req['date'] == "")
            $clause = '(supplier_orders.nama LIKE "%'.$keyword.'%" OR po_number LIKE "%'.$keyword.'%")';
        elseif($keyword == "" && $req['date'] != "")
            $clause = 'orders.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        else
            $clause = '';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->orderBy('po_number')->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Nomor PO</th><th>Nama Supplier</th><th>Payment</th><th>Total</th></tr>';
        
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->po_number.'</td>';
            $html .= '<td>'.$rep->supp.'</td>';
            $html .= '<td>'.$rep->paym.'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep->total)).'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/report-order-po/'))
                mkdir('upload/report-order-po/', 0777, true);
            
            file_put_contents('upload/report-order-po/'.$filename.'.pdf', $pdf);
            $file = 'upload/report-order-po/'.$filename.'.pdf';
            
            return $file;
        }
    }
    
	public function show($id) {
        $order = Order::findOrFail($id);
        $supplier = SupplierOrder::findOrFail($order->supplier_id);
        $payment = OrderPayment::whereOrderId($id)->get();
        $bayar = OrderPayment::select([DB::Raw('SUM(total) AS total')])->whereOrderId($id)->first();
        $totalnya = $order->total - (0 + $bayar->total);
        
        $detail = OrderDetail::join('models', 'models.id', '=', 'model_id')
            ->select(['model_id', 'name', 'model_name', 'qty', 'order_details.price', 'total'])
            ->whereOrderId($id)
            ->get();
        
		return response()->json([
			'supplier' => $supplier,
			'order' => $order,
			'detail' => $detail,
			'payment' => $payment,
			'totalnya' => $totalnya,
		], 200);
	}
}
