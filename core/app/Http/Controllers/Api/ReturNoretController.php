<?php

namespace App\Http\Controllers\Api;

use App\Retur;
use DB;
use PDF;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ReturNoretController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return View
	 */
	public function index(Request $request) {
		$report = Retur::select([
            'id', 'no_retur', 'no_ref', 'type_retur', 'created_at'
        ]);

		$datatables = app('datatables')->of($report)
				->addColumn('action', function ($report) {
					return '<a onclick="returNoret(' . $report->id . ')" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="' . trans('systems.edit') . '">Lihat Detail</a>';
				});

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('returs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('returs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}
	
	public function exportpdf(Request $req){
	    $keyword = $req['keyword'];
	    $range = explode(":", $req['date']);
	    
        $report = Retur::select([
            'id', 'no_retur', 'no_ref', 'type_retur', 'created_at'
        ]);
        
        if($keyword != "" && $req['date'] != "")
            $clause = '(no_ref LIKE "%'.$keyword.'%" OR no_retur LIKE "%'.$keyword.'%") AND returs.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        elseif($keyword != "" && $req['date'] == "")
            $clause = '(no_ref LIKE "%'.$keyword.'%" OR no_retur LIKE "%'.$keyword.'%")';
        elseif($keyword == "" && $req['date'] != "")
            $clause = 'returs.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        else
            $clause = '';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->orderBy('created_at', 'desc')->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Tanggal</th><th>Nomor Retur</th><th>Nomor Ref</th><th>Type Retur</th></tr>';
        
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->created_at.'</td>';
            $html .= '<td>'.$rep->no_retur.'</td>';
            $html .= '<td>'.$rep->no_ref.'</td>';
            $html .= '<td>'.$rep->type_retur.'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/report-retur-noret/'))
                mkdir('upload/report-retur-noret/', 0777, true);
            
            file_put_contents('upload/report-retur-noret/'.$filename.'.pdf', $pdf);
            $file = 'upload/report-retur-noret/'.$filename.'.pdf';
            
            return $file;
        }
    }
    
	public function show($id) {
		$data = Retur::join('models', 'models.id', '=', 'model_id')
            ->join('customers', 'customers.id', '=', 'customer_id')
            ->join('shops', 'shops.id', '=', 'shop_id')
            ->select(['returs.id', 'no_retur', 'no_ref', DB::Raw('CONCAT(customers.nama, " (", customer_code, ")") AS cus'), 'type_retur', 'qty', 'returs.note',
                      DB::Raw('CONCAT(models.name, " (", model_name, ")") AS modelname'), DB::Raw('shops.name AS shopname'), DB::Raw('price * qty AS cost')])
            ->where(['returs.id' => $id])
            ->first();

		return response()->json([
			'data' => $data,
			'id' => $id,
		], 200);
	}
}
