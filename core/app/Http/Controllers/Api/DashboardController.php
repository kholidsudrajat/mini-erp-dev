<?php

namespace App\Http\Controllers\Api;

use App\RoleUser;
use App\Cashier;
use App\Customer;
use App\Debt;
use App\User;
use App\Payment;
use App\Scheduler;
use App\Stock;
use App\Notification;
use App\NotificationDetail;
use App\Helpers\FormatConverter;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Tymon\JWTAuth\Facades\JWTAuth;

class DashboardController extends Controller
{
    public function index(Request $req)
    {
        if(isset($req['year']))
            $year = $req['year'];
        else
            $year = date('Y');
            
        $customer = DB::Table('v_sales')
            ->leftJoin('customers', 'customers.id', '=', 'customer_id')
            ->where('sales','!=','')
            ->orderBy('sales','DESC')
            ->limit(10)
            ->get();
            
        $product = DB::Table('v_product')
            ->select(['names', DB::Raw('SUM(tqty) AS qty')])
            ->groupBy('id')
            ->orderBy('qty','DESC')
            ->limit(10)
            ->get();
        
        for($i=1; $i<=12; $i++){
            $sales[$i] = Cashier::select([
                DB::Raw('SUM(subtotal) AS totals')
            ])->whereRaw('MONTH(created_at) = "'.$i.'" AND YEAR(created_at) = "'.$year.'"')->first();
        }
        
        $getrange = Cashier::select(DB::Raw('MIN(YEAR(created_at)) AS mins'), DB::Raw('MAX(YEAR(created_at)) AS maks'))->first();
        $zz = $getrange['maks'];
        $yy = $getrange['mins'];
        $years = array();
        for($a=$yy; $a<=$zz; $a++){
            $years[$a] = $a;
        }
        
        return response()->json([
            'customer'  =>  $customer,
            'product'   =>  $product,
            'sales'     =>  $sales,
            'years'     =>  $years
        ], 200);
    }
}
