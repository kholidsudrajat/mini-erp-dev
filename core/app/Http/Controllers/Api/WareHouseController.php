<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Shop;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Session;
use Datatables;
use DB;
use Auth;
use PDF;
use App\Helpers\FormatConverter;

class WareHouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
		DB::statement(DB::raw('set @rownum=0'));
        $warehouse = Shop::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'name'])->whereIsWarehouse('1');

         $datatables = app('datatables')->of($warehouse)
            ->addColumn('action', function ($warehouse) {
                return '<a onclick="editGudang('.$warehouse->id.')" class="bb btn btn-primary"><i class="fa fa-pencil"></i></a> 
                <a onclick="return deleteData('.$warehouse->id.')" class="bb btn btn-danger"><i class="fa fa-times"></i></a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('shops.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('shops.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
	
	public function exportpdf(Request $req){
	    $keyword = $req['keyword'];
	    $range = explode(":", $req['date']);
	    
        $report = Shop::select(['id', 'name']);
        
        $clause = 'is_warehouse = "1"';
        if($keyword != "" && $req['date'] != "")
            $clause .= ' AND name LIKE "%'.$keyword.'%" AND created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        elseif($keyword != "" && $req['date'] == "")
            $clause .= ' AND name LIKE "%'.$keyword.'%"';
        elseif($keyword == "" && $req['date'] != "")
            $clause .= ' AND created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Nama Gudang</th></tr>';
        
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->name.'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/gudang/'))
                mkdir('upload/gudang/', 0777, true);
            
            file_put_contents('upload/gudang/'.$filename.'.pdf', $pdf);
            $file = 'upload/gudang/'.$filename.'.pdf';
            
            return $file;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
//		if ($user->unique_number != $uniqueNumber || $user->role != User::ROLE_TEACHER || $user->status != User::STATUS_ACTIVE) {
//			return response()->json([
//				'status' => 404,
//				'message' => 'User is not found',
//			], 404);
//		}
//		
		$validators = \Validator::make($request->all(), [
			'name' => 'required',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$model = new Shop();
		$model->fill($request->only(['name']));
		$model->is_warehouse = '1';
		$model->created_at = $model->updated_at = Carbon::now()->toDateTimeString();
		$model->save();
		
		return response()->json([
			'status' => 201,
			'message' => 'Insert Success',
		], 201);
    }
	
	public function show($id)
	{
		$model = Shop::whereId($id)->first();
		if (!$model) {
			return response()->json([
				'status' => 404,
				'message' => 'Shop is not found',
			], 404);
		}
				
		return response()->json($model, 200);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
//		if ($user->unique_number != $uniqueNumber || $user->role != User::ROLE_TEACHER || $user->status != User::STATUS_ACTIVE) {
//			return response()->json([
//				'status' => 404,
//				'message' => 'User is not found',
//			], 404);
//		}
//		
		$validators = \Validator::make($request->all(), [
			'name' => 'required',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$model = Shop::findOrFail($id);
		$model->fill($request->only(['name']));
		$model->is_warehouse = '1';
		$model->updated_at = Carbon::now()->toDateTimeString();
		$model->save();
		
		return response()->json([
			'status' => 200,
			'message' => 'Update Success',
		], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Shop::destroy($id);

        return response()->json([
			'status' => 200,
			'message' => 'Delete Success',
		], 200);
    }
}
