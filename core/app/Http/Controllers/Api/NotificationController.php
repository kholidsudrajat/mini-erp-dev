<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Notification;
use App\NotificationDetail;
use App\Stock;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $notification = Notification::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'title', 'created_at']);

         $datatables = app('datatables')->of($notification)
            ->editColumn('title', function ($notification) {
                return '<div style="min-width:250px;">'.$notification->title.'</div>';
            })
            ->addColumn('action', function ($notification) {
                return '<a href="javascript:;" onclick="detail('.$notification->id.')" class="btn btn-primary">Lihat Detail</a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('notifications.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('notifications.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
	
	public function show($id)
    {
        $notification = Notification::findOrFail($id);
        
        $notification->update(['isread' => '1']);
        
        $detail = NotificationDetail::join('models', 'models.id', '=', 'model_id')
            ->join('shops', 'shops.id', '=', 'shop_id')
            ->select([
                DB::Raw('shops.name AS shop_name'), DB::Raw('models.name AS model_name'), 'qty'
            ])
            ->whereNotifId($id)
            ->get();

        return response()->json([
            'notification'  => $notification,
            'detail'        => $detail
        ]);
    }
}
