<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use App\Models;
use App\Shop;
use App\StockMovement;
use Illuminate\Http\Request;
use Illuminate\View\View;
use DB;
use PDF;

class ReportStockMovementController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return View
	 */
	public function index() {
		$prod = Models::count();

		$report = StockMovement::select([
					DB::Raw('IFNULL(SUM(stock_in),0) AS tmasuk'), DB::Raw('IFNULL(SUM(stock_out),0) AS tkeluar')
				])
				->first();

		return response()->json([
			'prod' => $prod,
			'report' => $report,
		], 200);
	}
	
	public function exportpdf(Request $req){
	    $keyword = $req['keyword'];
	    $range = explode(":", $req['date']);
	    
        $report = StockMovement::select(['name', 'model_id', DB::Raw('IFNULL(SUM(stock_in),0) AS stock_in'),
					DB::Raw('IFNULL(SUM(stock_out),0) AS stock_out')]);
        
        if($keyword != "" && $req['date'] != "")
            $clause = 'name LIKE "%'.$keyword.'%" AND stock_movement.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        elseif($keyword != "" && $req['date'] == "")
            $clause = 'name LIKE "%'.$keyword.'%"';
        elseif($keyword == "" && $req['date'] != "")
            $clause = 'stock_movement.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        else
            $clause = '';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->groupBy('model_id')->orderBy('name')->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Nama Barang</th><th>Barang Masuk</th><th>Barang Keluar</th></tr>';
        
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->name.'</td>';
            $html .= '<td>'.$rep->stock_in.'</td>';
            $html .= '<td>'.$rep->stock_out.'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/report-stock-movement/'))
                mkdir('upload/report-stock-movement/', 0777, true);
            
            file_put_contents('upload/report-stock-movement/'.$filename.'.pdf', $pdf);
            $file = 'upload/report-stock-movement/'.$filename.'.pdf';
            
            return $file;
        }
    }
	
	public function indexData(Request $request) {
		DB::statement(DB::raw('set @rownum=0'));
		$report = StockMovement::select(['name', 'model_id', DB::Raw('IFNULL(SUM(stock_in),0) AS stock_in'),
					DB::Raw('IFNULL(SUM(stock_out),0) AS stock_out')])
				->groupBy('model_id');

		$datatables = app('datatables')->of($report)
				->addColumn('action', function ($report) {
			return '<a onclick="stockMovement(' . $report->model_id . ')" href="javascript:;" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="' . trans('systems.edit') . '">Lihat Detail</a>';
		});

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('stock_movement.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('stock_movement.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}

	public function show($id, Request $req) {
		$limit = 5;
		$model = Models::findOrFail($id);

		$stock = DB::table('stock_by_shop')->select([DB::Raw('SUM(qtys) AS qtys'), DB::Raw('SUM(qtyw) AS qtyw')])->whereModelId($id)->first();

		$report = StockMovement::select(['type', 'transfer_from', 'transfer_to', DB::Raw('IFNULL(stock_in,0) AS stock_in'), DB::Raw('IFNULL(stock_out,0) AS stock_out'), DB::Raw('IFNULL(stock_shop,0) AS stock_shop'), DB::Raw('IFNULL(stock_warehouse,0) AS stock_warehouse'), 'created_at'])->whereModelId($id);

		$total = count($report->get());

		$report = $report->limit($limit);
		$report = $report->orderBy('created_at','DESC');
		$report = $report->get();

		$mod = $total % $limit;
		$pagin = intval($total / $limit);
		if ($mod > 0)
			$pagin = $pagin + 1;

		if ($pagin == 1)
			$pagin = 0;

		$shop = Shop::pluck('name', 'id');
		
		return response()->json([
			'model' => $model,
			'report' => $report,
			'stock' => $stock,
			'shop' => $shop,
			'pagin' => $pagin,
			'total' => $total,
		], 200);
	}

	public function filter($id, Request $req) {
		$limit = $req['limit'];
		$page = ($req['page'] - 1) * $limit;

		$table = "";

		$report = StockMovement::select(['type', 'transfer_from', 'transfer_to', DB::Raw('IFNULL(stock_in,0) AS stock_in'),
					DB::Raw('IFNULL(stock_out,0) AS stock_out'), DB::Raw('IFNULL(stock_shop,0) AS stock_shop'),
					DB::Raw('IFNULL(stock_warehouse,0) AS stock_warehouse'), 'created_at'])
				->whereModelId($id)
				->limit($limit)
				->offset($page);

		$shop = Shop::pluck('name', 'id');

		if ($req['range'] != "") {
			$rang = explode(":", $req['range']);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$report->whereBetween('stock_movement.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$report->whereBetween('stock_movement.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		if ($req['type'] != "") {
			if ($req['type'] != "semua")
				$report->whereType($req['type']);
		}

		if ($req['sort'] != "") {
			if ($req['asc'] == 1)
				$report->orderBy($req['sort'], 'DESC');
			else
				$report->orderBy($req['sort'], 'ASC');
		}else {
			$report->orderBy('created_at', 'ASC');
		}

		$report = $report->get();

		$i = $page + 1;
		foreach ($report as $rpt) {
			$to = $rpt['transfer_to'];
			$from = $rpt['transfer_from'];

			$table .= '
                <tr>
                    <td>' . $i++ . '</td>
                    <td>' . $rpt['created_at'] . '</td>';

			if ($from != 0)
				$table .= '<td>' . $shop[$from] . '</td>';
			else
				$table .= '<td> Supplier / Produksi </td>';

			if ($to != 0)
				$table .= '<td>' . $shop[$to] . '</td>';
			else
				$table .= '<td> Penjualan </td>';

			$table .= '<td>';
			if ($rpt['type'] == "in")
				$table .= $rpt['stock_in'];
			elseif ($rpt['type'] == "out")
				$table .= $rpt['stock_out'];
			elseif ($rpt['type'] == "toshop")
				$table .= $rpt['stock_shop'];
			elseif ($rpt['type'] == "towarehouse")
				$table .= $rpt['stock_warehouse'];
			$table .= '</td>';

			$table .= '<td>';
			if ($rpt['type'] == "in")
				$table .= 'Barang Masuk';
			elseif ($rpt['type'] == "out")
				$table .= 'Barang Keluar (Dibeli)';
			elseif ($rpt['type'] == "toshop")
				$table .= 'Barang Masuk ke Toko';
			elseif ($rpt['type'] == "towarehouse")
				$table .= 'Barang Masuk ke Gudang';
			$table .= '</td>';

			$table .= '</tr>';
		}

		$data[0] = $table;
		$data[1] = count($report);

		return $data;
	}
}
