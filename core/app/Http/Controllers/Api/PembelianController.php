<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FormatConverter;
use App\Models;
use App\Order;
use App\OrderDetail;
use App\PaymentMethod;
use App\Shipping;
use App\SupplierOrder;
use DB;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Tymon\JWTAuth\Facades\JWTAuth;

class PembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function form()
    {
        $supplier = SupplierOrder::pluck('nama', 'id');
        $model = Models::select(['id', DB::Raw('CONCAT(name, " - (", model_name, ")") AS name')])->pluck('name','id');
        
        $shipping = Shipping::get();
        $payment = PaymentMethod::get();
		
		return response()->json([
			'supplier' => $supplier,
			'model' => $model,
			'shipping' => $shipping,
			'payment' => $payment,
		], 200);
    }
    
    public function submit(Request $req)
    {
		$validators = \Validator::make($req->all(), [
			'po_number' => 'required|unique:orders',
			'supplier_id' => 'required',
			'shipping_type' => 'required',
			'shipping_id' => 'required',
			'payment_type' => 'required',
			'total' => 'required',
			'json' => 'required',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$cek = Order::wherePoNumber($req['po_number'])->count();
        if($cek != 0)
            return response()->json([
			'status' => 404,
			'message' => 'PO number is not fount',
		], 404);
        
        $data['po_number'] = $req['po_number'];
        $data['supplier_id'] = $req['supplier_id'];
        $data['shipping_type'] = $req['shipping_type'];
        $data['shipping_id'] = $req['shipping_id'];
        $data['payment_type'] = $req['payment_type'];
        $data['total'] = $req['total'];
        $data['dates'] = date('Y-m-d H:i:s');
        
        $id = Order::insertGetId($data);
        
        $json = '['.substr($req['json'],0,-1).']';
        $json = json_decode($json);
        foreach($json as $js){
            $datas['order_id'] = $id;
            $datas['model_id'] = $js->model_id;
            $datas['qty'] = $js->qty;
            $datas['price'] = $js->price;
            $datas['total'] = $js->total;
            
            OrderDetail::create($datas);
        }
		
        return response()->json([
			'status' => 201,
			'message' => 'Insert success',
		], 201);
    }
    
    
    public function getmodel(Request $req)
    {
        $model = Models::findOrFail($req['model']);
        
        return response()->json($model, 200);
    }
}