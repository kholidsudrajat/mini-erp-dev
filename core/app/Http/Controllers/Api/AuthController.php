<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FormatConverter;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller 
{
	/**
	 * login credentials
	 * 
	 * @param Request $request
	 * @return json
	 */
	public function login(Request $request, Response $response)
	{
		$credentials = $request->only(['username', 'password']);

		$validators = \Validator::make($request->all(), [
			'username' => 'required',
			'password' => 'required|min:6',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		try {
			if (!$token = JWTAuth::attempt($credentials)) {
				return response()->json([
					'status' => 401,
					'message' => 'Invalid Credentials',
				], 401);
			}
		} catch (Exception $ex) {
			return response()->json([
				'status' => 400,
				'message' => 'Could Not Create Token'
			], 400);
		}
		
		$user = User::whereUsername($request->username)
			->first();
		if (!$user) {
			return response()->json([
				'status' => 401,
				'message' => 'Invalid Credentials'
			], 401);
		}
		
		$token = JWTAuth::fromUser($user);
		$user->updated_at = Carbon::now()->toDateTimeString();
		$user->save();
		$user['token'] = $token;
		
		return response()->json([
			'status' => 200,
			'message' => 'Login Success',
			'data' => $user,
		], 200);
	}
	
	/**
	 * @param Request $request
	 * @return type
	 */
    public function logout(Request $request)
	{
		$user = JWTAuth::parseToken()->authenticate();
		JWTAuth::parseToken()->invalidate();
		
        return response()->json([
			'status' => 200,
			'message' => 'Logout is success',
		], 200);
    }
}