<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FormatConverter;
use App\Http\Controllers\Controller;
use App\RoleMenu;
use App\RoleUser;
use App\User;
use DB;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Session;

class RoleUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $roleuser = RoleUser::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'role']);

        $datatables = app('datatables')->of($roleuser)
            ->addColumn('action', function ($roleuser) {
                return '<a onclick="editRole('.$roleuser->id.')" href="javascript:;;" class="btn-xs btn btn-primary"><i class="fa fa-pencil"></i></a> 
                <a onclick="return deleteData('.$roleuser->id.')" class="btn-xs btn btn-danger"><i class="fa fa-times"></i></a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('costs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('costs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }

	/**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $validators = \Validator::make($request->all(), [
          'role' => 'required|max:255',
        ]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$requestData = $request->all();
        $requestData['slug'] = str_slug($request['role'], '-');

        $post = RoleUser::create($requestData);

        return response()->json([
			'status' => 201,
			'message' => 'Insert Success',
		], 201);
    }

    public function show($id)
    {
        $user = RoleUser::findOrFail($id);

        $role = RoleMenu::get();

		return response()->json([
			'user' => $user,
			'role' => $role,
		], 200);
    }

    public function update($id, Request $request)
    {
        $user = RoleUser::whereId($id)->first();
		
		$validators = \Validator::make($request->all(), [
          'role_menu' => 'required',
        ]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
        if(isset($request->role_menu)) {
            $role_menu = $request->role_menu;
            $request['role_menu'] = implode(",", $role_menu);
        }
		
        $requestData = $request->all();

        $user->update($requestData);

        return response()->json([
			'status' => 200,
			'message' => 'Update Success',
		], 200);
    }

    public function destroy($id)
    {
        RoleUser::destroy($id);

        return response()->json([
			'status' => 200,
			'message' => 'Delete Success',
		], 200);
    }
	
	
	public function pluck(Request $request)
	{
		$value = empty($request->value) ? 'role' : $request->value;
		$id = empty($request->id) ? 'id' : $request->id;
		$shop = RoleUser::pluck($value, $id)->prepend('Pilih Role', '0');
		
		return response()->json($shop, 200);
	}
}