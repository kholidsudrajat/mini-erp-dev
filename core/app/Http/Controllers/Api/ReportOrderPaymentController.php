<?php

namespace App\Http\Controllers\Api;

use App\Order;
use App\OrderDetail;
use App\OrderPayment;
use App\SupplierOrder;
use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use PDF;
use Auth;

class ReportOrderPaymentController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index(Request $request) {
        $order = OrderPayment::join('orders','orders.id','=','order_id')
            ->join('supplier_orders','supplier_orders.id','=','supplier_id')
            ->join('payment_method','payment_method.id','=','payment_type')
            ->select([
                'order_payments.id', 'inv_number', 'po_number', 'nama', 'name', 'order_payments.total'
            ]);

         $datatables = app('datatables')->of($order)
            ->addColumn('action', function ($order) {
                return '<a onclick="orderPayment('.$order->id.')" href="javascript:;;" class="bb btn btn-primary">Detail</a>';
            })
            ->editColumn('total', function ($order){
                return str_replace(',','.',number_format($order->total));
            });
        
		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}
        
	        if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('order_payments.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('order_payments.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}
	
	public function exportpdf(Request $req){
	    $keyword = $req['keyword'];
	    $range = explode(":", $req['date']);
	    
        $report = OrderPayment::join('orders','orders.id','=','order_id')
            ->join('supplier_orders','supplier_orders.id','=','supplier_id')
            ->join('payment_method','payment_method.id','=','payment_type')
            ->select([
                'order_payments.id', 'inv_number', 'po_number', 'nama', 'name', 'order_payments.total'
            ]);
        
        if($keyword != "" && $req['date'] != "")
            $clause = '(inv_number LIKE "%'.$keyword.'%" OR po_number LIKE "%'.$keyword.'%" OR nama LIKE "%'.$keyword.'%") AND order_payments.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        elseif($keyword != "" && $req['date'] == "")
            $clause = '(inv_number LIKE "%'.$keyword.'%" OR po_number LIKE "%'.$keyword.'%" OR nama LIKE "%'.$keyword.'%")';
        elseif($keyword == "" && $req['date'] != "")
            $clause = 'order_payments.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        else
            $clause = '';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->orderBy('inv_number')->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Nomor Invoice</th><th>Nomor PO</th><th>Nama Supplier</th><th>Payment</th><th>Total</th></tr>';
        
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->inv_number.'</td>';
            $html .= '<td>'.$rep->po_number.'</td>';
            $html .= '<td>'.$rep->nama.'</td>';
            $html .= '<td>'.$rep->name.'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep->total)).'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/report-order-payment/'))
                mkdir('upload/report-order-payment/', 0777, true);
            
            file_put_contents('upload/report-order-payment/'.$filename.'.pdf', $pdf);
            $file = 'upload/report-order-payment/'.$filename.'.pdf';
            
            return $file;
        }
    }
    
	public function show($id) {
        $order = OrderPayment::join('orders', 'orders.id', '=', 'order_id')
            ->select([
                'order_payments.total', 'po_number', 'inv_number', 'dates', 'supplier_id'
            ])
            ->where('order_payments.id',$id)
            ->first();
        
        $supplier = SupplierOrder::findOrFail($order->supplier_id);
        
		return response()->json([
			'order' => $order,
			'supplier' => $supplier,
		], 200);
	}
}
