<?php

namespace App\Http\Controllers\Api;

use App\Shop;
use DB;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ReportProfitLossController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return View
	 */
	public function index(Request $req) {		
        $mulai = $req['mulai'];
        $sampai = $req['sampai'];
        
        $profit = DB::Table('v_profit')
            ->select([
                DB::Raw('SUM(pendapatan) AS pendapatan'), DB::Raw('SUM(modal) AS modal'), DB::Raw('SUM(profit) AS profit'), 
                'name', 'shop_id'
            ]);
        
        if($mulai != '' && $sampai != '')
            $profit = $profit->whereRaw('tanggal BETWEEN "'.$mulai.'" AND "'.$sampai.'"');
        
        $profit = $profit->groupBy('shop_id')->get();
        
        $loss = DB::Table('v_loss')
            ->select([
                DB::Raw('SUM(pengeluaran) AS pengeluaran'), 'name', 'date', 'shop_id'
            ]);
        
        if($mulai != '' && $sampai != '')
            $loss = $loss->whereRaw('date BETWEEN "'.$mulai.'" AND "'.$sampai.'"');
        
        $loss = $loss->groupBy('shop_id')->get();
        
        $shop = Shop::whereIsShop('1')->get();
        
		return response()->json([
			'profit' => $profit,
			'loss' => $loss,
			'shop' => $shop,
		], 200);
	}
    
	public function filter(Request $req) {
        $shop = Shop::whereIsShop('1')->get();		
        $mulai = $req['mulai'];
        $sampai = $req['sampai'];
        
        $profit = DB::Table('v_profit')
            ->select([
                DB::Raw('SUM(pendapatan) AS pendapatan'), DB::Raw('SUM(modal) AS modal'), DB::Raw('SUM(profit) AS profit'), 
                'name', 'shop_id'
            ]);
        
        if($mulai != '' && $sampai != '')
            $profit = $profit->whereRaw('tanggal BETWEEN "'.$mulai.'" AND "'.$sampai.'"');
        
        $profit = $profit->groupBy('shop_id')->get();
        
        $loss = DB::Table('v_loss')
            ->select([
                DB::Raw('SUM(pengeluaran) AS pengeluaran'), 'name', 'date', 'shop_id'
            ]);
        
        if($mulai != '' && $sampai != '')
            $loss = $loss->whereRaw('date BETWEEN "'.$mulai.'" AND "'.$sampai.'"');
        
        $loss = $loss->groupBy('shop_id')->get();
        
        $tpr = array();
        $tpe = array();
        $netprofit = array();
        foreach($profit as $pr){
            $tpr[$pr->shop_id] = $pr->profit;
        }
        foreach($loss as $lo){
            $tpe[$lo->shop_id] = $lo->pengeluaran;
        }
        foreach($shop as $sh){
            $netprofit[$sh->id] = 0;
            $pl = 0;
            if(isset($tpr[$sh->id]))
                $pl = $tpr[$sh->id];
            $mi = 0;
            if(isset($tpe[$sh->id]))
                $mi = $tpe[$sh->id];
            
            $netprofit[$sh->id] += $pl - $mi;
        }
        //////////////////////////
        $plus = DB::Table('v_profit')
            ->select(DB::Raw('SUM(profit) AS profit'));
            
        if($mulai != '' && $sampai != '')
            $plus = $plus->whereRaw('tanggal BETWEEN "'.$mulai.'" AND "'.$sampai.'"');
        
        $plus = $plus->first();
        $plus = $plus->profit;
        
        $minus = DB::Table('v_loss')
            ->select(DB::Raw('SUM(pengeluaran) AS pengeluaran'));
            
        if($mulai != '' && $sampai != '')
            $minus = $minus->whereRaw('date BETWEEN "'.$mulai.'" AND "'.$sampai.'"');
        
        $minus = $minus->first();
        $minus = $minus->pengeluaran;
        
        $data['profit'] = $profit;
        $data['loss'] = $loss;
        $data['total'] = $plus - $minus;
        $data['netprofit'] = $netprofit;
        $data['shop'] = $shop;
        
		return $data;
	}
}