<?php

namespace App\Http\Controllers\Api;

use App\Order;
use App\OrderDelivery;
use App\OrderDeliveryDetail;
use App\SupplierOrder;
use DB;
use PDF;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ReportOrderDeliveryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return View
	 */
	public function index(Request $request) {
        $order = Order::join('order_delivery', 'order_delivery.order_id', '=', 'orders.id')
            ->join('order_delivery_details', 'order_delivery_details.order_delivery_id', '=', 'order_delivery.id')
            ->join('supplier_orders', 'supplier_orders.id', '=', 'supplier_id')
            ->select([
                'order_delivery.id', 'do_number', 'po_number', 'nama', DB::Raw('SUM(order_delivery_details.qty) AS qty')
            ])
            ->groupBy('order_delivery.id');

         $datatables = app('datatables')->of($order)
            ->addColumn('action', function ($order) {
                return '<a onclick="orderDelivery('.$order->id.')" href="javsacript:;" class="bb btn btn-primary">Detail</a>';
            });
        
		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}
        
	        if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('order_delivery.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('order_delivery.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}
	
	public function exportpdf(Request $req){
	    $keyword = $req['keyword'];
	    $range = explode(":", $req['date']);
	    
        $report = Order::join('order_delivery', 'order_delivery.order_id', '=', 'orders.id')
            ->join('order_delivery_details', 'order_delivery_details.order_delivery_id', '=', 'order_delivery.id')
            ->join('supplier_orders', 'supplier_orders.id', '=', 'supplier_id')
            ->select([
                'order_delivery.id', 'do_number', 'po_number', 'nama', DB::Raw('SUM(order_delivery_details.qty) AS qty')
            ]);
        
        if($keyword != "" && $req['date'] != "")
            $clause = '(do_number LIKE "%'.$keyword.'%" OR po_number LIKE "%'.$keyword.'%" OR nama LIKE "%'.$keyword.'%") AND order_delivery.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        elseif($keyword != "" && $req['date'] == "")
            $clause = '(do_number LIKE "%'.$keyword.'%" OR po_number LIKE "%'.$keyword.'%" OR nama LIKE "%'.$keyword.'%")';
        elseif($keyword == "" && $req['date'] != "")
            $clause = 'order_delivery.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        else
            $clause = '';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->groupBy('order_delivery.id')->orderBy('do_number')->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Nomor DO</th><th>Nomor PO</th><th>Nama Supplier</th><th>QTY</th></tr>';
        
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->do_number.'</td>';
            $html .= '<td>'.$rep->po_number.'</td>';
            $html .= '<td>'.$rep->nama.'</td>';
            $html .= '<td>'.$rep->qty.'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/report-order-delivery/'))
                mkdir('upload/report-order-delivery/', 0777, true);
            
            file_put_contents('upload/report-order-delivery/'.$filename.'.pdf', $pdf);
            $file = 'upload/report-order-delivery/'.$filename.'.pdf';
            
            return $file;
        }
    }
    
	public function show($id) {
        $order = OrderDelivery::join('orders', 'orders.id', '=', 'order_id')
            ->where('order_delivery.id',$id)
            ->first();
        
        $supplier = SupplierOrder::findOrFail($order->supplier_id);
        
        $do = OrderDelivery::join('orders', 'orders.id', '=', 'order_id')
            ->select([
                'orders.id', 'supplier_id', 'po_number', 'do_number', 'order_delivery.created_at'
            ])
            ->where('order_delivery.id', $id)
            ->first();
        
        $detail = OrderDeliveryDetail::join('models', 'models.id', '=', 'model_id')
            ->join('shops', 'shops.id', '=', 'shop_id')
            ->select([
                DB::Raw('CONCAT(models.name, " - (", model_name, ")") AS produk'), 'qty', DB::Raw('shops.name AS shop'), 'retur'
            ])
            ->whereOrderDeliveryId($id)
            ->get();
        
		return response()->json([
			'supplier' => $supplier,
			'order' => $order,
			'do' => $do,
			'detail' => $detail
		], 200);
	}
}
