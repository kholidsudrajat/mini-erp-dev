<?php

namespace App\Http\Controllers\Api;

use App\Cashier;
use App\CashierDetail;
use DB;
use PDF;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ReportInvoiceController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return View
	 */
	public function index() {
		$cas = Cashier::count();

		$report = Cashier::join('cashier_details', 'cashier.id', '=', 'cashier_id')
				->join('models', 'models.id', '=', 'model_id')
				->select([DB::Raw('SUM(qty * total_cost) AS tmodal'), DB::Raw('SUM(qty * price) AS tharga'),
					DB::Raw('SUM(qty * diff) AS tprofit')])
				->first();

		return response()->json([
			'cas' => $cas,
			'report' => $report,
		], 200);
	}
	
	public function exportpdf(Request $req){
	    $inv = $req['inv'];
	    $clause = '';
	    if($inv != '')
	        $clause = 'no_ref LIKE "%'.$inv.'%"';
	    
        $report = Cashier::join('cashier_details', 'cashier.id', '=', 'cashier_id')
				->join('models', 'models.id', '=', 'model_id')
				->select(['cashier.id', 'no_ref', 'cashier.created_at', DB::Raw('SUM(qty * total_cost) AS costs'), DB::Raw('SUM(discount) AS discount'),
				DB::Raw('SUM(qty * price) AS prices'), DB::Raw('(SUM(qty * price) - SUM(qty * total_cost) - SUM(discount)) AS diffs')]);
				
		if($clause != '')
		    $report = $report->whereRaw($clause);
		
		$report = $report->groupBy('cashier_id')->orderBy('no_ref')->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>No Ref</th><th>Harga Jual</th><th>Modal</th><th>Diskon</th><th>Profit</th><th>Tanggal</th></tr>';
        
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->no_ref.'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep->prices)).'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep->costs)).'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep->discount)).'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep->diffs)).'</td>';
            $html .= '<td>'.$rep->created_at.'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/report-sales-invoice/'))
                mkdir('upload/report-sales-invoice/', 0777, true);
            
            file_put_contents('upload/report-sales-invoice/'.$filename.'.pdf', $pdf);
            $file = 'upload/report-sales-invoice/'.$filename.'.pdf';
            
            return $file;
        }
    }
	
	public function indexData(Request $request) {
		DB::statement(DB::raw('set @rownum=0'));
		$list_penjualan = Cashier::join('cashier_details', 'cashier.id', '=', 'cashier_id')
				->join('models', 'models.id', '=', 'model_id')
				->select(['cashier.id', 'no_ref', 'cashier.created_at', DB::Raw('SUM(qty * total_cost) AS costs'), DB::Raw('SUM(discount) AS discount'),
				DB::Raw('SUM(qty * price) AS prices'), DB::Raw('(SUM(qty * price) - SUM(qty * total_cost) - SUM(discount)) AS diffs')])
				->groupBy('cashier_id');

		$datatables = app('datatables')->of($list_penjualan)
				->addColumn('action', function ($list_penjualan) {
					return '<a onclick="salesInvoice(' . $list_penjualan->id . ')" href="javascript:;;" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="' . trans('systems.edit') . '">Lihat Detail</a>';
				})
				->editColumn('costs', function($report) {
					return str_replace(',', '.', number_format($report->costs));
				})
		                ->editColumn('discount', function($report) {
		                   	return str_replace(',', '.', number_format($report->discount));
		            	})
				->editColumn('prices', function($report) {
					return str_replace(',', '.', number_format($report->prices));
				})
				->editColumn('diffs', function($report) {
                    return str_replace(',', '.', number_format($report->diffs));
                });

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('cashier.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('cashier.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}

	public function show($id) {
		$limit = 5;
		$list_penjualan = Cashier::join('customers', 'customers.id', '=', 'customer_id')->select('cashier.id', 'cashier.created_at', 'no_ref', 'nama', 'total', 'subtotal', 'discount')->where('cashier.id', $id)->first();

		$detail = CashierDetail::join('models', 'models.id', '=', 'model_id')->select('cashier_details.id', 'model_name', 'name', 'cashier_details.created_at', 'qty', 'price', 'total_cost', 'diff')->whereCashierId($id);

		$total = count($detail->get());

		$detail = $detail->limit($limit);
		$detail = $detail->get();

		$mod = $total % $limit;
		$pagin = intval($total / $limit);
		if ($mod > 0)
			$pagin = $pagin + 1;

		if ($pagin == 1)
			$pagin = 0;

		return response()->json([
			'list_penjualan' => $list_penjualan,
			'detail' => $detail,
			'pagin' => $pagin,
			'total' => $total,
		], 200);
	}

	public function filter($id, Request $req) {
		$limit = $req['limit'];
		$page = ($req['page'] - 1) * $limit;

		$table = "";

		$report = CashierDetail::join('models', 'models.id', '=', 'model_id')
				->select([
					'models.id', 'name', 'model_name', 'cashier_id', 'qty',
					DB::Raw('qty * total_cost AS cost'), DB::Raw('qty * price AS price'),
					DB::Raw('qty * diff AS diff'), 'cashier_details.created_at'])
				->whereCashierId($id)
				->limit($limit)
				->offset($page);

		if ($req['keyword'] != "") {
			$report->whereRaw('(model_name LIKE "%' . $req['keyword'] . '%" OR name LIKE "%' . $req['keyword'] . '%")');
		}

		if ($req['sort'] != "") {
			if ($req['asc'] == 1)
				$report->orderBy($req['sort'], 'DESC');
			else
				$report->orderBy($req['sort'], 'ASC');
		}

		$report = $report->get();

		$i = $page + 1;
		$qty = 0;
		$cost = 0;
		$price = 0;
		$diff = 0;
		foreach ($report as $rpt) {
			$table .= '
                <tr><td>' . $i++ . '</td>
                    <td>' . $rpt['name'] . '</td>
                    <td>' . $rpt['model_name'] . '</td>
                    <td>' . $rpt['qty'] . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt['cost'])) . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt['price'])) . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt['diff'])) . '</td>
                </tr>
                ';

			$qty += $rpt['qty'];
			$cost += $rpt['cost'];
			$price += $rpt['price'];
			$diff += $rpt['diff'];
		}

		$data[0] = $table;
		$data[1] = count($report);

		return $data;
	}

}
