<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use App\Models;
use App\Retur;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use PDF;

class ReturProductController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return View
	 */
	public function index(Request $request) {
		$report = DB::Table('v_retur_product')
            ->select(['model_id', 'names', DB::Raw('SUM(qty) AS qty'), DB::Raw('SUM(tmodal) AS tmodal'), DB::Raw('SUM(tharga) AS tharga'), 
                      DB::Raw('SUM(tprofit) AS tprofit')])
            ->groupBy('model_id');

		$datatables = app('datatables')->of($report)
				->addColumn('action', function ($report) {
					return '<a onclick="returProduct(' . $report->model_id . ')" href="javascript:;;" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="' . trans('systems.edit') . '">Lihat Detail</a>';
				})
				->editColumn('tmodal', function($report) {
					return str_replace(',', '.', number_format($report->tmodal));
				})
				->editColumn('tharga', function($report) {
					return str_replace(',', '.', number_format($report->tharga));
				})
				->editColumn('tprofit', function($report) {
                    return str_replace(',', '.', number_format($report->tprofit));
                });

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('v_retur_product.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('v_retur_product.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}
	
	public function exportpdf(Request $req){
	    $keyword = $req['keyword'];
	    $range = explode(":", $req['date']);
	    
        $report = DB::Table('v_retur_product')
            ->select(['model_id', 'names', DB::Raw('SUM(qty) AS qty'), DB::Raw('SUM(tmodal) AS tmodal'), DB::Raw('SUM(tharga) AS tharga'), 
                      DB::Raw('SUM(tprofit) AS tprofit')]);
        
        if($keyword != "" && $req['date'] != "")
            $clause = 'names LIKE "%'.$keyword.'%" AND v_retur_product.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        elseif($keyword != "" && $req['date'] == "")
            $clause = 'names LIKE "%'.$keyword.'%"';
        elseif($keyword == "" && $req['date'] != "")
            $clause = 'v_retur_product.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        else
            $clause = '';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->groupBy('model_id')->orderBy('names')->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Nama Barang</th><th>QTY</th><th>Modal</th><th>Harga Jual</th><th>Profit</th></tr>';
        
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->names.'</td>';
            $html .= '<td>'.$rep->qty.'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep->tmodal)).'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep->tharga)).'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep->tprofit)).'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/report-retur-product/'))
                mkdir('upload/report-retur-product/', 0777, true);
            
            file_put_contents('upload/report-retur-product/'.$filename.'.pdf', $pdf);
            $file = 'upload/report-retur-product/'.$filename.'.pdf';
            
            return $file;
        }
    }

	public function show($id, Request $req) {
		$limit = 5;
		$model = Models::findOrFail($id);
		$report = Retur::join('models', 'models.id', '=', 'model_id')
                ->join('customers', 'customer_id', '=', 'customers.id')
				->select([
					'models.id', DB::raw('CONCAT(name, " (", model_name,")") AS names'), 'qty', 'no_ref', 'no_retur', 'type_retur', 'returs.note',
                    DB::Raw('qty * total_cost AS cost'), DB::Raw('qty * price AS price'), DB::raw('CONCAT(nama, " (", customer_code,")") AS cus'),
					DB::Raw('qty * diff AS diff'), 'returs.created_at'])
				->whereModelId($id);

		$total = count($report->get());

		$report = $report->limit($limit);
		$report = $report->get();

		$mod = $total % $limit;
		$pagin = intval($total / $limit);
		if ($mod > 0)
			$pagin = $pagin + 1;

		if ($pagin == 1)
			$pagin = 0;
		
		return response()->json([
			'model' => $model,
			'report' => $report,
			'pagin' => $pagin,
			'total' => $total
		], 200);
	}

	public function shows($id, $noret) {
		$data = Retur::join('models', 'models.id', '=', 'model_id')
            ->join('customers', 'customers.id', '=', 'customer_id')
            ->join('shops', 'shops.id', '=', 'shop_id')
            ->select(['returs.id', 'no_retur', 'no_ref', DB::Raw('CONCAT(customers.nama, " (", customer_code, ")") AS cus'), 'type_retur', 'qty', 'returs.note',
                      DB::Raw('CONCAT(models.name, " (", model_name, ")") AS modelname'), DB::Raw('shops.name AS shopname'), DB::Raw('price * qty AS cost')])
            ->whereNoRetur($noret)
            ->first();
		
		return response()->json([
			'data' => $data,
			'id' => $id,
		], 200);
	}

	public function filter($id, Request $req) {
		$limit = $req['limit'];
		$page = ($req['page'] - 1) * $limit;

		$table = "";

		$report = Retur::join('models', 'models.id', '=', 'model_id')
                ->join('customers', 'customer_id', '=', 'customers.id')
				->select([
					'models.id', DB::raw('CONCAT(name, " (", model_name,")") AS names'), 'qty', 'no_ref', 'no_retur', 'type_retur', 'returs.note as note',
                    DB::Raw('qty * total_cost AS cost'), DB::Raw('qty * price AS price'), DB::raw('CONCAT(nama, " (", customer_code,")") AS cus'),
					DB::Raw('qty * diff AS diff'), 'returs.created_at'])
				->whereModelId($id)
				->limit($limit)
				->offset($page);

		if ($req['range'] != "") {
			$rang = explode(":", $req['range']);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$report->whereBetween('returs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$report->whereBetween('returs.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		if ($req['sort'] != "") {
			if ($req['asc'] == 1)
				$report->orderBy($req['sort'], 'DESC');
			else
				$report->orderBy($req['sort'], 'ASC');
		}else {
			$report->orderBy('no_ref', 'ASC');
		}

		$report = $report->get();

		$i = $page + 1;
		foreach ($report as $rpt) {
			$table .= '
                <tr>
                    <td>'.$i++.'</td>
                    <td>'.$rpt['created_at'].'</td>
                    <td><a href="returProductDetail('.$id.','.$rpt['no_retur'].')" href="javascript:;;">'.$rpt['no_retur'].'</a></td>
                    <td>'.$rpt['no_ref'].'</td>
                    <td>'.$rpt['cus'].'</td>
                    <td>'.$rpt['type_retur'].'</td>
                    <td>'.$rpt['names'].'</td>
                    <td>'.$rpt['qty'].'</td>
                    <td>'.$rpt['note'].'</td>
                </tr>
                ';
		}

		$data[0] = $table;
		$data[1] = count($report);

		return $data;
	}
}
