<?php

namespace App\Http\Controllers\Api;

use App\Order;
use App\OrderDetail;
use App\OrderPayment;
use App\SupplierOrder;
use DB;
use PDF;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ReportOrderSupplierController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return View
	 */
	public function index(Request $request) {
        $report = SupplierOrder::leftjoin('orders', 'supplier_orders.id', '=', 'supplier_id')
            ->leftjoin('order_details', 'order_id', '=', 'orders.id')
            ->select([
                'supplier_orders.id', 'nama', 'no_telp', 'pic', DB::Raw('COUNT(po_number) AS transaksi'), DB::Raw('SUM(order_details.total) AS total'), 
                DB::Raw('SUM(qty) AS qty')
            ])
            ->whereRaw('order_details.total > 0')
            ->groupBy('supplier_orders.id');

		$datatables = app('datatables')->of($report)
            ->addColumn('action', function ($report) {
				return '<a onclick="orderSupplier(' . $report->id . ')" href="javascript:;" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="' . trans('systems.edit') . '">Lihat Detail</a>';
            })
            ->editColumn('qty', function($report){
                return str_replace(',','',number_format($report->qty));
            })
            ->editColumn('total', function($report){
                return str_replace(',','.',number_format($report->total));
            });

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		return $datatables->make(true);
	}

    public function exportpdf(Request $req){
        $report = SupplierOrder::leftjoin('orders', 'supplier_orders.id', '=', 'supplier_id')
            ->leftjoin('order_details', 'order_id', '=', 'orders.id')
            ->select([
                'supplier_orders.id', 'nama', 'no_telp', 'pic', DB::Raw('COUNT(po_number) AS transaksi'), DB::Raw('SUM(order_details.total) AS total'), 
                DB::Raw('SUM(qty) AS qty')
            ])
            ->whereRaw('order_details.total > 0 AND nama LIKE "%'.$req['supp'].'%"')
            ->groupBy('supplier_orders.id')
            ->orderBy('nama')
            ->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Nama Supplier</th><th>No Telp</th><th>PIC</th><th>QTY</th><th>Total</th></tr>';
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep['nama'].'</td>';
            $html .= '<td>'.$rep['no_telp'].'</td>';
            $html .= '<td>'.$rep['pic'].'</td>';
            $html .= '<td>'.$rep['qty'].'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep['total'])).'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/report-order-supplier/'))
                mkdir('upload/report-order-supplier/', 0777, true);
            
            file_put_contents('upload/report-order-supplier/'.$filename.'.pdf', $pdf);
            $file = 'upload/report-order-supplier/'.$filename.'.pdf';
            
            return $file;
        }
    }
    
	public function show($id) {
        $supplier = SupplierOrder::leftjoin('orders', 'supplier_orders.id', '=', 'supplier_id')
            ->leftjoin('order_details', 'order_details.order_id', '=', 'orders.id')
            ->select([
                'supplier_orders.id', 'nama', 'no_telp', 'email', 'pic', 'alamat', DB::Raw('COUNT(po_number) AS transaksi'), 
                DB::Raw('SUM(order_details.total) AS total'), DB::Raw('SUM(order_details.qty) AS qty')
            ])
            ->where('supplier_orders.id',$id)
            ->groupBy('supplier_orders.id')
            ->first();
        
        $order = Order::leftjoin('supplier_orders', 'supplier_orders.id', 'supplier_id')
            ->leftjoin('order_details', 'order_details.order_id', '=', 'orders.id')
            ->leftjoin('shipping', 'shipping.id', '=', 'shipping_id')
            ->leftjoin('payment_method', 'payment_method.id', '=', 'payment_type')
            ->select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), DB::Raw('SUM(order_details.qty) AS qty'), 'orders.id', 'po_number', 'supplier_orders.nama AS supp', 
                'shipping.name AS ship', 'payment_method.name AS paym', 'payment_type', 'shipping_type', 'orders.total', 'status'
            ])
            ->whereSupplierId($id)
            ->groupBy('orders.id')
            ->get();
        
        return response()->json([
			'supplier' => $supplier,
			'order' => $order,
		], 200);
	}

    public function shows($id, $idm){
        $supplier = SupplierOrder::findOrFail($id);
        $order = Order::findOrFail($idm);
        $payment = OrderPayment::whereOrderId($idm)->get();
        $bayar = OrderPayment::select([DB::Raw('SUM(total) AS total')])->whereOrderId($idm)->first();
        $totalnya = $order->total - (0 + $bayar->total);
        
        $detail = OrderDetail::join('models', 'models.id', '=', 'model_id')
            ->select(['model_id', 'name', 'model_name', 'qty', 'order_details.price', 'total'])
            ->whereOrderId($idm)
            ->get();
        
		return response()->json([
			'supplier' => $supplier,
			'order' => $order,
			'detail' => $detail,
			'payment' => $payment,
			'totalnya' => $totalnya,
		], 200);
    }
}
