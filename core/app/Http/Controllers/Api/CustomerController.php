<?php

namespace App\Http\Controllers\Api;

use App\Customer;
use App\CustomerAddress;
use App\Helpers\FormatConverter;
use App\Http\Controllers\Controller;
use App\Shop;
use Carbon\Carbon;
use DB;
use PDF;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Tymon\JWTAuth\Facades\JWTAuth;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(Request $request)
    {
		DB::statement(DB::raw('set @rownum=0'));
        $customer = Customer::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'customer_code', 'nama', 'no_telp', 'limit_debt'])
            ->where('id','!=','0');

         $datatables = app('datatables')->of($customer)
            ->addColumn('action', function ($customer) {
                return '<a onclick="editCustomer('.$customer->id.')"  class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.edit') .'"><i class="fa fa-pencil"></i></a> 
                        <a onclick="deleteData('.$customer->id.')" class="btn btn-xs btn-danger rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.delete') .'"><i class="fa fa-trash"></i></a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('customers.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('customers.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
	
	public function exportpdf(Request $req){
	    $keyword = $req['keyword'];
	    $range = explode(":", $req['date']);
	    
        $report = Customer::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'customer_code', 'nama', 'no_telp', 'limit_debt']);
        
        $clause = 'id != 0 ';
        if($keyword != "" && $req['date'] != "")
            $clause .= '(nama LIKE "%'.$keyword.'%" OR customer_code LIKE "%'.$keyword.'%") AND created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        elseif($keyword != "" && $req['date'] == "")
            $clause .= '(nama LIKE "%'.$keyword.'%" OR customer_code LIKE "%'.$keyword.'%")';
        elseif($keyword == "" && $req['date'] != "")
            $clause .= 'created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Kode Pelanggan</th><th>Nama Pelanggan</th><th>Nomor Telepon</th></tr>';
        
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->customer_code.'</td>';
            $html .= '<td>'.$rep->nama.'</td>';
            $html .= '<td>'.$rep->no_telp.'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/customer/'))
                mkdir('upload/customer/', 0777, true);
            
            file_put_contents('upload/customer/'.$filename.'.pdf', $pdf);
            $file = 'upload/customer/'.$filename.'.pdf';
            
            return $file;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
	$validators = \Validator::make($request->all(), [
		'nama' => 'required',
		'no_telp' => 'required',
		'alamat' => 'required'
	]);
	
	if ($validators->fails()) {
		return response()->json([
			'status' => 400,
			'message' => 'Some parameters is invalid',
			'validators' => FormatConverter::parseValidatorErrors($validators),
		], 400);
	}
	
	$customer = new Customer();
	$customer->fill($request->all());
	$customer->customer_code = Customer::generateCode();
	$customer->created_at = $customer->updated_at = Carbon::now()->toDateTimeString();
	$customer->save();
		
        $id = $customer->id;
        
        $data['customer_id'] = $customer;
        $data['alamat'] = $request['alamat'];
        $data['shipping_id'] = $request['shipping'];
        
        CustomerAddress::create($data);
        
	return response()->json([
		'status' => 201,
		'message' => 'Insert Success',
	], 201);
    }
	
	public function show($id)
	{
		$model = Customer::whereId($id)->first();
        $address = CustomerAddress::join('shipping', 'shipping.id', '=', 'shipping_id')
            ->select([
                'customer_address.id', 'alamat', 'shipping.name'
            ])
            ->whereCustomerId($id)
            ->get();
        
		if (!$model) {
			return response()->json([
				'status' => 404,
				'message' => 'Customer is not found',
			], 404);
		}
				
		return response()->json([
            'model' =>  $model,
            'address'   =>  $address
        ], 200);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function update($id, Request $request)
    {
		$validators = \Validator::make($request->all(), [
			'nama' => 'required',
			'no_telp' => 'required',
			'alamat' => 'required'
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$model = Customer::findOrFail($id);
		$model->fill($request->all());
		$model->updated_at = Carbon::now()->toDateTimeString();
		$model->save();
		
		return response()->json([
			'status' => 200,
			'message' => 'Update Success',
		], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse|Redirector
     */
    public function destroy($id)
    {
        Customer::destroy($id);

        return response()->json([
			'status' => 200,
			'message' => 'Delete Success',
		], 200);
    }

    public function tambah(Request $request)
    {
        $data['customer_id'] = $request['customer_id'];
        $data['alamat'] = $request['alamat'];
        $data['shipping_id'] = $request['shipping'];

        $post = CustomerAddress::create($data);

        return response()->json([
			'status' => 200,
			'message' => 'Insert Success',
		], 200);
    }

    public function hapus(Request $request)
    {
        $id = $request['id'];
        CustomerAddress::destroy($id);

        return response()->json([
			'status' => 200,
			'message' => 'Delete Success',
		], 200);

    }
    
    public function pluck(Request $request)
	{
		$value = empty($request->value) ? 'nama' : $request->value;
		$id = empty($request->id) ? 'id' : $request->id;
		$customer= Customer::pluck($value, $id);
		
		return response()->json($customer, 200);
	}
}
