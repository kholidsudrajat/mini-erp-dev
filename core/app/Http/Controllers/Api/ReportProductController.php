<?php

namespace App\Http\Controllers\Api;

use App\Cashier;
use App\CashierDetail;
use App\Models;
use DB;
use PDF;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ReportProductController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return View
	 */
	public function index() {
		$prod = Models::join('cashier_details', 'models.id', '=', 'model_id')
				->groupBy('model_id')
				->get();
		$prod = count($prod);
		
		$report = Models::join('cashier_details', 'cashier_details.model_id', '=', 'models.id')
				->select([
					DB::Raw('SUM(qty) AS tqty'), DB::Raw('SUM(qty * total_cost) AS tmodal'), 
					DB::Raw('SUM(qty * price) AS tharga'), DB::Raw('SUM(qty * diff) AS tprofit')])
				->first();
				
		return response()->json([
			'prod' => $prod,
			'report' => $report,
		], 200);
	}
	
	public function exportpdf(Request $req){
	    $model = $req['model'];
	    $range = explode(":", $req['date']);
	    
        $report = DB::Table('v_product')->select(['id', 'names', DB::Raw('SUM(tqty) AS tqty'), DB::Raw('SUM(tmodal) AS tmodal'), DB::Raw('SUM(tharga) AS tharga'), DB::Raw('SUM(tprofit) AS tprofit')]);
        
        if($model != "" && $req['date'] != "")
            $clause = 'names LIKE "%'.$model.'%" AND created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        elseif($model != "" && $req['date'] == "")
            $clause = 'names LIKE "%'.$model.'%"';
        elseif($model == "" && $req['date'] != "")
            $clause = 'created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        else
            $clause = '';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->groupBy('id')->orderBy('names')->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Nama Barang</th><th>QTY</th><th>Modal</th><th>Harga Jual</th><th>Profit</th></tr>';
        
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->names.'</td>';
            $html .= '<td>'.$rep->tqty.'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep->tmodal)).'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep->tharga)).'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep->tprofit)).'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/report-sales-product/'))
                mkdir('upload/report-sales-product/', 0777, true);
            
            file_put_contents('upload/report-sales-product/'.$filename.'.pdf', $pdf);
            $file = 'upload/report-sales-product/'.$filename.'.pdf';
            
            return $file;
        }
    }
	
	public function indexData(Request $request) {
		DB::statement(DB::raw('set @rownum=0'));
		$report = DB::Table('v_product')
            ->select(['id', 'names', DB::Raw('SUM(tqty) AS tqty'), DB::Raw('SUM(tmodal) AS tmodal'), DB::Raw('SUM(tharga) AS tharga'), DB::Raw('SUM(tprofit) AS tprofit')])
            ->groupBy('id');

		$datatables = app('datatables')->of($report)
				->addColumn('action', function ($report) {
					return '<a onclick="salesProduct(' . $report->id . ')" href="javascript:;;" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="' . trans('systems.edit') . '">Lihat Detail</a>';
				})
				->editColumn('tmodal', function($report) {
					return str_replace(',', '.', number_format($report->tmodal));
				})
				->editColumn('tharga', function($report) {
					return str_replace(',', '.', number_format($report->tharga));
				})
				->editColumn('tprofit', function($report) {
                    return str_replace(',', '.', number_format($report->tprofit));
                });

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		if ($range = $datatables->request->get('range')) {
			$rang = explode(":", $range);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$datatables->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$datatables->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		return $datatables->make(true);
	}

	public function show($id, Request $req) {
		$limit = 5;
		$model = Models::findOrFail($id);
		$report = CashierDetail::join('models', 'models.id', '=', 'model_id')
				->join('cashier', 'cashier_id', '=', 'cashier.id')
				->select([
					'models.id', DB::raw('CONCAT(name, " (", model_name,")") AS names'), 'cashier_id', 'qty',
					'no_ref', DB::Raw('qty * total_cost AS cost'), DB::Raw('qty * price AS price'),
					DB::Raw('qty * diff AS diff'), 'cashier_details.created_at'])
				->whereModelId($id);

		$total = count($report->get());

		$report = $report->limit($limit);
		$report = $report->get();

		$mod = $total % $limit;
		$pagin = intval($total / $limit);
		if ($mod > 0)
			$pagin = $pagin + 1;

		if ($pagin == 1)
			$pagin = 0;

		return response()->json([
			'model' => $model,
			'report' => $report,
			'pagin' => $pagin,
			'total' => $total,
		], 200);
	}

	public function cashierDetail($id, $idm) {
		$list_penjualan = Cashier::join('customers', 'customers.id', '=', 'customer_id')->select('cashier.id', 'cashier.created_at', 'no_ref', 'nama', 'total', 'subtotal', 'discount')->where('cashier.id', $id)->first();

		$detail = CashierDetail::join('models', 'models.id', '=', 'model_id')->select('cashier_details.id', 'model_name', 'name', 'cashier_details.created_at', 'qty', 'price')->whereCashierId($id)->get();
		
		return response()->json([
			'list_penjualan' => $list_penjualan,
			'detail' => $detail,
			'idm' => $idm
		], 200);
	}

	public function filter($id, Request $req) {
		$limit = $req['limit'];
		$page = ($req['page'] - 1) * $limit;

		$table = "";

		$report = CashierDetail::join('models', 'models.id', '=', 'model_id')
				->join('cashier', 'cashier_id', '=', 'cashier.id')
				->select([
					'models.id', DB::raw('CONCAT(name, " (", model_name,")") AS names'), 'cashier_id', 'qty',
					'no_ref', DB::Raw('qty * total_cost AS cost'), DB::Raw('qty * price AS price'),
					DB::Raw('qty * diff AS diff'), 'cashier_details.created_at'])
				->whereModelId($id)
				->limit($limit)
				->offset($page);

		if ($req['range'] != "") {
			$rang = explode(":", $req['range']);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$report->whereBetween('cashier_details.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$report->whereBetween('cashier_details.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		if ($req['sort'] != "") {
			if ($req['asc'] == 1)
				$report->orderBy($req['sort'], 'DESC');
			else
				$report->orderBy($req['sort'], 'ASC');
		}else {
			$report->orderBy('no_ref', 'ASC');
		}

		$report = $report->get();

		$i = $page + 1;
		$qty = 0;
		$cost = 0;
		$price = 0;
		$diff = 0;
		foreach ($report as $rpt) {
			$table .= '
                <tr><td>' . $i++ . '</td>
                    <td><a onclick="cashierDetail(' . $rpt['cashier_id'] . ',' . $rpt['id'] . ')" href="javascript:;;">' . $rpt['no_ref'] . '</a></td>
                    <td>' . $rpt['created_at'] . '</td>
                    <td>' . $rpt['qty'] . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt['cost'])) . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt['price'])) . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt['diff'])) . '</td>
                </tr>
                ';

			$qty += $rpt['qty'];
			$cost += $rpt['cost'];
			$price += $rpt['price'];
			$diff += $rpt['diff'];
		}

		$table .= ' 
            <tr>
                <td colspan="3"><b class="pull-right">Total</b></td>
                <td><b>' . $qty . '</b></td>
                <td><b>' . str_replace(',', '.', number_format($cost)) . '</b></td>
                <td><b>' . str_replace(',', '.', number_format($price)) . '</b></td>
                <td><b>' . str_replace(',', '.', number_format($diff)) . '</b></td>
            </tr>
            ';

		$data[0] = $table;
		$data[1] = count($report);

		return $data;
	}
}
