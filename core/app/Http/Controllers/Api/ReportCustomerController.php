<?php

namespace App\Http\Controllers\Api;

use App\Cashier;
use App\CashierDetail;
use App\Customer;
use App\Debt;
use App\Payment;
use DB;
use PDF;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ReportCustomerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return View
	 */
	public function index() {
		$cus = Customer::count();
		$report = Customer::leftJoin('v_sale', 'v_sale.customer_id', '=', 'customers.id')
				->select([DB::Raw('SUM(subtotal) as sale'), DB::Raw('SUM(debt) as debt')])
				->first();
		
		return response()->json([
			'report' => $report,
			'customer_count' => $cus,
		], 200);
	}

    public function exportpdf(Request $req){
        $report = Customer::leftJoin('v_report_cus', 'v_report_cus.customer_id', '=', 'customers.id')
            ->select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'customer_code', 'nama', 'no_telp', 'alamat', 'limit_debt', 'sales', 'debt'
            ])
            ->whereRaw('nama LIKE "%'.$req['cus'].'%" OR customer_code LIKE "%'.$req['cus'].'%"')
            ->get();    
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Kode Pelanggan</th><th>Nama Pelanggan</th><th>No Telp</th><th>Alamat</th><th>Limit Piutang</th><th>Penjualan</th><th>Piutang</th></tr>';
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep['customer_code'].'</td>';
            $html .= '<td>'.$rep['nama'].'</td>';
            $html .= '<td>'.$rep['no_telp'].'</td>';
            $html .= '<td>'.$rep['alamat'].'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep['limit_debt'])).'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep['sales'])).'</td>';
            $html .= '<td>'.str_replace(',', '.', number_format($rep['debt'])).'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/report-sales-customer/'))
                mkdir('upload/report-sales-customer/', 0777, true);
            
            file_put_contents('upload/report-sales-customer/'.$filename.'.pdf', $pdf);
            $file = 'upload/report-sales-customer/'.$filename.'.pdf';
            
            return $file;
        }
    }
	
	/**
	 * @param Request $request
	 * @return type
	 */
	public function indexData(Request $request) {
		DB::statement(DB::raw('set @rownum=0'));
		$report = Customer::leftJoin('v_report_cus', 'v_report_cus.customer_id', '=', 'customers.id')
            ->select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'customer_code', 'nama', 'no_telp', 'alamat', 'limit_debt', 'sales', 'debt'
            ]);

		$datatables = app('datatables')->of($report)
				->addColumn('action', function ($report) {
					return '<a onclick="salesCustomer(' . $report->id . ')" href="javascript:;;" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="' . trans('systems.edit') . '">Lihat Detail</a>';
				})
				->editColumn('limit_debt', function($report) {
					return str_replace(',', '.', number_format($report->limit_debt));
				})
				->editColumn('sales', function($report) {
					return str_replace(',', '.', number_format($report->sales));
				})
				->editColumn('debt', function($report) {
                    return str_replace(',', '.', number_format($report->debt));
                });

		if ($keyword = $request->get('search')['value']) {
			$datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
		}

		return $datatables->make(true);
	}
	

	public function show($id, Request $req) {
		$limit = 5;
		$customer = Customer::leftJoin('v_report_cus', 'v_report_cus.customer_id', '=', 'customers.id')
				->select([
					DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'customers.id', 'customer_code', 'nama', 'no_telp', 'alamat', 'limit_debt', 'sales', 'debt'
				])
				->where('customers.id', $id)
				->first();

		$report = DB::table('v_sale')
            ->select(['id', 'no_ref', 'subtotal', DB::Raw('SUM(pays) AS pay'), DB::Raw('SUM(debt) AS debt'), 'created_at'])
            ->where('customer_id', $id);

		$total = $report->count();
        
		$report = $report->limit($limit)
            ->groupBy('no_ref');
		$report = $report->get();
        
		$mod = $total % $limit;
		$pagin = intval($total / $limit);
		if ($mod > 0)
			$pagin = $pagin + 1;

		if ($pagin == 1)
			$pagin = 0;

		return response()->json([
			'customer' => $customer,
			'report' => $report,
			'pagin' => $pagin,
			'total' => $total,
		], 200);
	}

	public function cashierDetail($id, $cashierId) {
		$list_penjualan = Cashier::join('customers', 'customers.id', '=', 'customer_id')->select(['cashier.id', 'cashier.created_at', 'no_ref', 'nama', 'total', 'subtotal', 'discount'])->where('cashier.id', $cashierId)->first();

		$detail = CashierDetail::join('models', 'models.id', '=', 'model_id')->select('cashier_details.id', 'model_name', 'name', 'cashier_details.created_at', 'qty', 'price')->whereCashierId($cashierId)->get();

        $pay = Payment::leftjoin('payment_method', 'pm_id', '=', 'payment_method.id')->select('payments.created_at', 'name', 'pay')->whereNoRef($list_penjualan->no_ref)->get();
        
        $debt = Debt::whereNoRef($list_penjualan->no_ref)->first();
		
		return response()->json([
			'list_penjualan' => $list_penjualan,
			'detail' => $detail,
			'pay' => $pay,
			'debt' => $debt,
		], 200);
	}

	public function filter($id, Request $req) {
		$limit = $req['limit'];
        $page = 1;
        if($req['page'] > 0)
            $page = ($req['page'] - 1) * $limit;
        
		$table = "";

		$report = DB::table('v_sale')
            ->select(['id', 'no_ref', 'subtotal', DB::Raw('SUM(pays) AS pay'), DB::Raw('SUM(debt) AS debt'), 'created_at'])
            ->where('customer_id', $id)
            ->limit($limit)
            ->offset($page);

		if ($req['range'] != "") {
			$rang = explode(":", $req['range']);
			if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]) {
				$report->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			} else if ($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
				$report->whereBetween('created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
			}
		}

		if ($req['sort'] != "") {
			if ($req['asc'] == 1)
				$report->orderBy($req['sort'], 'DESC');
			else
				$report->orderBy($req['sort'], 'ASC');
		}else {
			$report->orderBy('no_ref', 'ASC');
		}

		$report = $report->groupBy('no_ref')->get();
        
		$i = $page + 1;
		$total = 0;
		$pay = 0;
		$debt = 0;
		foreach ($report as $rpt) {
			$table .= '
                <tr><td>' . $i++ . '</td>
                    <td><a onclick="cashierDetail(' . $id . ',' . $rpt->id . ')" href="javascript:;;">' . $rpt->no_ref . '</a></td>
                    <td>' . $rpt->created_at . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt->subtotal)) . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt->pay)) . '</td>
                    <td>' . str_replace(',', '.', number_format($rpt->debt)) . '</td>
                </tr>
                ';

			$total += $rpt->subtotal;
			$pay += $rpt->pay;
			$debt += $rpt->debt;
		}

		$table .= ' 
            <tr>
                <td colspan="3"><b class="pull-right">Total</b></td>
                <td><b>' . str_replace(',', '.', number_format($total)) . '</b></td>
                <td><b>' . str_replace(',', '.', number_format($pay)) . '</b></td>
                <td><b>' . str_replace(',', '.', number_format($debt)) . '</b></td>
            </tr>
            ';

		$data[0] = $table;
		$data[1] = count($report);

		return $data;
	}
	
	public function paydebt(Request $request){
		$data['no_ref'] = $request['noref'];
	        $data['customer_id'] = $request['customer_id'];
	        $data['pm_id'] = 0;
	        $data['pay'] = $request['pay'];
	        
	        Payment::create($data);
	
		return response()->json([
			'status' => 201,
			'message' => 'Payment Success',
		], 201);
	}
}
