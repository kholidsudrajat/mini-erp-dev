<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Shipping;
use App\Models;
use App\Cashier;
use App\Stock;
use App\StockLog;
use App\Helpers\FormatConverter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use DB;
use PDF;

class ModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $req)
    {
		DB::statement(DB::raw('set @rownum=0'));
        $model = DB::table('v_model')
            ->select([
                DB::raw('@rownum  := @rownum + 1 AS rownum'), 'qty', 'id', 'name', 'model_name', 'image', 'info', 'total_cost', 'price', 'diff', 'category'
                ]);
                
        if($req['keyword'] != '')
            $model = $model->whereRaw('name LIKE "%'.$req['keyword'].'%" OR model_name LIKE "%'.$req['keyword'].'%"');
            
        $model = $model->get();
        
        return response()->json($model, 200);
    }
	
	public function exportpdf(Request $req){
	    $keyword = $req['keyword'];
	    $range = explode(":", $req['date']);
	    
        $report = DB::table('v_model')
            ->select([
                'qty', 'id', 'name', 'model_name', 'image', 'info', 'total_cost', 'price', 'diff', 'category'
                ]);
        
        $clause = '';
        if($keyword != "" && $req['date'] != "")
            $clause .= '(name LIKE "%'.$keyword.'%" OR model_name LIKE "%'.$keyword.'%") AND created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        elseif($keyword != "" && $req['date'] == "")
            $clause .= '(name LIKE "%'.$keyword.'%" OR model_name LIKE "%'.$keyword.'%")';
        elseif($keyword == "" && $req['date'] != "")
            $clause .= 'created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Nama Barang</th><th>Kode Barang</th><th>Kategori</th><th>Harga</th><th>Stok</th></tr>';
        
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->name.'</td>';
            $html .= '<td>'.$rep->model_name.'</td>';
            $html .= '<td>'.$rep->category.'</td>';
            $html .= '<td>'.str_replace(',','.',number_format($rep->price)).'</td>';
            $html .= '<td>'.$rep->qty.'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/produk/'))
                mkdir('upload/produk/', 0777, true);
            
            file_put_contents('upload/produk/'.$filename.'.pdf', $pdf);
            $file = 'upload/produk/'.$filename.'.pdf';
            
            return $file;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
		$validators = \Validator::make($request->except('sjson'), [
			'name' => 'required',
			'model_name' =>  'required|max:255|alpha_dash|unique:models,model_name,NULL,id,category,'.$request->category,
			'category' => 'required',
			'file' => 'required',
			'total_cost' => 'required|numeric',
			'price' => 'required|numeric',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$files = $request['file'];
		if($files!=""){
            $path = Models::UPLOAD_DESTINATION_PATH;
            $name = rand(10000,99999).'.'.$files->getClientOriginalExtension();
            $files->move($path,$name);
            $request['image'] = $name;
        }
		
		$model = new Models();
		$model->fill($request->all());
		$model->diff = $model->price - $model->total_cost;
		$model->created_at = $model->updated_at = Carbon::now()->toDateTimeString();
		$model->save();
        
        $id = $model->id;
        $json = '['.substr($request->sjson,0,-1).']';
        $json = json_decode($json);
        
        foreach($json as $index){
            foreach($index as $in => $val){
                $datas['model_id'] = $id;
                $datas['shop_id'] = $in;
                $datas['qty'] = $val;

                $cek = Stock::whereModelId($id)->whereShopId($in);
                if($cek->count() > 0){
                    $olddata = $cek->first();
                    $oldqty = $olddata['qty'];
                    $newqty = $oldqty + $val;

                    $cek->update(['qty' => $newqty]);
                }else
                    $stock = Stock::create($datas);

                $data = array(
                    'model_id'      =>  $id,
                    'qty'           =>  $val,
                    'transfer_to'   =>  $in,
                    'type'          =>  'in',
                );

                $this->createlog($data);
            }            
        }
		
		return response()->json([
			'status' => 201,
			'message' => 'Insert Success',
		], 201);
    }
	
	public function show($id)
	{
		$model = Models::whereId($id)->first();
		if (!$model) {
			return response()->json([
				'status' => 404,
				'message' => 'Product is not found',
			], 404);
		}
				
		return response()->json($model, 200);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
		$validators = \Validator::make($request->all(), [
			'name' => 'required',
			'model_name' => 'required|max:255|unique:models,model_name,'.$id.'|alpha_dash',
			'category' => 'required',
			'info' => 'required',
			'total_cost' => 'required|numeric',
			'price' => 'required|numeric',
		]);
		
		$model = Models::whereId($id)->first();
		if (!$model) {
			return response()->json([
				'status' => 404,
				'message' => 'Product is not found.',
			], 404);
		}
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$files = $request['file'];
		if($files!=""){
            $path = Models::UPLOAD_DESTINATION_PATH;
            $name = rand(10000,99999).'.'.$files->getClientOriginalExtension();
            $files->move($path,$name);
            $request['image'] = $name;
        }
			
		$model->fill($request->all());
		$model->diff = $model->price - $model->total_cost;
		$model->updated_at = Carbon::now()->toDateTimeString();
		$model->save();
		
		return response()->json([
			'status' => 200,
			'message' => 'Update Success',
		], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Models::destroy($id);

        return response()->json([
			'status' => 200,
			'message' => 'Delete Success',
		], 200);
    }
    
    public function pluck()
    {
   	$model = Models::select(['id', DB::Raw('CONCAT(name, " - (", model_name, ")") AS name')])->pluck('name','id')->prepend('Pilih Produk', '0');
   	
   	return response()->json($model , 200);
    }
    
    public function getProduk(Request $req)
    {
        $type = $req['type'];
        $model_id = $req['model_id'];
        
        $result = array();
        if($req['noref'] != ""){
            $noref = $req['noref'];
            $data = Cashier::join('cashier_details', 'cashier_id', '=', 'cashier.id')
                ->whereNoRef($noref)
                ->whereModelId($model_id)
                ->first();

            $result['qty'] = $data->qty;
        }
        
        if($type == 'cash' || $type == 'deposit'){
            $data = Models::findOrFail($model_id);
            $result['price'] = $data->price;
        }elseif($type == 'barang'){
            $data = Stock::join('shops', 'shops.id', '=', 'shop_id')
                ->select(['shop_id', 'name'])
                ->whereModelId($model_id)
                ->get();
            
            $result['data'] = $data;
        }
        
        return $result;
    }
    
    public function createlog($data)
    {
        StockLog::create($data);
        
        return 1;
    }
}
