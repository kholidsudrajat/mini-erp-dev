<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

use App\Helpers\FormatConverter;
use App\Cashier;
use App\CashierDetail;
use App\CashierStatus;
use App\Shop;
use App\Models;
use App\Salesman;
use App\Payment;
use App\Debt;

class PenjualanController extends Controller
{
    public function index(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $list_penjualan = DB::table('v_penjualan')
            ->select([
                'name', 'nama', 'id', 'no_ref', 'created_at', 'total', 'status', 'no_resi'
            ]);

         $datatables = app('datatables')->of($list_penjualan)
            ->addColumn('action', function ($list_penjualan) {
                return '<a href="javascript:;" onclick="detail('.$list_penjualan->id.')" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.edit') .'">Lihat Detail</a>';
            })
            ->addColumn('actions', function ($list_penjualan) {
                return '<a href="javascript:;" onclick="retur('.$list_penjualan->id.')" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.edit') .'">Retur</a>';
            })
            ->editColumn('status', function ($list_penjualan){
                if($list_penjualan->status == null || $list_penjualan->status == 'pickup')
                    return '<a class="btn btn-xs btn-info">Pick-Up</a>';
                elseif($list_penjualan->status == 'dikirim')
                    return '<a class="btn btn-xs btn-success">Dikirim</a>';
                else
                    return '<a class="btn btn-xs btn-warning">Pending</a>';
            })
            ->editColumn('total', function ($list_penjualan){
                return str_replace(',', '.', number_format($list_penjualan->total));
            })
            ->editColumn('name', function ($list_penjualan){
                if($list_penjualan->name != "")
					return $list_penjualan->name;
				else
					return '-';
            })
            ;

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('cashier.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('cashier.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
    
    public function show($id)
    {
        $list_penjualan = Cashier::join('customers','customers.id','=','customer_id')->select('cashier.id', 'cashier.created_at', 'no_ref', 'nama', 'total', 'subtotal', 'discount', 'customer_id', 'signature')->where('cashier.id',$id)->first();

        $detail = CashierDetail::join('models','models.id','=','model_id')->select('cashier_details.id', 'model_name', 'name', 'cashier_details.created_at', 'qty', 'price')->whereCashierId($id)->get();
        
        $status = CashierStatus::whereNoRef($list_penjualan->no_ref)->first();
        
        $pay = Payment::leftjoin('payment_method', 'pm_id', '=', 'payment_method.id')->select('payments.created_at', 'name', 'pay')->whereNoRef($list_penjualan->no_ref)->get();
        
        $debt = Debt::whereNoRef($list_penjualan->no_ref)->first();
        
        return response()->json([
			'list_penjualan' => $list_penjualan,
			'detail' => $detail,
			'status' => $status,
			'pay' => $pay,
			'debt' => $debt
		], 200);
    }

    public function updatenoresi(Request $req)
    {
        $noref = $req['noref'];
        
        $data['no_resi'] = $req['no_resi'];
        $status = CashierStatus::whereNoRef($noref);
        $status->update($data);
        
        return response()->json([
			'status' => 201,
			'message' => "Update success",
		], 201);
    }
    
    public function paydebt(Request $req)
    {
        $data['no_ref'] = $req['noref'];
        $data['customer_id'] = $req['customer_id'];
        $data['pm_id'] = 0;
        $data['pay'] = $req['pay'];
        
        Payment::create($data);
        
        return response()->json([
			'status' => 201,
			'message' => "Payment success",
		], 201);
    }
}
