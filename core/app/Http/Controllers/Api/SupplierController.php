<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FormatConverter;
use App\Supplier;
use App\SupplierBahan;
use App\SupplierModel;
use Carbon\Carbon;
use DB;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Tymon\JWTAuth\Facades\JWTAuth;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $supplier = Supplier::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'nama', 'no_telp', 'email', 'pic', 'alamat', 'keterangan']);

         $datatables = app('datatables')->of($supplier)
            ->addColumn('action', function ($supplier) {
                return '<a id="addbahan" onclick="addbahansupplier('. $supplier->id.')" href="javascript:;" class="bb btn btn-success">Tambah Bahan</a> 
                <a id="addmodel" onclick="addmodelsupplier('. $supplier->id.')" href="javascript:;" class="bb btn btn-primary">Tambah Motif</a> 
                <a onclick="supplier('.$supplier->id.')" href="javascript:;" class="bb btn btn-info">Detail</a> 
                <a onclick="supplierEdit('.$supplier->id.')" href="javascript:;" class="bb btn btn-warning">Edit</a> 
                <a onclick="return deleteData('.$supplier->id.')" href="javascript:;" class="bb btn btn-danger">Delete</a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('suppliers.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('suppliers.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
		$user = JWTAuth::parseToken()->authenticate();
		$validators = \Validator::make($request->all(), [
			'nama' => 'required',
			'no_telp' => 'required',
			'email' => 'required',
			'pic' => 'required',
			'alamat' => 'required',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$model = new Supplier();
		$model->fill($request->all());
		$model->created_at = $model->updated_at = Carbon::now()->toDateTimeString();
		$model->save();
		
		return response()->json([
			'status' => 201,
			'message' => 'Insert Success',
		], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return View
     */
    public function show($id)
    {
        $supplier = Supplier::findOrFail($id);
        $bahan = SupplierBahan::whereSupplierId($id)->get();
        $model = SupplierModel::whereSupplierId($id)->get();
		
		return response()->json([
			'supplier' => $supplier,
			'bahan' => $bahan,
			'model' => $model,
		], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function update($id, Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
		$validators = \Validator::make($request->all(), [
			'nama' => 'required',
			'no_telp' => 'required',
			'email' => 'required',
			'pic' => 'required',
			'alamat' => 'required',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$model = Supplier::findOrFail($id);
		$model->fill($request->all());
		$model->updated_at = Carbon::now()->toDateTimeString();
		$model->save();
		
		return response()->json([
			'status' => 200,
			'message' => 'Update Success',
		], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse|Redirector
     */
    public function destroy($id)
    {
        SupplierBahan::whereSupplierId($id)->delete();
        SupplierModel::whereSupplierId($id)->delete();
        Supplier::destroy($id);

        return response()->json([
			'status' => 200,
			'message' => 'Delete Success',
		], 200);
    }
    
    public function addbahan($id)
    {
        $bahan = SupplierBahan::whereSupplierId($id)->get();
		
		return response()->json([
			'id' => $id,
			'bahan' => $bahan,
		], 200);
    }
    
    public function savebahan(Request $req)
    {
        $id = $req['supplier_id'];
		$validators = \Validator::make($req->all(), [
			'supplier_id' => 'required|exists:suppliers,id',
			'name' => 'required',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$model = new SupplierBahan();
		$model->fill($req->all());
		$model->created_at = $model->updated_at = Carbon::now()->toDateTimeString();
		$model->save();
		
		return response()->json([
			'status' => 201,
			'message' => 'Insert Success',
		], 201);
    }
    
    public function deletebahan($id)
    {
        SupplierBahan::destroy($id);
        
        return response()->json([
			'status' => 200,
			'message' => 'Delete Success',
		], 200);
    }
    
    public function addmodel($id)
    {
        $model = SupplierModel::whereSupplierId($id)->get();
        
        return response()->json([
			'id' => $id,
			'model' => $model,
		], 200);
    }
    
    public function savemodel(Request $req)
    {
        $id = $req['supplier_id'];
        $validators = \Validator::make($req->all(), [
			'supplier_id' => 'required|exists:suppliers,id',
			'name' => 'required',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$model = new SupplierModel();
		$model->fill($req->all());
		$model->created_at = $model->updated_at = Carbon::now()->toDateTimeString();
		$model->save();
		
		return response()->json([
			'status' => 201,
			'message' => 'Insert Success',
		], 201);
    }
    
    public function deletemodel($id)
    {
        SupplierModel::destroy($id);
        
        return response()->json([
			'status' => 200,
			'message' => 'Delete Success',
		], 200);
    }
}
