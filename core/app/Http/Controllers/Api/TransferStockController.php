<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;

use App\Helpers\FormatConverter;
use App\Stock;
use App\StockLog;
use App\Shop;
use App\Models;

class TransferStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $stock = DB::table('v_stock')
            ->select([
                'shop_name', 'model_name', 'id', 'qty'
            ])
            ->where('qty','!=','0')
            ->orderBy('model_name');

         $datatables = app('datatables')->of($stock)
            ->addColumn('action', function ($transferstock) {
                return '<a id="transfer" href="javascript:;" onclick="transfer('.$transferstock->id.')" class="btn btn-xs btn-primary rounded">Transfer Stok</a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        return $datatables->make(true);
    }
    
    public function multiple()
    {
        $stock = DB::table('v_stock')
            ->select([
                'shop_name', 'model_name', 'id', 'qty', 'shop_id'
            ])
            ->where('qty','!=','0')
            ->orderBy('model_name')
            ->get();
        
        $shop = Shop::get();
        
        return response()->json([
			'stock' => $stock,
			'shop' => $shop
		], 200);
    }

    public function transfermultiple(Request $req)
    {
        $json = json_decode($req['json']);
        $text = "";
        foreach($json as $index => $value){
            $data = Stock::findOrFail($index);
            $shop = Stock::whereShopId($value->shop_id)->whereModelId($data['model_id'])->first();

            $qty = $value->qty;

            if($qty <= $data['qty']){
                $qty_new = $data['qty'] - $qty;

                $old = Stock::findOrFail($data['id']);
                $old->update(['qty' => $qty_new]);

                if(count($shop) != 0){
                    $new = Stock::findOrFail($shop['id']);
                    $qty_new = $new['qty'] + $qty;
                    $new->update(['qty' => $qty_new]);
                }else{
                    Stock::create(['model_id' => $data['model_id'], 'qty' => $qty, 'shop_id' => $value->shop_id]);
                }
            }

            $cek = Shop::findOrFail($value->shop_id);
            if($cek['is_shop'] == '1')
                $type = 'toshop';
            else
                $type = 'towarehouse';

            $datas = array(
                'model_id'      =>  $data['model_id'],
                'qty'           =>  $value->qty,
                'transfer_from' =>  $data['shop_id'],
                'transfer_to'   =>  $value->shop_id,
                'type'          =>  $type,
            );

            $this->createlog($datas);
            $model = Models::findOrFail($data['model_id']);
            $shop = Shop::findOrFail($data['shop_id']);
            $shops = Shop::findOrFail($value->shop_id);
            $text .= '- '. $value->qty.' : '.$model->name.' ('.$model->model_name.') dari '.$shop->name.' ke '.$shops->name.' berhasil dikirim! <br>';
        }
        
        return response()->json([
			'text' => $text
		], 200);
    }
    
    public function transfer($id)
    {
        $cek = Stock::findOrFail($id);
        $qty = $cek['qty'];
        $shop = Shop::where('id','!=',$cek['shop_id'])->pluck('name','id')->prepend('Pilih Toko / Gudang', '');
        $shops = Shop::findOrFail($cek['shop_id']);
        $model = Models::findOrFail($cek['model_id']);
        
        return response()->json([
			'id' => $id,
			'shop' => $shop,
			'qty' => $qty,
			'shops' => $shops,
			'model' => $model
		], 200);
    }
    
    public function prosestransfer(Request $req)
    {
        $data = Stock::findOrFail($req['stock_id']);
        $shop = Stock::whereShopId($req['shop_id'])->whereModelId($data['model_id'])->first();

        $qty = $req['qty'];
        
        if($qty <= $data['qty']){
            $qty_new = $data['qty'] - $qty;
            
            $old = Stock::findOrFail($data['id']);
            $old->update(['qty' => $qty_new]);
            
            if(count($shop) != 0){
                $new = Stock::findOrFail($shop['id']);
                $qty_new = $new['qty'] + $qty;
                $new->update(['qty' => $qty_new]);
            }else{
                Stock::create(['model_id' => $data['model_id'], 'qty' => $qty, 'shop_id' => $req['shop_id']]);
            }
        }
            
        $cek = Shop::findOrFail($req['shop_id']);
        if($cek['is_shop'] == '1')
            $type = 'toshop';
        else
            $type = 'towarehouse';
            
        $datas = array(
            'model_id'      =>  $data['model_id'],
            'qty'           =>  $req['qty'],
            'transfer_from' =>  $data['shop_id'],
            'transfer_to'   =>  $req['shop_id'],
            'type'          =>  $type,
        );
        
        $this->createlog($datas);
        $result = $data['qty'] - $req['qty'];
        
        return response()->json([
			'result' => $result
		], 200);
    }
    
    public function createlog($data)
    {
        StockLog::create($data);
        
        return 1;
    }
}
