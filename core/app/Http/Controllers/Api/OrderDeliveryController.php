<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FormatConverter;
use App\Models;
use App\Shop;
use App\Order;
use App\OrderRetur;
use App\OrderDelivery;
use App\OrderDeliveryDetail;
use App\OrderDetail;
use App\SupplierOrder;
use App\Stock;
use App\StockLog;
use DB;
use PDF;
use Illuminate\Http\Request;

class OrderDeliveryController extends Controller
{
    public function index(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $order = Order::leftjoin('supplier_orders', 'supplier_orders.id', 'supplier_id')
            ->leftjoin('shipping', 'shipping.id', '=', 'shipping_id')
            ->leftjoin('payment_method', 'payment_method.id', '=', 'payment_type')
            ->select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'orders.id', 'po_number', 'supplier_orders.nama AS supp', 'shipping.name AS ship', 
                'payment_method.name AS paym', 'payment_type', 'shipping_type', 'total', 'status'
            ]);

         $datatables = app('datatables')->of($order)
            ->addColumn('action', function ($order) {
                if($order->status != '2')
                    return '<a href="javascript:;" onclick="orderDelivery('.$order->id.')" class="bb btn btn-primary">Detail</a> <a href="javascript:;" onclick="takeall('.$order->id.')" class="bb btn btn-success">Terima Barang Penuh</a>';
                else
                    return '<a href="javascript:;" onclick="orderDelivery('.$order->id.')" class="bb btn btn-primary">Detail</a>';
            })
            ->editColumn('total', function ($order){
                return str_replace(',','.',number_format($order->total));
            })
            ->editColumn('ship', function ($order){
                if($order->ship != "")
                    return $order->ship;
                else
                    return $order->shipping_type;
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('order_delivery.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('order_delivery.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
	
	public function exportpdf(Request $req){
	    $keyword = $req['keyword'];
	    $range = explode(":", $req['date']);
	    
        $report = Order::leftjoin('supplier_orders', 'supplier_orders.id', 'supplier_id')
            ->leftjoin('shipping', 'shipping.id', '=', 'shipping_id')
            ->leftjoin('payment_method', 'payment_method.id', '=', 'payment_type')
            ->select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'orders.id', 'po_number', 'supplier_orders.nama AS supp', 'shipping.name AS ship', 
                'payment_method.name AS paym', 'payment_type', 'shipping_type', 'total', 'status'
            ]);
        
        if($keyword != "" && $req['date'] != "")
            $clause .= '(po_number LIKE "%'.$keyword.'%" OR supp LIKE "%'.$keyword.'%" OR ship LIKE "%'.$keyword.'%" OR paym LIKE "%'.$keyword.'%") AND orders.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        elseif($keyword != "" && $req['date'] == "")
            $clause .= '(po_number LIKE "%'.$keyword.'%" OR supp LIKE "%'.$keyword.'%" OR ship LIKE "%'.$keyword.'%" OR paym LIKE "%'.$keyword.'%")';
        elseif($keyword == "" && $req['date'] != "")
            $clause .= 'orders.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        else
            $clause = '';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->groupBy('orders.id')->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Nomor PO</th><th>Supplier</th><th>Shipping</th><th>Payment</th><th>Total</th></tr>';
        
        foreach($report as $rep){
            if($rep->ship != "")
                $ship = $rep->ship;
            else
                $ship = $rep->shipping_type;
            
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->po_number.'</td>';
            $html .= '<td>'.$rep->supp.'</td>';
            $html .= '<td>'.$ship.'</td>';
            $html .= '<td>'.$rep->paym.'</td>';
            $html .= '<td>'.str_replace(',','.',number_format($rep->total)).'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/order-delivery/'))
                mkdir('upload/order-delivery/', 0777, true);
            
            file_put_contents('upload/order-delivery/'.$filename.'.pdf', $pdf);
            $file = 'upload/order-delivery/'.$filename.'.pdf';
            
            return $file;
        }
    }
    
    public function indexdata()
    {
        $default = Shop::whereDefault('1')->first();
        
        return response()->json([
			'default' => $default,
		], 200);
    }
    
    public function add($id)
    {
        $order = Order::findOrFail($id);
        $supplier = SupplierOrder::pluck('nama', 'id')->prepend('Pilih Supplier',0);
        
        $sisa = array();
        $data1 = Order::join('order_details', 'orders.id', '=', 'order_id')
            ->where('orders.id',$id)
            ->get();
        $data2 = OrderDelivery::join('order_delivery_details', 'order_delivery.id', '=', 'order_delivery_id')
            ->where('order_id',$id)
            ->get();
        
        foreach($data1 as $dat){
            $sisa[$dat->model_id] = $dat->qty;
        }
        foreach($data2 as $dat){
            $sisa[$dat->model_id] -= $dat->qty + $dat->retur;
        }
        
        $detail = OrderDetail::join('models', 'models.id', '=', 'model_id')->whereOrderId($id)->get();
        $shop = Shop::pluck('name','id');
		
		return response()->json([
			'id' =>  $id,
            'detail' => $detail,
            'shop' => $shop,
            'order' => $order,
            'supplier' => $supplier,
            'sisa' => $sisa,
		], 200);
    }
    
    public function show($id)
    {
        $order = Order::join('supplier_orders', 'supplier_orders.id', '=', 'supplier_id')
            ->select([
                'orders.id', 'po_number', 'supplier_orders.nama'
            ])
            ->where(['orders.id' => $id])
            ->first();
        
        $detail = OrderDelivery::whereOrderId($id)->get();
		
		return response()->json([
			'order' => $order,
			'detail' => $detail
		], 200);
    }
    
    public function detail($id, $oid)
    {    
        $do = OrderDelivery::join('orders', 'orders.id', '=', 'order_id')
            ->select([
                'orders.id', 'supplier_id', 'po_number', 'do_number'
            ])
            ->where(['order_delivery.id' => $oid])
            ->first();
            
        $supplier = SupplierOrder::pluck('nama', 'id')->prepend('Pilih Supplier',0);
        
        $detail = OrderDeliveryDetail::join('models', 'models.id', '=', 'model_id')
            ->join('shops', 'shops.id', '=', 'shop_id')
            ->select([
                DB::Raw('CONCAT(models.name, " - (", model_name, ")") AS produk'), 'qty', DB::Raw('shops.name AS shop'), 'retur'
            ])
            ->whereOrderDeliveryId($oid)
            ->get();
        
        return response()->json([
			'do' => $do,
			'supplier' => $supplier,
			'detail' => $detail
		], 200);
    }
    
    public function submit(Request $req)
    {
		$validators = \Validator::make($req->all(), [
			'order_id' => 'required',
			'json' => 'required',
			'do_number' => 'required|unique:order_delivery',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
        
        $data['order_id'] = $req['order_id'];
        $data['do_number'] = $req['do_number'];
        
        $id = OrderDelivery::insertGetId($data);
        
        $order_id = $req['order_id'];
        Order::findOrFail($order_id)->update(['status' => '1']);
        
        $json = json_decode('['.substr($req['json'],0,-1).']');
        foreach($json as $js){
            $datas['order_delivery_id'] = $id;
            $datas['model_id'] = $js->model_id;
            $datas['qty'] = $js->qty;
            $datas['retur'] = $js->retur;
            $datas['shop_id'] = $js->shop_id;
            
            OrderDeliveryDetail::create($datas);
            
            if($js->retur > 0){
                $dt['order_delivery_id'] = $id;
                $dt['model_id'] = $js->model_id;
                $dt['qty'] = $js->retur;
                
                OrderRetur::create($dt);
            }
            
            if($js->qty > 0){
                $stock = Stock::whereShopId($js->shop_id)->whereModelId($js->model_id)->first();
                
                if(!isset($stock)){
                    $dd['shop_id'] = $js->shop_id;
                    $dd['model_id'] = $js->model_id;
                    $dd['qty'] = $js->qty;
                    
                    Stock::create($dd);
                }else{
                    $qty = $stock->qty;
                    $qty = $qty + $js->qty;
                    $dd['qty'] = $qty;
                    
                    $stock->update($dd);
                }
                
                $de = array(
                    'model_id'      =>  $js->model_id,
                    'qty'           =>  $js->qty,
                    'transfer_to'   =>  $js->shop_id,
                    'type'          =>  'in',
                );

                $this->createlog($de);
            }
        }
		
		return response()->json([
			'status' => 201,
			'message' => "Insert success",
		], 201);
    }
    
    public function takeall($order_id)
    {
        $shop = Shop::whereDefault('1')->first();
        $data['order_id'] = $order_id;
        $data['do_number'] = 'DO-'.date('YmdHis');
        
		$validators = \Validator::make($data, [
			'order_id' => 'required',
			'do_number' => 'required|unique:order_delivery',
		]);
        
        if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
        
        $id = OrderDelivery::insertGetId($data);
        
        Order::findOrFail($order_id)->update(['status' => '2']);
        
        $sisa = array();
        $data1 = Order::join('order_details', 'orders.id', '=', 'order_id')
            ->where('orders.id',$order_id)
            ->get();
        $data2 = OrderDelivery::join('order_delivery_details', 'order_delivery.id', '=', 'order_delivery_id')
            ->where('order_id',$order_id)
            ->get();
        
        foreach($data1 as $dat){
            $sisa[$dat->model_id] = $dat->qty;
        }
        foreach($data2 as $dat){
            $sisa[$dat->model_id] -= $dat->qty + $dat->retur;
        }
        
        foreach($sisa as $index => $value){
            $datas['order_delivery_id'] = $id;
            $datas['model_id'] = $index;
            $datas['qty'] = $value;
            $datas['retur'] = 0;
            $datas['shop_id'] = $shop->id;
            
            if($value > 0){
                OrderDeliveryDetail::create($datas);

                $stock = Stock::whereShopId($shop->id)->whereModelId($index)->first();

                if(!isset($stock)){
                    $dd['shop_id'] = $shop->id;
                    $dd['model_id'] = $index;
                    $dd['qty'] = $value;

                    Stock::create($dd);
                }else{
                    $qty = $stock->qty;
                    $qty = $qty + $value;
                    $dd['qty'] = $qty;

                    $stock->update($dd);
                }

                $de = array(
                    'model_id'      =>  $index,
                    'qty'           =>  $value,
                    'transfer_to'   =>  $shop->id,
                    'type'          =>  'in',
                );

                $this->createlog($de);
            }
        }
		
		return response()->json([
			'status' => 201,
			'message' => "Insert success",
		], 201);
    }
    
    public function createlog($data)
    {
        StockLog::create($data);
        
        return 1;
    }
}