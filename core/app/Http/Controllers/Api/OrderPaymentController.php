<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FormatConverter;
use App\Models;
use App\Order;
use App\OrderDetail;
use App\OrderPayment;
use App\PaymentMethod;
use App\Shipping;
use App\SupplierOrder;
use DB;
use PDF;
use Illuminate\Http\Request;

class OrderPaymentController extends Controller
{
    public function index(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $order = Order::leftjoin('supplier_orders', 'supplier_orders.id', 'supplier_id')
            ->leftjoin('shipping', 'shipping.id', '=', 'shipping_id')
            ->leftjoin('payment_method', 'payment_method.id', '=', 'payment_type')
            ->select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'orders.id', 'po_number', 'supplier_orders.nama AS supp', 'shipping.name AS ship', 
                'payment_method.name AS paym', 'payment_type', 'shipping_type', 'total'
            ]);

         $datatables = app('datatables')->of($order)
            ->addColumn('action', function ($order) {
                return '<a onclick="orderPayment('.$order->id.')" href="javascript:;" class="bb btn btn-primary">Detail</a>';
            })
            ->editColumn('total', function ($order){
                return str_replace(',','.',number_format($order->total));
            })
            ->editColumn('ship', function ($order){
                if($order->ship != "")
                    return $order->ship;
                else
                    return $order->shipping_type;
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
	
	public function exportpdf(Request $req){
	    $keyword = $req['keyword'];
	    $range = explode(":", $req['date']);
	    
        $report = Order::leftjoin('supplier_orders', 'supplier_orders.id', 'supplier_id')
            ->leftjoin('shipping', 'shipping.id', '=', 'shipping_id')
            ->leftjoin('payment_method', 'payment_method.id', '=', 'payment_type')
            ->select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'orders.id', 'po_number', 'supplier_orders.nama AS supp', 'shipping.name AS ship', 
                'payment_method.name AS paym', 'payment_type', 'shipping_type', 'total', 'status'
            ]);
        
        if($keyword != "" && $req['date'] != "")
            $clause .= '(po_number LIKE "%'.$keyword.'%" OR supp LIKE "%'.$keyword.'%" OR ship LIKE "%'.$keyword.'%" OR paym LIKE "%'.$keyword.'%") AND orders.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        elseif($keyword != "" && $req['date'] == "")
            $clause .= '(po_number LIKE "%'.$keyword.'%" OR supp LIKE "%'.$keyword.'%" OR ship LIKE "%'.$keyword.'%" OR paym LIKE "%'.$keyword.'%")';
        elseif($keyword == "" && $req['date'] != "")
            $clause .= 'orders.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        else
            $clause = '';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->groupBy('orders.id')->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Nomor PO</th><th>Supplier</th><th>Shipping</th><th>Payment</th><th>Total</th></tr>';
        
        foreach($report as $rep){
            if($rep->ship != "")
                $ship = $rep->ship;
            else
                $ship = $rep->shipping_type;
            
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->po_number.'</td>';
            $html .= '<td>'.$rep->supp.'</td>';
            $html .= '<td>'.$ship.'</td>';
            $html .= '<td>'.$rep->paym.'</td>';
            $html .= '<td>'.str_replace(',','.',number_format($rep->total)).'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/order-payment/'))
                mkdir('upload/order-payment/', 0777, true);
            
            file_put_contents('upload/order-payment/'.$filename.'.pdf', $pdf);
            $file = 'upload/order-payment/'.$filename.'.pdf';
            
            return $file;
        }
    }
    
    public function add($id)
    {
        $total = Order::findOrFail($id);
        $bayar = OrderPayment::select([DB::Raw('SUM(total) AS total')])->whereOrderId($id)->first();
        
        $totalnya = $total->total - (0 + $bayar->total);
		
		return response()->json([
			'id' => $id,
			'totalnya' => $totalnya,
		], 200);
    }
    
    public function show($id)
    {
        $total = Order::findOrFail($id);
        $bayar = OrderPayment::select([DB::Raw('SUM(total) AS total')])->whereOrderId($id)->first();
        
        $totalnya = $total->total - (0 + $bayar->total);
        
        $order = Order::findOrFail($id);
        $detail = OrderDetail::join('models', 'models.id', '=', 'model_id')->whereOrderId($id)->get();
        $payments = OrderPayment::whereOrderId($id)->get();
        
        $supplier = SupplierOrder::pluck('nama', 'id')->prepend('Pilih Supplier',0);
        $model = Models::select(['id', DB::Raw('CONCAT(name, " - (", model_name, ")") AS name')])
            ->pluck('name','id')
            ->prepend('Pilih Produk',0);
        
        $shipping = Shipping::get();
        $payment = PaymentMethod::get();
		
		return response()->json([
			'order' => $order,
			'detail' => $detail,
			'supplier' => $supplier,
			'model' => $model,
			'shipping' => $shipping,
			'payment' => $payment,
			'payments' => $payments,
			'totalnya' => $totalnya,
		], 200);
    }
    
    public function submit(Request $req)
    {
		$validators = \Validator::make($req->all(), [
			'order_id' => 'required|exists:orders,id',
			'inv_number' => 'required',
			'total' => 'required',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
        OrderPayment::create($req->all());
        $order_id = $req['order_id'];
        Order::findOrFail($order_id)->update(['status' => '1']);
		
		return response()->json([
			'status' => 201,
			'message' => "Insert success",
		], 201);
    }
    
    public function edit($id)
    {
        $order = Order::findOrFail($id);
        $detail = OrderDetail::join('models', 'models.id', '=', 'model_id')->whereOrderId($id)->get();
        
        $supplier = SupplierOrder::pluck('nama', 'id')->prepend('Pilih Supplier',0);
        $model = Models::select(['id', DB::Raw('CONCAT(name, " - (", model_name, ")") AS name')])
            ->pluck('name','id')
            ->prepend('Pilih Produk',0);
        
        $shipping = Shipping::get();
        $payment = PaymentMethod::get();
		
		return response()->json([
			'order' => $order,
			'detail' => $detail,
			'supplier' => $supplier,
			'model' => $model,
			'shipping' => $shipping,
			'payment' => $payment,
		], 200);
    }
    
    public function update($id, Request $request)
    {
        $validators = \Validator::make($request->all(), [
			'po_number' => 'required',
			'supplier_id' => 'required',
			'shipping_type' => 'required',
			'shipping_id' => 'required',
			'payment_type' => 'required',
			'total' => 'required',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
        $requestData = $request->all();

        $order = Order::findOrFail($id);
        $order->update($requestData);

		return response()->json([
			'status' => 200,
			'message' => 'Update success',
		], 200);
    }
    
    public function destroy($id)
    {
        OrderDetail::whereOrderId($id)->delete();
        Order::destroy($id);

        return response()->json([
			'status' => 200,
			'message' => 'Delete success',
		], 200);
    }
}