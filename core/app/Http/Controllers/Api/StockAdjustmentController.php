<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FormatConverter;
use App\Http\Controllers\Controller;
use App\Stock;
use App\StockAdjustment;
use App\StockLog;
use App\Shop;
use App\Models;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Tymon\JWTAuth\Facades\JWTAuth;
use PDF;

class StockAdjustmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(Request $request)
    {
		DB::statement(DB::raw('set @rownum=0'));
        $stock = StockAdjustment::join('shops', 'shops.id', '=', 'shop_id')
            ->join('models', 'models.id', '=', 'model_id')
            ->select([
                'stock_adjustments.id', 'stock_adjustments.date', DB::Raw('shops.name AS shop_name'), DB::Raw('models.name AS model_name'), 
                'stock_adjustments.shop_id', 'stock_adjustments.model_id', 'stock_adjustments.type', 'stock_adjustments.qty', 'stock_adjustments.note'
            ]);

        $datatables = app('datatables')->of($stock)
            ->editColumn('qty', function($stock){
                if($stock->type == 'min')
                    return $stock->qty * -1;
                else
                    return $stock->qty;
            })
            ->addColumn('action', function ($stock) {
                return '<a id="transfer" onclick="edit('.$stock->id.')" class="btn btn-xs btn-primary rounded"><i class="fa fa-pencil"></i></a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('stock_adjustments.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('stock_adjustments.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
	
	public function exportpdf(Request $req){
	    $range = explode(":", $req['date']);
	    
        $report = StockAdjustment::join('shops', 'shops.id', '=', 'shop_id')
            ->join('models', 'models.id', '=', 'model_id')
            ->select([
                'stock_adjustments.id', 'stock_adjustments.date', DB::Raw('shops.name AS shop_name'), DB::Raw('models.name AS model_name'), 
                'stock_adjustments.shop_id', 'stock_adjustments.model_id', 'stock_adjustments.type', 'stock_adjustments.qty', 'stock_adjustments.note'
            ]);
        
        if($req['date'] != "")
            $clause = 'stock_adjustments.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        else
            $clause = '';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->orderBy('date', 'desc')->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Tanggal</th><th>Toko / Gudang</th><th>Nama Barang</th><th>QTY</th><th>Keterangan</th></tr>';
        
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->date.'</td>';
            $html .= '<td>'.$rep->shop_name.'</td>';
            $html .= '<td>'.$rep->model_name.'</td>';
            $html .= '<td>'.$rep->qty.'</td>';
            $html .= '<td>'.$rep->note.'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/report-stock-adjustment/'))
                mkdir('upload/report-stock-adjustment/', 0777, true);
            
            file_put_contents('upload/report-stock-adjustment/'.$filename.'.pdf', $pdf);
            $file = 'upload/report-stock-adjustment/'.$filename.'.pdf';
            
            return $file;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
		$validators = \Validator::make($request->all(), [
			'date' => 'required',
			'shop_id' => 'required|exists:shops,id',
			'model_id' => 'required|exists:models,id',
			'qty' => 'required|numeric',
			'qtys' => 'required|numeric',
			'note' => 'required',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$req['date'] = $request['date'];
	        $req['shop_id'] = $request['shop_id'];
	        $req['model_id'] = $request['model_id'];
	        
	        $qty = $request['qty'] - $request['qtys'];
	        if($qty > 0)
	            $req['type'] = 'plus';
	        else
	            $req['type'] = 'min';
	        
	        $req['qty'] = intval($qty);
	        $req['note'] = $request['note'];
	
	        StockAdjustment::create($req);
	        
	        if($req['type'] == 'min')
	            $req['qty'] = $req['qty'] * -1;
	        
	        $cek = Stock::whereModelId($req['model_id'])->whereShopId($req['shop_id']);
	        
	        $datas['model_id'] = $req['model_id'];
	        $datas['shop_id'] = $req['shop_id'];
	        $datas['qty'] = $req['qty'];
	        
	        if($cek->count() > 0){
	            $olddata = $cek->first();
	            $oldqty = $olddata['qty'];
	            $newqty = $oldqty + $req['qty'];
	            
	            $cek->update(['qty' => $newqty]);
	        }else
	            $stock = Stock::create($datas);
	
	        $data = array(
	            'model_id'      =>  $req['model_id'],
	            'qty'           =>  $req['qty'],
	            'transfer_to'   =>  $req['shop_id'],
	            'type'          =>  'adjustment',
	        );
        
        $log = new StockLog();
		$log->fill([
            'model_id'      =>  $req['model_id'],
            'qty'           =>  $req['qty'],
            'transfer_to'   =>  $req['shop_id'],
            'type'          =>  'adjustment',
        ]);
		$log->save();

		
		return response()->json([
			'status' => 201,
			'message' => 'Insert Success',
		], 201);
    }
    
    public function show($id)
    {
        $stockadjustment = StockAdjustment::findOrFail($id);

        return response()->json($stockadjustment, 200);
    }
    
    public function update($id, Request $request)
    {
    	$model = StockAdjustment::findOrFail($id);
        $model->fill($request->all());
        $model->updated_at = Carbon::now()->toDateTimeString();
        $model->save();

        return response()->json([
            'status' => 200,
            'message' => 'Update Success',
        ], 200);
    }
    
    public function getstock(Request $req)
    {
        $shop = Shop::findOrFail($req['shop']);
        $model = Models::findOrFail($req['model']);
        $allstock = Stock::select([
            DB::Raw('SUM(qty) AS qty')
        ])
            ->whereModelId($req['model'])
            ->first();
        $stocks = Stock::whereModelId($req['model'])
            ->whereShopId($req['shop'])
            ->first();

        return response()->json([
        	'shop'	=> $shop,
        	'model'	=> $model,
        	'allstock'	=> $allstock,
        	'stocks'	=> $stocks,
        ], 200);
    }
}
