<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FormatConverter;
use App\Http\Controllers\Controller;
use App\PettyCash;
use Carbon\Carbon;
use DB;
use PDF;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class PettyCashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(Request $request)
    {
		DB::statement(DB::raw('set @rownum=0'));
        $pettycash = DB::table('v_petty_cash')
            ->select([
                'date', DB::Raw('SUM(cost_in) AS cost_in'), DB::Raw('SUM(cost_out) AS cost_out')
            ])
            ->groupBy('date');

         $datatables = app('datatables')->of($pettycash)
             ->editColumn('date', function($inputcost){
                 return date_format(date_create($inputcost->date), 'd M Y');
             })
             ->editColumn('cost_in', function ($pettycash) {
                 if($pettycash->cost_in == "")
                     return '-';
                 else
                     return str_replace(',','.',number_format($pettycash->cost_in));
             })
             ->editColumn('cost_out', function ($pettycash) {
                 if($pettycash->cost_out == "")
                     return '-';
                 else
                     return str_replace(',','.',number_format($pettycash->cost_out));
             })
             ->addColumn('action', function ($pettycash) {
                 return '<a href="javascript:;;" onclick="pettyCashDetail(&quot;'.$pettycash->date.'&quot;)" class="bb btn btn-primary">Lihat Detail</a>';
             });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('petty_cash.date', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('petty_cash.date', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }

    public function exportpdf(Request $req){
	    $range = explode(":", $req['date']);
	    
        $report = DB::table('v_petty_cash')
            ->select([
                'date', DB::Raw('SUM(cost_in) AS cost_in'), DB::Raw('SUM(cost_out) AS cost_out')
            ]);
        
        $clause = '';
        if($req['date'] != "")
            $clause .= 'date BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->groupBy('date')->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Tanggal</th><th>Pemasukan</th><th>Pengeluaran</th></tr>';
        
        foreach($report as $rep){
            if($rep->cost_in == "")
                $cost_in = '-';
            else
                $cost_in = str_replace(',','.',number_format($rep->cost_in));
                
            if($rep->cost_out == "")
                $cost_out = '-';
            else
                $cost_out = str_replace(',','.',number_format($rep->cost_out));
            
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.date_format(date_create($rep->date), 'd M Y').'</td>';
            $html .= '<td>'.$cost_in.'</td>';
            $html .= '<td>'.$cost_out.'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/uang-kas/'))
                mkdir('upload/uang-kas/', 0777, true);
            
            file_put_contents('upload/uang-kas/'.$filename.'.pdf', $pdf);
            $file = 'upload/uang-kas/'.$filename.'.pdf';
            
            return $file;
        }
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
		$validators = \Validator::make($request->all(), [
			'date' => 'required',
			'type' => 'required',
			'cost' => 'required|numeric',
			//'file' => 'required|file|image',
			'description' => 'required',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$model = new PettyCash();
		$model->fill($request->all());
		$model->file = null;
		$files = $request['file'];
		if($files!="" && $request['type'] == 'out'){
            $path = PettyCash::UPLOAD_DESTINATION_PATH;
            $name = rand(10000,99999).'.'.$files->getClientOriginalExtension();
            $files->move($path,$name);
			$model->file = $name;
        }
		$model->created_at = $model->updated_at = Carbon::now()->toDateTimeString();
		$model->save();
		
		return response()->json([
			'status' => 201,
			'message' => 'Insert Success',
		], 201);
    }
	
	public function show($id)
	{
		$pettycash = DB::table('v_petty_cash')
            ->where('date', $id)
            ->select([ DB::Raw('SUM(cost_in) AS cost_in'), DB::Raw('SUM(cost_out) AS cost_out'), 'date' ])
            ->groupBy('date')
            ->first();

        $petty = PettyCash::where('date', $id)->get();
        
		return response()->json([
			'pettycash' => $pettycash,
			'petty' => $petty,
		], 200);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function update($id, Request $request)
    {
		$validators = \Validator::make($request->all(), [
			'date' => 'required',
			'type' => 'required',
			'cost' => 'required|numeric',
			//'file' => 'required|file|image',
			'description' => 'required',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$model = PettyCash::findOrFail($id);
		$model->fill($request->all());
		$files = $request['file'];
		if($files!="" && $request['type'] == 'out'){
            $path = PettyCash::UPLOAD_DESTINATION_PATH;
            $name = rand(10000,99999).'.'.$files->getClientOriginalExtension();
            $files->move($path,$name);
			$model->file = $name;
        }
		$model->updated_at = Carbon::now()->toDateTimeString();
		$model->save();
		
		return response()->json([
			'status' => 200,
			'message' => 'Update Success',
		], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse|Redirector
     */
    public function destroy($id)
    {
        PettyCash::destroy($id);

        return response()->json([
			'status' => 200,
			'message' => 'Delete Success',
		], 200);
    }
}
