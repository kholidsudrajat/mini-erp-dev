<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FormatConverter;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Tymon\JWTAuth\Facades\JWTAuth;


class UserController extends Controller
{
    public function index(Request $request)
    {
		$user = JWTAuth::parseToken()->authenticate();
        $id_user = $user->id;
        DB::statement(DB::raw('set @rownum=0'));
        $user = User::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'users.id', 'users.name',
            'email', 'username', 'photo'])->where('id','!=',$id_user);

         $datatables = app('datatables')->of($user)
             ->editColumn('photo', function($user){
                 if($user->photo != '')
                     return '<img src="'.url('files/profile').'/'.$user->photo.'" style="max-width:100px;" />';
                 else
                     return '<img src="'.url('img/profiles').'/avatar.jpg" style="max-width:100px;" />';
             })
            ->addColumn('action', function ($user) {
                return '<a onclick="editUser('.$user->id.')" href="javascript:;;" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.edit') .'"><i class="fa fa-pencil"></i></a> <a onclick="deleteData('.$user->id.')" class="btn btn-xs btn-danger rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.delete') .'"><i class="fa fa-trash"></i></a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('users.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('users.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $validators = \Validator::make($request->all(), [
          'name' => 'required|max:255',
          'email' => 'required|email|max:255|unique:users',
          'username' => 'required|max:255|unique:users|alpha_dash',
          'password' => 'required|min:6',
		  'role' => 'required|exists:role_user,id',
		  'shop_id' => 'required|exists:shops,id',
        ]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
        $request["password"] = bcrypt($request->password);
        $request["role"] = $request->role;
        $requestData = $request->all();
		$requestData['created_at'] = Carbon::now()->toDateTimeString();
		$requestData['updated_at'] = Carbon::now()->toDateTimeString();

        $user = User::create($requestData);

        return response()->json([
			'status' => 201,
			'message' => 'Insert Success',
		], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return View
     */
    public function show($id)
    {
        $user = User::whereId($id)->first();

		return response()->json($user, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function update($id, Request $request)
    {
        $user = User::whereId($id)->first();
        $request["password"] = ($request->password == "") ? $user['password'] : bcrypt($request->password);

        $files = $request['photos'];
        if($files!=""){
            if(!is_dir('files/profile/'))
                mkdir('files/profile/', 0777, true);
            
            $path = 'files/profile/';
            $name = rand(10000,99999).'.'.$files->getClientOriginalExtension();
            $files->move($path,$name);
            $request['photo'] = $name;
        }
        
        if($request['email'] != $user['email'] || $request['username'] != $user['username'])
        {
            $validators = \Validator::make($request->all(), [
              'name' => 'required|max:255',
              'email' => 'required|email|max:255|unique:users'
            ]);
			
			if ($validators->fails()) {
				return response()->json([
					'status' => 400,
					'message' => 'Some parameters is invalid',
					'validators' => FormatConverter::parseValidatorErrors($validators),
				], 400);
			}
        }
        $requestData = $request->all();

        $user->update($requestData);

        if(isset($request['photo']))
            return $request['photo'];
        else
            return response()->json([
			    'status' => 200,
			    'message' => 'Update Success',
		    ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse|Redirector
     */
    public function destroy($id)
    {
        User::destroy($id);

        return response()->json([
			'status' => 200,
			'message' => 'Delete Success',
		], 200);
    }
}
