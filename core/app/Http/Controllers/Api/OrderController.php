<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FormatConverter;
use App\Models;
use App\Order;
use App\OrderDetail;
use App\PaymentMethod;
use App\Shipping;
use App\SupplierOrder;
use DB;
use PDF;
use Illuminate\Http\Request;

class OrderController extends Controller
{    
    public function index(Request $request)
    {
        $order = Order::leftjoin('supplier_orders', 'supplier_orders.id', 'supplier_id')
            ->leftjoin('shipping', 'shipping.id', '=', 'shipping_id')
            ->leftjoin('payment_method', 'payment_method.id', '=', 'payment_type')
            ->leftjoin('order_payments', 'orders.id', '=', 'order_id')
            ->select([
                'orders.id', 'po_number', 'supplier_orders.nama AS supp', 'shipping.name AS ship', 'payment_method.name AS paym', 'payment_type', 
                'shipping_type', 'orders.total', DB::Raw('SUM(order_payments.total) AS pay'), 'status'
            ])
            ->groupBy('orders.id');

         $datatables = app('datatables')->of($order)
            ->addColumn('action', function ($order) {
                if($order->status != 0){
                    return '<a onclick="order('.$order->id.')" href="javascript:;" class="bb btn btn-success"><i class="fa fa-eye"></i></a>';
                }else{
                    return '<a onclick="order('.$order->id.')" href="javascript:;" class="bb btn btn-success"><i class="fa fa-eye"></i></a>
                    <a onclick="orderEdit('.$order->id.')" href="javascript:;" class="bb btn btn-primary"><i class="fa fa-pencil"></i></a> 
                    <a onclick="return deleteData('.$order->id.')" class="bb btn btn-danger"><i class="fa fa-times"></i></a>';
                }
                    
            })
            ->editColumn('total', function ($order){
                return str_replace(',','.',number_format($order->total));
            })
            ->editColumn('pay', function ($order){
                return str_replace(',','.',number_format($order->total - $order->pay));
            })
            ->editColumn('ship', function ($order){
                if($order->ship != "")
                    return $order->ship;
                else
                    return $order->shipping_type;
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
	
	public function exportpdf(Request $req){
	    $keyword = $req['keyword'];
	    $range = explode(":", $req['date']);
	    
        $report = Order::leftjoin('supplier_orders', 'supplier_orders.id', 'supplier_id')
            ->leftjoin('shipping', 'shipping.id', '=', 'shipping_id')
            ->leftjoin('payment_method', 'payment_method.id', '=', 'payment_type')
            ->leftjoin('order_payments', 'orders.id', '=', 'order_id')
            ->select([
                'orders.id', 'po_number', 'supplier_orders.nama AS supp', 'shipping.name AS ship', 'payment_method.name AS paym', 'payment_type', 
                'shipping_type', 'orders.total', DB::Raw('SUM(order_payments.total) AS pay'), 'status'
            ]);
        
        $clause = '';
        if($keyword != "" && $req['date'] != "")
            $clause .= '(po_number LIKE "%'.$keyword.'%" OR supp LIKE "%'.$keyword.'%" OR ship LIKE "%'.$keyword.'%" OR paym LIKE "%'.$keyword.'%") AND orders.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        elseif($keyword != "" && $req['date'] == "")
            $clause .= '(po_number LIKE "%'.$keyword.'%" OR supp LIKE "%'.$keyword.'%" OR ship LIKE "%'.$keyword.'%" OR paym LIKE "%'.$keyword.'%")';
        elseif($keyword == "" && $req['date'] != "")
            $clause .= 'orders.created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->groupBy('orders.id')->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Nomor PO</th><th>Supplier</th><th>Shipping</th><th>Payment</th><th>Total</th><th>Sisa Hutang</th></tr>';
        
        foreach($report as $rep){
            if($rep->ship != "")
                $ship = $rep->ship;
            else
                $ship = $rep->shipping_type;
            
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->po_number.'</td>';
            $html .= '<td>'.$rep->supp.'</td>';
            $html .= '<td>'.$ship.'</td>';
            $html .= '<td>'.$rep->paym.'</td>';
            $html .= '<td>'.str_replace(',','.',number_format($rep->total)).'</td>';
            $html .= '<td>'.str_replace(',','.',number_format($rep->total - $rep->pay)).'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/order-list/'))
                mkdir('upload/order-list/', 0777, true);
            
            file_put_contents('upload/order-list/'.$filename.'.pdf', $pdf);
            $file = 'upload/order-list/'.$filename.'.pdf';
            
            return $file;
        }
    }
    
    public function show($id)
    {
        $order = Order::findOrFail($id);
        $detail = OrderDetail::join('models', 'models.id', '=', 'model_id')
            ->select(['model_id', 'name', 'model_name', 'qty', 'order_details.price', 'total'])
            ->whereOrderId($id)
            ->get();
        
        $list = SupplierOrder::pluck('nama', 'id');
        $model = Models::select(['id', DB::Raw('CONCAT(name, " - (", model_name, ")") AS name')])
            ->pluck('name','id')
            ->prepend('Pilih Produk',0);
        
        $shipping = Shipping::pluck('name', 'id');
        $payment = PaymentMethod::get();
		
		return response()->json([
			'order' => $order,
			'detail' => $detail,
			'list' => $list,
			'model' => $model,
			'shipping' => $shipping,
			'payment' => $payment,
		], 200);
    }
    
    public function update($id, Request $request)
    {
		$validators = \Validator::make($request->all(), [
			'po_number' => 'required',
			'supplier_id' => 'required',
			'shipping_type' => 'required',
			'shipping_id' => 'required',
			'payment_type' => 'required',
			'total' => 'required',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
        $requestData = $request->all();

        $order = Order::findOrFail($id);
        $order->update($requestData);

		return response()->json([
			'status' => 200,
			'message' => 'Update success',
		], 200);
    }
    
    public function adddetail($id, Request $request)
    {
        $total = $request->qty * $request->price;
        $requestData = [
            'order_id'  =>  $id,
            'model_id'  =>  $request->model_id,
            'qty'       =>  $request->qty,
            'price'     =>  $request->price,
            'total'     =>  $total
        ];

        $order = OrderDetail::create($requestData);
        
        $summ = OrderDetail::selectRaw('SUM(total) AS totals')->whereOrderId($id)->first();
        $sum = $summ->totals;
        
        $orders = Order::findOrFail($id);
        $orders->update(['total' => $sum]);

        return response()->json([
			'status' => 200,
			'message' => 'Update success',
		], 200);
    }
    
    public function updatedetail($id, Request $request)
    {
        $total = $request->qty * $request->price;
        $requestData = [
            'qty'   =>  $request->qty,
            'price' =>  $request->price,
            'total' =>  $total
        ];

        $order = OrderDetail::whereOrderId($id)->whereModelId($request->model_id)->first();
        $order->update($requestData);
        
        $summ = OrderDetail::selectRaw('SUM(total) AS totals')->whereOrderId($id)->first();
        $sum = $summ->totals;
        
        $orders = Order::findOrFail($id);
        $orders->update(['total' => $sum]);

        return response()->json([
			'status' => 200,
			'message' => 'Update success',
		], 200);
    }
    
    public function deletedetail($id, Request $request)
    {
        $order = OrderDetail::whereOrderId($id)->whereModelId($request->model_id)->first();
        $order->delete();
        
        $summ = OrderDetail::selectRaw('SUM(total) AS totals')->whereOrderId($id)->first();
        $sum = $summ->totals;
        
        $orders = Order::findOrFail($id);
        $orders->update(['total' => $sum]);

        return response()->json([
			'status' => 200,
			'message' => 'Update success',
		], 200);
    }
    
    public function destroy($id)
    {
        OrderDetail::whereOrderId($id)->delete();
        Order::destroy($id);

        return response()->json([
			'status' => 200,
			'message' => 'Delete success',
		], 200);
    }
}