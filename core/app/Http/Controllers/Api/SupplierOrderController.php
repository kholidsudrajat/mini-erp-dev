<?php

namespace App\Http\Controllers\Api;

use App\Helpers\FormatConverter;
use App\SupplierOrder;
use Carbon\Carbon;
use DB;
use PDF;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Tymon\JWTAuth\Facades\JWTAuth;

class SupplierOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $supplier = SupplierOrder::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'nama', 'no_telp', 'email', 'pic', 'alamat']);

         $datatables = app('datatables')->of($supplier)
            ->addColumn('action', function ($supplier) {
                return '<a onclick="supplierEdit('.$supplier->id.')" class="bb btn btn-primary"><i class="fa fa-pencil"></i></a> 
                <a onclick="return deleteData('.$supplier->id.')" class="bb btn btn-danger"><i class="fa fa-times"></i></a>';
            });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('supplier_orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('supplier_orders.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
	
	public function exportpdf(Request $req){
	    $keyword = $req['keyword'];
	    $range = explode(":", $req['date']);
	    
        $report = SupplierOrder::select(['id', 'nama', 'no_telp', 'email', 'pic', 'alamat']);
        
        $clause = '';
        if($keyword != "" && $req['date'] != "")
            $clause .= '(nama LIKE "%'.$keyword.'%" OR email LIKE "%'.$keyword.'%") AND created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        elseif($keyword != "" && $req['date'] == "")
            $clause .= '(nama LIKE "%'.$keyword.'%" OR email LIKE "%'.$keyword.'%")';
        elseif($keyword == "" && $req['date'] != "")
            $clause .= 'created_at BETWEEN "'.$range[0].' 00:00:00" AND "'.$range[1].' 23:59:59"';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Nama Supplier</th><th>Nomor Telepon</th><th>Email</th><th>PIC</th><th>Alamat</th></tr>';
        
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->nama.'</td>';
            $html .= '<td>'.$rep->no_telp.'</td>';
            $html .= '<td>'.$rep->email.'</td>';
            $html .= '<td>'.$rep->pic.'</td>';
            $html .= '<td>'.$rep->alamat.'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/order-supplier/'))
                mkdir('upload/order-supplier/', 0777, true);
            
            file_put_contents('upload/order-supplier/'.$filename.'.pdf', $pdf);
            $file = 'upload/order-supplier/'.$filename.'.pdf';
            
            return $file;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
		$user = JWTAuth::parseToken()->authenticate();
		$validators = \Validator::make($request->all(), [
			'nama' => 'required',
			'no_telp' => 'required',
			'email' => 'required',
			'pic' => 'required',
			'alamat' => 'required',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$model = new SupplierOrder();
		$model->fill($request->all());
		$model->created_at = $model->updated_at = Carbon::now()->toDateTimeString();
		$model->save();
		
		return response()->json([
			'status' => 201,
			'message' => 'Insert Success',
		], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return View
     */
    public function show($id)
    {
        $supplier = SupplierOrder::findOrFail($id);

		return response()->json($supplier, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function update($id, Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
		$validators = \Validator::make($request->all(), [
			'nama' => 'required',
			'no_telp' => 'required',
			'email' => 'required',
			'pic' => 'required',
			'alamat' => 'required',
		]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
		
		$model = SupplierOrder::findOrFail($id);
		$model->fill($request->all());
		$model->updated_at = Carbon::now()->toDateTimeString();
		$model->save();
		
		return response()->json([
			'status' => 200,
			'message' => 'Update Success',
		], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse|Redirector
     */
    public function destroy($id)
    {
        SupplierOrder::destroy($id);

        return response()->json([
			'status' => 200,
			'message' => 'Delete Success',
		], 200);
    }
    
    public function pluck()
    {
   	$supplier = SupplierOrder::pluck('nama','id')->prepend('Pilih Supplier', '0');
   	
   	return response()->json($supplier , 200);
    }
}
