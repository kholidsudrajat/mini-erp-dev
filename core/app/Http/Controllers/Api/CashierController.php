<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Session;
use Datatables;
use DB;
use Auth;
use PDF;

use App\Helpers\FormatConverter;
use App\Cashier;
use App\CashierDetail;
use App\CashierStatus;
use App\Customer;
use App\CustomerAddress;
use App\RoleMenu;
use App\Salesman;
use App\Models;
use App\Shop;
use App\Stock;
use App\StockLog;
use App\Debt;
use App\Payment;
use App\PaymentMethod;
use App\Discount;
use App\Shipping;
use App\User;

class CashierController extends Controller
{
    public function getnotes($id)
    {
        $data = Customer::findOrFail($id);
        $result = $data['note'];
        
        return $result;
    }
    
    public function index(Request $req)
    {
        $current = 1;
        if($req['current'] != '')
            $current = $req['current'];
        $limit = 21;
		$shop = Shop::whereIsShop('1')->pluck('name','id');
        
        $method = PaymentMethod::pluck('name','id');
        
        if(Auth::User()->shop_id < 1){
            $ids = Shop::whereIsShop('1')->first();

            if(Session::get('shop1') == "")
                $idshop['1'] = $ids['id'];

            if(Session::get('shop2') == "")
                $idshop['2'] = $ids['id'];

            if(Session::get('shop3') == "")
                $idshop['3'] = $ids['id'];
        }else{
            if(Session::get('shop1') == "")
                $idshop['1'] = Auth::user()->shop_id;

            if(Session::get('shop2') == "")
                $idshop['2'] = Auth::user()->shop_id;

            if(Session::get('shop3') == "")
                $idshop['3'] = Auth::user()->shop_id;
        }
        if($req['shops'] != 'null')
	        $idshop[$current] = $req['shops'];
        $key = $req['keyword'];
        
        for($u = 1; $u <= 3; $u++){
            $clause = 'qty > 0 AND is_shop = "1" AND shop_id = "'.$idshop[$u].'"';
            
            if($current == $u)
                $clause .= ' AND (model_name LIKE "%'.$key.'%" OR models.name LIKE "%'.$key.'%")';

            $models = Models::select(['models.id', 'models.name', 'qty', 'price', 'model_name', 'image'])
                ->leftjoin('stocks', 'model_id', '=', 'models.id')
                ->leftjoin('shops', 'shops.id', '=', 'shop_id')
                ->whereRaw($clause);

            $totals = count($models->get());
            
            $model[$u] = $models->limit($limit)->orderBy('models.id', 'DESC')->get();

            $mod = $totals % $limit;
            $pagin[$u] = intval($totals / $limit);

            if($mod > 0)
                $pagin[$u] = $pagin[$u] + 1;

            if($pagin[$u] == 1)
                $pagin[$u] = 0;
        }
            
        $mod = Models::all();
        $cus = Customer::pluck('nama','id');
        $sales = User::whereRole('3')->pluck('name','id')->prepend('Pilih Sales','0');
        
        if(Session::get('cart1') !== "")
           $cart['1'] = json_decode('['.substr(Session::get('cart1'),0,-1).']');
        else
           $cart['1'] = "";
        
        if(Session::get('cart2') !== "")
           $cart['2'] = json_decode('['.substr(Session::get('cart2'),0,-1).']');
        else
           $cart['2'] = "";
        
        if(Session::get('cart3') !== "")
           $cart['3'] = json_decode('['.substr(Session::get('cart3'),0,-1).']');
        else
           $cart['3'] = "";
        
        $name = array();
        $mname = array();
        $price = array();
        foreach($mod as $models){
            $id = $models['id'];
            $name[$id] = $models['name'];
            $mname[$id] = $models['model_name'];
            $price[$id] = $models['price'];
        }
        
        return response()->json([
			'sales'      => $sales,
			'model'      => $model,
			'cus'        => $cus,
			'name'       => $name,
			'mname'      => $mname,
			'price'      => $price,
			'shop'       => $shop,
			'idshop'     => $idshop,
			'pagin'      => $pagin,
			'totals'     => $totals,
			'method'     => $method,
			'cart'       => $cart,
			'current'    => $current,
			'key'        => $key
		], 200);
    }
    
    public function details($tab, Request $req)
    {
        $limit = 21;
        $subtotal = Session::get('subtotal'.$tab);
        $total = Session::get('total'.$tab);
        $discount = Session::get('discount'.$tab);
        $pot = Session::get('pot'.$tab);
        $shop = Shop::whereIsShop('1')->pluck('name','id');
        
        $method = PaymentMethod::pluck('name','id');
        
        if(Auth::User()->shop_id < 1){
            $idshop = Session::get('shop'.$tab);
            $ids = Shop::whereIsShop('1')->first();

            if($idshop == ""){
                Session::put('shop', $ids['id']);
                $idshop = Session::get('shop'.$tab);
            }
        }else
            $idshop = Auth::User()->shop_id;
        
        $model = Models::select(['models.id', 'models.name', 'qty', 'price', 'model_name', 'image'])->join('stocks', 'model_id', '=', 'models.id')->join('shops', 'shops.id', '=', 'shop_id');
            
        $totals = count($model->get());
            
        $model = $model->limit($limit)->groupBy('models.id')->get();
        
        $mod = Models::all();
        
        if(Session::get('cart'.$tab) != "")
           $cart = json_decode('['.substr(Session::get('cart'.$tab),0,-1).']');
        else
           $cart = "";
        
        $name = array();
        $mname = array();
        $price = array();
        foreach($mod as $models){
            $id = $models['id'];
            $name[$id] = $models['name'];
            $mname[$id] = $models['model_name'];
            $price[$id] = $models['price'];
        }
        
        $mod = $totals % $limit;
        $pagin = intval($totals / $limit);
        
        if($mod > 0)
            $pagin = $pagin + 1;
        
        if($pagin == 1)
            $pagin = 0;
        
        $notes = $this->getnotes($req['cus']);
        
        return response()->json([ 
            'model' => $model, 
            'cart' => $cart, 
            'name' => $name, 
            'mname' => $mname, 
            'price' => $price, 
            'subtotal' => $subtotal, 
            'discount' => $discount, 
            'pot' => $pot, 
            'total' => $total, 
            'shop' => $shop, 
            'idshop' => $idshop, 
            'pagin' => $pagin, 
            'totals' => $totals, 
            'method' => $method, 
            'tab' => $tab, 
            'notes' => $notes, 
        ], 200);
    } 
    
    public function shipping($noref)
    {
        $data = Cashier::whereNoRef($noref)->first();
        $cus = $data['customer_id'];
        $addr = CustomerAddress::whereCustomerId($cus)->first();
        
        if($cus == '0' || !isset($addr)){
            return response()->json([
				'status' => 400,
				'message' => 'Address is not set'
			], 400);
        }
        
        $address = CustomerAddress::whereCustomerId($cus)->get();
        $ship = Shipping::findOrFail($addr->shipping_id);
        $shipping = Shipping::get();
        
        return response()->json([ 
            'address' => $address, 
            'shipping' => $shipping,
            'addr' => $addr,
            'ship' => $ship,
            'noref' => $noref
        ], 200);
    }
    
    public function changeaddress(Request $req)
    {
        $address = CustomerAddress::findOrFail($req['address']);
        
        return response()->json([ 
            'shipping_id' => $address['shipping_id']
        ], 200);
    }
    
    public function saveshipping(Request $req)
    {
        $data['no_ref'] = $req['no_ref'];
        $data['status'] = $req['status'];
        if($data['status'] == 'dikirim'){
            $data['address_id'] = $req['address_id'];
            $data['shipping_id'] = $req['shipping_id'];
        }else{
            $data['address_id'] = 0;
            $data['shipping_id'] = 0;
        }
        
        CashierStatus::create($data);
        
        return response()->json([
			'status' => 201,
			'message' => "Save Shipping success",
		], 201);
    }
    
    public function prints($noref)
    {
        $cashier = Cashier::join('shops', 'shop_id', '=', 'shops.id')
            ->leftjoin('customers', 'customer_id', '=', 'customers.id')
            ->select([
                'cashier.id', 'no_ref', 'cashier.created_at', DB::Raw('shops.name AS shop_name'), DB::Raw('customers.nama AS customer_name'), 'subtotal', 
                'discount', 'total', 'cashier.note', 'signature'
            ])
            ->whereNoRef($noref)
            ->first();
        
        $detail = CashierDetail::join('models', 'models.id', '=', 'model_id')
            ->select(['model_name', 'name', 'price', 'qty'])
            ->whereCashierId($cashier['id'])
            ->get();
        
        $payment = Payment::join('payment_method', 'payment_method.id', '=', 'pm_id')
                ->select(['name', 'pay'])
                ->whereNoRef($noref)
                ->where('pay', '>', '0')
                ->get();
        
        $debt = Debt::whereNoRef($noref)->first();
        
        return response()->json([ 
            'cashier' => $cashier, 
            'detail' => $detail,
            'payment' => $payment,
            'debt' => $debt
        ], 200);
    }
    
    public function prints_(Request $req)
    {
        $noref = $req['noref'];
        $cashier = Cashier::join('shops', 'shop_id', '=', 'shops.id')
            ->join('customers', 'customer_id', '=', 'customers.id')
            ->select([
                'cashier.id', 'no_ref', 'cashier.created_at', DB::Raw('shops.name AS shop_name'), DB::Raw('customers.nama AS customer_name'), 'subtotal', 
                'discount', 'total', 'cashier.note'
            ])
            ->whereNoRef($noref)
            ->first();
        
        $detail = CashierDetail::join('models', 'models.id', '=', 'model_id')
            ->select(['model_name', 'name', 'price', 'qty'])
            ->whereCashierId($cashier['id'])
            ->get();
        
        $payment = Payment::join('payment_method', 'payment_method.id', '=', 'pm_id')
                ->select(['name', 'pay'])
                ->whereNoRef($noref)
                ->where('pay', '>', '0')
                ->get();
        
        $debt = Debt::whereNoRef($noref)->first();
        
        $reprint = '';
        if($req['reprint'] == 1)
            $reprint = 'Re-Printed';
        
        $data['cashier'] = $cashier;
        $data['detail'] = $detail;
        $data['payment'] = $payment;
        $data['debt'] = $debt;
        $data['reprint'] = $reprint;
        
        $pdf = PDF::loadView('admin.cashier.print', $data);
        
        $output = $pdf->output();
        if($req['reprint'] == 1){
            file_put_contents('upload/invoice/TRX-'.$noref.' (Re-Printed).pdf', $output);
            $file = 'upload/invoice/TRX-'.$noref.' (Re-Printed).pdf';
        }else{
            file_put_contents('upload/invoice/TRX-'.$noref.'.pdf', $output);
            $file = 'upload/invoice/TRX-'.$noref.'.pdf';
        }
        
        return response()->json([ 
            'file' => $file
        ], 200);
    }
    
    public function shop($id, Request $req)
    {
        $tab = $req['tab'];
        Session::put('current',$tab);
        Session::put('shop'.$tab,$id);
        Session::put('cart'.$tab,'');
        Session::put('pot'.$tab, '0');
        Session::put('subtotal'.$tab, '0');
        Session::put('discount'.$tab, '0');
        Session::put('total'.$tab, '0');
        Session::put('cart2'.$tab,'');
        Session::put('pot2'.$tab, '0');
        Session::put('subtotal2'.$tab, '0');
        Session::put('discount2'.$tab, '0');
        Session::put('total2'.$tab, '0');
        
        return response()->json([
			'status' => 201,
			'message' => "Change Shop success",
		], 201);
    }
    
    public function modelDetail($id)
    {
        $model = Models::findOrFail($id);
        
        return response()->json([
			'model'      => $model
		], 200);
    }
    
    public function addqty(Request $req)
    {
        $tab = $req['tab'];
        $cart = Session::get('cart'.$tab);
        $sub = $req['subtotal'];
        
        if(Session::get('cart'.$tab) != ""){
            $cart = Session::get('cart'.$tab);
            $json = "[".substr($cart,0,-1)."]";
            $json = json_decode($json);
            
            $new_cart = $cart;
            $i = 1;
            foreach($json as $index)
            {
                $mod = $index->model_id;
                $qty = $index->qty;
                $subtotal = $index->subtotal;
                if($index->model_id == $req['model_id']){
                    $old = '{"model_id" : "'.$mod.'", "qty" : "'.$qty.'", "subtotal" : "'.$subtotal.'"},';
                    $qtys = $qty + $req['qty'];
                    $subs = $req['subtotal'];
                    $subs = $subtotal + $subs;
                    $new = '{"model_id" : "'.$mod.'", "qty" : "'.$qtys.'", "subtotal" : "'.$subs.'"},';
                    $new_cart = str_replace($old, $new, $cart);
                    $i++;
                }else{
                    $i++;
                    if($i<=2)
                        $new_cart = $new_cart.'{"model_id" : "'.$req['model_id'].'", "qty" : "'.$req['qty'].'", "subtotal" : "'.$sub.'"},';
                }
            }
        }else{
            $new_cart = $cart.'{"model_id" : "'.$req['model_id'].'", "qty" : "'.$req['qty'].'", "subtotal" : "'.$sub.'"},';
        }
        
        $subtotal = Session::get('subtotal'.$tab);
        $discount = 0;
        $total = 0;
        $discount = Session::get('discount'.$tab);
        $pot = Session::get('pot'.$tab);
        if($subtotal != ""){
            $subtotal += $req['subtotal'];
            if($pot == '1')
                $disc = $discount / 100 * $subtotal;
            else
                $disc = $discount;
            $total = $subtotal - $disc;
        }
        
        Session::put('cart'.$tab,$new_cart);
        Session::put('subtotal'.$tab, $subtotal);
        Session::put('discount'.$tab, $discount);
        Session::put('total'.$tab, $total);
        Session::put('current',$tab);
        
        return response()->json([
			'status' => 201,
			'message' => "Add Qty success",
		], 201);
    }
    
    public function adddisc(Request $req)
    {        
        Session::put('pot', $req['pot']);
        Session::put('subtotal', $req['subtotal']);
        Session::put('discount', $req['discount']);
        Session::put('total', $req['total']);
        
        return response()->json([
			'status' => 201,
			'message' => "Add Discount success",
		], 201);
    }
    
    public function hapusqty($id, Request $req)
    {
        $tab = $req['tab'];
        $old = Session::get('cart'.$tab);
        $old = substr($old,0,-1);
        $ex = str_replace(array('{','}'),'',$old);
        $ex = explode(',"',$ex);
        
        foreach($ex as $in){
            $dd = $in;
        }
        
        unset($ex[$id]);
        
        $new = "";
        foreach($ex as $in => $val){
            if(substr($val,0,1)=='"')
                $val = substr($val,1,200);
            $new .= '{"'.$val.'},';
        }
        
        $subtotal = Session::get('subtotal'.$tab);
        if($subtotal > 0)
            $subtotal = $subtotal - $req['subtotal'];
        
        $pot = Session::get('pot'.$tab);
        $discount = Session::get('discount'.$tab);
        $disc = $discount;
        if($pot == '1')
            $disc = $discount / 100 * $subtotal;
        $total = $subtotal - $disc;
        
        if(count($ex) > 0){
            Session::put('cart'.$tab,$new);
            Session::put('subtotal'.$tab, $subtotal);
            Session::put('discount'.$tab, $discount);
            Session::put('total'.$tab, $total);
        }else{
            Session::put('cart'.$tab, '');
            Session::put('subtotal'.$tab, '0');
            Session::put('discount'.$tab, '0');
            Session::put('total'.$tab, '0');
        }
        Session::put('current',$tab);
        
        return response()->json([
			'status' => 201,
			'message' => "Remove success",
		], 201);
    }
    
    public function ajaxmodel(Request $req)
    {
        $u = Session::get('current');
        if($u == '')
            $u = 1;
        $tab = $req['tab'];
        $limit = 21;
        $page = ($req['page'] - 1) * $limit;
        
        $ids = Shop::whereIsShop('1')->first();
    
        if(Auth::User()->shop_id < 1){
            $idshop['1'] = Session::get('shop1');
            $idshop['2'] = Session::get('shop2');
            $idshop['3'] = Session::get('shop3');
            $ids = Shop::whereIsShop('1')->first();

            if(Session::get('shop1') == ""){
                Session::put('shop1', $ids['id']);
                $idshop['1'] = Session::get('shop1');
            }

            if(Session::get('shop2') == ""){
                Session::put('shop2', $ids['id']);
                $idshop['2'] = Session::get('shop2');
            }

            if(Session::get('shop3') == ""){
                Session::put('shop3', $ids['id']);
                $idshop['3'] = Session::get('shop3');
            }
        }else{
            $idshop['1'] = Auth::User()->shop_id;
            $idshop['2'] = Auth::User()->shop_id;
            $idshop['3'] = Auth::User()->shop_id;
        }
        
        $list = "";
        $key = $req['keyword'];
        
        $clause = 'qty > 0 AND shop_id = "'.$idshop[$tab].'" AND (model_name LIKE "%'.$key.'%" OR models.name LIKE "%'.$key.'%")';
        $model = Models::select(['models.id', 'models.name', 'qty', 'price', 'model_name', 'image'])
            ->join('stocks', 'model_id', '=', 'models.id')->join('shops', 'shops.id', '=', 'shop_id')
            ->whereRaw($clause)
            ->limit($limit)
            ->offset($page)
            ->orderBy('models.id', 'DESC')
            ->get();
        
        foreach($model as $mod)
        {
            $list .= '<div class="form-group col-md-4 col-sm-4 col-xs-4" id="mods'. $u .'_'. $mod['id'] .'">
                    <div class="form-group-default">
                        <div class="displaytable">
                            <div class="displaytablecell">
                                <label class="title-prod">'. $mod['name'] .'</label>
                            </div>
                        </div>
                        <a class="col-md-12 gmb" style="cursor:pointer;" id="popup">
                            <div class="frame-image">
                                <img src="'. url('files/models').'/'.$mod['image'] .'" />
                                <div class="text-overlay">
                                    <span><i class="font-10">IDR '. str_replace(',','.',number_format($mod['price'])) .'</i></span>
                                    <span class="font-10"><b>('. $mod['qty'] .' in stock)</b></span>
                                </div>
                            </div>
                        </a>
                        <span class="font-10">'. $mod['model_name'] .'</span>
                        <br/>
                        <div class="clearfix" style="padding-bottom: 3px;"></div>
                        <input name="qty" type="number" value="1" style="width:62%" id="qty'.$mod['id'].'_'.$u.'" />
                        <input style="display:none;" id="stk'.$mod['id'].'" value="'. $mod['qty'] .'" />
                        <input style="display:none;" id="stocks'.$mod['id'].'" value="'. $mod['qty'] .'" />
                        <a onclick="addtocart('.$u.','.$mod['id'].','.$mod['price'].')" class="btn btn-xs btn-primary" style="margin-top: -4px;"><i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>';
        }
        
        return response()->json([ 
            'list' => $list
        ], 200);
    }
    
    public function bayar(Request $req)
    {
        define('UPLOAD_DIR', 'files/signature/');
        $img = $req['tanda_tangan'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data_img = base64_decode($img);
        $file = UPLOAD_DIR . uniqid() . '.png';
        $success = file_put_contents($file, $data_img);

        $tab = $req['tab'];
        $count = Cashier::count();
        if ($count < 10)
            $count = '0' . $count;

        $noref = $req['noref'];

        $data['no_ref'] = $noref;
        $data['shop_id'] = $req['shop_id'];
        $data['customer_id'] = $req['customer_id'];
        $data['salesman_id'] = $req['salesman_id'];
        $data['note'] = $req['notes'];
        $data['discount'] = 0 + $req['discount'];
        $data['subtotal'] = 0 + $req['subtotal'];
        $data['total'] = 0 + $req['total'];
        $data['signature'] = $file;
        $data['method_pembayaran'] = $req['method_pembayaran'];
        $data['updated_at'] = date('Y-m-d H:i:s');

        $id = Cashier::insertGetId($data);
        
        $cart = $req['cart'];
        $cart = json_decode('['.substr($cart,0,-1).']');
        
        $datas = array();
        $bb = array();
        foreach($cart as $in => $val)
        {
            $datas['cashier_id'] = $id;
            $datas['model_id'] = $val->model_id;
            $bb[$val->model_id] = $val->qty;
            $datas['qty'] = $val->qty;
            
            $this->removestock($datas['model_id'], $datas['qty'], $data['shop_id']);
            $datas['transfer_from'] = $data['shop_id'];
            $datas['type'] = 'out';
            $this->createlog($datas);
            
            CashierDetail::create($datas);
        }
            
        /////////INSERT INTO TABLE DISCOUNT
        $json = '['.substr($req['jdisc'],0,-1).']';
        $json = json_decode($json);
        
        $value['no_ref'] = $noref;
        foreach($json as $index){
            foreach($index as $in => $val){
                foreach($val as $i => $o){
                    $value['type'] = $in;
                    $value['model_id'] = $i;
                    $value['discount'] = $o;
                    
                    if($i == 0)
                        $value['qty'] = 1;
                    else
                        $value['qty'] = $bb[$i];
                    
                    Discount::create($value);
                }
            }
        }
        
        /////////INSERT INTO TABLE PAYMENT
        $payment = '['.substr($req['payment'],1).']';
        
        $payment = json_decode($payment);
        $dt['no_ref'] = $noref;
        $dt['customer_id'] = $req['customer_id'];
        $debt = $data['total'];
        foreach($payment as $index){
            foreach($index as $in => $val){
                $dt['pm_id'] = $in;
                $dt['pay']   = $val;
                
                $debt -= $val;
            }
    
            if($dt['pay'] > 0)
                Payment::create($dt);
        }
        
        /////INSERT INTO TABLE DEBT IF TOTAL > PAY
        if($debt > 0){
            $due = strtotime('+'.$req['due_date'].' days');
            $duedate = date('Y-m-d 23:59:59',$due);
            
            $do['no_ref'] = $noref;
            $do['customer_id'] = $req['customer_id'];
            $do['debt'] = $debt * -1;
            $do['due_date'] = $duedate;
            
            Debt::create($do);
        }
        
        return response()->json([ 
            'noref' => $noref
        ], 200);
    }
    
    public function removestock($model_id, $qty, $shop_id)
    {
        $stock = Stock::where(['shop_id' => $shop_id])->whereModelId($model_id)->first();
        $old_stock = $stock->qty;
        $new_stock = $old_stock - $qty;
        
        $update = Stock::whereShopId($stock->shop_id)->whereModelId($model_id)->update(['qty' => $new_stock]);
        
        return $update;
    }
    
    public function index2(Request $req)
    {
        $shop = Shop::whereIsShop('1')->pluck('name','id');        
        $idshop = $req['shop'];
        
        $mod = $req['model'];
        $mods = Models::join('stocks', 'model_id', '=', 'models.id')
            ->join('shops', 'shops.id', '=', 'shop_id')
            ->select([
                'models.id', 'models.name', 'qty', 'price', 'model_name', 'image'
            ])
            ->whereRaw('qty > 0 AND shop_id="'.$idshop.'"')
            ->groupBy('models.id')
            ->get();
        
        $clause = 'qty > 0 AND shop_id = "'.$idshop.'" AND models.id = "'.$mod.'"';
        $model = Models::join('stocks', 'model_id', '=', 'models.id')
            ->join('shops', 'shops.id', '=', 'shop_id')
            ->select([
                'models.id', 'models.name', 'qty', 'price', 'model_name', 'image'
            ])
            ->whereRaw($clause)
            ->groupBy('models.id')
            ->first();
        
        $mod = Models::all();
        $cus = Customer::pluck('nama','id');
        $method = PaymentMethod::pluck('name','id');
        
        $name = array();
        $mname = array();
        $price = array();
        foreach($mod as $models){
            $id = $models['id'];
            $name[$id] = $models['name'];
            $mname[$id] = $models['model_name'];
            $price[$id] = $models['price'];
        }
        
        return response()->json([
            'shop'      => $shop,
            'mod'       => $mod,
            'cus'       => $cus,
            'id'        => $id,
            'mods'      => $mods,
            'model'     => $model,
            'method'    => $method,
            'cus'       => $cus
		], 200);
    }
    
    public function ajaxmodels(Request $req)
    {
        $idshop = $req['shop'];
        
        $list = '<ul id="listnone">';
        $key = $req['keyword'];
        $clause = 'qty > 0 AND shop_id="'.$idshop.'" AND (model_name LIKE "%'.$key.'%" OR models.name LIKE "%'.$key.'%")';
        $model = Models::select(['models.id', 'models.name', 'qty', 'price', 'model_name', 'category'])
            ->join('stocks', 'model_id', '=', 'models.id')
            ->join('shops', 'shops.id', '=', 'shop_id')
            ->whereRaw($clause)
            ->groupBy('models.id')
            ->get();
        
        $i = 0;
        foreach($model as $mod)
        {
            $i++;
            $list .= '<li onclick="return pilih(&#39;'.$mod->id.'&#39;)">'.$mod->name.' - ('.$mod->model_name.') - '.$mod->category.'</li>';
        }
        
        $list .= '</ul>';
        
        if($i < 1)
            $list = "";
        
        return response()->json([ 
            'list' => $list
        ], 200);
    }
    
    public function bayars(Request $req)
    {      
        $noref = $req['no_ref'];
        $data['no_ref'] = $noref;
        $data['customer_id'] = $req['customer_id'];
        $data['shop_id'] = $req['shop_id'];
        $data['discount'] = 0 + $req['discount'];
        $data['subtotal'] = 0 + $req['subtotal'];
        $data['total'] = 0 + $req['total'];
        $data['method_pembayaran'] = $req['method_pembayaran'];
        $data['updated_at'] = date('Y-m-d H:i:s');
        
        $id = Cashier::insertGetId($data);
        
        if($req['cart'] != "")
           $cart = json_decode($req['cart']);
        else
           $cart = [];
           
        $datas = array();
        foreach($cart as $in => $val)
        {
            $datas['cashier_id'] = $id;
            $datas['model_id'] = $val->model_id;
            $datas['qty'] = $val->qty;
            
            $this->removestock($datas['model_id'], $datas['qty'], $data['shop_id']);
            $datas['transfer_from'] = $data['shop_id'];
            $datas['type'] = 'out';
            $this->createlog($datas);
            
            CashierDetail::create($datas);
        }
        
        Discount::create([
            'no_ref'    => $noref,
            'type'      => $req['pot'],
            'model_id'  => 0,
            'qty'       => 1,
            'discount'  => 0 + $req['discount']
        ]);
        
        /////////INSERT INTO TABLE PAYMENT
        $payment = '['.substr($req['payment'],1).']';
        
        $payment = json_decode($payment);
        $dt['no_ref'] = $noref;
        $dt['customer_id'] = $req['customer_id'];
        $debt = $data['total'];
        foreach($payment as $index){
            foreach($index as $in => $val){
                $dt['pm_id'] = $in;
                $dt['pay']   = $val;
                
                $debt -= $val;
            }
    
            if($dt['pay'] > 0)
                Payment::create($dt);
        }
        
        /////INSERT INTO TABLE DEBT IF TOTAL > PAY
        if($debt > 0){
            $due = strtotime('+'.$req['due_date'].' days');
            $duedate = date('Y-m-d 23:59:59',$due);
            
            $do['no_ref'] = $noref;
            $do['customer_id'] = $req['customer_id'];
            $do['debt'] = $debt * -1;
            $do['due_date'] = $duedate;
            
            Debt::create($do);
        }
        
        Session::put('cart_','');
        Session::put('pot_', '0');
        Session::put('subtotal_', '0');
        Session::put('discount_', '0');
        Session::put('total_', '0');
        
        return $noref;
    }  
    
    public function createlog($data)
    {
        StockLog::create($data);
        
        return 1;
    }
}