<?php

namespace App\Http\Controllers\Api;

use App\Cashier;
use App\CashierDetail;
use App\Customer;
use App\Deposit;
use App\Helpers\FormatConverter;
use App\Models;
use App\PettyCash;
use App\Retur;
use App\Shop;
use DB;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ReturInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(Request $request)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $list_penjualan = DB::table('v_penjualan')
            ->select([
                'name', 'nama', 'id', 'no_ref', 'created_at', 'total', 'status', 'no_resi'
            ]);

         $datatables = app('datatables')->of($list_penjualan)
            ->addColumn('action', function ($list_penjualan) {
                return '<a onclick="list_penjualan(\"'.$list_penjualan->id.'\")" href="javascript:;" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.edit') .'">Lihat Detail</a>';
            })
            ->addColumn('actions', function ($list_penjualan) {
                return '<a onclick="returInvoice(&#39;'.$list_penjualan->no_ref.'&#39;)" href="javascript:;" class="btn btn-xs btn-primary rounded" data-toggle="tooltip" title="" data-original-title="'. trans('systems.edit') .'">Retur</a>';
            })
            ->editColumn('status', function ($list_penjualan){
                if($list_penjualan->status == null || $list_penjualan->status == 'pickup')
                    return '<a class="btn btn-xs btn-info">Pick-Up</a>';
                elseif($list_penjualan->status == 'dikirim')
                    return '<a class="btn btn-xs btn-success">Dikirim</a>';
                else
                    return '<a class="btn btn-xs btn-warning">Pending</a>';
            })
            ->editColumn('total', function ($list_penjualan){
                return str_replace(',', '.', number_format($list_penjualan->total));
            })
            ->editColumn('name', function ($list_penjualan){
                if($list_penjualan->name != "")
					return $list_penjualan->name;
				else
					return '-';
            })
            ;

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        if ($range = $datatables->request->get('range')) {
            $rang = explode(":", $range);
            if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] != $rang[1]){
                $datatables->whereBetween('cashier.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }else if($rang[0] != "Invalid date" && $rang[1] != "Invalid date" && $rang[0] == $rang[1]) {
                $datatables->whereBetween('cashier.created_at', ["$rang[0] 00:00:00", "$rang[1] 23:59:59"]);
            }
        }

        return $datatables->make(true);
    }
    
    public function show($noref)
    {
        $cashier = Cashier::whereNoRef($noref)->first();
        $detail = CashierDetail::whereCashierId($cashier->id)->get();
        
        $idshop = $cashier->shop_id;
        
        $shop = Shop::whereId($idshop)->pluck('name','id');
        $customer = Customer::select([DB::Raw('CONCAT(nama, " - (", customer_code, ")") AS name'), 'id'])
            ->whereId($cashier->customer_id)
            ->first();
        
        $model = Models::join('stocks', 'models.id', '=', 'stocks.model_id')
            ->join('cashier_details', 'models.id', '=', 'cashier_details.model_id')
            ->select([DB::Raw('CONCAT(name, " - (", model_name, ")") AS model_name'), 'models.id'])
            ->whereCashierId($cashier->id)
            ->groupBy('models.id')
            ->get();
        
		return response()->json([
			'noref' => $noref,
			'model' => $model,
			'shop' => $shop,
			'customer' => $customer,
			'idshop' => $idshop,
			'cashier' => $cashier,
		], 200);
    }
    
    public function save(Request $req){
		
		$validators = \Validator::make($req->all(), [
          'customer_id' => 'required|exists:customers,id',
          'type_retur' => 'required',
          'model_id' => 'required',
          'shop_id' => 'required',
          'shops_id' => 'required',
          'qty' => 'required',
          'cost' => 'required',
          'note' => 'required',
          'noref' => 'required',
        ]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}
			
        $type = $req['type_retur'];
        
        $count = Retur::count() + 1;
        if($count < 10)
            $count = '00'.$count;
        elseif($count > 9 && $count < 100)
            $count = '0'.$count;
        elseif($count > 99)
            $count = $count;

        $noret = 'RTR-0'.$req['shop_id'].date('ymdHi').$count;
        $data['no_retur'] = $noret;
        $data['customer_id'] = $req['customer_id'];
        $data['shop_id'] = $req['shop_id'];
        $data['type_retur'] = $type;
        $data['model_id'] = $req['model_id'];
        $data['qty'] = $req['qty'];
        if($type == "cash" || $type == "deposit"){
            $data['cost'] = $req['cost'];
        }else
            $data['cost'] = 0;
        $data['note'] = $req['note'];

        $create = Retur::create($data);
        
        $model = Models::findOrFail($req['model_id']);
        $cus = Customer::findOrFail($req['customer_id']);
        if($type == "cash"){
            ////INSERT TO PETTY CASH
            $model = Models::findOrFail($req['model_id']);
            $cus = Customer::findOrFail($req['customer_id']);
            
            $datas['date'] = date('Y-m-d');
            $datas['type'] = 'out';
            $datas['cost'] = $req['cost'];
            $datas['description'] = 'Retur Produk '.$model->name.' ('.$model->model_name.'), Customer : '.$cus->nama.' ('.$cus->customer_code.'), nomor retur : '.$noret;
            
            $creates = PettyCash::create($datas);
            ////////
            
            return response()->json([
				'status' => 201,
				'message' => 'Insert success',
			], 201);
        }elseif($type == "deposit"){
            ////INSERT TO DEPOSIT
            $datas['customer_id'] = $req['customer_id'];
            $datas['no_retur'] = $noret;
            $datas['deposit'] = $req['cost'];
            $datas['note'] = 'Retur Produk '.$model->name.' ('.$model->model_name.'), Customer : '.$cus->nama.' ('.$cus->customer_code.'), nomor retur : '.$noret;
            
            $creates = Deposit::create($datas);            
            ///////////
            
            return response()->json([
				'status' => 201,
				'message' => 'Insert success',
			], 201);
        }elseif($type == "barang"){
            ////REMOVE STOCK
            /*$stock = Stock::whereShopId($req['shop_id'])
                ->whereModelId($req['model_id'])
                ->first();
            
            $stocks = Stock::whereShopId($req['shops_id'])
                ->whereModelId($req['model_id'])
                ->first();
            
            $qty_old = $stock['qty'] - $req['qty'];
            $qty_new = $stocks['qty'] + $req['qty'];
            
            $stock->update(['qty' => $qty_old]);
            $stocks->update(['qty' => $qty_new]);
            
            $dat = array(
                'model_id'      =>  $req['model_id'],
                'qty'           =>  $req['qty'],
                'transfer_from' =>  $req['shop_id'],
                'transfer_to'   =>  $req['shops_id'],
                'type'          =>  'retur',
            );

            StockLog::create($dat);*/
            
            ///////////
            
            return response()->json([
				'status' => 201,
				'message' => 'Insert success',
			], 201);
        }else{
            return response()->json([
				'status' => 400,
				'message' => 'Insert fail',
			], 400);
        }
    }
}
