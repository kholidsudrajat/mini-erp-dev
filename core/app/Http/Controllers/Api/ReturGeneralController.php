<?php

namespace App\Http\Controllers\Api;

use App\Cashier;
use App\Customer;
use App\Deposit;
use App\Models;
use App\PettyCash;
use App\Retur;
use App\Shop;
use App\Stock;
use App\StockLog;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Session;
use App\Helpers\FormatConverter;
use Carbon\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;

class ReturGeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $idshop = Session::get('shops');
        
        if(Auth::User()->shop_id == 0){
            $idshop = Session::get('shops');
            $ids = Shop::whereIsShop('1')->first();

            if($idshop == ""){
                Session::put('shops', $ids['id']);
                $idshop = Session::get('shops');
            }
        }else
            $idshop = Auth::User()->shop_id;
        
        $shop = Shop::whereIsShop('1')->pluck('name','id');
        $customer = Customer::select([DB::Raw('CONCAT(nama, " - (", customer_code, ")") AS name'), 'id'])
            ->get();
        
        $model = Models::join('stocks', 'models.id', '=', 'model_id')
            ->select([DB::Raw('CONCAT(name, " - (", model_name, ")") AS model_name'), 'models.id'])
            ->whereShopId($idshop)
            ->get();
        
        return view('admin.retur-general.index', compact('model', 'shop', 'customer', 'idshop'));
    }
    
    public function save(Request $req){
		
		$validators = \Validator::make($req->all(), [
          'customer_id' => 'required|exists:customers,id',
          'type_retur' => 'required',
          'model_id' => 'required',
          'shop_id' => 'required',
          'shops_id' => 'required',
          'qty' => 'required',
          'note' => 'required',
          'noref' => 'required',
          'cost' => 'required',
        ]);
		
		if ($validators->fails()) {
			return response()->json([
				'status' => 400,
				'message' => 'Some parameters is invalid',
				'validators' => FormatConverter::parseValidatorErrors($validators),
			], 400);
		}

        $type = $req['type_retur'];
        
        $count = Retur::count() + 1;
        if($count < 10)
            $count = '00'.$count;
        elseif($count > 9 && $count < 100)
            $count = '0'.$count;
        elseif($count > 99)
            $count = $count;

        $noret = 'RTR-0'.$req['shop_id'].date('ymdHi').$count;
        $data['no_retur'] = $noret;
        $data['no_ref'] = $req['noref'];
        $data['customer_id'] = $req['customer_id'];
        $data['shop_id'] = $req['shop_id'];
        $data['type_retur'] = $type;
        $data['model_id'] = $req['model_id'];
        $data['qty'] = $req['qty'];
        if($type == "cash" || $type == "deposit"){
            $data['cost'] = $req['cost'];
        }else
            $data['cost'] = 0;
        $data['note'] = $req['note'];

        $create = Retur::create($data);
        
        $model = Models::findOrFail($req['model_id']);
        $cus = Customer::findOrFail($req['customer_id']);
        if($type == "cash"){
            ////INSERT TO PETTY CASH
            $model = Models::findOrFail($req['model_id']);
            $cus = Customer::findOrFail($req['customer_id']);
            
            $datas['date'] = date('Y-m-d');
            $datas['type'] = 'out';
            $datas['cost'] = $req['cost'];
            $datas['description'] = 'Retur Produk '.$model->name.' ('.$model->model_name.'), Customer : '.$cus->nama.' ('.$cus->customer_code.'), nomor retur : '.$noret;
            
            $creates = PettyCash::create($datas);
            ////////
            
            return response()->json([
				'status' => 201,
				'message' => 'Insert success',
			], 201);
        }elseif($type == "deposit"){
            ////INSERT TO DEPOSIT
            $datas['customer_id'] = $req['customer_id'];
            $datas['no_retur'] = $noret;
            $datas['deposit'] = $req['cost'];
            $datas['note'] = 'Retur Produk '.$model->name.' ('.$model->model_name.'), Customer : '.$cus->nama.' ('.$cus->customer_code.'), nomor retur : '.$noret;
            
            $creates = Deposit::create($datas);            
            ///////////
            
            return response()->json([
				'status' => 201,
				'message' => 'Insert success',
			], 201);
        }elseif($type == "barang"){
            ////REMOVE STOCK
            $stock = Stock::whereShopId($req['shop_id'])
                ->whereModelId($req['model_id'])
                ->first();
            
            $stocks = Stock::whereShopId($req['shops_id'])
                ->whereModelId($req['model_id'])
                ->first();
            
            $qty_old = $stock['qty'] - $req['qty'];
            $qty_new = $stocks['qty'] + $req['qty'];
            
            $stock->update(['qty' => $qty_old]);
            $stocks->update(['qty' => $qty_new]);
            
            $dat = array(
                'model_id'      =>  $req['model_id'],
                'qty'           =>  $req['qty'],
                'transfer_from' =>  $req['shop_id'],
                'transfer_to'   =>  $req['shops_id'],
                'type'          =>  'retur',
            );

            StockLog::create($dat);
            
            ///////////
            
            return response()->json([
				'status' => 201,
				'message' => 'Insert success',
			], 201);
        }else{
            return response()->json([
				'status' => 400,
				'message' => 'Insert fail',
			], 404);
        }
    }
    
    public function shop(Request $req)
    {
        Session::put('shops', $req['shop_id']);
        
        return 1;
    }
    
    public function getProduk(Request $req)
    {
        $type = $req['type'];
        $model_id = $req['model_id'];
        
        $result = array();
        if($req['noref'] != ""){
            $noref = $req['noref'];
            $data = Cashier::join('cashier_details', 'cashier_id', '=', 'cashier.id')
                ->whereNoRef($noref)
                ->whereModelId($model_id)
                ->first();

            $result['qty'] = $data->qty;
        }
        
        if($type == 'cash' || $type == 'deposit'){
            $data = Models::findOrFail($model_id);
            $result['price'] = $data->price;
        }elseif($type == 'barang'){
            $data = Stock::join('shops', 'shops.id', '=', 'shop_id')
                ->select(['shop_id', 'name'])
                ->whereModelId($model_id)
                ->get();
            
            $result['data'] = $data;
        }
        
        return $result;
    }
}
