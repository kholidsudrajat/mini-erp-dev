<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Stock;
use App\Models;
use App\Shop;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Session;
use Datatables;
use DB;
use PDF;
use Auth;
use App\Helpers\FormatConverter;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
		DB::statement(DB::raw('set @rownum=0'));
        $stock = DB::table('v_stock')
            ->select([
                DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'shop_name', 'v_stock.model_name','models_name', 'v_stock.id', 'qty', 'models.category', 'models.id AS model_id'
                ])
                ->leftJoin('models', 'models.name', '=', 'v_stock.model_name');

         $datatables = app('datatables')->of($stock)
             ->editColumn('model_name', function($stock){
                 return '<a href="javascript:;" onclick="addstock('.$stock->model_id.')">'.$stock->model_name.'</a>';
             });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        return $datatables->make(true);
    }
	
	public function exportpdf(Request $req){
	    $keyword = $req['keyword'];
	    
        $report = DB::table('v_stock')
        ->select([
            'shop_name', 'v_stock.model_name','models_name', 'v_stock.id', 'qty', 'models.category', 'models.id AS model_id'
            ])
            ->leftJoin('models', 'models.name', '=', 'v_stock.model_name');
        
        $clause = '';
        if($keyword != "")
            $clause .= '(v_stock.model_name LIKE "%'.$keyword.'%" OR models_name LIKE "%'.$keyword.'%")';
        
        if($clause != "")
            $report = $report->whereRaw($clause);
        
        $report = $report->orderBy('model_id')->get();
            
        $no = 1;
        $html = '<html><head><style>table{border-collapse: collapse; width: 100%; font-size:12px;}table, td, th{border: 1px solid black; padding: 10px; text-align: left;}</style></head><body>';
        $html .= '<table><tr><th>#</th><th>Nama Barang</th><th>Kode Barang</th><th>Kategori</th><th>QTY</th><th>Toko / Gudang</th></tr>';
        
        foreach($report as $rep){
            $html .= '<tr>';
            $html .= '<td>'.$no++.'</td>';
            $html .= '<td>'.$rep->model_name.'</td>';
            $html .= '<td>'.$rep->models_name.'</td>';
            $html .= '<td>'.$rep->category.'</td>';
            $html .= '<td>'.$rep->qty.'</td>';
            $html .= '<td>'.$rep->shop_name.'</td>';
            $html .= '</tr>';
        }
        $html .= '</table></body></html>';
        
        if($no > 1){
            $filename = date('ymdhis');
            $pdf = PDF::loadHtml($html)->setPaper('a4', 'landscape')->output();
            
            if(!is_dir('upload/stok/'))
                mkdir('upload/stok/', 0777, true);
            
            file_put_contents('upload/stok/'.$filename.'.pdf', $pdf);
            $file = 'upload/stok/'.$filename.'.pdf';
            
            return $file;
        }
    }

	public function lists()
	{
		$shop = Shop::pluck('name','id')->prepend('Pilih Toko / Gudang', 0);
        $model = Models::join('stocks', 'model_id', '=', 'models.id')
            ->select([
                DB::Raw('CONCAT(models.name, " - ", models.category, " (Stock : ", SUM(qty), ")") AS names'), 'models.id'
            ])
            ->groupBy('models.id')
            ->get();
	            
		$list['shop'] = $shop;
        $list['model'] = $model;
		
		return response()->json($list , 200);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $validators = \Validator::make($request->all(), [
            'model_id' => 'required|exists:models,id',
            'sjson' => 'required',
        ]);

        if ($validators->fails()) {
            return response()->json([
                'status' => 400,
                'message' => 'Some parameters is invalid',
                'validators' => FormatConverter::parseValidatorErrors($validators),
            ], 400);
        }

        $id = $request['model_id'];
            $json = '['.substr($request['sjson'],0,-1).']';
            $json = json_decode($json);

            foreach($json as $index){
                foreach($index as $in => $val){
                    $datas['model_id'] = $id;
                    $datas['shop_id'] = $in;
                    $datas['qty'] = $val;

                    $cek = Stock::whereModelId($id)->whereShopId($in);
                    if($cek->count() > 0){
                        $olddata = $cek->first();
                        $oldqty = $olddata['qty'];
                        $newqty = $oldqty + $val;

                        $cek->update(['qty' => $newqty]);
                    }else
                        $stock = Stock::create($datas);

            $log = new \App\StockLog();
            $log->fill([
                'model_id'      =>  $id,
                'qty'           =>  $val,
                'transfer_to'   =>  $in,
                'type'          =>  'in',
            ]);
            
                    $log->save();
                }            
            }

        return response()->json([
            'status' => 201,
            'message' => 'Insert Success',
        ], 201);
    }
}
