<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrxBahanDetail extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'trx_bahan_details';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['trx_id', 'warna_id', 'kg', 'total'];

    
}
