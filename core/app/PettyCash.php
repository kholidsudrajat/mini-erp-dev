<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PettyCash extends Model
{
	const UPLOAD_DESTINATION_PATH = 'files/petty-cash/';
	
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'petty_cash';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['date', 'type', 'cost', 'description', 'file'];

    
}
