<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'role_menu';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['menu', 'slug', 'created_at', 'updated_at', 'order'];

    public function parentMenu()
    {
        return $this->hasOne('App\RoleMenu', 'id', 'parent_menu');
    }

}
