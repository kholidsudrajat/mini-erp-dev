<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{	
	/**
     * The attributes that should be mutated to dates.
     *
     * @var array

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'photo',
        'role',
        'role_menu',
        'shop_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	protected $with = [
		'roleUser',
		'shop',
	];

    public function roleUser()
    {
        return $this->hasOne('App\RoleUser', 'id', 'role');
    }
	
	public function shop()
	{
		return $this->hasOne('App\Shop', 'id', 'shop_id');
	}
}
