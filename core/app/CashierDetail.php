<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashierDetail extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cashier_details';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['cashier_id', 'model_id', 'qty'];   
}