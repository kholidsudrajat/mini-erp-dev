<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempBahanDetail extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'temp_bahan_details';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['temp_id', 'warna_id', 'kg'];

    
}
