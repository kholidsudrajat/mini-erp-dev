<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Notification;

use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $notif = Notification::limit(5)->orderBy('created_at', 'DESC')->get();
        View::share('notif', $notif);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
