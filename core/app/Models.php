<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Models extends Model
{
	const UPLOAD_DESTINATION_PATH = 'files/models/';
	
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'models';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'model_name', 'category', 'image', 'keterangan', 'total_cost', 'price', 'diff'];
}
