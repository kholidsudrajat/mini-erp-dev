<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationDetail extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notification_details';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['notif_id', 'model_id', 'shop_id', 'qty'];
}
