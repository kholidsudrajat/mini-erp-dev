<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stocks';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
	
	protected $with = [
		'model',
		'shop',
	];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['model_id', 'qty', 'shop_id'];
	
	public function model()
	{
		return $this->hasOne('App\Models', 'id', 'model_id');
	}
	
	public function shop()
	{
		return $this->hasOne('App\Shop', 'id', 'shop_id');
	}
}
