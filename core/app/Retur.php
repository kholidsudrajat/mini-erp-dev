<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Retur extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'returs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['no_retur', 'no_ref', 'shop_id', 'customer_id', 'model_id', 'type_retur', 'qty', 'cost', 'note'];

    
}
