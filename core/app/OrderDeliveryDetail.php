<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDeliveryDetail extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_delivery_details';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['order_delivery_id', 'model_id', 'qty', 'retur', 'shop_id'];
}
