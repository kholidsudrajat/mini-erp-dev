<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempBahan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'temp_bahans';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tanggal', 'supplier_id', 'harga', 'bahan_id', 'motif_id'];

    
}
