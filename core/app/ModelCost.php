<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelCost extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'models_cost';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['model_id', 'cost_id', 'costs'];

    
}
