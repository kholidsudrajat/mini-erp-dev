<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['customer_code', 'nama', 'no_telp', 'limit_debt', 'note', 'email', 'alamat'];

	/**
	 * @return string
	 */
	public static function generateCode()
	{
		$count = self::max('id');
        $code = $count + 1;
        if(strlen($code)==1)
            $code = "000".$code;
        elseif(strlen($code)==2)
            $code = "00".$code;
        elseif(strlen($code)==3)
            $code = "0".$code;
        elseif(strlen($code)==4)
            $code = $code;
        
        return 'C'.date('ymd').$code;
	}
    
}
