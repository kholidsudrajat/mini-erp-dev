<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salesman extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'salesmans';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'telp'];

    
}
