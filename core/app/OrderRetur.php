<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderRetur extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_returs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['order_delivery_id', 'model_id', 'qty'];
}
