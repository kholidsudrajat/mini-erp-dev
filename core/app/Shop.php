<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shops';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'is_shop', 'is_warehouse'];
	
	public function scopeIsWarehouse($query)
	{
		return $query->where($this->table . '.is_warehouse', '=', 1);
	}
	
	public function scopeIsShop($query)
	{
		return $query->where($this->table . '.is_shop', '=', 1);
	}

    
}
