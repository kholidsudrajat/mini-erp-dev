<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cashier extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cashier';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['no_ref', 'customer_id', 'shop_id', 'note', 'discount', 'subtotal', 'total', 'signature', 'method_pembayaran'];
}
