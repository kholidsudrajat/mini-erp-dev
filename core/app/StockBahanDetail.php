<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockBahanDetail extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stock_bahan_details';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['stock_id', 'warna_id', 'kg', 'total'];

    
}
