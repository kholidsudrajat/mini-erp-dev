<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockLog extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stocks_log';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['model_id', 'qty', 'transfer_from', 'transfer_to', 'type'];

    
}
