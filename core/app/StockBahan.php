<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockBahan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stock_bahans';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['supplier_id', 'bahan_id', 'motif_id', 'roll', 'qty', 'total'];

    
}
