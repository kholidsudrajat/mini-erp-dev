<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModalPcsBahan extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'modal_pcs_bahan';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['modal_pcs_id', 'stock_bahan_detail_id'];

    
}
