<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OperationalCost extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'operational_costs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['label', 'shop_id', 'cost', 'date'];

    
}
