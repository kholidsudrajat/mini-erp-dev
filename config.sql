-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `type` varchar(25) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `config` (`type`, `value`) VALUES
('low_stock',	'10'),
('high_stock',	'20'),
('product_best_seller',	'7'),
('email_checklist',	'1'),
('phone_checklist',	'1'),
('email_list',	'fhmabdurahman@gmail.com, fahmi@cranium.id'),
('phone_list',	'0811223546, 082355456');

-- 2017-10-04 08:20:48
